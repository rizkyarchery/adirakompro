@php
  use Illuminate\Support\Facades\DB; 
  $lang = App::getLocale();
  
  $code = null;
  $menu_link = null;
  
  switch ($lang) {
    case 'en':
      $code = '002';
      $lang_link = "/en";
      $menu_link = "en/";
      break;
    
    default:
      $code = '001';
      $lang_link = null;
      break;
  }
  $sw       = DB::table('settingweb')->where('kode', $code)->first(); 
  $footer   = DB::table('footer_brand')->where('deleted_at', null)->where('status', 1)->where('lang', $lang)->orderBy('id','DESC')->get();
  $menus    = DB::table('menu')->where('code',$code)->first();

  $list_menus = json_decode($menus->data);
  
@endphp

<!DOCTYPE html>
<html lang="{{ $lang }}">
<head>
  <meta charset="UTF-8">
  {{-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> --}}
  <meta name="viewport" content="width=device-width, target-densityDpi=device-dpi">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-158708521-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-158708521-1');
  </script>
  <link rel="stylesheet" href="/frontend/style/plugins/bootstrap/css/bootstrap.min.css">
  <link rel="icon" href="/favicon.ico" type="image/gif" sizes="16x16">
  <link rel="stylesheet" href="/frontend/style/css/main.css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
  
  @stack('meta_tag')

  {{-- Dependency --}}
  <link rel="stylesheet" href="/frontend/style/plugins/owlcarousel/dist/assets/owl.carousel.min.css">
  <link rel="stylesheet" href="/frontend/style/plugins/owlcarousel/dist/assets/owl.theme.dark.css">
  <link rel="stylesheet" href="/frontend/style/plugins/fontawesome/css/all.css">
  
  @stack('style')
  <script src="/frontend/style/plugins/jquery/jquery.min.js"></script>
@php

$prefix = config('app.app_prefix');

@endphp


  <title>@yield('page_title') - Adira Insurance</title> 
</head>
<body>

<div class="adira-nav-mobile-wrapper">
  <div class="adira-nav-mobile-head">
    <div class="adira-nav-mobile-head-item-left">
      <a href="/en">
        <img src="/frontend/assets/images/common/flag-uk.png"  style="width: 25px;height: 25px;" alt="">
      </a>
      <a href="/">
        <img src="/frontend/assets/images/common/flag-indo.png" style="width: 25px;height: 25px;"  alt="">
      </a>
    </div>
    <div class="adira-nav-mobile-head-item-right">
      <i class="fas iconbar fa-times" style="color: #5f5f5f;"></i>
    </div>
  </div>
  <div class="adira-nav-mobile-body">
    <ul class="adira-nav-mobile-ul-first">
            
      {{-- Looping menu --}}
      {{-- Level 1 Start --}}
      @foreach ($list_menus as $menu)
        @if(isset($menu->children))
          <li class="adira-nav-mobile-li-first">
            <i class="fas fa-chevron-down adira-nav-mobile-icon"></i>
            <a href="#"> {{ $menu->name }} </a>
            <ul class="adira-nav-mobile-ul-second">

              {{-- Level 2 Start --}}
              @foreach ($menu->children as $child)
                @if(isset($child->children))
                  <li class="adira-nav-mobile-li-second">
                    <i class="fas fa-chevron-down adira-nav-mobile-icon"></i>
                    <div class="adira-nav-mobile-div-second-nested">
                      <a href="{{ $child->link }}">
                        {{ $child->name }}
                      </a>
                    </div>
                    
                    <ul class="adira-nav-mobile-ul-third">
                      {{-- Level 3 Start --}}
                      @foreach ($child->children as $grandchild)
                        @if(isset($grandchild->children))
                          <li>
                            <i class="fas fa-chevron-down adira-nav-mobile-icon"></i>
                            <div class="adira-nav-mobile-div-third-nested">
                              <a href="{{ $grandchild->link }}">
                                {{ $grandchild->name }}
                              </a>
                            </div>
                            <ul class="adira-nav-mobile-ul-four">
                              @foreach ($grandchild->children as $g)
                                <li>
                                  <a href="{{ $g->link }}">
                                    {{ $g->name }}
                                  </a>
                                </li>
                              @endforeach
                            </ul>
                          </li>
                        @else 
                          <li>
                            <a href="{{ $grandchild->link }}">
                              {{ $grandchild->name }}
                            </a>
                          </li>
                        @endif
                        
                      @endforeach
                      {{-- Level 3 End --}}
                    </ul>

                  </li>
                @else
                  <li class="adira-nav-mobile-li-second">
                        <a href="{{ $child->link }}">
                          {{ $child->name }}
                        </a>
                  </li>
                @endif
              @endforeach
            </ul>
          </li>
        @else 
          <li style="width: 145px;text-align: center;">
            <a href="{{ $menu->link }}">{{ $menu->name }}</a>
          </li>
        @endif
      @endforeach
      {{-- Level 1 End --}}
      
    </ul>
  </div>
  <div class="adira-nav-mobile-foot">
    <form action="/{{ $menu_link }}search" class="form-inline">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Search..."/>
      </div>
      &nbsp;
      <div class="form-group">
        <button class="btn adira-search-btn">Search</button>
      </div>
    </form>
  </div>
</div>

  {{-- Header Logo --}}
  <div class="container-fluid">
    <div class="container">
      <div class="row mt-4 mb-4">
        <div class="col-lg-3">
          <img src="/frontend/assets/images/company/award-logo.png" alt="award-logo">
        </div>
        <div class="col-lg-8 offset-lg-1 logo-wrapper">
          <a href="{{ isset($sw->link_web) ? $sw->link_web.$lang_link : null }}" >
          <img src="{{ url(''.isset($sw->logo_web) ? $sw->logo_web : null.'') }}" alt="{{ isset($sw->alt_teks) ? $sw->alt_teks : null }}">
          </a>
        </div>
      </div>
    </div>
  </div>

  <nav class="navbar navbar-expand-md navbar-light navigation-bar">
    <div class="container">
        {{-- <a class="navbar-brand" href="#">Navbar</a> --}}
        {{-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button> --}}
        <button data-toggle="collapse-side" data-target=".side-collapse" data-target-2=".adira-nav-mobile-wrapper" type="button" class="navbar-toggler">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
              
              {{-- Looping menu --}}
              {{-- Level 1 Start --}}
              @foreach ($list_menus as $menu)
                @if(isset($menu->children))
                  <li class="nav-item dropdown" >
                    <a class="nav-link py-0 font-weight-bold roboto-bold" href="{{ $menu->link }}" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                      {{ $menu->name }}
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                      {{-- Level 2 Start --}}
                      @foreach ($menu->children as $child)
                    
                        @if(isset($child->children))
                          <li class="dropdown-list">
                            <a class="dropdown-item dropdown-toggle" href="{{ $child->link }}">
                              {{ $child->name }}
                            </a>
                            
                            <ul class="dropdown-menu dropdown-menu-2nd">
                              {{-- Level 3 Start --}}
                              @foreach ($child->children as $grandchild)
                                @if(isset($grandchild->children))
                                  <li class="dropdown-list-child">
                                    <a class="dropdown-item dropdown-toggle" href="{{ $grandchild->link }}">
                                      {{ $grandchild->name }}
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-child">
                                      @foreach ($grandchild->children as $g)
                                        <li>
                                          <a class="dropdown-item" href="{{ $g->link }}">
                                            {{ $g->name }}
                                          </a>
                                        </li>
                                      @endforeach
                                    </ul>
                                  </li>
                                @else 
                                  <li>
                                    <a class="dropdown-item" href="{{ $grandchild->link }}">
                                      {{ $grandchild->name }}
                                    </a>
                                  </li>
                                @endif
                                
                              @endforeach
                              {{-- Level 3 End --}}
                            </ul>

                          </li>
                        @else
                          <li>
                            <a class="dropdown-item" href="{{ $child->link }}">
                              {{ $child->name }}
                            </a>
                          </li>
                        @endif
                        
                      @endforeach
                      {{-- Level 2 End --}}
                    </ul>
                  </li>
                @else 
                  <li class="nav-item" style="width: 145px;text-align: center;">
                    <a class="nav-link py-0 font-weight-bold" href="{{ $menu->link }}">{{ $menu->name }}</a>
                  </li>
                @endif
              @endforeach
              {{-- Level 1 End --}}
              
            </ul>
            <div class="my-2 my-lg-0" style="width: 100%;">
              <div style="float:right;">
                <span class="adira-language-link">
                  <a href="/" id="id">ID</a> | <a href="/en" id="en">EN</a> </span> &nbsp; <span class="search-icon"><i class="fas fa-search"></i></span>
              </div>
              <div class="search-form d-none">
                <form action="/{{ $menu_link }}search" class="form-inline" style="margin: 17px 28px">
                  <div class="form-group">
                    <input type="text" class="form-control" name="q" placeholder="Search..."/>
                  </div>
                  &nbsp;
                  <div class="form-group">
                    <button class="btn btn-primary adira-search-btn">Search</button>
                  </div>
                </form>
              </div>
            </div>
        </div> <!-- /container -->
    </div>
  </nav>

  @yield('content')

  {{-- Prefooter --}}
  <section class="section adira-bg-dark adira-border-bottom-dark">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <ul class="prefooter-link" style="margin: 10px auto;">
            <li><a href="{{ isset($sw->link_disclaimer) ? $sw->link_disclaimer : null }}">{{ isset($sw->disclaimer) ? $sw->disclaimer : null }}</a></li>
            <li><a href="{{ isset($sw->link_sitemap) ? $sw->link_sitemap : null }}">{{ isset($sw->sitemap) ? $sw->sitemap : null }}</a></li>
          </ul>
        </div>
      </div> 
    </div>
  </section>

  {{-- Footer --}}
  <footer class="pt-3" style="background-color:#595959;">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <ul class="footer-address">
            <li><span style="font-weight:bold;color: aliceblue;">{{ isset($sw->nm_perusahaan) ? $sw->nm_perusahaan : null }}</span></li>
            <li><span>{{ isset($sw->alamat) ? $sw->alamat : null }}</span></li>
            <li><span>{{ isset($sw->city) ? $sw->city : null }} {{ isset($sw->postal_code) ? $sw->postal_code : null }}</span></li>
            <li><span>T. {{ isset($sw->no_telp) ? $sw->no_telp : null }} | F. {{ isset($sw->fax) ? $sw->fax : null }}</span></li>
          </ul>
        </div>
        <div class="col-md-4">
          <div style="height: 100%;display: flex;justify-content: center;align-items: center;">
            <ul class="footer-sosmed">
              <li><a href="{{ isset($sw->link_sosmed1) ? $sw->link_sosmed1 : null }}" target="blank"><img src="{{ url(''.isset($sw->logo_sosmed1) ? $sw->logo_sosmed1 : null.'') }}" alt="{{ isset($sw->alt_teks_fb) ? $sw->alt_teks_fb : null }}"></a></li>
              <li><a href="{{ isset($sw->link_sosmed2) ? $sw->link_sosmed2 : null }}"  target="blank"><img src="{{ url(''.isset($sw->logo_sosmed2) ? $sw->logo_sosmed2 : null.'') }}" alt="{{ isset($sw->alt_teks_ig) ? $sw->alt_teks_ig : null }}"></a></li>
              <li><a href="{{ isset($sw->link_sosmed3) ? $sw->link_sosmed3 : null }}"  target="blank"><img src="{{ url(''.isset($sw->logo_sosmed3) ? $sw->logo_sosmed3 : null.'') }}" alt="{{ isset($sw->alt_teks_twit) ? $sw->alt_teks_twit : null }}"></a></li>
            </ul>
          </div>
        </div>
      </div>
      
      <hr class="footer-rule"/>

      <div class="row p-4">
        <div class="footer-brand">
          @if(isset($footer)) 
            @foreach ($footer as $foot )
              <div class="footer-brand-item">
                <div class="footer-brand-img-grey">
                  <a href="{{ isset($foot->link) ? $foot->link : null }}" target="blank"><img src="/Upload/footer_brand/{{ isset($foot->foto) ? $foot->foto : null }}" alt="{{ isset($foot->alt_teks) ? $foot->alt_teks : null }}"></a>
                </div>
                <div class="footer-brand-img-color" style="display:none;">
                  <a href="{{ isset($foot->link) ? $foot->link : null }}" target="blank"><img src="/Upload/footer_brand/{{ isset($foot->image) ? $foot->image : null }}" alt="{{ isset($foot->alt_teks) ? $foot->alt_teks : null }}"></a>
                </div>
              </div>
            @endforeach
          @endif
        </div>
      </div>

    </div>
  </footer>

  {{-- Copyright --}}
  <section class="section" style="background-color:#b3b3b3;">
    <div class="container">
      <div class="row adira-bottom-footer">
        <div class="col-md-12 mb-3 mt-3" style="margin: auto;">
          <p class="copyright">{{ isset($sw->copyright) ? $sw->copyright : null }}</p>
        </div>
      </div>
    </div>
  </section>

  <script src="/frontend/style/plugins/popper/popper.js"></script>
  <script src="/frontend/style/plugins/bootstrap/js/bootstrap.min.js"></script>
  <script src="/frontend/style/plugins/owlcarousel/dist/owl.carousel.min.js"></script>
  <script src="/frontend/style/js/main.js"></script>
  {{-- <script src="https://platform-api.sharethis.com/js/sharethis.js#property=5dd4191afad6d20013de6a25&product=inline-share-buttons" async="async" type="text/javascript"></script> --}}

  <script>
      $('#container').hide();
      $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
      });


      $("#header-slider").owlCarousel({
        loop: true,
        margin: 0,
        nav: false,
        dots: false,
        autoplay:true,
        paginationSpeed: 1000,
        autoplayTimeout:2000,
        autoplayHoverPause:true,
        transitionStyle : "fade",
        lazyLoad: true,
        responsive: {
          0: {
              items: 1,
              dots: true,
              margin:0
          },
          600: {
              items: 1,
              dots: true
          },
          1000: {
              items: 1,
              dots: true
          }
        }
      });

      $("#whats-on-slider").owlCarousel({
        margin: 0,
        dots: true,
        responsive: {
          0: {
              items: 1,
              dots: true,
              margin:0
          },
          600: {
              items: 2,
              dots: true
          },
           780: {
              items: 3,
              dots: true
          },
          1000: {
              items: 4,
              dots: true
          }
        }
      });

      $("#product-slider").owlCarousel({
        // margin: -250,
        dots: false,
        loop: false,
        nav: true,
        lazyLoad: true,
        center:false,
        navText: ["<i class='fas fa-caret-left'></i>", "<i class='fas fa-caret-right'></i>"],
        responsive: {
          0: {
              items: 1,
              dots: false,
              margin:0
          },
          425: {
              items: 2,
              dots: false,
              margin:-50
          },
          600: {
              items: 4,
              dots: false
          },
          1000: {
              items: 5,
              dots: false
          },
          1024: {
              items: 5,
              dots: false
          }
        }
      });

      $("#testimony-slider").owlCarousel({
        loop: false,
        margin: 0,
        nav: false,
        dots: true,
        autoplay:false,
        paginationSpeed: 1000,
        autoplayTimeout:2000,
        autoplayHoverPause:true,
        transitionStyle : "fade",
        lazyLoad: true,
        responsive: {
          0: {
              items: 1,
              dots: true,
          },
          600: {
              items: 1,
              dots: true
          },
          1000: {
              items: 1,
              dots: true
          }
        }
      });

      $(document).ready( function () {
        $( '.dropdown-menu a.dropdown-toggle' ).on( 'hover', function ( e ) {
          var $el = $( this );
          $el.toggleClass('active-dropdown');
          var $parent = $( this ).offsetParent( ".dropdown-menu" );
          if ( !$( this ).next().hasClass( 'show' ) ) {
              $( this ).parents( '.dropdown-menu' ).first().find( '.show' ).removeClass( "show" );
          }
          var $subMenu = $( this ).next( ".dropdown-menu" );
          $subMenu.toggleClass( 'show' );
          
          $( this ).parent( "li" ).toggleClass( 'show' );

          $( this ).parents( 'li.nav-item.dropdown.show' ).on( 'hidden.bs.dropdown', function ( e ) {
              $( '.dropdown-menu .show' ).removeClass( "show" );
              $el.removeClass('active-dropdown');
          } );
          
          if ( !$parent.parent().hasClass( 'navbar-nav' ) ) {
              $el.next().css( { "top": $el[0].offsetTop, "left": $parent.outerWidth() - 4 } );
          }

          return false;
        });

        $('.dropdown-list').mouseenter(function() {
          var lebar = $(this).width();
          var offset = $(this).offset();
          
          $(this).children('.dropdown-menu').css("top",offset.top - 145 + "px").css("left",lebar)
          
        }).mouseleave(function() {  
          
        });
        
        $('.dropdown-list-child').mouseenter(function() {
          var lebar = $(this).width();
          var offset = $(this).offset();
          
          $(this).children('.dropdown-menu-child').css("top",offset.top - 200 + "px").css("left",lebar)
          
        }).mouseleave(function() {
          
        });

        // Search Bar
        $('.search-icon').click(function(){
          var status = $('.search-form').hasClass('d-none')

          if(status) {
            $('.search-form').removeClass('d-none')
            $('.search-form').addClass('d-show')
          } else {
            $('.search-form').removeClass('d-show')
            $('.search-form').addClass('d-none')
          }

          
        })

      });

  </script>
  @stack('script')

</body>
</html>