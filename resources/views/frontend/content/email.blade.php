<h3>Hallo, You've got message from {{ $request->name }} !</h3>

<table border="0" cellpadding="4" cellspacing="0">
    <thead style="text-align:justify;">
        <tr><th>From</th><th>:</th><th>{{ $request->name }}</th></tr>
        <tr><th>Phone Number</th><th>:</th><th>{{ $request->phone_number }}</th></tr>
        <tr><th>Email</th><th>:</th><th>{{ $request->email }}</th></tr>
        <tr><th>Message</th><th>:</th><th>{{ $request->message }}</th></tr>   
       </tr>
    </thead>
</table>
 
 