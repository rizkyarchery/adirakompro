@extends('frontend.layouts.index')

@section('page_title',$page->title)

@section('content')

@php
$lang = App::getLocale();
@endphp

<section class="section">
  <div class="container mt-5 mb-5">
    <h3 class="mb-5" style="font-weight: bold;">{!! $page->title !!}</h3>
    @if($page->image != null)
    <img src="/{{ $page->image }}" class="mb-3 w-100" alt=""/>
    @endif

    @if($message = Session::get('success'))
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert">×</button> 
          <strong>{{ $message }}</strong>
        </div>
    @endif
    
    <?=$content?>
    <div id="container">
    </div>
  </div>
</section>

@include('frontend.content.modals')
@endsection

@push('meta_tag')
  <meta name="description"  content="{{ $page->meta_description   ?? $page->meta_description }}">
  <meta name="title"        content="{{ $page->meta_title         ?? $page->meta_title }}">
@endpush

@push('style')

@endpush

@push('script')
<script>
  var lang = '{{ $lang }}'
  if(lang == 'id'){
    var url_id = "{{ $page->slug ?? '' }}";
    var url_en = "en/{{ $page_parent->slug ?? ''}}";
  }else{
    var url_id = "{{ $page_parent->slug ?? ''}}";
    var url_en = "en/{{ $page->slug ?? '' }}";
  }
  
  $('#id').attr('href', '/'+url_id);
  $('#en').attr('href', '/'+url_en);

  $(document).on('click', '.btn-bengkel', function(){
    var find = $('.adira-input-garage').val();
      if(find == ""){
        return false
      }else{
        $.ajax({
        url: '/find-bengkel?q='+find,
        type: 'GET',
        success: function(response){
         $('#accordion').fadeOut(function(){
          $('.workshop-delete').remove();
            $('#container').show();
            $('#container').after(response);
         });
        }
      })
      }
    })

    $(document).on('click', '.btn-office', function(){
    var find = $('.adira-input-office').val();
      if(find == ""){
        return false
      }else{
        $.ajax({
        url: '/find-office?q='+find,
        type: 'GET',
        success: function(response){
          console.log(response);
         $('#accordion').fadeOut(function(){
          $('.remove-office').remove();
            $('#container').show();
            $('#container').after(response);
         });
        }
      })
      }
    })
</script>
@endpush