{{-- Laporan keuangan tahunan --}}

<section class="section">
    <div class="container mt-5 mb-5">
    
        <div class="adira-tabs">
            <input type="radio" name="tabs" id="tabone" checked="checked">
            <label for="tabone">Laporan Keuangan Tahunan</label>
            <div class="adira-tab">
                <h1>Tab One Content</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
        
            <input type="radio" name="tabs" id="tabtwo">
            <label for="tabtwo">Laporan Keuangan Triwulan</label>
            <div class="adira-tab">
                <h1>Tab Two Content</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
        </div>
    
    </div>
</section>

{{-- Struktur Organisasi --}}

<section>
    <div class="container">
        <div class="row">
        <div class="col-md-4">
            <div class="adira-structure-management-wrapper">
                <div class="adira-structure-management-img">
                    <img src="/frontend/assets/images/common/asia-laptop.jpg" class="w-100" alt="">
                </div>
                <div class="p-2">
                    <div>
                        <span class="bold">Manggi Taruna Habir</span>
                    </div>
                    <div class="mb-3">
                        <small>Komisaris Utama</small>
                    </div>
                    <div class="mb-1">
                        <span>Lorem ipsum dolor sit amet consectetur adipisicing elit. </span>
                    </div>
                    <div class="mb-1 adira-structure-management-read-more">
                        <u><a href="#" class="structure-modal" 
                        data-structure-name="Manggi Taruna Habir"
                        data-structure-council="Komisaris Utama"
                        data-structure-description="Lorem ipsum dolor sit amet consectetur, adipisicing elit. Vitae, inventore quo voluptatibus quae blanditiis sed! Repellat dignissimos aliquam repellendus voluptas nostrum! Quisquam sequi ut modi a odio, nisi rerum consequatur.Lorem ipsum dolor sit amet consectetur, adipisicing elit. Vitae, inventore quo voluptatibus quae blanditiis sed! Repellat dignissimos aliquam repellendus voluptas nostrum! Quisquam sequi ut modi a odio, nisi rerum consequatur.Lorem ipsum dolor sit amet consectetur, adipisicing elit. Vitae, inventore quo voluptatibus quae blanditiis sed! Repellat dignissimos aliquam repellendus voluptas nostrum! Quisquam sequi ut modi a odio, nisi rerum consequatur."
                        data-structure-image="/frontend/assets/images/common/asia-laptop.jpg">Profile Selengkapnya</a></u>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</section>

{{-- Career --}}
<section class="section">
  <div class="container mt-5 mb-5">
    <div class="row">
      <div class="col-md-6">
        <div class="adira-carrer-wrap">
          <div class="adira-carrer-position">
            <span>Job Position</span>
          </div>
          <div class="mt-2">
            <i class="far fa-circle pr-2"></i> <span>Min: x year Experience</span>
          </div>
          <div class="mt-2">
            <i class="far fa-circle pr-2"></i> <span>Indonesia - Jakarta Raya</span>
          </div>
          <div class="mt-4">
            <button class="btn btn-secondary">Lamar Pekerjaan</button>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="adira-carrer-wrap">
          <div class="adira-carrer-position">
            <span>Job Position</span>
          </div>
          <div class="mt-2">
            <i class="far fa-circle pr-2"></i> <span>Min: x year Experience</span>
          </div>
          <div class="mt-2">
            <i class="far fa-circle pr-2"></i> <span>Indonesia - Jakarta Raya</span>
          </div>
          <div class="mt-4">
            <button class="btn btn-secondary">Lamar Pekerjaan</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

{{-- Unduh Brosur --}}
<section class="section">
  <div class="container mt-5 mb-5">
    <div class="row">
      <div class="col-md-12">
        <i class="far fa-arrow-alt-circle-down adira-unduh-yellow"></i> <span><u>Unduh Brosur</u></span>
      </div>
    </div>
  </div>
</section>

{{-- Daftar bengkel --}}
{{-- entar tambahin accordionnya yah pembuka --}}
<section class="section">
  <div class="container">
    <div class="row mt-5 mb-5">
      <div class="col-md-8">
        <div>
          <input type="text" class="form-control adira-input-garage" placeholder="Find by Name or City" />
        </div>
      </div>
      <div class="col-md-4">
        <div>
          <button class="btn btn-secondary">Find</button>
        </div>
      </div>
    </div>
  </div>
</section>

<div class="row">
  <div class="col-md-6">
    <div class="adira-garage-wrap">
      <div class="adira-garage-location-label p-2">
        <span>Location Street</span>
      </div>
      <div class="p-2">
        <table>
          <tr>
            <td><i class="fas fa-map-marker-alt adira-garage-icon"></i> &nbsp;</td><td> Address</td>
          </tr>
          <tr>
            <td><i class="fas fa-phone-alt adira-garage-icon"></i></i> &nbsp;</td><td> (021) 123456</td>
          </tr>
          <tr>
            <td><img src="/frontend/assets/images/common/office.png" alt=""> &nbsp;</td><td> Non Authorize</td>
          </tr>
          <tr>
            <td><i class="fas fa-fax adira-garage-icon"></i> &nbsp;</td><td> (021) 98765</td>
          </tr>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="adira-garage-wrap">
      <div class="adira-garage-location-label p-2">
        <span>Location Street</span>
      </div>
      <div class="p-2">
        <table>
          <tr>
            <td><i class="fas fa-map-marker-alt adira-garage-icon"></i> &nbsp;</td><td> Address</td>
          </tr>
          <tr>
            <td><i class="fas fa-phone-alt adira-garage-icon"></i></i> &nbsp;</td><td> (021) 123456</td>
          </tr>
          <tr>
            <td><img src="/frontend/assets/images/common/office.png" alt=""> &nbsp;</td><td> Non Authorize</td>
          </tr>
          <tr>
            <td><i class="fas fa-fax adira-garage-icon"></i> &nbsp;</td><td> (021) 98765</td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
{{-- entar tambahin accordionnya yah penutup --}}

{{-- Sitemap --}}
<section class="section">
  <div class="container">
    <div class="row">
      <div class="col-md-2">
        <ul class="adira-sitemap-wrap">
          <li class="adira-sitemap-bold">Profile</li>
          <li>About AI</li>
          <li>Visi, misi & corporate value</li>
          <li>Struktur & Management</li>
          <li>Anggaran Dasar</li>
          <li class="adira-sitemap-bold">Penghargaan</li>
          <li class="adira-sitemap-bold">Informasi Investor</li>
          <li>Prospektus</li>
          <li>Laporan Tahunan</li>
          <li>Laporan keuangan</li>
          <li>Informasi RUPS</li>
        </ul>
      </div>
      <div class="col-md-2">
        <ul class="adira-sitemap-wrap">
          <li class="adira-sitemap-bold">CSR</li>
          <li class="adira-sitemap-bold">Tata Kelola</li>
          <li class="adira-sitemap-bold">Berita & Artikel</li>
          <li>News</li>
          <li>Artikel</li>
          <li class="adira-sitemap-bold">Hubungi Kami</li>
          <li>Kantor Pusat & Kantor Cabang</li>
          <li>Feedback</li>
        </ul>
      </div>
      <div class="col-md-2">
        <ul class="adira-sitemap-wrap">
          <li class="adira-sitemap-bold">Retail</li>
          <li>Autocilin</li>
          <li>Motopro</li>
          <li>Travellin</li>
          <li>Home Insurance</li>
          <li>Arthacilin</li>
          <li>Motolite</li>
          <li>Mobilite</li>
          <li>Asuransi Demam Berdarah</li>
          <li>Asuransi Tipus</li>
        </ul>
      </div>
      <div class="col-md-3">
        <ul class="adira-sitemap-wrap">
          <li class="adira-sitemap-bold">Corporate</li>
          <li>Asuransi Kendaraan Bermotor</li>
          <li>Asuransi Property</li>
          <li>Asuransi Alat Berat</li>
          <li>Asuransi Kesehatan Medicillin</li>
          <li>Asuransi Kredit Perdagangan</li>
          <li>Asuransi Harta Benda</li>
          <li>Asuransi Pengangkutan</li>
          <li class="adira-sitemap-bold">Syariah</li>
          <li>Asuransi Motor Mikro Syariah</li>
          <li>Asuransi Travellin Syariah</li>
          <li>Asuransi Salaam Sehat Syariah</li>
          <li>Asuransi Salaam Card Syariah</li>
        </ul>
      </div>
      <div class="col-md-2">
        <ul class="adira-sitemap-wrap">
          <li class="adira-sitemap-bold">Layanan</li>
          <li class="adira-sitemap-bold">Adira Care</li>
          <li class="adira-sitemap-bold">Autocillin Rescure</li>
          <li class="adira-sitemap-bold">Autocillin Mobile Service</li>
          <li class="adira-sitemap-bold">Autocillin Mobile Claim</li>
          <li class="adira-sitemap-bold">Autocillin Apps</li>
          <li class="adira-sitemap-bold">Autocillin Garage</li>
          <li class="adira-sitemap-bold">Daftar Bengkel</li>
        </ul>
      </div>
    </div>
  </div>
</section>
{{-- Sitemap --}}

{{-- Contact Us --}}

<section class="section">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.151968681224!2d106.85139891413783!3d-6.243694362874458!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f3bb776ff2a5%3A0x9b1ee3e3dbb2a3a!2sGraha%20Adira!5e0!3m2!1sen!2sid!4v1574182011802!5m2!1sen!2sid" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
      </div>
    </div>
    <div class="row mb-5">
      <div class="col-md-7 mt-5">
        <form action="">
          <div class="form-group">
            <label for="name" class="adira-contact-bold">@lang('global.full_name')</label>
            <input id="name" type="text" name="name" class="form-control" placeholder="@lang('global.full_name')" />
          </div>
          <div class="form-group">
            <label for="email" class="adira-contact-bold">@lang('global.email')</label>
            <input id="email" type="text" name="email" class="form-control" placeholder="@lang('global.email')" />
          </div>
          <div class="form-group">
            <label for="telephone" class="adira-contact-bold">@lang('global.telephone')</label>
            <input id="telephone" type="text" class="form-control" placeholder="@lang('global.telephone')" />
          </div>
          <div class="form-group">
            <label for="message" class="adira-contact-bold">@lang('global.message')</label>
            <textarea name="message" id="message" cols="30" rows="10" class="form-control"></textarea>
          </div>
          <div class="form-group">
            <button class="btn btn-secondary float-right">SEND</button>
          </div>
        </form>
      </div>
      <div class="col-md-4 mt-5">
        <div>
          <ul style="padding:0px;list-style-type: none;">
            <li style="font-weight:bold;">Graha Adira</li>
            <li>MT. Haryono Kav.42</li>
            <li>Jakarta 12780</li>
            <li>T. +62-21 2966 7373 | F. +62-21 2966 7345</li>
          </ul>
        </div>
        <div>
          <ul style="padding:0px;list-style-type: none;">
            <li style="font-weight:bold;">Adira Care</li>
            <li>Call 1500 456</li>
            <li>SMS +62-812 111 3456</li>
            <li>adiracare@asuransiadira.co.id</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>

{{-- Contact Us --}}