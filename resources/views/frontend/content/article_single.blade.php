@extends('frontend.layouts.index')

@section('page_title', $article->judul)

@section('content')
@php

  use Illuminate\Support\Str;
  use Carbon\Carbon;
  use App\Modules\Setting\Models\Setting;

  $lang_switch  = App::getLocale();
  $lang         = App::getLocale();
  switch ($lang) {
    case 'en':
        $lang = 'en/';
        $code = '002';
      break;
    case 'id':
        $lang = '';
        $code = '001';
      break;
    default:
      break;
  }

  $article_switch = $article;

  $setting = Setting::where('kode',$code)->first();

@endphp

{{-- {{ dd($article)  }} --}}
<section class="section mt-5 mb-5">
  <div class="container">
    <div class="mb-4">
      <span class="adira-breadcrumb"><a href="articles">Articles / </a></span>
      <span class="adira-breadcrumb"><a href="/{{ $lang.$article->slug }}">{{ $article->judul }}</a></span>
    </div>

    {{-- Meta data --}}
    <div class="mb-4">
      <h3>{{ $article->judul }}</h3>
      <span>
        @lang('global.published_date')
      </span>
      <span>
        {{ $article->created_at->format('M, d Y H:i')  }}
      </span>
      <span>
        | @lang('global.publisher') {{ $article->publisher }}
      </span>
    </div>

    {{-- Thumbnail --}}
    <div class="adira-thumbnail">
      @if(isset($article->foto))
        <img src="/Upload/Article/{{ $article->foto }}" class="w-100" alt="">
      @endif
    </div>

    {{-- Content --}}
    <div class="mt-3 mb-4">
      {!! $article->isi_artikel !!}
    </div>

    {{-- Button Share --}}
    <div style="display: flex;align-items: center;">
      <span class="adira-share-word">@lang('global.share_this_article') : </span>
      <div style="display:flex;">
        <div class="mr-1">
          <a href="https://www.facebook.com/sharer/sharer.php?u={{ $setting->link_web }}/{{ $lang . $article->slug }}" target="_blank">
            <img src="/frontend/assets/images/common/share_facebook.png" style="width:25px;height:25px;"  alt="" />
          </a>
        </div>
        <div class="mr-1">
          <a href="http://www.twitter.com/share?url={{ $setting->link_web }}/{{ $lang . $article->slug }}" target="_blank">
            <img src="/frontend/assets/images/common/share_twitter.png"  style="width:25px;height:25px;" alt="" />
          </a>
        </div>
      </div>
    </div>

    {{-- Tags --}}
    @if(isset($article->tags[0]))
    <div>
      <span style="font-size:40px;">#</span>
        @foreach ($article->tags as $tags)
        <a href="/articles/tag/{{ $tags->slug }}"><span class="adira-tag-badge">{{ ucfirst($tags->name) }}</span></a>
        @endforeach
    </div>
    @endif

    <hr style="border:1px solid #cecece;" />

    <h1 class="text-center">@lang('global.related_article')</h1>

    {{-- Whats on --}}
    <div class="row mt-4">
      <div id="whats-on-slider-content" class="owl-carousel owl-theme">
      @foreach($articles as $article)
        <div class="whats-on-item" style="width: 280px;">
          <div>
            @if (file_exists(public_path($article->thumbnail)))
              <img src="{{ asset('/Upload/Article/thumbnail/'. $article->thumbnail) }}" class="w-100" alt="{{ $article->alt_teks }}" height="200px">
            @else
              <img src="{{ asset('frontend/assets/images/common/no-images.jpg') }}" class="w-100" height="200px" alt="no-images">
            @endif
          </div>
          <div class="mt-1 mb-1">
            <span style="font-size: 17px;color:#000;font-weight:bold;" data-toggle="tooltip" data-placement="right" title="{{ $article->judul }}">
              {{ Str::limit($article->judul, 60, '...') }}
            </span>
          </div>
          <div class="mt-1 mb-1">
            <span data-toggle="tooltip" data-placement="right" title="{{ $article->excerpt }}" style="font-size:13px;">
                {{ Str::limit($article->excerpt, 120, '...') }}
            </span>
          </div>
          <div class="mt-4">
            <a href="{{ $setting->link_web }}/{{ $lang . $article->slug }}" class="btn btn-secondary" target="_BLANK">@lang('global.read_more')</a>
          </div>
        </div>
      @endforeach
      </div>
    </div>
    
  </div>
</section>
@endsection

@push('meta_tag')
  <meta name="description"  content="{{ $article->meta_description  ?? $article->meta_description }}">
  <meta name="keywords"     content="{{ $article->keyword           ?? $article->keyword }}">
@endpush

@push('style')

@endpush

@push('script')
<script>
  $(function () {

    var lang = '{{ $lang_switch }}'
    if(lang == 'id'){
      var url_id = "{{ $article_switch->slug ?? '' }}";
      var url_en = "en/{{ $article_child->slug ?? ''}}";
    }else{
      var url_id = "{{ $article_child->slug ?? ''}}";
      var url_en = "en/{{ $article_switch->slug ?? '' }}";
    }
    
    $('#id').attr('href', '/'+url_id);
    $('#en').attr('href', '/'+url_en);


    $('[data-toggle="tooltip"]').tooltip()

    $("#whats-on-slider-content").owlCarousel({
      margin: 0,
      dots: true,
      responsive: {
        0: {
            items: 1,
            dots: true,
            margin:0
        },
        600: {
            items: 3,
            dots: true
        },
        1000: {
            items: 3,
            dots: true
        }
      }
    });
  })
</script>
@endpush