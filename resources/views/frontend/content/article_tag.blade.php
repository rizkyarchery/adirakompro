@extends('frontend.layouts.index')

@section('page_title', "Article")

@section('content')
@php


  use Carbon\Carbon;
  use \Illuminate\Support\Str;
  use App\Modules\Setting\Models\Setting;

  $lang = App::getLocale();
  \Carbon\Carbon::setLocale($lang);

  switch ($lang) {
    case 'en':
        $lang = 'en/';
        $code = '002';
      break;
    case 'id':
        $lang = '';
        $code = '001';
      break;
    default:
      break;
  }

  $setting = Setting::where('kode',$code)->first();


@endphp

<section class="section mt-5 mb-5">
  <div class="container">
    <div class="row mb-3">
      <div class="col-md-6">
          <h3>@lang('global.article_tag')</h3>
      </div>
    </div>
    <div class="row">
      
    @if(isset($articles[0]))
        @foreach ($articles as $article)
          <div class="col-lg-4 col-md-6 mt-4">
            <div class="adira-articles-list-item">
              <div>
                <div style="background-color: #f7c621;width: 54px;position: absolute;left: 19%;border: 1px solid #ababab;">
                  <div style="width: 50px;text-align: center;font-weight: bold;height: 30px;">
                    <span style="font-size: 19px;">{{ $article->created_at->format('d') }}</span>
                  </div>
                  <div style="height: 30px;text-align: center;font-weight: bold;">
                    <span style="font-size: 13px;">{{ $article->created_at->format('M y') }}</span>
                  </div>
                </div>
              </div>
              <div style="height:200px;">
                <a href="{{ $setting->link_web }}/{{ $lang . $article->slug }}" target="_BLANK">
                  @if (file_exists(public_path('/Upload/Article/thumbnail/' . $article->thumbnail)))
                    <img src="{{ asset('/Upload/Article/thumbnail/' . $article->thumbnail) }}" class="w-100" alt="{{ $article->alt_teks }}">
                  @else
                    <img src="{{ asset('frontend/assets/images/common/no-images.jpg') }}" class="w-100">
                  @endif
                </a>
              </div>
              <div style="height:50px;" class="adira-articles-list-item-title p-1 mr-3 ml-3 mt-1 mb-1 text-center">
                <a href="{{ $setting->link_web }}/{{ $lang . $article->slug }}" target="_BLANK">
                  <span data-toggle="tooltip" data-placement="right" title="{{ $article->judul }}">{{ Str::limit($article->judul, 54,'...')  }}</span>
                </a>
              </div>
              <div style="height: 76px;" class="p-1 mr-3 ml-3 mt-2 text-center">
                <span style="font-size:13px;" data-toggle="tooltip" data-placement="right" title="{{ $article->excerpt }}">{{ Str::limit($article->excerpt, 90,'...')  }}</span>
              </div>
              <div class="adira-articles-list-item-read-more mt-4 ml-3">
                <a href="{{ $setting->link_web }}/{{ $lang . $article->slug }}" target="_BLANK"><small>@lang('global.read_more') ></small></a>
                <div style="float:right;display:flex;margin-right:10px;">
                  <div class="mr-2">
                    <a href="https://www.facebook.com/sharer/sharer.php?u={{ $setting->link_web }}/{{ $lang . $article->slug }}" target="_blank">
                      <img src="/frontend/assets/images/common/share_facebook.png" style="width:25px;height:25px;"  alt="" />
                    </a>
                  </div>
                  <div class="mr-2">
                    <a href="http://www.twitter.com/share?url={{ $setting->link_web }}/{{ $lang . $article->slug }}" target="_blank">
                      <img src="/frontend/assets/images/common/share_twitter.png"  style="width:25px;height:25px;" alt="" />
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
        @endforeach
    @else 
        <div class="col-md-12">
            <h6 style=" font-style: italic;">@lang('global.not_found_article')</h6>
        </div>
    @endif
      
    </div>
    <div class="row mt-5">
      <div class="col-md-12" style="display:flex;justify-content:center;">
        {{ $articles->links() }}
      </div>
    </div>
  </div>
</section>

@endsection

@push('script')
<script>
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })
</script>
@endpush