@extends('frontend.layouts.index')

@section('page_title', $page->title)

@section('content')

<style>
  
  /* body {
    font-family: "myriadpro","Helvetica",Helvetica,Arial,sans-serif !important;
    -webkit-font-smoothing: antialiased;
    font-weight: normal;
    font-style: normal;
  }

  @font-face {
    font-family: "myriadpro";
    src: url('/frontend/style/fonts/MYRIADPRO-REGULAR.woff') format("truetype");
  } */

  /* Khusus homepage */
  #header-slider .owl-dots {
    display: none;
  }

  #product-slider .owl-prev:hover {
    background      : transparent;
    color           : #FFF;
    text-decoration : none;
  }

  #product-slider .owl-next:hover {
    background      : transparent;
    color           : #FFF;
    text-decoration : none;
  }

  .owl-prev {
    width: 30px;
    height: 15px;
    position: absolute;
    top: 35%;
    left: 10px;
    display: block !important;
    border:0px solid black;
    outline: none !important;
  }

  .owl-next {
    width: 30px;
    height: 15px;
    position: absolute;
    top: 35%;
    right: 25px;
    display: block !important;
    border:0px solid black;
    outline: none !important;
  }
  .owl-prev i, .owl-next i {
    transform : scale(3,2); 
    color: rgba(254,203,14,1);
  }
  /* @media (min-width:1024px) {
    .owl-item { 
      width : 162px !important; 
    }
  } */

  #product-slider {
    margin: 0px auto;
  }
  #product-slider .owl-stage-outer {
    margin: 0 auto;
  }
  
</style>

{{-- Slider --}}
  <section class="section">
    <div id="header-slider" class="owl-carousel">
      @foreach ($banner as $b )
        <div class="owl-slide">
        <div class="owl-text">
          <a href="{{ isset($b->link) ? $b->link : null }}" target="blank"><img src="{{ url(''.isset($b->foto) ? $b->foto : null.'') }}" class="img-fluid w-100" alt="{{ isset($b->alt_teks) ? $b->alt_teks : null }}"></a>
        </div>
      </div>
      @endforeach
    </div>
  </section>

  {{-- Whats on --}}
  <section class="section p-5 adira-bg-yellow @if(count($news) < 1) d-none @endif">
    <div class="container">
      <div class="separator">WHAT'S ON</div>
      <div class="row mt-4">
        <div id="whats-on-slider" class="owl-carousel owl-theme">
        @foreach($news as $new)
          <div class="whats-on-item" style="max-width: 250px;">
            <div>
              <a href="{{ $setting->link_web }}/{{$language.$new->slug }}" style="text-decoration:none;color:#000;">
                {{-- @if (file_exists(public_path('/Upload/News/thumbnail/'.$new->thumbnail))) --}}
                 @if (file_exists(public_path($new->thumbnail)))
                  <img src="{{ asset('/' .$new->thumbnail) }}" class="w-100" alt="{{ $new->alt_teks }}">
                @else
                  <img src="{{ asset('frontend/assets/images/common/no-images.jpg') }}" class="w-100" alt="images">
                @endif
              </a>
            </div>
            <div class="mt-1 mb-1 adira-whats-on-title-wrapper">
              <a href="{{ $setting->link_web }}/{{$language.$new->slug }}" style="text-decoration:none;color:#000;">
                <span style="font-size: 17px;color: #000;font-weight: bold;"  data-toggle="tooltip" data-placement="right" title="{{ $new->title }}">
                  {{ Str::limit($new->title, 66,'...') }}
                </span>
              </a>
            </div>
            <div class="mt-1 mb-1">
              <a href="{{ $setting->link_web }}/{{ $language.$new->slug }}" style="text-decoration:none;color:#000;">
                <span style="font-size: 13px;" data-toggle="tooltip" data-placement="right" title="{{ $new->excerpt }}">
                  {{ Str::limit($new->excerpt, 60,'...')  }}
                </span>
              </a>
            </div>
            <div class="mt-1">
              <a href="{{ $setting->link_web }}/{{$language.$new->slug }}" class="btn btn-black">@lang('global.read_more')</a>
            </div>
          </div>
        @endforeach
        </div>
      </div>
    </div>
  </section>

  {{-- Product --}}
  <section class="section p-5 adira-bg-white @if(count($products) < 1) d-none @endif">
    <div class="container">
      <div class="separator">PRODUCTS</div>
      <div class="row mt-4">
        <div id="product-slider" class="owl-carousel owl-theme">
          @foreach ($products as $product)
              <div class="product-slider-item p-2">
                <div class="adira-bg-yellow product-item-wrapper">
                  <div class="adira-bg-white" style="border-radius: 50%;">
                    <a href="{{ isset($product->link) ? $product->link : null }}" target="blank"><img src="/Upload/product/{{ isset($product->foto) ? $product->foto : null }}" class="product-icon-img" alt="{{ isset($product->alt_teks) ? $product->alt_teks : null }}"/></a>
                  </div>
                </div>
              </div>
          @endforeach
          {{-- <div class="product-slider-item p-2">
            <div class="adira-bg-yellow product-item-wrapper">
              <div class="adira-bg-white" style="border-radius: 50%;">
                <img src="/frontend/assets/images/company/icon2.png" class="product-icon-img"/>
              </div>
            </div>
          </div> --}}
          {{-- <div class="product-slider-item p-2">
            <div class="adira-bg-yellow product-item-wrapper">
              <div class="adira-bg-white" style="border-radius: 50%;">
                <img src="/frontend/assets/images/company/icon3.png" class="product-icon-img"/>
              </div>
            </div>
          </div> --}}
          {{-- <div class="product-slider-item p-2">
            <div class="adira-bg-yellow product-item-wrapper">
              <div class="adira-bg-white" style="border-radius: 50%;">
                <img src="/frontend/assets/images/company/icon4.png" class="product-icon-img"/>
              </div>
            </div>
          </div> --}}
          {{-- <div class="product-slider-item p-2">
            <div class="adira-bg-yellow product-item-wrapper">
              <div class="adira-bg-white" style="border-radius: 50%;">
                <img src="/frontend/assets/images/company/icon5.png" class="product-icon-img"/>
              </div>
            </div>
          </div> --}}
          {{-- <div class="product-slider-item p-2">
            <div class="adira-bg-yellow product-item-wrapper">
              <div class="adira-bg-white" style="border-radius: 50%;">
                <img src="/frontend/assets/images/company/icon6.png" class="product-icon-img"/>
              </div>
            </div>
          </div> --}}
        </div>
      </div>
    </div>
  </section>

  {!! $content !!}
@endsection

@push('script')
<script>
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })
</script>
@endpush