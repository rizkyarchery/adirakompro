@extends('frontend.layouts.index')

@section('page_title', "Article")

@section('content')
@php

  use Carbon\Carbon;
  use \Illuminate\Support\Str;
  use App\Modules\Setting\Models\Setting;

  $lang = App::getLocale();
  \Carbon\Carbon::setLocale($lang);

  switch ($lang) {
    case 'en':
        $lang = 'en/';
        $code = '002';
      break;
    case 'id':
        $lang = '';
        $code = '001';
      break;
    default:
      break;
  }

  $setting = Setting::where('kode',$code)->first();


@endphp

<section class="section mt-5 mb-5">
  <div class="container">
    <div class="row mb-3">
      <div class="col-md-6">
          <h3>@lang('global.news')</h3>
      </div>
    </div>
    <div class="row">
      
      @if(isset($news))
        @foreach ($news as $berita)
          <div class="col-lg-4 col-md-6 mt-4">
            <div class="adira-articles-list-item">
              <div>
                <div style="background-color: #f7c621;width: 54px;position: absolute;left: 47px;border: 1px solid #ababab;">
                  <div style="width: 50px;text-align: center;font-weight: bold;height: 30px;">
                    <span style="font-size: 19px;">{{ $berita->created_at->format('d') }}</span>
                  </div>
                  <div style="height: 30px;text-align: center;font-weight: bold;">
                    <span style="font-size: 13px;">{{ $berita->created_at->format('M y') }}</span>
                  </div>
                </div>
              </div>
              <div style="height:200px;">
                <a href="{{ $setting->link_web }}/{{ $lang . $berita->slug }}" target="_BLANK">
                  @if (file_exists(public_path($berita->thumbnail)))
                    <img src="{{ asset('/'.$berita->thumbnail) }}" class="w-100" alt="{{ $berita->alt_teks }}">
                  @else
                    <img src="{{ asset('frontend/assets/images/common/no-images.jpg') }}" class="w-100" alt="">
                  @endif
                </a>
              </div>
              <div style="height:50px;" class="adira-articles-list-item-title p-1 mr-3 ml-3 mt-1 mb-1 text-center">
                <a href="{{ $setting->link_web }}/{{ $lang . $berita->slug }}" target="_BLANK">
                  <span data-toggle="tooltip" data-placement="right" title="{{ $berita->title }}">{{ Str::limit($berita->title, 50,'...')  }}</span>
                </a>
              </div>
              <div style="height: 76px;" class="p-1 mr-3 ml-3 mt-2 text-center">
                <span style="font-size:13px;" data-toggle="tooltip" data-placement="right" title="{{ $berita->excerpt }}">{{ Str::limit($berita->excerpt, 90,'...')  }}</span>
              </div>
              <div class="adira-articles-list-item-read-more mt-4 ml-3">
                <a href="{{ $setting->link_web }}/{{ $lang . $berita->slug }}" target="_BLANK"><small>@lang('global.read_more') ></small></a>
                <div style="float:right;display:flex;margin-right:10px;">
                  <div class="mr-2">
                    <a href="https://www.facebook.com/sharer/sharer.php?u={{ $setting->link_web }}/{{ $lang . $berita->slug }}" target="_blank">
                      <img src="/frontend/assets/images/common/share_facebook.png" style="width:25px;height:25px;"  alt="" />
                    </a>
                  </div>
                  <div class="mr-2">
                    <a href="http://www.twitter.com/share?url={{ $setting->link_web }}/{{ $lang . $berita->slug }}" target="_blank">
                      <img src="/frontend/assets/images/common/share_twitter.png"  style="width:25px;height:25px;" alt="" />
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
        @endforeach
      @endif
      
    </div>
    <div class="row mt-5">
      <div class="col-md-12" style="display:flex;justify-content:center;">
        {{ $news->links() }}
      </div>
    </div>
  </div>
</section>

@endsection

@push('script')
<script>
  $('#id').attr('href', '/news');
  $('#en').attr('href', '/en/news');

  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })
</script>
@endpush