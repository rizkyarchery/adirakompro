@extends('frontend.layouts.index')

@section('page_title', $news->title)

@section('content')
@php

  use Illuminate\Support\Str;
  use Carbon\Carbon;
  use App\Modules\Setting\Models\Setting;

  $lang_switch  = App::getLocale();
  $lang = App::getLocale();

  switch ($lang) {
    case 'en':
        $lang = 'en/';
        $code = '002';
      break;
    case 'id':
        $lang = '';
        $code = '001';
      break;
    default:
      break;
  }

  $news_switch = $news;

  $setting = Setting::where('kode',$code)->first();

@endphp
{{-- {{ dd($news->image) }} --}}
<section class="section mt-5 mb-5">
  <div class="container">
    <div class="mb-4">
      <span class="adira-breadcrumb"><a href="news">News / </a></span>
      <span class="adira-breadcrumb"><a href="/{{ $lang.$news->slug }}">{{ $news->title }}</a></span>
    </div>

    {{-- Meta data --}}
    <div class="mb-4">
      <h3>{{ $news->title }}</h3>
      <span>
        @lang('global.published_date')
      </span>
      <span>
        {{ $news->created_at->format('M, d Y H:i')  }}
      </span>
      <span>
        | @lang('global.publisher') {{ $news->publisher }}
      </span>
    </div>

    {{-- Thumbnail --}}
    <div class="adira-thumbnail">
      @if(isset($news->image))
        <img src="/{{ $news->image }}" class="w-100" alt="">
      @endif
    </div>

    {{-- Content --}}
    <div class="mt-3 mb-4">
      {!! $news->content !!}
    </div>

    {{-- Button Share --}}
    <div style="display: flex;align-items: center;">
      <span class="adira-share-word">@lang('global.share_this_news') : </span>
      <div style="display:flex;">
        <div class="mr-2">
          <a href="https://www.facebook.com/sharer/sharer.php?u={{ $setting->link_web }}/{{ $lang . $news->slug }}" target="_blank">
            <img src="/frontend/assets/images/common/share_facebook.png" style="width:25px;height:25px;"  alt="" />
          </a>
        </div>
        <div class="mr-2">
          <a href="http://www.twitter.com/share?url={{ $setting->link_web }}/{{ $lang . $news->slug }}" target="_blank">
            <img src="/frontend/assets/images/common/share_twitter.png"  style="width:25px;height:25px;" alt="" />
          </a>
        </div>
      </div>
    </div>

    <hr style="border:1px solid #cecece;" />

    <h1 class="text-center">@lang('global.related_news')</h1>

    {{-- Whats on --}}
    <div class="row mt-4">
      <div id="whats-on-slider" class="owl-carousel owl-theme">
      @foreach($berita as $ber)
        <div class="whats-on-item" style="width: 200px;">
          <div>
            @if (file_exists(public_path($ber->thumbnail)))
              <img src="{{ asset('/'.$ber->thumbnail) }}" class="w-100" alt="{{ $ber->alt_teks }}" height="200px">
            @else
              <img src="{{ asset('frontend/assets/images/common/no-images.jpg') }}" class="w-100" height="200px">
            @endif
          </div>
          <div class="mt-1 mb-1">
            <span style="font-size: 17px;color:#000;font-weight:bold;" data-toggle="tooltip" data-placement="right" title="{{ $ber->title }}">
              {{ Str::limit($ber->title, 35, '...') }}
            </span>
          </div>
          <div class="mt-1 mb-1">
            <span data-toggle="tooltip" data-placement="right" title="{{ $ber->excerpt }}" style="font-size:13px;">
                {{ Str::limit($ber->excerpt, 40, '...') }}
            </span>
          </div>
          <div class="mt-1">
            <a href="{{ $setting->link_web }}/{{ $lang . $ber->slug }}" class="btn btn-secondary" target="_BLANK">@lang('global.read_more')</a>
          </div>
        </div>
      @endforeach
      </div>
    </div>
    
  </div>
</section>
@endsection

@push('meta_tag')
  <meta name="description"  content="{{ $ber->meta_description  ?? $ber->meta_description }}">
  <meta name="keywords"     content="{{ $ber->keyword           ?? $ber->keyword }}">
@endpush

@push('style')

@endpush

@push('script')
<script>
  $(function () {
    var lang = '{{ $lang_switch }}'
    if(lang == 'id'){
      var url_id = "{{ $news_switch->slug ?? '' }}";
      var url_en = "en/{{ $news_child->slug ?? ''}}";
    }else{
      var url_id = "{{ $news_child->slug ?? ''}}";
      var url_en = "en/{{ $news_switch->slug ?? '' }}";
    }
    
    $('#id').attr('href', '/'+url_id);
    $('#en').attr('href', '/'+url_en);

    $('[data-toggle="tooltip"]').tooltip()
  })
</script>
@endpush