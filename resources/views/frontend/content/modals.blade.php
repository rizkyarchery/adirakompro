{{-- Modal Popup Image --}}
<div id="imagepopup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
            <img src="#" class="img-responsive imagepopup_source">
            </div>
        </div>
    </div>
</div>

{{-- Model Structure Organisation --}}

<div id="structureOrganisation" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
        <div class="">
          <div class="row">
            <div class="col-md-4">
              <img src="#" class="img-responsive modal_structure_organisation_img w-100">
            </div>
            <div class="col-md-8 modal_structure_organisation_data">
              <div class="mb-2">
                <span class="modal_structure_organisation_name"></span>
              </div>
              <div>
                <p class="modal_structure_organisation_description"></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>