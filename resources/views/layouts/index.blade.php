<?php
$PREFIX = config('app.app_prefix');
use Illuminate\Support\Facades\DB; 


  $sw = DB::table('settingweb')->where('kode', '001')->first();
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
 
    <title>{{ isset($sw->title) ? $sw->title : null }} | Dashboard </title>
  

  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{ asset('assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{ asset('assets/plugins/jqvmap/jqvmap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('assets/dist/css/adminlte.min.css')}}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ asset('assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('assets/plugins/daterangepicker/daterangepicker.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{ asset('assets/plugins/summernote/summernote-bs4.css')}}">
  <!--swall-->
  <link rel="stylesheet" href="{{ asset('assets/plugins/sweetalert2/sweetalert2.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <script src="{{ asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Summernote -->
<script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js')}}"></script>
<!--swall-->
 <script src="{{ asset('assets/plugins/sweetalert2/sweetalert2.js')}}"></script>
 <style>
 
 </style>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
     
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-dark navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
       <a class="nav-link" href="{{ route('logout') }}"
          onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
          {{ __('Logout') }}
      </a>

      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
      </form>
      </li>
    </ul>
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{ url('/'.$PREFIX.'/user/profile/'.Auth::user()->id.'') }}" class="nav-link" data-toggle="tooltip" title="Profile {{ Auth::user()->name }}"><i class="fa fa-user-circle"> Profile</i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="" class="nav-link"></a>
      </li>
    </ul> 
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
   <a href="{{ url('/') }}" class="brand-link">
      <img src="{{ asset(''.isset($sw->logo_web) ?$sw->logo_web : null.'')}}" alt="AdminLTE Logo" class="brand-image"><br>
      {{-- <span class="brand-text font-weight-light">{{ $sw->nm_web }}</span> --}}
    </a>
   <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          {{-- <img src="{{ asset('assets/dist/img/AdminLTELogo.png')}}" class="img-circle elevation-2" alt="User Image"> --}}
        </div>
        <div class="info">
          <a href="{{ url('/'.$PREFIX.'') }}" class="d-block">Login as {{ Auth::user()->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        
        <li class="nav-item has-treeview">
          <a href="{{ url('/'.$PREFIX.'') }}" class="nav-link {{ Request::is(''.$PREFIX.'') ? 'active' : '' }}"><i class="nav-icon fas fa-tachometer-alt"></i><p>Dashboard</p> </a>
        </li>
        @if(auth()->user()->can('View Role'))
        <li class="nav-item">
          <a href="/{{ $PREFIX }}/role" class="nav-link {{ Request::is([$PREFIX . '/role', $PREFIX . '/role/*']) ? 'active' : '' }}"> <i class="nav-icon fas fa-users-cog"></i><p>Roles</p></a>
        </li>
        @endif
        @if(auth()->user()->can('View User'))
        <li class="nav-item">
          <a href="/{{ $PREFIX }}/user" class="nav-link {{ Request::is([$PREFIX . '/user', $PREFIX . '/user/*']) ? 'active' : '' }}"><i class="nav-icon fas fa-user-circle"></i><p>User</p></a>
        </li>
        @endif
        @if(auth()->user()->can('View Annual Report'))
        <li class="nav-item">
          <a href="/{{ $PREFIX }}/annual-report" class="nav-link {{ Request::is([$PREFIX . '/annual-report', $PREFIX . '/annual-report/*']) ? 'active' : '' }}"> <i class="nav-icon fas fa-sitemap"></i><p>Annual Report</p></a>
        </li>
        @endif
        @if(auth()->user()->can('View Article'))
        <li class="nav-item">
          <a href="/{{ $PREFIX }}/article" class="nav-link {{ Request::is([$PREFIX . '/article',$PREFIX . '/art/*']) ? 'active' : '' }}"> <i class="nav-icon fas fa-newspaper"></i> <p>Article </p> </a>
        </li>
        @endif
        @if(auth()->user()->can('View Award'))
        <li class="nav-item">
          <a href="/{{ $PREFIX }}/award" class="nav-link {{ Request::is([$PREFIX . '/award', $PREFIX . '/award/*']) ? 'active' : '' }}"> <i class="nav-icon fas fa-trophy"></i><p>Award</p></a>
        </li>
        @endif
        {{-- <li class="nav-item">
          <a href="{{ url('/admin/kategori') }}" class="nav-link {{ Request::is('admin/kategori') ? 'active' : '' }}"> <i class="nav-icon fas fa-th-large"></i> <p>Category </p> </a>
        </li>
        <li class="nav-item">
          <a href="{{ url('/admin/menu') }}" class="nav-link {{ Request::is('admin/menu') ? 'active' : '' }}"> <i class="nav-icon fas fa-list-alt"></i> <p>Menu </p> </a>
        </li> --}}
        @if(auth()->user()->can('View Branch Office'))
        <li class="nav-item">
          <a href="/{{ $PREFIX }}/branchoffice" class="nav-link {{ Request::is($PREFIX . '/branchoffice', $PREFIX . '/branchoffice/*') ? 'active' : '' }}"> <i class="nav-icon fas fa-building"></i><p>Branch Office</p></a>
        </li>
        @endif
        @if(auth()->user()->can('View Banner'))
        <li class="nav-item">
          <a href="/{{ $PREFIX }}/banner" class="nav-link {{ Request::is([$PREFIX . '/banner', $PREFIX . '/br/*']) ? 'active' : '' }}"> <i class="nav-icon fas fa-file-image"></i><p>Banner</p></a>
        </li>
        @endif
        @if(auth()->user()->can('View Career'))
        <li class="nav-item">
          <a href="/{{ $PREFIX }}/career" class="nav-link {{ Request::is([$PREFIX . '/career', $PREFIX . '/career/*']) ? 'active' : '' }}"> <i class="nav-icon fas fa-laptop"></i><p>Career</p></a>
        </li>
        @endif
         @if(auth()->user()->can('View Contact Inbox'))
        <li class="nav-item">
          <a href="/{{ $PREFIX }}/contact-inbox" class="nav-link {{ Request::is([$PREFIX . '/contact-inbox', $PREFIX . '/contact-inbox/*']) ? 'active' : '' }}"><i class="nav-icon fas fa-envelope"></i><p>Contact Inbox</p></a>
        </li>
        @endif
        @if(auth()->user()->can('View File'))
        <li class="nav-item">
          <a href="/{{ $PREFIX }}/file" class="nav-link {{ Request::is($PREFIX . '/file') ? 'active' : '' }}"> <i class="nav-icon fas fa-file"></i><p>File</p></a>
        </li>
        @endif
        @if(auth()->user()->can('View Finance Report'))
        <li class="nav-item">
          <a href="/{{ $PREFIX }}/report" class="nav-link {{ Request::is([$PREFIX . '/report', $PREFIX . '/report/*']) ? 'active' : '' }}"> <i class="nav-icon fas fa-sitemap"></i><p>Finance Report</p></a>
        </li>
        @endif
        @if(auth()->user()->can('View Footer Brand'))
        <li class="nav-item">
          <a href="/{{ $PREFIX }}/footer" class="nav-link {{ Request::is([$PREFIX . '/footer', $PREFIX . '/fb/*']) ? 'active' : '' }}"> <i class="nav-icon fas fa-mouse-pointer"></i><p>Footer Brand</p></a>
        </li>
        @endif
        @if(auth()->user()->can('View Master Image'))
        <li class="nav-item">
          <a href="/{{ $PREFIX }}/file-image-master" class="nav-link {{ Request::is([$PREFIX . '/file-image-master', $PREFIX . '/file-image-master/*']) ? 'active' : '' }}"><i class="nav-icon fas fa-images"></i><p>Master Image</p></a>
        </li>
        @endif
        @if(auth()->user()->can('View Menu'))
        <li class="nav-item">
          <a href="/{{ $PREFIX }}/menu" class="nav-link {{ Request::is($PREFIX . '/menu') ? 'active' : '' }}"> <i class="nav-icon fas fa-bars"></i><p>Menu</p></a>
        </li>
        @endif
        @if(auth()->user()->can('View News'))
        <li class="nav-item">
          <a href="/{{ $PREFIX }}/news" class="nav-link {{ Request::is([$PREFIX . '/news', $PREFIX . '/news/*']) ? 'active' : '' }}"> <i class="nav-icon fas fa-newspaper"></i><p>News</p></a>
        </li>
        @endif
        @if(auth()->user()->can('View Product'))
        <li class="nav-item">
          <a href="/{{ $PREFIX }}/produk" class="nav-link {{ Request::is([$PREFIX . '/produk', $PREFIX . '/pr/*']) ? 'active' : '' }}"> <i class="nav-icon fas fa-shopping-cart"></i><p>Product</p></a>
        </li>
        @endif
        @if(auth()->user()->can('View Page'))
        <li class="nav-item">
          <a href="/{{ $PREFIX }}/page" class="nav-link {{ Request::is([$PREFIX . '/page', $PREFIX . '/page/*']) ? 'active' : '' }}"><i class="nav-icon fas fa-book"></i><p>Page</p></a>
        </li>
        @endif
        {{-- @if(auth()->user()->can('View Sitemap'))
        <li class="nav-item">
          <a href="/{{ $PREFIX }}/sitemap" class="nav-link {{ Request::is($PREFIX . '/sitemap') ? 'active' : '' }}"> <i class="nav-icon fas fa-binoculars"></i> <p>Sitemap </p> </a>
        </li>
        @endif --}}
        @if(auth()->user()->can('View Organizational Structure'))
        <li class="nav-item">
          <a href="/{{ $PREFIX }}/structure-organisation" class="nav-link {{ Request::is([$PREFIX . '/structure-organisation', $PREFIX . '/structure-organisation/*']) ? 'active' : '' }}"> <i class="nav-icon fas fa-binoculars"></i> <p>Organizational Structure </p> </a>
        </li>
        @endif 
        @if(auth()->user()->can('View Testimony'))
        <li class="nav-item">
          <a href="/{{ $PREFIX }}/testimoni" class="nav-link {{ Request::is([$PREFIX . '/testimoni', $PREFIX . '/ts/*']) ? 'active' : '' }}"> <i class="nav-icon fas fa-star"></i><p>Testimony</p></a>
        </li>
        @endif
        @if(auth()->user()->can('View Workshop'))
        <li class="nav-item">
          <a href="/{{ $PREFIX }}/workshop" class="nav-link {{ Request::is([$PREFIX . '/workshop', $PREFIX . '/workshop/*']) ? 'active' : '' }}"><i class="nav-icon fas fa-car"></i><p>Workshop</p></a>
        </li>
        @endif
        @if(auth()->user()->can('Manage Setting'))
        <li class="nav-item">
          <a href="/{{ $PREFIX }}/setting" class="nav-link {{ Request::is($PREFIX . '/setting') ? 'active' : '' }}"><i class="nav-icon fas fa-cog"></i><p>Setting</p></a>
        </li>
        @endif
        </ul>



          
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- /.content-header -->
@yield('content')
    <!-- Main content -->
    {{-- <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
       
      </div><!-- /.container-fluid -->
    </section> --}}
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2019 <a href="http://www.bandingin.com">Bandingin.com</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.0.0-rc.3
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{ asset('assets/plugins/chart.js/Chart.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{ asset('assets/plugins/sparklines/sparkline.js')}}"></script>
<!-- JQVMap -->
<script src="{{ asset('assets/plugins/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{ asset('assets/plugins/jqvmap/maps/jquery.vmap.usa.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('assets/plugins/jquery-knob/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{ asset('assets/plugins/moment/moment.min.js')}}"></script>
<script src="{{ asset('assets/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>

<!-- overlayScrollbars -->
<script src="{{ asset('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/dist/js/adminlte.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('assets/dist/js/demo.js')}}"></script>
<script>
$(document).ready(function(){
 $('body').tooltip({selector: '[data-toggle="tooltip"]'});
});
</script>
</body>
</html>
