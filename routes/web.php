<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Route::group(['prefix' => '/webadmin'], function(){
  Route::get('/webadmin', 'HomeController@index')->name('home');

  Route::get('/'.config('app.app_prefix').'/login', 'Auth\LoginController@showLoginForm')->name('login');
  Route::get('/'.config('app.app_prefix').'/logout', 'Auth\LoginController@logout')->name('logout');
  Route::post('/'.config('app.app_prefix').'/login', 'Auth\LoginController@login')->name('login.attempt');
  Route::post('/'.config('app.app_prefix').'/logout', 'Auth\LoginController@logout')->name('logout.attempt');
  Route::get('/'.config('app.app_prefix').'/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('custom.password.reset');
  Route::get('/'.config('app.app_prefix').'/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('custom.reset.password');
  Route::post('/'.config('app.app_prefix').'/password/reset', 'Auth\ResetPasswordController@reset');
  // });

Route::group(['prefix' => LaravelLocalization::setLocale(), 
              'middleware' => [   'localizationRedirect', 
                                  'localeViewPath' ] ], function(){ 
  
  Route::get('/',                 'Front\FrontController@index')->name('content');
  Route::post('/post/contact',         'Front\FrontController@contact');
  Route::post('/en/post/contact',         'Front\FrontController@contact');
  Route::get('/search',           'Front\FrontController@search');
  Route::get('/find-bengkel',     'Front\FrontController@findBengkel');
  Route::get('/find-office',      'Front\FrontController@findOffice');
  Route::get('/articles',         'Front\FrontController@listArticle')->name('articles');
  Route::get('/articles/tag/{slug}', 'Front\FrontController@articles_slug');
  Route::get('/news',             'Front\FrontController@listNews')->name('news');
  Route::get('/{slug}',           'Front\FrontController@content')->name('article');
});


Route::get('/',               'Front\FrontController@index')->name('content');
