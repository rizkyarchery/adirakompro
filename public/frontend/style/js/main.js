
$(document).ready(function(){

  if($('.collapse_1')[0] != undefined) {
    var lebar = $('.collapse_1')[0].scrollHeight
    $('.collapse_1').css('max-height',lebar + "px");
  
    $('.collapse_1')
      .siblings('.accordion.accordion-adira')
      .find('i')
      .eq(0)
      .removeClass('fa-chevron-circle-right')
      .addClass('fa-chevron-circle-down')
  }
  
});


/**
 * Accordion Start
 */
  var acc = document.getElementsByClassName("accordion");
  var i;

  for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
      this.classList.toggle("accordion-active");
      var panel = this.nextElementSibling;
      if (panel.style.maxHeight) {
        panel.style.maxHeight = null;
        $(this).find('i').removeClass('fa-chevron-circle-down').addClass('fa-chevron-circle-right')
      } else {
        $(this).find('i').removeClass('fa-chevron-circle-right').addClass('fa-chevron-circle-down')
        panel.style.maxHeight = panel.scrollHeight + "px";
      }
    });
  }
/**
 * Accordion End
 */
  

 /**
  * Image Popup Start
  */

 $('.imgmodal').click(function(){
  var img = $(this).data('img')

  $('.imagepopup_source').css('width','100%')
  $('.imagepopup_source').attr('src',img)
  $('#imagepopup').modal('show')
})

/**
 * Image Popup end
 */

 /**
  * Structure Organisation modal start
  */

 $('.structure-modal').on('click',function(e){
  e.preventDefault();

  var name          = $(this).data('structure-name');
  var img           = $(this).data('structure-image');
  var council       = $(this).data('structure-council');
  var description   = $(this).data('structure-description');

  $('.modal_structure_organisation_img').attr('src',img);
  $('.modal_structure_organisation_name').text(name);
  $('.modal_structure_organisation_description').text(description);

  $('#structureOrganisation').modal('show');
})

/**
  * Structure Organisation modal end
  */

/**
 * Footer Brand Hover Start
 */

$('.footer-brand-item').on('mouseenter', function () {
  $(this).children('.footer-brand-img-grey').css('display', "none");
  $(this).children('.footer-brand-img-color').css('display', "block");
  // $(this).children('.footer-brand-img-color').find('img').css('opacity', 0);
}).on('mouseleave', function () {
  $(this).children('.footer-brand-img-grey').css('display', "block");
  $(this).children('.footer-brand-img-color').css('display', "none");
  // $(this).children('.footer-brand-img-color').find('img').css('opacity', 1);
});

 /**
  * Footer Brand Hover end
  */

  /**
   * Mobile Navbar Start
   */

   
  var sideslider = $('[data-toggle=collapse-side]');
  var sel = sideslider.attr('data-target');
  var sel2 = sideslider.attr('data-target-2');
  sideslider.click(function(event) {
      $(sel).toggleClass('in');
      $(sel2).toggleClass('out');
  });

  $('.adira-nav-mobile-head-item-right').click(function(){
    $(sel2).removeClass('out');
  })

  $(document).on('click','.adira-nav-mobile-li-first',function(e){
    e.preventDefault();

    // give bold to the parent
    $(this).children().eq(1).toggleClass('adira-nav-mobile-ul-first-a-active')

    // change icon
    $(this).children().eq(0).toggleClass('fa-chevron-up')

    // slide toggle
    $(this).children().eq(2).slideToggle()
  })

  $(document).on('click','.adira-nav-mobile-div-second-nested',function(e){
    e.preventDefault();
    e.stopPropagation();

    // change icon
    $(this).prev().toggleClass('fa-chevron-up')
    
    // give bold to the parent
    $(this).children().toggleClass('adira-nav-mobile-ul-first-a-active')

    // console.log($(this).siblings())
    $(this).siblings().eq(1).slideToggle()

  })

  $(document).on('click','.adira-nav-mobile-li-second',function(e) {
    e.stopPropagation();
  })

  $(document).on('click','.adira-nav-mobile-div-third-nested',function(e) {
    e.stopPropagation();

    // change icon
    $(this).prev().toggleClass('fa-chevron-up')
    
    // give bold to the parent
    $(this).children().toggleClass('adira-nav-mobile-ul-first-a-active')

    $(this).siblings().eq(1).slideToggle()
  })

   /**
    * Mobile Navbar End
    */

  /**
   * Tooltip Start
   */

    $('[data-toggle="tooltip"]').tooltip()

  /**
  * Tooltip End
  */


  /**
   * Homepage Service Hover Start
   */

  $('.adira-gallery-item-service-parent').mouseenter(function(){
    $(this).children().css('left',0 + '%')
  }).mouseleave(function() {
    $(this).children().css('left', '-' + 100 + '%')
  });

  /**
  * Homepage Service Hover End
  */