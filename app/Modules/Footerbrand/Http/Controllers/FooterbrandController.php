<?php

namespace App\Modules\Footerbrand\Http\Controllers;

use Str;
use File;
use Session;
use DataTables;
use Auth;
use Validator;
use Illuminate\Http\Request;
use App\Modules\Setting\Models\Setting;
use App\Modules\Footerbrand\Models\Footerbrand;

use App\Http\Controllers\Controller;

class FooterbrandController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function json(Request $request){

     if($request->columns[3]['search']['value'] != null) {
         $footerbrand = Footerbrand::where('lang',$request->columns[3]['search']['value'])->orderBy('id', 'DESC')->get();
     }else{
        $footerbrand = Footerbrand::orderBy('id', 'DESC')->get();
     }
       
    return Datatables::of($footerbrand)
            ->addColumn('action', function ($footerbrand) {
                   $html='';
                    if(auth()->user()->can('Edit Footer Brand')){
                        $html='<center><a href="/'.config('app.app_prefix').'/footer/edit/'.$footerbrand->id.'" data-toggle="tooltip" title="update" class="btn btn-xs btn-warning" style="margin-bottom:4px;margin-right:2px;"><i class="fa fa-edit"></i></a>';
                       }  
                     if(auth()->user()->can('Delete Footer Brand')){
                        $html=$html.' <a data-id="'.$footerbrand->id.'" class="btn btn-xs btn-danger" style="margin-bottom:4px;" data-toggle="tooltip" title="delete" id="hapusfooter"><i class="fa fa-trash"></i></a></center>';
                    }
                        return $html;
                })                  
            ->addIndexColumn()
            ->make(true);
  }

  public function index(){
      $footerbrand = Footerbrand::all();
      $settingweb = Setting::find('001');
    return view('footerbrand::footer_brand',['footerbrand' => $footerbrand,'settingweb' => $settingweb]);
  }

  public function tambah(){
    $footerbrand = Footerbrand::all();
    $settingweb = Setting::find('001');

    return view('footerbrand::tambah_footerbrand',['footerbrand' => $footerbrand,'settingweb' => $settingweb]);
  }

  public function insert(Request $request) {
               
    $validator = Validator::make($request->all(),[
          'nama' =>'required',
          'ket' =>'required',
          'foto' =>'required|file|image|mimes:jpeg,png,jpg|max:2048',
          'image' =>'required|file|image|mimes:jpeg,png,jpg|max:2048',
          'status' => 'required',
          'link' => 'required',
          'alt_teks' => 'required',
          'lang' => 'required',
    ]);
    
    if(!$validator->fails()){
      //foto
      $file         = $request->file('foto');
      $path         = 'Upload/footer_brand/';
      $nama_file    = Str::random(40) .'.'.$file->getClientOriginalExtension();
      
      $tujuan_upload =  $path;
      $request->file('foto')->move($tujuan_upload,$nama_file);

      //image
      $file1        = $request->file('image');
      $path1        = 'Upload/footer_brand/';

      $nama_file1 = Str::random(40) .'.'.$file1->getClientOriginalExtension();
      $tujuan_upload1 =  $path1;
      $request->file('image')->move($tujuan_upload,$nama_file1);
              
      Footerbrand::create([
          'nama' => $request->nama,
          'ket' => $request->ket,
          'foto' => $nama_file,
          'image' => $nama_file1,
          'lang'  => $request->lang,
          'status' => $request->status,
          'link' => $request->link,
          'alt_teks' => $request->alt_teks,
          'created_by' => Auth::user()->name,
          'updated_by' => Auth::user()->name
      ]);
      
      Session::flash('sukses','Data Has Been Created');
      return redirect(config('app.app_prefix').'/footer');
    }

    return redirect(config('app.app_prefix') .'/fb/tambah')
                        ->withErrors($validator)
                        ->withInput();
  }

  public function edit($id){
    $footerbrand = Footerbrand::find($id);
    $settingweb = Setting::find('001');
      return view('footerbrand::edit_footerbrand', ['footerbrand' => $footerbrand,'settingweb' => $settingweb]);
  }

  public function update($id, Request $request){

    $validator = Validator::make($request->all(),[
        'nama' =>'required',
        'ket' =>'required',
        'status' => 'required',
        'link' => 'required',
        'alt_teks' => 'required',
        'lang'    => 'required',
    ]);

    if(!$validator->fails()){
      $footerbrand = Footerbrand::find($id);

      //foto
      if($request->file('foto') == ""){
            $footerbrand->foto = $footerbrand->foto;
      }else{
          $file       = $request->file('foto');
          $path       = 'Upload/footer_brand/';
          $fileName   =  Str::random(40) .'.'.$file->getClientOriginalExtension();
          $request->file('foto')->move($path, $fileName);
          $footerbrand->foto = $fileName;
      }
      //image
      if($request->file('image') == ""){
          $footerbrand->image = $footerbrand->image;
      }else{
          $file1       = $request->file('image');
          $path1       = 'Upload/footer_brand/';
          $fileName1   =  Str::random(40) .'.'.$file1->getClientOriginalExtension();
          $request->file('image')->move($path1, $fileName1);
          $footerbrand->image = $fileName1;
      }
      
      $footerbrand->nama          = $request->nama;
      $footerbrand->ket           = $request->ket;
      $footerbrand->link          = $request->link;
      $footerbrand->status        = $request->status;
      $footerbrand->alt_teks      = $request->alt_teks;
      $footerbrand->lang          = $request->lang;
      $footerbrand->updated_by      = Auth::user()->name;
      
      $footerbrand->save();

      Session::flash('sukses21','Data Has Been Updated');

      return redirect(config('app.app_prefix').'/footer');
    }
    return redirect(config('app.app_prefix') .'/footer/'.$id.'/edit')
                        ->withErrors($validator)
                        ->withInput();
    
  }

  public function delete($id) {
    $footerbrand = Footerbrand::find($id);
    File::delete($footerbrand->foto);
    File::delete($footerbrand->image);
    $footerbrand->delete();

    $callback = [
        "message" => "Data has been Deleted",
        "code"   => 200
    ];

    return json_encode($callback, TRUE);
  }
}
