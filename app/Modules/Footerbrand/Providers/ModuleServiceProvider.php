<?php

namespace App\Modules\Footerbrand\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('footerbrand', 'Resources/Lang', 'app'), 'footerbrand');
        $this->loadViewsFrom(module_path('footerbrand', 'Resources/Views', 'app'), 'footerbrand');
        $this->loadMigrationsFrom(module_path('footerbrand', 'Database/Migrations', 'app'), 'footerbrand');
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('footerbrand', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('footerbrand', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
