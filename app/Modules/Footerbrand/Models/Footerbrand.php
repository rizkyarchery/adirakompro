<?php

namespace App\Modules\Footerbrand\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Footerbrand extends Model
{
     protected $table = 'footer_brand';
    protected $fillable = ['nama','foto','ket','status','link','alt_teks','image','lang'];

    use SoftDeletes;
    protected $dates =['deleted_at'];
}
