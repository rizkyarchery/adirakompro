<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::group(['middleware'=> 'auth','prefix' => config('app.app_prefix')], function () {
    Route::get('/footer/json', 'FooterbrandController@json');
    Route::resource('/footer', 'FooterbrandController');
    Route::get('/fb/tambah','FooterbrandController@tambah');
    Route::post('/footer/insert','FooterbrandController@insert');
    Route::get('/footer/edit/{id}','FooterbrandController@edit');
    Route::put('/footer/update/{id}', 'FooterbrandController@update');
    ROUTE::post('/footer/hapus/{id}', 'FooterbrandController@delete');
});