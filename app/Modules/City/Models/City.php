<?php

namespace App\Modules\City\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    //
    protected $table = 'city';

    public function workshop(){
        return $this->hasMany('App\Modules\Workshop\Models\Workshop', 'city');
    }

    public function office(){
        return $this->hasMany('App\Modules\Branchoffice\Models\BranchOffice', 'city_id');
    }

    public function provinces(){
        return $this->belongsTo('App\Modules\Province\Models\Province', 'province_id');
    }
}
