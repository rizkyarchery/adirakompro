<?php

namespace App\Modules\Anualreport\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('anualreport', 'Resources/Lang', 'app'), 'anualreport');
        $this->loadViewsFrom(module_path('anualreport', 'Resources/Views', 'app'), 'anualreport');
        $this->loadMigrationsFrom(module_path('anualreport', 'Database/Migrations', 'app'), 'anualreport');
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('anualreport', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('anualreport', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
