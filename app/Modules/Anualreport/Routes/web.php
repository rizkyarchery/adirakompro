<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => config('app.app_prefix')], function () {
    Route::get('/annual-report/list', 'AnualReportController@listAll');
    Route::post('/annual-report/delete/{id}', 'AnualReportController@delete');
    Route::resource('/annual-report', 'AnualReportController');
});
