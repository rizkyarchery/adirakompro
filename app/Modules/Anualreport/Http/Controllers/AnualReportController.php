<?php

namespace App\Modules\Anualreport\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Session;
use Validator;
use Auth;
use DataTables;
use App\Http\Controllers\Controller;
use App\Modules\Anualreport\Models\AnnualReport;

class AnualReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function listAll(Request $request){

        if($request->columns[4]['search']['value'] != null) {
           $annuals = AnnualReport::where("lang",$request->columns[4]['search']['value'])->orderBy('id', 'desc')->get();
        }else{
           $annuals = AnnualReport::orderBy('id', 'desc')->get();
        }
        
        return DataTables::of($annuals)
            ->addColumn('action', function($annuals){
                 $html='';
                 if(auth()->user()->can('Edit Annual Report')){
                $html = '<a href="/'.config('app.app_prefix').'/annual-report/'.$annuals->id.'/edit" class="btn btn-primary" >Edit</a>';
                 }
                if(auth()->user()->can('Delete Annual Report')){ 
                $html= $html.' <button id="btn-annual" data-id="'. $annuals->id .'" class="btn btn-danger">Delete</button>';
                 }
                return $html;
            })
            ->addIndexColumn()
            ->make(true);

    }

    public function index()
    {
        //
        return view('anualreport::list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('anualreport::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image'         =>'required',
            'files'         =>'required|max:15000',
            'status'        =>'required',
            'lang'          =>'required'
        ]);
        
        if(!$validator->fails()) {
          //IMAGE UPLOAD
          $file_image = $request->file('image');
          $path_image = 'Upload/Report/';
          $file_name_image = $path_image.$file_image->getClientOriginalName();
              
          $direction_image = 'Upload/Report/';
          $request->file('image')->move($direction_image, $file_name_image);
          //END IMAGE UPLOAD

          //FILE PDF UPLOAD
          $file_pdf = $request->file('files');
          $path_pdf = 'Upload/Report/';
          $file_pdf_name = $path_pdf.$file_pdf->getClientOriginalName();

          $direction_pdf = 'Upload/Report/';
          $request->file('files')->move($direction_pdf, $file_pdf_name);
          //END PDF UPLOAD

          $report = new AnnualReport();
          $report->name = $request->name;
          $report->year = $request->year;
          $report->release_date = $request->release_date;
        //   $report->description = $request->description;
          $report->image = $file_name_image;
          $report->file = $file_pdf_name;
          $report->created_by = Auth::user()->name;
          $report->updated_by = Auth::user()->name;
          $report->lang = $request->lang;
          $report->status = $request->status;
          $report->save();

          Session::flash('sukses','Annual Report has been inserted!');
          return redirect(config('app.app_prefix') . '/annual-report');
        }
       
        return redirect(config('app.app_prefix') .'/annual-report/create')
                        ->withErrors($validator)
                        ->withInput();
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $annual_report = AnnualReport::where('id',$id)->first();
        $data['annual'] = $annual_report;
        return view('anualreport::edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $report = AnnualReport::find($id);
        $report->name = $request->name;
        $report->year = $request->year;
        $report->lang = $request->lang;
        $report->status = $request->status;
        $report->release_date = $request->release_date;
        $report->updated_by = Auth::user()->name;
        // $report->description = $request->description;
        if($request->file('image') != null){
            $file_image = $request->file('image');
            $path_image = 'Upload/Report/';
            $file_name_image = $path_image.$file_image->getClientOriginalName();
                
            $direction_image = 'Upload/Report/';
            $request->file('image')->move($direction_image, $file_name_image);
            $request->file('image')->image = $file_name_image;
            $report->image = $file_name_image;
        }

        if($request->file('files') != null){
            $file_pdf = $request->file('files');
            $path_pdf = 'Upload/Report/';
            $file_pdf_name = $path_pdf.$file_pdf->getClientOriginalName();

            $direction_pdf = 'Upload/Report/';
            $request->file('files')->move($direction_pdf, $file_pdf_name);
            $report->file = $file_pdf_name;
        }

        $report->save();
        Session::flash('sukses','Annual Report has been updated!');
        return redirect(config('app.app_prefix') . '/annual-report');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
        $annual = AnnualReport::find($id);
        $annual->delete();

        $data = [
            "status" => 200,
            "message" => "Successfully Delete Annual Report"
        ];

        return json_encode($data);
    }
}
