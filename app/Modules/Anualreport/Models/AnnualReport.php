<?php

namespace App\Modules\Anualreport\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class AnnualReport extends Model
{
    //
    use SoftDeletes;
    protected $table = 'annual_report';

    protected $dates =['deleted_at'];
}
