@extends('layouts.index')
@section('content')

<?php

$prefix = config('app.app_prefix');

?>

<link rel="stylesheet" href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css')}}">
 <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Annual Reports</h1>
            </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="home">Home</a></li>
            <li class="breadcrumb-item active">Annual Report</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  
  <section class="content">
    <div class="container">
      @if ($message = Session::get('sukses'))
				<div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
					<strong>{{ $message }}</strong>
				</div>
      @endif
        <div class="row justify-content-center">
      <div class="col-md-12">
          <div class="card">
            <div class="card-header bg-info">
              Annual Reports
            </div>
            <div class="card-body">
              
               @if(auth()->user()->can('Create Annual Report'))
            <a class="btn btn-success btn-sm" href="annual-report/create"  style="color:white;" data-toggle="tooltip" title="Create Annual Report"><i class="fa fa-plus"> Create Annual Report</i></a>
            @endif
             <div class="col-md-2">
            <br>
            <br>
               <label>Language</label>
              <div class="input-group mb-3">
                  <select name="language" id="lang" class="custom-select">
                    <option value="" selected>Select all</option>
                    <option value="ID">ID</option>
                    <option value="EN">EN</option>
                  </select> 
                  <div class="input-group-append">
                  </div>
                </div>
              </div>
            <hr>
                <div class="table-responsive">
                    <table  class="table-hover table-striped table-bordered table-list" id="annualDataTables">
                      <thead>
                        <tr style="vertical-align:middle;text-align:center;font-weigth:bold">
                            <th>No</th>
                            <th>Name</th>
                            <th>Year</th>
                            <th>Release Date</th>
                            <th>Language</th>
                            <th>Action</th>
                          </tr> 
                      </thead>
                    </table>
                </div>
            </div>
          </div>
        </div>
    </div>
    </div>
      
</section>
 <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
 <script>

    $(function() {
            
      $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
      });

      var annual = $('#annualDataTables').DataTable({
          processing: true,
          serverSide: true,
          ajax: {
            url : '/{{ $prefix }}/annual-report/list',
            // data : { data : language}
          },
          columns: [
              { data: 'DT_RowIndex', name:'DT_RowIndex'},
              { data: 'name', name: 'name' },
              { data: 'year', name: 'year' },
              { data: 'release_date', name: 'release_date' },
              { data: 'lang', name: 'lang' },
              { 
                data: 'action', 
                name: 'action', 
                orderable: false, 
                searchable: false
              }
            ]
      });

    $('#lang').on('change',function(){
    annual.columns(4).search( this.value ).draw();
  })
    
    $(document).on('click', '#btn-annual', function () {
      var id = $(this).data("id");

      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
            $.ajax({
            type: "POST",
            url: "/{{ $prefix }}/annual-report/delete/" + id,
            success: function(data){
              var json  = JSON.parse(data)
              switch (json.status) {
                case 200:
                  Swal.fire({
                              type: 'success',
                              title: 'Success',
                              text: json.message,
                          })
                  break;
                default:
                  break;
              }

              // refresh datatablesnya kalau sudah menghapus rolenya
              annual.ajax.reload()
              }
            });
        }
      })
    })
        
  });

  </script>
<script>
  // Add the following code if you want the name of the file appear on select
  $(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
  });
</script>
@endsection