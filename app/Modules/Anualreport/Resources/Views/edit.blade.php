@extends('layouts.index')
@section('content')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" />
<?php 

$PREFIX = config('app.app_prefix');

?>
{{-- <link rel="stylesheet" href="/assets/plugins/icheck-bootstrap/icheck-bootstrap.css"> --}}
<br>
<div class="container">
  <div class="card-header bg-info">Edit Annual Report</div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <form action="{{route('annual-report.update', $annual->id)}}" method="post" enctype="multipart/form-data">
		        {{ csrf_field() }}
                {{ method_field('PUT') }}
              <div class="form-group">
                <label>Language <sup class="text-danger">*</sup></label><br>
                <select name="lang" class="form-control">
                  @if($annual->lang == "ID")
                  <option selected>ID</option>
                  <option>EN</option>
                  @else
                  <option>ID</option>
                  <option selected>EN</option>
                  @endif
                </select>
                @if($errors->has('lang'))
                    <div class="text-danger">
                    {{ $errors->first('lang')}}
                  </div>
                @endif
              </div>
              <div class="form-group">
                <label>Name</label>
                <input type="text" name="name" value="{{ $annual->name }}" class="form-control">
                @if($errors->has('name'))
                    <div class="text-danger">
                    {{ $errors->first('name')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>Year</label>
                <select name="year" class="form-control">
                    @for($i = 2000;$i <= 2050;$i++)
                        <option value="{{ $i }}" @if($i == $annual->year) selected @endif>{{ $i }}</option>
                    @endfor
                </select>
                @if($errors->has('year'))
                    <div class="text-danger">
                    {{ $errors->first('year')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>Release Date</label>
                <input type="text" name="release_date" id="realase-date" value="{{ $annual->release_date }}" class="form-control">
                @if($errors->has('release_date'))
                    <div class="text-danger">
                    {{ $errors->first('release_date')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>Image <sup class="text-danger">*</sup></label> &nbsp; <small>Recommended Size <span class="text-danger"> Width 224 pixel x  Height 173 pixel</span></small>
                <br />
                <img src="/{{ $annual->image }}" width="50%" />
                <br />
                <br />
                <input type="file" name="image">
                @if($errors->has('image'))
                    <div class="text-danger">
                    {{ $errors->first('image')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>PDF File <sup class="text-danger">*</sup></label> &nbsp; <small>Limit Size Upload <span class="text-danger"> 15 Mega Byte</span></small>
                <br />
                <ul class="mailbox-attachments clearfix">
                    <a target="_blank" href="/{{ $annual->file }}">
                        <li>
                            <span class="mailbox-attachment-icon"><i class="fa fa-file-pdf"></i></span>
                        </li>
                    </a>
                 </ul>
                <input type="file" name="files">
                @if($errors->has('files'))
                    <div class="text-danger">
                    {{ $errors->first('files')}}
                    </div>
                @endif
              </div>
                 <div class="form-group">
                <label>Status <sup class="text-danger">*</sup></label>
                <select class="form-control" name="status">
                <option value="1" {{ $annual->status == 1 ? 'selected="selected"' : '' }}>Active</option>
                <option value="0" {{ $annual->status == 0 ? 'selected="selected"' : '' }}>Not Active</option>
                </select>
            </div>
          </div>
          <div class="card-footer">
          <center>
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="/{{ $PREFIX }}/annual-report" class="btn btn-secondary">Back</a>
          </center>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>
<script src="/assets/plugins/icheck-bootstrap/icheck.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script>
$('#realase-date').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
})
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>
@endsection