@extends('layouts.index')
@section('content')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" />
<?php 

$PREFIX = config('app.app_prefix');

?>
{{-- <link rel="stylesheet" href="/assets/plugins/icheck-bootstrap/icheck-bootstrap.css"> --}}
<br>
<div class="container">
  <div class="card-header bg-info">Create Annual Report</div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <form action="{{route('annual-report.store')}}" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="form-group">
                <label>Language <sup class="text-danger">*</sup></label><br>
                <select name="lang" class="form-control">
                  <option>Choose Language</option>
                  <option value="ID">ID</option>
                  <option value="EN">EN</option>
                </select>
                @if($errors->has('lang'))
                    <div class="text-danger">
                    {{ $errors->first('lang')}}
                  </div>
                @endif
              </div>
              <div class="form-group">
                <label>Name <sup class="text-danger">*</sup></label>
                <input type="text" name="name" class="form-control" required>
                @if($errors->has('name'))
                    <div class="text-danger">
                    {{ $errors->first('name')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>Year <sup class="text-danger">*</sup></label>
                <select name="year" class="form-control" required>
                    <option>Choose Year</option>
                    @for($i = 2000;$i <= 2050;$i++)
                    <option value="{{ $i }}">{{ $i }}</option>
                    @endfor
                </select>
                @if($errors->has('year'))
                    <div class="text-danger">
                    {{ $errors->first('year')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>Release Date <sup class="text-danger">*</sup></label>
                <input  type="text" 
                        name="release_date" 
                        id="realase-date" 
                        class="form-control" 
                        autocomplete="off"
                        required>
                @if($errors->has('release_date'))
                    <div class="text-danger">
                    {{ $errors->first('release_date')}}
                    </div>
                @endif
              </div>
              {{-- <div class="form-group">
                <label>Description</label>
                <textarea name="description" rows="3" class="form-control"></textarea>
                @if($errors->has('description'))
                    <div class="text-danger">
                    {{ $errors->first('description')}}
                    </div>
                @endif
              </div> --}}
              <div class="form-group">
                <label>Images <sup class="text-danger">*</sup></label> &nbsp; <small>Recommended Size <span class="text-danger"> Width 224 pixel x  Height 173 pixel</span></small>
                <br />
                <input type="file" name="image" required>
                @if($errors->has('image'))
                    <div class="text-danger">
                    {{ $errors->first('image')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>PDF File <sup class="text-danger">*</sup></label> &nbsp; <small>Limit Size Upload <span class="text-danger"> 15 Mega Byte</span></small>
                <br />
                <input type="file" name="files" required>
                @if($errors->has('files'))
                  @foreach ($errors->get('files') as $message)
                    <div class="text-danger">
                      {{ $message }}
                    </div>
                  @endforeach
                @endif
              </div>
               <div class="form-group">
                 <label>Status <sup class="text-danger">*</sup></label><br>
                  <select name="status" id="status" class="form-control @error('status') is-invalid @enderror" >
                    <option value="0" selected disabled>- Choose Status -</option>
                    <option value="1">Active</option>
                    <option value="0">Not Active</option>
                </select>
                @if($errors->has('status'))
                  @foreach ($errors->get('status') as $message)
                    <div class="text-danger">
                      {{ $message }}
                    </div>
                  @endforeach
                @endif
              </div>
          </div>
          <div class="card-footer">
          <center>
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="/{{ $PREFIX }}/annual-report" class="btn btn-secondary">Back</a>
          </center>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>
<script src="/assets/plugins/icheck-bootstrap/icheck.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script>
$('#realase-date').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
})
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>
@endsection