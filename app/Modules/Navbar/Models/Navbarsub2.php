<?php

namespace App\Modules\Navbar\Models;

use Illuminate\Database\Eloquent\Model;

class Navbarsub2 extends Model
{
    protected $table = 'navbar_submenu2';
    protected $fillable = ['judul_sub2','link_sub2','id_sub'];
}
