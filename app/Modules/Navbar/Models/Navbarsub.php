<?php

namespace App\Modules\Navbar\Models;

use Illuminate\Database\Eloquent\Model;

class Navbarsub extends Model
{
    protected $table = 'navbar_submenu';
    protected $fillable = ['judul_sub','link_sub','id_navbar'];
}
