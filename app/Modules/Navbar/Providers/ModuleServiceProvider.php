<?php

namespace App\Modules\Navbar\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('navbar', 'Resources/Lang', 'app'), 'navbar');
        $this->loadViewsFrom(module_path('navbar', 'Resources/Views', 'app'), 'navbar');
        $this->loadMigrationsFrom(module_path('navbar', 'Database/Migrations', 'app'), 'navbar');
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('navbar', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('navbar', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
