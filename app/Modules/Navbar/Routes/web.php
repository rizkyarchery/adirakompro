<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/



Route::group(['middleware'=> 'auth','prefix' => config('app.app_prefix')], function () {
      Route::get('/navbar/json','NavbarController@json');
      Route::resource('/navbar','NavbarController'); 
      Route::post('/navbar/insert','NavbarController@insert');
      Route::put('/navbar/edit/{id}', 'NavbarController@edit');
      Route::put('/navbar/edit2/{id}', 'NavbarController@edit2');
      ROUTE::DELETE('/navbar/destroy/{id}', 'NavbarController@destroy');
      Route::get('/navbar/show/{id}','NavbarController@show');
      Route::get('/navbar/show2/{id}','NavbarController@show2'); 
      Route::post('/navbar/insert_detail','NavbarController@insert_detail');
      Route::post('/navbar/update_detail','NavbarController@update_detail');
      ROUTE::DELETE('/navbar/delete/{id}', 'NavbarController@delete');
      Route::post('/navbar/insert_detailsub2','NavbarController@insert_detailsub2');
      Route::post('/navbar/update_detailsub2','NavbarController@update_detailsub2');
      ROUTE::DELETE('/navbar/deletesub2/{id}', 'NavbarController@deletesub2');
});