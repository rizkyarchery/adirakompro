<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class Manajemenuser extends Model {

    use HasRoles;

     protected $table = 'users';
     protected $fillable = [
        'name', 'email', 'password','jabatan',
    ];

    public static function bootHasRoles()
    {
        static::deleting(function ($model) {
            if (! $model->isForceDeleting()) {
                // don't do anything if we're only soft deleting the model
                return;
            }

            $model->roles()->detach();
            $model->permissions()->detach();
        });
    }
}
