<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/



Route::group(['middleware'=> 'auth','prefix' => config('app.app_prefix')], function () {
    // Route::group user {
        Route::get('/user/json', 'ManajemenuserController@json');
        Route::resource('/user', 'ManajemenuserController');
        Route::post('/user/insert','ManajemenuserController@insert');
        Route::put('/edit/{id}', 'ManajemenuserController@edit');
        Route::post('/user/update','ManajemenuserController@update');
        Route::post('/user/update_profile','ManajemenuserController@update_profile');
        Route::post('/user/delete/{id}', 'ManajemenuserController@delete');
        Route::get('/user/profile/{id}', 'ManajemenuserController@profile');
    // }

});