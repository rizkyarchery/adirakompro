@extends('layouts.index')
@section('content')
<?php

$prefix = config('app.app_prefix');

?>
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
              <li class="breadcrumb-item active">Profile</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
     <!-- /.content-header -->
<section class="content">
    <div class="container">
         @if ($message = Session::get('sukses'))
            <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button> 
              <strong>{{ $message }}</strong>
            </div>
         @endif
      <div class="row">
      <div class="col-md-4">
           <div class="card">
            <div class="card-header bg-info"><i class="fa fa-user-circle"> User Profile </i></div>
               <div class="card-body">
                 <div class="table-responsive">
       <table class="table table-striped table-hover">
           <thead style="vertical-align:middle;text-align:justify;font-weigth:bold">
             <tr class="table-light"><th>{{ $user->name }}</th></tr>
             <tr class="table-light"><th>{{ $user->email }}</th></tr>
             <tr class="table-light"><th>{{ $user->roles[0]->name }}</th></tr>
           </thead>
       </table>
      </div>
               </div> <div class="card-footer"></div>
           </div>
      </div>
      <div class="col-md-8">
          <div class="card">
            <div class="card-header bg-info"><i class="fa fa-key"> Change Password </i></div>
               <div class="card-body">
                  <form action="/{{ $prefix }}/user/update_profile" method="post">
                   {{ csrf_field() }}
                   <input type="hidden" name="id" value="{{ $user->id }}">
                    <div class="form-group row">
                       <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                      <div class="col-md-6">
                          <input id="password3" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>
                        <div class="col-md-6">
                            <input id="password-confirm3" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        </div>
                      </div>
                        <br>
                        @foreach ($errors->all() as $error)
                          <div class="text-danger">
                          {{ $error }}<br />
                          </div>
                          @endforeach
                    <center><div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary save3" value="create"  data-toggle="tooltip" title="Change Password"><i class="fa fa-save"> Change Password</i>
                        </button>
                    </div></center>
                </form> 
               </div>
               <div class="card-footer"></div>
          </div>
       </div>
        
     </div>  
    </div>
</section>
 <script type="text/javascript">
        $(function () {
            $(".save3").click(function () {
                var password = $("#password3").val();
                var confirmPassword = $("#password-confirm3").val();
                if (password != confirmPassword) {
                   Swal.fire({
                                type: 'error',
                                title: 'Sorry...',
                                text: 'Passwords are not the same..',
                            })
                    return false;
               }
                return true;
            });
        });
    </script>
@endsection
