@extends('layouts.index')
@section('content')
<?php

$prefix = config('app.app_prefix');

?>
 <link rel="stylesheet" href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css')}}">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Users</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
              <li class="breadcrumb-item active">User</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    
    <section class="content">
      <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-info">Users</div>

                <div class="card-body">
                   @if(auth()->user()->can('Create User'))
                   <a class="btn btn-success btn-sm" href="javascript:void(0)" id="createNewUser" data-toggle="tooltip" title="Add User"><i class="fa fa-plus">&nbsp Add User</i></a>
                @endif
                   <br><br>
                   <div class="table-responsive">
	<table id="tabeluser" class="table-hover table-striped table-bordered table-list">
                  <thead>
                    <tr style="vertical-align:middle;text-align:center;font-weigth:bold">
                        <th>No</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Action</th>
                      </tr> 
                  </thead>
                  <tbody><tbody>
                </table>
   </div></div>
             </div>
        </div>
    </div>
</div>

 <!--modal -->
 <div class="modal fade" id="ajaxModel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary text-white">
                <h6 class="modal-title" id="modelHeading"></h6>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
        </button>
            </div>
            <div class="modal-body">
                <div id="error"><ul> @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>                   
                @endforeach</ul></div>
                <form id="productForm" name="productForm" class="form-horizontal">
                   <input type="hidden" name="id">
                   <div class="form-group row">
                      <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Full Name') }} <sup class="text-danger">*</sup></label>
                        <div class="col-md-6">
                            <input  type="text" class="form-control" name="name" id="namas" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            <p id="error_para" style="color:red;"></p>
                        </div>
                    </div>
                    <div class="form-group row">
                      <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }} <sup class="text-danger">*</sup></label>
                      <div class="col-md-6">
                        <input  type="email" 
                                id="emails" 
                                class="form-control @error('email') is-invalid @enderror" 
                                name="email" 
                                value="{{ old('email') }}" 
                                required 
                                autocomplete="email">
                        <small><sup class="text-danger">*</sup>Password will sending to your email</small>
                        <p id="error_paraemail" style="color:red;"></p>
                      </div>
                    </div>
                  <div class="form-group row">
                          <label for="role" class="col-md-4 col-form-label text-md-right">Role <sup class="text-danger">*</sup></label>
                       <div class="col-md-6">
                           <select name="role" id="roles" class="form-control" >
                                <option value=" " selected disabled>- Choose Role -</option>
                                @foreach ($role as $roles)
                                <option value="{{ $roles->name }}">{{ $roles->name }}</option>
                                 @endforeach
                           </select>
                           <p id="error_parajabatan" style="color:red;"></p>
                       </div>
                    </div>
                     <!-- <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                <p id="error_parapwd" style="color:red;"></p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div> -->
                   <br>
                    <div class="col-sm-offset-2 col-sm-10" style="margin-left:65%">
                     <button type="submit" class="btn btn-primary save" id="saveBtn" value="create">Register
                     </button>
                    </div>
                </form>
            </div>
        <div class="modal-footer"></div>
        </div>
    </div>
</div>
  <!--endmodal -->

    <!--modal -->
 <div class="modal fade" id="ajaxModel2" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary text-white">
                <h6 class="modal-title" id="modelHeading2"></h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
        </button>
            </div>
            <div class="modal-body">
                <form id="productForm2" name="productForm" class="form-horizontal">
                   <input type="hidden" name="id" id="id">
                   <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                         <div class="form-group row">
                          <label for="role" class="col-md-4 col-form-label text-md-right">Role</label>
                       <div class="col-md-6">
                           <select name="role" id="role" class="form-control">
                                @foreach ($role as $roles)
                                <option value="{{ $roles->name }}">{{ $roles->name }}</option>
                                @endforeach
                            </select>
                         </div>
                    </div>
                    <div class="form-group row">
                          <label for="role" class="col-md-4 col-form-label text-md-right">Status</label>
                       <div class="col-md-6">
                           <select class="form-control" name="status" id="status">
                              <option value="1">Active</option>
                              <option value="0">Not Active</option>
                           </select>
                      </div>
                    </div>
                  <br>
                    <div class="col-sm-offset-2 col-sm-10" style="margin-left:65%">
                     <button type="submit" class="btn btn-primary" id="saveBtn2" value="create">Save
                     </button>
                    </div>
                </form>
            </div><div class="modal-footer"></div>
        </div>
    </div>
</div>
            <!--endmodal -->
<div class="modal fade" id="ajaxModel3" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary text-white">
                <h6 class="modal-title" id="modelHeading3"></h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
        </button>
            </div>
            <div class="modal-body">
                   <form id="productForm3" name="productForm3" class="form-horizontal">
                   <input type="hidden" name="id" id="ids3">
                      <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password3" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm3" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
                   <br>
                    <div class="col-sm-offset-2 col-sm-10" style="margin-left:55%">
                     <button type="submit" class="btn btn-primary save3" id="saveBtn3" value="create">Change Password
                     </button>
                    </div>
                </form>
            </div><div class="modal-footer"></div>
        </div>
    </div>
</div>

    </section>
   
    <!-- /.content -->
   <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
   <script type="text/javascript">
        $(function () {
            $(".save").click(function () {
                var password = $("#password").val();
                var confirmPassword = $("#password-confirm").val();
                if (password != confirmPassword) {
                   Swal.fire({
                                type: 'error',
                                title: 'Sorry...',
                                text: 'Passwords are not the same..',
                            })
                    return false;
               }
                return true;
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $(".save3").click(function () {
                var password = $("#password3").val();
                var confirmPassword = $("#password-confirm3").val();
                if (password != confirmPassword) {
                   Swal.fire({
                                type: 'error',
                                title: 'Sorry...',
                                text: 'Passwords are not the same..',
                            })
                    return false;
               }
                return true;
            });
        });
    </script>
<script>
  $(function() {
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    var table_user = $('#tabeluser').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/{{ $prefix }}/user/json',
        order: [[ 0, "desc" ]],
        columns: [
            { data: 'DT_RowIndex', name:'DT_RowIndex'},
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            { data: 'roles', render(data) {
              return data[0].name;
              }
            },
            { data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
       
    $('#createNewUser').click(function () {
        $('#saveBtn').html('Save');
        $('#saveBtn').val("create-product");
        $('#id').val('');
        $('#productForm').trigger("reset");
        $('#modelHeading').html("Add User");
        $('#ajaxModel').modal('show');
    });

     $('#saveBtn').click(function (e) {
        e.preventDefault();
        var obj_json = {name: $('#namas').val(), email: $('#emails').val(), role: $('#roles').val()}
        //$(this).html('Sending..');
        $.ajax({
          data: JSON.stringify(obj_json),
          url: "/{{ $prefix }}/user/insert",
          type: "POST",
          dataType: 'json',
          contentType: "application/json; charset=utf-8",
          success: function (data) {
              switch(data.status_code)
              {
                case 200:
                  $('#productForm').trigger("reset");
                  $('#ajaxModel').modal('hide');
                  Swal.fire({
                                    type: 'success',
                                    title: 'Success...',
                                    text: 'Data Has Been Created',
                                })
                  table_user.ajax.reload();
                  break;
                case 422:
                  switch(Object.keys(data.message)[0]){
                    case 'email':
                      Swal.fire({
                        type: 'error',
                        title: 'Sorry...',
                        text: data.message.email
                      })
                    break;

                    case 'name': 
                      Swal.fire({
                        type: 'error',
                        title: 'Sorry...',
                        text: data.message.name
                      })
                    break;

                    case 'role':
                    Swal.fire({
                      type: 'error',
                      title: 'Sorry...',
                      text: data.message.role
                    })
                    break;
                  }
                  
                  break;
              }      
            },
          error: function (data) {
           //console.log(data.responseJSON.errors.email);
            //alert(data.responseJSON.errors.email);
            var error="";
            var name = document.getElementById( "namas" );
            if( name.value == "" )
            {
            error = "name can not be empty";
            document.getElementById( "error_para" ).innerHTML = error;
            return false;
            }
            var email = document.getElementById( "emails" );
            if( email.value == "" || email.value.indexOf( "@" ) == -1 )
            {
            error = " Write Valid Email Address. ";
            document.getElementById( "error_paraemail" ).innerHTML = error;
            return false;
              }
              var role = document.getElementById( "roles" );
            if( role.value == " ")
            {
            error = "name can not be empty";
            document.getElementById( "error_parajabatan" ).innerHTML = error;
            return false;
            }
              // var password = document.getElementById( "password" );
            // if( password.value == "" || password.value >= 8 )
            // {
            // error = " Password Minimal 8 Digits. ";
            // document.getElementById( "error_parapwd" ).innerHTML = error;
            // return false;
              // }
            else
            {
            return true;
            }
              $('#saveBtn').html('Save Changes');
          }
          
      });
      
    });

     $('body').on('click', '.editProduct', function () {
      var id = $(this).data('id');
           $.get("{{ url(''.$prefix.'/user/') }}" +'/' + id +'/edit', function (data) {
           console.log(data.roles[0].name)
            //  console.log(data)
          $('#modelHeading2').html("Edit Data");
          $('#saveBtn2').val("edit-user");
          $('#ajaxModel2').modal('show');
          $('#id').val(data.id);
          $('#name').val(data.name); 
          $('#email').val(data.email);
          $('#status').val(data.status);
          $('#role').val(data.roles[0].name).change();
          // $('#role').val('').change()
          // $('#jabatan').val(data.jabatan);       
       })
   });

    $('#saveBtn2').click(function (e) {
        e.preventDefault();
        // $(this).html('Sending..');
        $.ajax({
          data: $('#productForm2').serialize(),
          url: "/{{ $prefix }}/user/update",
          type: "POST",
          dataType: 'json',
          success: function (data) {
            switch(data.status_code)
              {
                case 200:
                  $('#productForm').trigger("reset");
                  $('#ajaxModel2').modal('hide');
                  Swal.fire({
                      type: 'success',
                      title: 'Success...',
                      text: 'Data Has Been updated',
                  })
                  table_user.ajax.reload();
                  break;
                case 422:
                  switch(Object.keys(data.message)[0]){
                    case 'email':
                      Swal.fire({
                        type: 'error',
                        title: 'Sorry...',
                        text: data.message.email
                      })
                    break;

                    case 'name': 
                      Swal.fire({
                        type: 'error',
                        title: 'Sorry...',
                        text: data.message.name
                      })
                    break;

                    case 'role':
                    Swal.fire({
                      type: 'error',
                      title: 'Sorry...',
                      text: data.message.role
                    })
                    break;
                  }
                  
                  break;
              }
            },
          error: function (data) {
              console.log('Error:', data);
              $('#saveBtn2').html('Save Changes');
          }
          
      });
      
    });

    $('body').on('click', '.editpwd', function () {
       var id = $(this).data('id');
           $.get("{{ url(''.$prefix.'/user/') }}" +'/' + id +'/edit', function (data) {
          $('#modelHeading3').html("Change Password");
          $('#saveBtn3').val("edit-user");
          $('#ajaxModel3').modal('show');
          $('#ids3').val(data.id);
        })
   });

    $('#saveBtn3').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');
        $.ajax({
          data: $('#productForm3').serialize(),
          url: "/{{ $prefix }}/user/update2",
          type: "POST",
          dataType: 'json',
          success: function (data) {
              $('#productForm3').trigger("reset");
              $('#ajaxModel3').modal('hide');
              Swal.fire({
                                type: 'success',
                                title: 'Success...',
                                text: 'Password Has Been Updated',
                            })
              table.draw();
            },
          error: function (data) {
              console.log('Error:', data);
              $('#saveBtn3').html('Save Changes');
          }
          
      });
       table_user.ajax.reload()
    });

    $(document).on('click', '.deleteUser', function () {
      var id = $(this).data("id");

      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "POST",
            url: "/{{ $prefix }}/user/delete/" +id,
            success: function(data){
            var json  = JSON.parse(data)

              switch (json.code) {
                case 200:
                    Swal.fire({
                              type: 'success',
                              title: 'Success..',
                              text: 'Data has been Deleted!',
                          })

                    table_user.ajax.reload();
                  break;
                default:
                  break;
              }

            
            } // end success
          
          }); // end ajax 

        } // end if
      }) // end swal

      
    });



      });


    </script>
     
@endsection

