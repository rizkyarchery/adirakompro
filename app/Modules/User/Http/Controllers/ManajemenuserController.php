<?php

namespace App\Modules\User\Http\Controllers;

use Mail;
use Session;
use DataTables;
use Auth;
use App\Mail\AchmadEmail;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Validator;
use App\User;
use App\Modules\User\Models\Manajemenuser;
use App\Modules\Setting\Models\Setting;


class ManajemenuserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    //  public function index()
    // {
    //      $manajemenuser = Manajemenuser::all();
    // 	 return view('manajemenuser',['manajemenuser' => $manajemenuser]);
    // }
  public function json(){
      
    $manajemenuser = User::with('roles')->get();
    
    return Datatables::of($manajemenuser)
          ->addColumn('action', function ($manajemenuser) {
            // $btn1 = '<center><a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$manajemenuser->id.'" data-original-title="password" title="Change Password"  class="btn btn-warning btn-sm editpwd"><i class="fa fa-key"></i></a> ';
            $btn='';
            if(auth()->user()->can('Edit User')){
              $btn = '<center><a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$manajemenuser->id.'" data-original-title="Edit"  title="Update" class="edit btn btn-primary btn-sm editProduct"><i class="fa fa-edit"></i></a>';
            }
            if(auth()->user()->can('Delete User')){
            $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$manajemenuser->id.'" data-original-title="Delete"  title="Delete" class="btn btn-danger btn-sm deleteUser"><i class="fa fa-trash"></i></a></center>';
        }
            return $btn;
            }
        )
        ->addIndexColumn()
        ->make(true);
  }

  public function index(){
        $data['manajemenuser'] = Manajemenuser::all();
        $data['settingweb']    = Setting::where('kode', '001');
        $data['role']          = Role::all();
      	return view('user::manajemenuser',$data);
    }

    public function profile($id){
        $data['user'] = User::with('roles')->where('id',$id)->first();
        $data['settingweb']    = Setting::where('kode', '001');
       	return view('user::profile',$data);
    }

    public function insert(Request $request){
       	$validate = Validator::make($request->all(), [
		    'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'role' => ['required'],
        ]);

        //dd($validate->errors());

        if(!$validate->fails()){
            $password = Str::random(12);
            $request->pwd =$password;
            Mail::to($request->email)->send(new AchmadEmail($request));
            
            $user = new User();
            $user->name             = $request->name;
            $user->password         = bcrypt($password);
            $user->email            = $request->email;
            $user->status           = 1;
            $user->updated_by      = Auth::user()->name;
            $user->created_by      = Auth::user()->name;
            $user->save();
            $user->assignRole($request->role);   
            $res = ['status_code' => 200, 'message' => 'User Has Been Created'];
            return response()->json($res);
        }else{
            $res = ['status_code' => 422, 'message' => $validate->errors()];
            return response()->json($res);
        }
    }

 public function edit($id){
        $user = User::with('roles')->where('id',$id)->first();
        // $user = User::where('id',$id)->first();
        // $data['roles']         = $user->roles()->pluck('name');
        return response()->json($user);
     }

 public function update(Request $request){
    $validate = Validator::make($request->all(), [
        'name' => 'required'
    ]);

    if(!$validate->fails()){
        $user = User::find($request->id);
        if($user->email == $request->email){
            $user->id               = $request->id;
            $user->name             = $request->name;
            $user->email            = $request->email;
            $user->status           = $request->status;
            $user->updated_by      = Auth::user()->name;
            $user->save();
            $user->syncRoles($request->role);    
            $res = ['status_code' => 200, 'message' => 'User Has Been updated'];
            return response()->json($res);
        }else{
            $userEmailCheck = User::where('email', $request->email)->first();
            if($userEmailCheck != null){
                $res = ['status_code' => 422, 'message' => ['email' => 'Email has been used']];
                return response()->json($res);
            }else{
                $user->id               = $request->id;
                $user->name             = $request->name;
                $user->email            = $request->email;
                $user->status           = $request->status;
                $user->updated_by      = Auth::user()->name;
                $user->save();
                $user->syncRoles($request->role);    
                $res = ['status_code' => 200, 'message' => 'User Has Been updated'];
                return response()->json($res);
            }
        }
    }else{
        $res = ['status_code' => 422, 'message' => $validate->errors()];
        return response()->json($res);
    }

    // User::updateOrCreate(['id' => $request->id],
    //             [   'name' => $request->name,
    //                 'email' => $request->email,
    //             ]);     
        
}

    public function update_profile(Request $request){

        $message = [
            "password.min" => "Password Minimal 10 Character",
            "password.regex" => "Password Must Have 1 Number, 1 Lowercase, 1 Uppercase and 1 Symbol"
        ];

        $validator = Validator::make($request->all(), [
            'password' => ['required', 'min:10', 'confirmed', 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$/']
        ], $message);
        if(!$validator->fails()){
            User::updateOrCreate(['id' => $request->id],['password' => bcrypt($request->password),]); 
            Session::flash('sukses','Password has been Updated');       
            return redirect(config('app.app_prefix').'/user/profile/'.$request->id.'');
        }

        return redirect(config('app.app_prefix').'/user/profile/'.$request->id)
                        ->withErrors($validator)
                        ->withInput();

    }

    public function delete($id){
       Manajemenuser::where('id',$id)->delete();
       
       $callback = [
            "message" => "Data has been Deleted",
            "code"   => 200
        ];

        return json_encode($callback, TRUE);
    }

    public function destroy($id){
       Manajemenuser::find($id)->delete();
       $callback = [
            "message" => "Data has been Deleted",
            "code"   => 200
        ];

        return json_encode($callback, TRUE);
    }

}
