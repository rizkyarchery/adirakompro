<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::group(['middleware'=> 'auth','prefix' => config('app.app_prefix')], function () {
    Route::get('/categoryfile/json', 'CategoryController@json');
    Route::resource('/categoryfile', 'CategoryController');
    Route::post('/categoryfile/store','CategoryController@store');
    ROUTE::DELETE('/categoryfile/destroy/{id}', 'CategoryController@destroy');
    Route::put('/categoryfile/edit/{id}', 'CategoryController@edit');
});

Route::group(['middleware'=> 'auth','prefix' => config('app.app_prefix')], function () {
      Route::get('/file/json','Filescontroller@json'); 
      Route::resource('/file', 'Filescontroller');
      Route::post('/file/insert','Filescontroller@insert');
      Route::post('/file/update/{id}','Filescontroller@update');
      ROUTE::post('/file/destroy/{id}', 'Filescontroller@destroy');
});
