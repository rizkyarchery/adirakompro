<?php

namespace App\Modules\Files\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category_file';
    protected $fillable = ['kategori'];
}
