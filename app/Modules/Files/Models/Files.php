<?php

namespace App\Modules\Files\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Files extends Model
{
    protected $table = 'file';
    protected $fillable = ['foto','ket','id_kategori','alt_teks', 'file_name'];

    use SoftDeletes;
    protected $dates =['deleted_at'];
     
}
