<?php

namespace App\Modules\Files\Http\Controllers;

use Auth;
use DB;
use File;
use Session;
use DataTables;
use Illuminate\Http\Request;
use App\Modules\Files\Models\Files;
use App\Modules\Files\Models\Category;
use App\Modules\Setting\Models\Setting;

use App\Http\Controllers\Controller;

class Filescontroller extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function json(){
        $file = Files::orderBy('id', 'DESC')->get();  

        return Datatables::of($file)
         ->addColumn('action', function ($file) {
             $btn='';
              if(auth()->user()->can('Edit File')){
                $btn = '<a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal_edit'.$file->id.'" style="color:white;"><i class="fa fa-edit"></i>&nbspEdit</a>';
              }

              if(auth()->user()->can('Delete File')){
                $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$file->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteProduct"><i class="fa fa-trash"></i>&nbspDelete</a></center>';
              }
              
            return $btn;
          })
        ->addIndexColumn()
        ->make(true);
    }

     public function index(){
        $file           = Files::all();
        // $kategori       = Category::all();
        $settingweb     = Setting::where('kode','001')->first();
    	return view('files::file',['file' => $file,'settingweb' => $settingweb]);
    }

    public function insert(Request $request) {
        // dd($request->file_name);
        $this->validate($request,[
            'foto' =>'required|mimes:pdf|max:150000'
         ]);
         
        $file = $request->file('foto');
        $path       = 'Upload/Files/';
		$nama_file = $path.$file->getClientOriginalName();
       	$tujuan_upload =  $path;
        $request->file('foto')->move($tujuan_upload,$nama_file);
        
        Files::create([
            'foto' => $nama_file,
            'file_name' => $request->file_name,
            'ket' => $request->ket,
            'id_kategori' => 1,
            'alt_teks' => $request->alt_teks,
            'created_by' => Auth::user()->name,
            'updated_by' => Auth::user()->name
        ]);

        Session::flash('sukses','File Has Been Created');
        return redirect(config('app.app_prefix'). '/file');
    }

    public function destroy($id) {
        $file = Files::find($id);
        File::delete($file->foto);
        $file->delete();

       $callback = [
            "message" => "Data has been Deleted",
            "code"   => 200
        ];

        return json_encode($callback, TRUE);
    }

     public function update($id, Request $request){

     $filess = Files::find($id);

    if($request->file('foto') == "")
        {
            $filess->foto = $filess->foto;
        } 
        else
        {
            File::delete($filess->foto);
            $file       = $request->file('foto');
            $path       = 'Upload/Files/';
            $fileName   =  $path.$file->getClientOriginalName();
            $request->file('foto')->move($path, $fileName);
            $filess->foto = $fileName;
        }  

    $filess->alt_teks = $request->alt_teks;
    $filess->ket = $request->ket;
    $filess->id_kategori = 1;
    $filess->file_name = $request->file_name;
    $filess->updated_by = Auth::user()->name;
    $filess->save();
    Session::flash('sukses','File Has Been Updated');
    return redirect(config('app.app_prefix').'/file');
}
}
