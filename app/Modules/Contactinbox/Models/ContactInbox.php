<?php

namespace App\Modules\Contactinbox\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactInbox extends Model
{
    //
    protected $table = 'contact_inbox';
    
    use SoftDeletes;
    protected $dates =['deleted_at'];
}
