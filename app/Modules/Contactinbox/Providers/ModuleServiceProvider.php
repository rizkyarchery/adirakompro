<?php

namespace App\Modules\Contactinbox\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('contactinbox', 'Resources/Lang', 'app'), 'contactinbox');
        $this->loadViewsFrom(module_path('contactinbox', 'Resources/Views', 'app'), 'contactinbox');
        $this->loadMigrationsFrom(module_path('contactinbox', 'Database/Migrations', 'app'), 'contactinbox');
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('contactinbox', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('contactinbox', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
