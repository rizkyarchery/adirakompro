<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Route::group(['prefix' => 'contactinbox'], function () {
//     Route::get('/', function () {
//         dd('This is the Contactinbox module index page. Build something great!');
//     });
// });

Route::group(['middleware'=> 'auth','prefix' => config('app.app_prefix')], function () {
    Route::get('/contact-inbox/list', 'ContactInboxController@listAll');
    Route::post('/contact-inbox/delete/{id}', 'ContactInboxController@delete');
    Route::resource('/contact-inbox', 'ContactInboxController');
    Route::get('/contact-inbox/view_contact/{id}', 'ContactInboxController@view_contact');
});


 Route::post('/contacts','ContactInboxController@contacts');
