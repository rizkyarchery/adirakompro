<?php

namespace App\Modules\Contactinbox\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use DataTables;
use DB;
use Str;
use Session;
use Validator;
use Auth;
use App;
use App\Modules\Contactinbox\Models\ContactInbox;

class ContactInboxController extends Controller
{
    private $lang;

    public function __construct() {
        // get the language first
        $lang = App::getLocale();

        // Set Language
        $this->lang = $lang;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listAll(){
        $contact = ContactInbox::orderBy('id','desc')->get();
        return DataTables::of($contact)
            ->addColumn('action', function ($contact){
                $html = '<a href="/'.config('app.app_prefix').'/contact-inbox/view_contact/'.$contact->id.'" class="btn  btn-outline-primary btn-block" target="_BLANK" data-toggle="tooltip" title="View Detail"><i class="fa fa-eye"> View</i></a>';
            return $html;
            })
            ->addIndexColumn()
            ->make(true);
    }

    public function index(){
        //
        return view('contactinbox::list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function contacts(Request $request){
       
        $ContactInbox                      = new ContactInbox();
        $ContactInbox->name                = $request->name;
        $ContactInbox->phone_number        = $request->phone_number;
        $ContactInbox->email               = $request->email;
        $ContactInbox->message             = $request->message;
        $ContactInbox->save();
        Session::flash('sukses','Data Has Been Send');
        return redirect()->back();          
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view_contact($id){
      $data['contact'] =  ContactInbox::where('id',$id)->first();
      return view('contactinbox::look', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
