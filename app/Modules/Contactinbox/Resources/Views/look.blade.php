@extends('layouts.index')
@section('content')
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
              <li class="breadcrumb-item active">Contact</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

<section class="content">
    <div class="container">
        <div class="col-md-12">
           <div class="card">
            <div class="card-header bg-info"><i class="fa fa-address-card"> Contact Inbox </i></div>
               <div class="card-body">
                 <div class="table-responsive">
       <table class="table table-striped table-hover">
           <thead style="vertical-align:middle;text-align:justify;font-weigth:bold">
             <tr class="table-light">
                 <th width="150px" style="vertical-align:middle;">Name</th>
                 <th width="5px"  style="text-align:center;vertical-align:middle;">:</th>
                 <th>{{ $contact->name }}</th>
            </tr>
             <tr class="table-light">
                 <th width="150px"  style="vertical-align:middle;">Phone Number</th>
                 <th width="5px"  style="text-align:center;vertical-align:middle;">:</th>
                 <th>{{ $contact->phone_number }}</th>
            </tr>
             <tr class="table-light">
                 <th width="150px"  style="vertical-align:middle;">Email</th>
                 <th width="5px"  style="text-align:center;vertical-align:middle;">:</th>
                 <th>{{ $contact->email }}</th>
            </tr>
             <tr class="table-light">
                 <th width="150px" style="vertical-align:middle;">Message</th>
                 <th width="5px"  style="text-align:center;vertical-align:middle;">:</th>
                 <th>{{ $contact->message }}</th>
            </tr>
           </thead>
       </table>
      </div>
               </div>
               <div class="card-footer"></div>
           </div>
      </div>
    </div>
</section>
@endsection
