@extends('layouts.index')
@section('content')

<?php

$prefix = config('app.app_prefix');

?>

<link rel="stylesheet" href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css')}}">
 <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Contact Inbox</h1>
            </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="home">Home</a></li>
            <li class="breadcrumb-item active">Contact Inbox</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  
  <section class="content">
    <div class="container">
    @if ($message = Session::get('sukses'))
				<div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
					<strong>{{ $message }}</strong>
				</div>
                @endif
        <div class="row justify-content-center">
      <div class="col-md-12">
          <div class="card">
            <div class="card-header bg-info">
              Contact Inbox
            </div>
            <div class="card-body">
            <div class="col-md-12">
            <hr>
                <div class="table-responsive">
                    <table  class="table-hover table-striped table-bordered table-list" id="contactDatatables">
                      <thead>
                        <tr style="vertical-align:middle;text-align:center;font-weigth:bold">
                            <th>No</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone Number</th>
                            <th>Action</th>
                          </tr> 
                      </thead>
                    </table>
                </div>
            </div>
          </div>
        </div>
    </div>
      </div>
</section>
 <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
 <script>

    $(function() {
            
      $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
      });
      var contact_table = $('#contactDatatables').DataTable({
          // order: [[ 0, 'desc' ]],
          processing: true,
          serverSide: true,
          ajax: '/{{ $prefix }}/contact-inbox/list',
          columns: [
             { data: 'DT_RowIndex', name:'DT_RowIndex'},
              { data: 'name', name: 'name' },
              { data: 'email', name: 'email' },
              { data: 'phone_number', name: 'phone_number' },
              {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
      });

      // $('#lang').on('change',function(){
      //   role_table
      //   .columns( 2 )
      //   .search( this.value )
      //   .draw();
      // })

    $(document).on('click', '#btn-status-page', function(){
      var id = $(this).data("id");
      var status = $(this).data("status");
      var btnText = "";
      if(status == 0){
        btnText = "Yes, show this page!";
      }else{
        btnText = "Yes, hide this page!";
      }
      Swal.fire({
        title: 'Are you sure?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: btnText
      }).then((result) => {
        if(result.value){
          $.ajax({
            url: '/{{ $prefix }}/page/status/'+id+"/"+status,
            type: 'GET',
            success: function(result){
              var json  = JSON.parse(result)
              switch (json.status) {
                case 200:
                  Swal.fire({
                              type: 'success',
                              title: 'Oops...',
                              text: json.message,
                          })
                  break;
                default:
                  break;
              }

              // refresh datatablesnya kalau sudah menghapus rolenya
              role_table.ajax.reload()
            }
          })
        }
      })
    })
    
    $(document).on('click', '#btn-page', function () {
      var id = $(this).data("id");

      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
            $.ajax({
            type: "POST",
            url: "/{{ $prefix }}/page/delete/" + id,
            success: function(data){
              var json  = JSON.parse(data)
              switch (json.status) {
                case 200:
                  Swal.fire({
                              type: 'success',
                              title: 'Success',
                              text: json.message,
                          })
                  break;
                default:
                  break;
              }

              // refresh datatablesnya kalau sudah menghapus rolenya
              role_table.ajax.reload()
              }
            });
        }
      })

      

    })        
  });

  </script>
<script>
  // Add the following code if you want the name of the file appear on select
  $(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
  });
</script>
@endsection