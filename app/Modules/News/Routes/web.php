<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Route::group(['prefix' => 'news'], function () {
//     Route::get('/', function () {
//         dd('This is the News module index page. Build something great!');
//     });
// });
Route::group(['middleware'=> 'auth','prefix' => config('app.app_prefix')], function () {
    Route::get('/news/list', 'NewsController@listAll');
    Route::get('/news/add/en/{slug}', 'NewsController@create_en');
    Route::get('/news/status/{id}/{status}', 'NewsController@updateStatus');
    Route::post('/news/delete/{id}', 'NewsController@delete');
    Route::resource('/news', 'NewsController');
});
