@extends('layouts.index')
@section('content')
<?php 

$PREFIX = config('app.app_prefix');

?>
{{-- <link rel="stylesheet" href="/assets/plugins/icheck-bootstrap/icheck-bootstrap.css"> --}}
<br>
<div class="container">
  <div class="card-header bg-info">Edit News News</div>
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-body">
            <form action="{{route('news.update', $news->id)}}" method="post" enctype="multipart/form-data">
		          {{ csrf_field() }}
              {{ method_field('PUT') }}
              <div class="form-group">
                <label>Title <sup class="text-danger">*</sup></label>
                <input type="text" name="title" value="{{ $news->title }}" class="form-control">
                @if($errors->has('title'))
                    <div class="text-danger">
                    {{ $errors->first('title')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>Excerpt <sup class="text-danger">*</sup></label>
                <textarea name="excerpt" rows="3" class="form-control">{{ $news->excerpt }}</textarea>
                @if($errors->has('excerpt'))
                    <div class="text-danger">
                    {{ $errors->first('excerpt')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>Content <sup class="text-danger">*</sup></label>
                <textarea name="content" rows="3" class="form-control" id="content">{{ $news->content }}</textarea>
                @if($errors->has('content'))
                    <div class="text-danger">
                    {{ $errors->first('content')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>Keyword <sup class="text-danger">*</sup></label>
                <input type="text" name="keyword" value="{{ $news->keyword }}" class="form-control">
                @if($errors->has('keyword'))
                    <div class="text-danger">
                    {{ $errors->first('keyword')}}
                    </div>
                @endif
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
      <div class="card">
        <div class="card-body">
        <div class="form-group">
                <label>Meta Title <sup class="text-danger">*</sup></label>
                <input type="text" name="meta_title" value="{{ $news->meta_title }}" class="form-control">
                @if($errors->has('meta_title'))
                    <div class="text-danger">
                    {{ $errors->first('meta_title')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>Meta Description <sup class="text-danger">*</sup></label>
                <textarea name="meta_desc" rows="3" class="form-control">{{ $news->meta_desc }}</textarea>
                @if($errors->has('meta_desc'))
                    <div class="text-danger">
                    {{ $errors->first('meta_desc')}}
                    </div>
                @endif
              </div>
          
              <div class="form-group">
                <label>Image <sup class="text-danger">*</sup><small>Recommended Size<span class="text-danger"> 1600 x 600</span></small></label>
                <br />
                <img src="/{{$news->image}}" width="50%">
                <br />
                <div class="custom-file">
                <input type="file" name="image" class="custom-file-input" id="customFile">
                <label class="custom-file-label" for="customFile">Choose file</label>
                </div>
                @if($errors->has('image'))
                    <div class="text-danger">
                    {{ $errors->first('image')}}
                    </div>
                @endif
              </div>
          <div class="form-group">
                <label>Language</label>
                <select name="lang" class="form-control">
                    <option></option>
                    @if($news->lang == 'ID')
                    <option selected>ID</option>
                    <option>EN</option>
                    @else
                    <option>ID</option>
                    <option selected>EN</option>
                    @endif
                </select>
                @if($errors->has('excerpt'))
                    <div class="text-danger">
                    {{ $errors->first('excerpt')}}
                    </div>
                @endif
          </div>
          {{-- <div class="form-group">
                <label>Image <sup class="text-danger">*</sup></label>
                <br />
                <input type="file" name="image">
                @if($errors->has('image'))
                    <div class="text-danger">
                    {{ $errors->first('image')}}
                    </div>
                @endif
          </div> --}}
            <div class="form-group">
                <label>Thumbnail <sup class="text-danger">*</sup><small>Recommended Size<span class="text-danger"> 320 x 302</span></small></label>
                <br />
                <img src="/{{$news->thumbnail}}" width="50%">
                <br />
                <div class="custom-file">
                <input type="file" name="thumbnail" class="custom-file-input" id="customFile">
                <label class="custom-file-label" for="customFile">Choose file</label>
                </div>
                @if($errors->has('thumbnail'))
                    <div class="text-danger">
                    {{ $errors->first('thumbnail')}}
                    </div>
                @endif
              </div>
              <center>
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="/{{ $PREFIX }}/news" class="btn btn-secondary">Back</a>
          </center>
        </div>
      </div>
      </div>
      </div>
      </form>
      <br />
    </div>
  </div>
<script src="/assets/plugins/icheck-bootstrap/icheck.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#content').summernote({
      height: "300px",
      styleWithSpan: false
    });
  }); 
</script>
<script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

// $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
//   checkboxClass: 'icheckbox_flat-green',
//   radioClass   : 'iradio_flat-green'
// })

</script>
@endsection