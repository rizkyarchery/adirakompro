<?php

namespace App\Modules\News\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use DB;
use Str;
use Session;
use Validator;
use Auth;
use App\Modules\News\Models\News;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listAll(request $request){
        if($request->columns[2]['search']['value'] != null){
            $page = DB::table('table_news')->where('lang',$request->columns[2]['search']['value'])->where('deleted_at', '=', null)->orderBy('id','desc')->get();
        }else{
            $page = DB::table('table_news')->where('deleted_at', '=', null)->orderBy('id','desc')->get();
        }
        return DataTables::of($page)
                ->addColumn('action', function ($page) { 
                    
                    // filter news language
                    switch ($page->lang) {
                        case 'ID':
                            $html   = '<a href="/'.$page->slug.'" target="_BLANK" class="btn btn-secondary"><i class="fa fa-eye"></i></a> &nbsp;';
                            $html   .= '<a href="/'.config('app.app_prefix').'/news/add/en/'.$page->slug.'" data-toggle="tooltip" title="Create News in english" class="btn btn-success"><i class="fa fa-plus"></i></a> &nbsp';
                        break;
                        case 'EN':
                            $html   = '<a href="/en/'.$page->slug.'" target="_BLANK" class="btn btn-secondary"><i class="fa fa-eye"></i></a> &nbsp;';
                        break;
                        
                        default:
                            $html   = '<a href="/'.$page->slug.'" target="_BLANK" class="btn btn-secondary"><i class="fa fa-eye"></i></a> &nbsp;';
                        break;
                    }
                    if(auth()->user()->can('Edit News')){
                    $html .= '<a href="/'.config('app.app_prefix').'/news/'.$page->id.'/edit" class="btn btn-primary" ><i class="fa fa-edit"></i></a> &nbsp;';
                    }
                    if(auth()->user()->can('Delete News')){
                    $html .= '<button id="btn-news" data-id="'. $page->id .'" class="btn btn-danger"><i class="fa fa-trash"></i></button>';
                    }
                    if($page->status == 0){
                        $html .= '&nbsp; <button id="btn-status-news" data-id="'. $page->id .'" data-status="1" class="btn btn-warning btn-aja">Show</button>';
                    }else{
                        $html .= '&nbsp; <button id="btn-status-news" data-id="'. $page->id .'" data-status="0" class="btn btn-success">Hide</button>';
                    }
                    
                    return $html;
            })
            ->addIndexColumn()
            ->make(true);
    }

    public function index()
    {
        //
        return view('news::list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('news::create');
    }

    public function create_en($slug){
        $data['slug'] = $slug;
        return view('news::create_en', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(),[
            'title' =>'required',
            'content' =>'required',
            'image' =>'image|mimes:jpeg,png,jpg|max:2048',
            'excerpt' =>'required',
            'lang' =>'required',
            'meta_title' =>'required',
            'meta_desc' =>'required',
            'keyword' =>'required',
            'thumbnail' =>'image|mimes:jpeg,png,jpg|max:2048'
      ]);

      if(!$validator->fails()){

        if($request->file('image') != null) {
            //IMAGE
            $file_image       = $request->file('image');
            $path_image       = 'Upload/News/image/';
            $name_file_image  = $path_image.Str::random(40) .'.'.$file_image->getClientOriginalExtension();
            //   $request->image = $name_file_image;
            $request->file('image')->move($path_image,$name_file_image);

            //THUMBNAIL
            $file_thumbnail       = $request->file('thumbnail');
            $path_thumbnail      = 'Upload/News/thumbnail/';
            $name_file_thumbnail  = $path_thumbnail.Str::random(40) .'.'.$file_thumbnail->getClientOriginalExtension();
            //   $request->thumbnail = $name_file_thumbnail;
            $request->file('thumbnail')->move($path_thumbnail,$name_file_thumbnail);
        }

        if($request->news_parent != null){
            $slug_news_id = checkSlug('table_news',Str::slug($request->title,'-'));
            $newsID = News::where('slug', $request->news_parent)->first();
            $newsID->news_parent = $slug_news_id;
            $newsID->save();
        }
        $news                       = new News();
        $news->title                = $request->title;
        $news->news_parent          = $request->news_parent;
        $news->content              = $request->content;
        $news->image                = isset($name_file_image) ? $name_file_image : null;
        $news->excerpt              = $request->excerpt;
        $news->lang                 = $request->lang;
        $news->meta_title           = $request->meta_title;
        $news->meta_desc            = $request->meta_desc;
        $news->keyword              = $request->keyword;
        $news->thumbnail            = isset($name_file_thumbnail) ? $name_file_thumbnail : null;
        $news->publisher            = Auth::user()->name;
        $news->status               = 1;
        $slug_news = checkSlug('table_news',Str::slug($request->title,'-'));
        $news->slug                 = $slug_news;
        $news->updated_by      = Auth::user()->name;
        $news->created_by      = Auth::user()->name;

        $news->save();

        Session::flash('sukses','News has been Created');
        return redirect(config('app.app_prefix').'/news');
      }
  
      return redirect(config('app.app_prefix') .'/news/create')
                        ->withErrors($validator)
                        ->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data['news'] = News::where('id', $id)->first();
        return view('news::edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(),[
            'title' =>'required',
            'content' =>'required',
            'excerpt' =>'required',
            'lang' =>'required',
            'meta_title' =>'required',
            'meta_desc' =>'required',
            'keyword' =>'required'
      ]);

      if(!$validator->fails()){
        $news = News::find($id);
        if($request->file('image') != null){
            //IMAGE
            $file_image       = $request->file('image');
            $path_image       = 'Upload/News/image/';
            $name_file_image  = $path_image.Str::random(40) .'.'.$file_image->getClientOriginalExtension();
            //   $request->image = $name_file_image;
            $request->file('image')->move($path_image,$name_file_image);
            $news->image = $name_file_image;
        }
        
        if($request->file('thumbnail') != null){
            //THUMBNAIL
            $file_thumbnail                 = $request->file('thumbnail');
            $path_thumbnail                 = 'Upload/News/thumbnail/';
            $name_file_thumbnail            = $path_thumbnail.Str::random(40) .'.'.$file_thumbnail->getClientOriginalExtension();
            $request->file('thumbnail')->move($path_thumbnail,$name_file_thumbnail);
            $news->thumbnail                = $name_file_thumbnail;
        }

        $news->title                    = $request->title;
        $news->content                  = $request->content;
        $news->excerpt                  = $request->excerpt;
        $news->lang                     = $request->lang;
        $news->meta_title               = $request->meta_title;
        $news->meta_desc                = $request->meta_desc;
        $news->keyword                  = $request->keyword;
        $news->updated_by      = Auth::user()->name;

        $news->save();

        Session::flash('sukses','News has been Updated');
        return redirect(config('app.app_prefix').'/news');
      }

      return redirect(config('app.app_prefix') .'/news/'.$id.'/edit')
                        ->withErrors($validator)
                        ->withInput();
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
        $news = News::find($id);
        $news->delete();

        $data = [
            "status" => 200,
            "message" => "Successfully delete news!"
        ];

        return json_encode($data);
    }

    public function updateStatus($id, $status){
        //dd($id);
        $news = News::find($id);
        $news->status = $status;
        $news->save();

        if($status == 0){
            $data = [
                "status" => 200,
                "message" => "Successfully hiding the news"
            ];
    
            return json_encode($data);
        }else{
            $data = [
                "status" => 200,
                "message" => "Successfully show the news"
            ];
    
            return json_encode($data);
        }
    }
}
