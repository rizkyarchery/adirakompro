<?php

namespace App\Modules\News\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    //
    protected $table = 'table_news';

    use SoftDeletes;
    protected $dates =['deleted_at'];
}
