<?php

namespace App\Modules\Fileimagemaster\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FileImageMaster extends Model
{
    //
    protected $table = "file_image_master";

    use SoftDeletes;
    protected $dates =['deleted_at'];
}
