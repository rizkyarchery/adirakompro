@extends('layouts.index')
@section('content')
<?php 

$PREFIX = config('app.app_prefix');

?>
{{-- <link rel="stylesheet" href="/assets/plugins/icheck-bootstrap/icheck-bootstrap.css"> --}}
<br>
<div class="container">
  <div class="card-header bg-info">Edit Image</div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <form action="{{route('file-image-master.update', $file->id)}}" method="post" enctype="multipart/form-data">
		          {{ csrf_field() }}
                  {{ method_field('PUT') }}
              <div class="form-group">
                <small>JPEG, JPG, PNG File Recommended Image Format</small>
                <br />
                <table>
                  <tr>
                    <td><small>Banner Size</small></td>
                    <td></td>
                    <td><small>1600 x 600</small></td>
                  </tr>
                  <tr>
                    <td><small>Homepage Service</small></td>
                    <td></td>
                    <td><small>250 x 150</small></td>
                  </tr>
                </table>
              </div>
              <div class="form-group">
                <label>Image <sup class="text-danger">*</sup></label>
                <br />
                <img src="/{{ $file->image }}" width="50%">
                <br />
                <br />
                <div class="custom-file">
                <input type="file" name="image" class="custom-file-input">
                 <label class="custom-file-label" for="customFile">Choose file</label>
                </div>
                @if($errors->has('image'))
                    <div class="text-danger">
                    {{ $errors->first('image')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>Alt Text <sup class="text-danger">*</sup></label>
                <input type="text" name="alt_text" value="{{ $file->alt_text }}" class="form-control">
                @if($errors->has('alt_text'))
                    <div class="text-danger">
                    {{ $errors->first('alt_text')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>Description</label>
                <textarea name="description" rows="3" class="form-control">{{ $file->description }}</textarea>
                @if($errors->has('alt_text'))
                    <div class="text-danger">
                    {{ $errors->first('alt_text')}}
                    </div>
                @endif
              </div>
          </div>
          <div class="card-footer">
          <center>
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="/{{ $PREFIX }}/file-image-master" class="btn btn-secondary">Back</a>
          </center>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>
<script src="/assets/plugins/icheck-bootstrap/icheck.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#content').summernote({
      height: "300px",
      styleWithSpan: false
    });
  }); 
</script>
<script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

// $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
//   checkboxClass: 'icheckbox_flat-green',
//   radioClass   : 'iradio_flat-green'
// })

</script>
@endsection