<?php

namespace App\Modules\Fileimagemaster\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('fileimagemaster', 'Resources/Lang', 'app'), 'fileimagemaster');
        $this->loadViewsFrom(module_path('fileimagemaster', 'Resources/Views', 'app'), 'fileimagemaster');
        $this->loadMigrationsFrom(module_path('fileimagemaster', 'Database/Migrations', 'app'), 'fileimagemaster');
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('fileimagemaster', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('fileimagemaster', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
