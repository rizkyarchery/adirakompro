<?php

namespace App\Modules\Fileimagemaster\Http\Controllers;

use Illuminate\Http\Request;

use DataTables;
use DB;
use Session;
use File;
use Validator;
use Auth;
use App\Modules\Fileimagemaster\Models\FileImageMaster;

use App\Http\Controllers\Controller;

class FileImageMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function listAll(){
        $file = FileImageMaster::orderBy('id', 'desc')->get();
        return DataTables::of($file)
            ->addColumn('action', function($file){
                $html='';
            if(auth()->user()->can('Edit Master Image')){
                $html = '<a href="/'.config('app.app_prefix').'/file-image-master/'.$file->id.'/edit" class="btn btn-primary" >Edit</a>';
            }
            if(auth()->user()->can('Delete Master Image')){
                $html=$html.' <button id="btn-file" data-id="'. $file->id .'" class="btn btn-danger">Delete</button>';
            }
                return $html;
            })
            ->addIndexColumn()
            ->make(true);
    }

    public function index()
    {
        //
        return view('fileimagemaster::list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('fileimagemaster::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(),[
            'image' =>'required|file|image|mimes:jpeg,png,jpg|max:2048',
            'alt_text' => 'required'
         ]);
        
        if(!$validator->fails()){
            $file = $request->file('image');
            $path = 'Upload/Master-image/';
            $file_name = $path.$file->getClientOriginalName();
            
            $direction = 'Upload/Master-image/';
            $request->file('image')->move($direction, $file_name);

            $file_image = new FileImageMaster();
            $file_image->image = $file_name;
            $file_image->alt_text = $request->alt_text;
            $file_image->description = $request->description;
            $file_image->created_by = Auth::user()->name;
            $file_image->updated_by = Auth::user()->name;
            $file_image->save();

            Session::flash('sukses','Image master has been inserted!');
            return redirect(config('app.app_prefix') . '/file-image-master');
        }

        return redirect(config('app.app_prefix') .'/file-image-master/create')
                        ->withErrors($validator)
                        ->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $file = FileImageMaster::where('id',$id)->first();
        $data['file'] = $file;
        return view('fileimagemaster::edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(),[
            'alt_text' => 'required'
         ]);
        
        if(!$validator->fails()){
            if($request->file('image') != null){
                $file = $request->file('image');
                $path = 'Upload/Master-image/';
                $file_name = $path.$file->getClientOriginalName();
                
                $direction = 'Upload/Master-image/';
                $request->file('image')->move($direction, $file_name);
    
                $file_image = FileImageMaster::find($id);
                $file_image->image = $file_name;
                $file_image->alt_text = $request->alt_text;
                $file_image->description = $request->description;
                $file_image->updated_by = Auth::user()->name;
                $file_image->save();
    
                Session::flash('sukses','Image master has been updated!');
                return redirect(config('app.app_prefix') . '/file-image-master');
            }else{
                $file_image = FileImageMaster::find($id);
                $file_image->alt_text = $request->alt_text;
                $file_image->description = $request->description;
                $file_image->updated_by = Auth::user()->name;
                $file_image->save();
    
                Session::flash('sukses','Image master has been updated!');
                return redirect(config('app.app_prefix') . '/file-image-master');
            }
        }

        return redirect(config('app.app_prefix') .'/file-image-master/'.$id.'/edit')
                        ->withErrors($validator)
                        ->withInput();
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
        $file = FileImageMaster::find($id);
        $file->delete();

        $data = [
            "status" => 200,
            "message" => "Successfully Delete Image"
        ];

        return json_encode($data);
    }
}
