@extends('layouts.index')
@section('content')
<?php 
  $PREFIX = config('app.app_prefix');
?>

<br><br>
<div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="card">
          <div class="card-header bg-info">Create Award</div>
          <div class="card-body">
            <form action="/{{ $PREFIX }}/award" method="post" enctype="multipart/form-data">
		          {{ csrf_field() }}
              <div class="form-group">
                <label>Language <sup class="text-danger">*</sup></label><br>
                <select name="lang" class="form-control" required>
                  <option value="" selected disabled>Choose Language</option>
                  <option>ID</option>
                  <option>EN</option>
                </select>
                @if($errors->has('lang'))
                    <div class="text-danger">
                    {{ $errors->first('lang')}}
                  </div>
                @endif
              </div>
              <div class="form-group">
                <label>Name <sup class="text-danger">*</sup></label><br>
                <input  type="text" 
                        name="name" 
                        class="form-control" 
                        placeholder="fill this name..." 
                        value="{{ old('name') }}"
                        required>
                @if($errors->has('name'))
                    <div class="text-danger">
                    {{ $errors->first('name')}}
                  </div>
                @endif
              </div>
              <div class="form-group">
                <label>Year <sup class="text-danger">*</sup></label> <br>
                <select class="form-control" name="year" required>
                  <option value="" disabled selected>Choose Year</option>
                  @for ($i = 2045; $i > 1950; $i--)
                    <option value="{{ $i }}">{{ $i }}</option>
                  @endfor
                </select>
                @if($errors->has('year'))
                    <div class="text-danger">
                    {{ $errors->first('year')}}
                  </div>
                @endif
              </div>
              <div class="form-group">
                <label>Brand</label> <br>
                <input  type="text"  
                        name="brand" 
                        class="form-control" 
                        placeholder="fill this brand..." 
                        value="{{ old('brand') }}"
                        required>
                @if($errors->has('brand'))
                    <div class="text-danger">
                    {{ $errors->first('brand')}}
                  </div>
                @endif
              </div>
              <div class="form-group">
                  <label>From</label> <br>
                  <input  type="text" 
                          name="from" 
                          class="form-control" 
                          placeholder="fill this from..." 
                          value="{{ old('from') }}">
                  @if($errors->has('from'))
                      <div class="text-danger">
                      {{ $errors->first('from')}}
                    </div>
                  @endif
                </div>
              <div class="form-group">
                <label>Upload Image <sup class="text-danger">*</sup>  <small>Recommended Size<span class="text-danger"> 320 x 302</span></small></label> <br>
                <div class="custom-file">
                <input  type="file" 
                        name="image" 
                        class="custom-file-input" 
                        id="customFile" 
                        accept="image/*" 
                        required>
                <label class="custom-file-label" for="customFile">Choose file</label>
                </div>
                @if($errors->has('image'))
                    <div class="text-danger">
                    {{ $errors->first('image')}}
                  </div>
                @endif
              </div>
               <div class="form-group">
                  <label>Award Thumbnail Alt.</label> <br>
                  <input  type="text" 
                          name="alt_text" 
                          class="form-control" 
                          placeholder="fill this Thumbnail Alt..." 
                          value="{{ old('alt_text') }}">
                  @if($errors->has('alt_text'))
                      <div class="text-danger">
                      {{ $errors->first('alt_text')}}
                    </div>
                  @endif
                </div>
              <div class="form-group">
                  <button type="submit" class="btn btn-primary">Save</button>
                  <a href="/{{ $PREFIX }}/award" class="btn btn-secondary">Back</a>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<script src="/assets/plugins/icheck-bootstrap/icheck.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#content').summernote({
      height: "300px",
      styleWithSpan: false
    });
  }); 
</script>
<script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

// $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
//   checkboxClass: 'icheckbox_flat-green',
//   radioClass   : 'iradio_flat-green'
// })

</script>
@endsection
