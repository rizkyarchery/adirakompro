@extends('layouts.index')
@section('content')
<?php 
  $PREFIX = config('app.app_prefix');
?>

<br><br>
<div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="card">
          <div class="card-header bg-info">Create Award</div>
          <div class="card-body">
            <form action="{{ route('award.update',$award['id']) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
              <div class="form-group">
                <label>Language <sup class="text-danger">*</sup></label><br>
                <select name="lang" class="form-control">
                  @if($award['lang'] == "ID")
                  <option selected>ID</option>
                  <option>EN</option>
                  @else
                  <option>ID</option>
                  <option selected>EN</option>
                  @endif
                </select>
                @if($errors->has('lang'))
                    <div class="text-danger">
                    {{ $errors->first('lang')}}
                  </div>
                @endif
              </div>
              <div class="form-group">
                <label>Name <sup class="text-danger">*</sup></label><br>
                <input type="text" name="name" value="{{ $award['award'] }}" class="form-control" placeholder="fill this name...">
                @if($errors->has('name'))
                    <div class="text-danger">
                    {{ $errors->first('name')}}
                  </div>
                @endif
              </div>
              <div class="form-group">
                <label>Year <sup class="text-danger">*</sup></label> <br>
                <select class="form-control" name="year">
                  <option value="" disabled selected>Choose Year</option>
                  @for ($i = 2045; $i > 1950; $i--)
                    <option value="{{ $i }}" @if($award['year'] == $i) selected @endif>{{ $i }}</option>
                  @endfor
                </select>
                @if($errors->has('year'))
                    <div class="text-danger">
                    {{ $errors->first('year')}}
                  </div>
                @endif
              </div>
              <div class="form-group">
                <label>Brand</label> <br>
                <input type="text" name="brand" value="{{ $award['brand'] }}" class="form-control" placeholder="fill this brand...">
                @if($errors->has('brand'))
                    <div class="text-danger">
                    {{ $errors->first('brand')}}
                  </div>
                @endif
              </div>
              <div class="form-group">
                  <label>From</label> <br>
                  <input type="text" name="from" value="{{ $award['from'] }}" class="form-control" placeholder="fill this from...">
                  @if($errors->has('from'))
                      <div class="text-danger">
                      {{ $errors->first('from')}}
                    </div>
                  @endif
                </div>
                 <div class="form-group">
                        <label>Upload Image <sup class="text-danger">*</sup> <small>Recommended Size<span class="text-danger"> 320 x 302</span></small></label> 
                          <div class="row">
                             <div class="col s6">
                               <img src="{{ URL::to("$award->image")}}" id="showgambar" style="max-width:200px;max-height:200px;float:left;" />
                             </div>
                        </div>
                        <br>
                    <div class="row">
                       <div class="input-field col s6">
                           <div class="custom-file">
                           <input type="file" id="inputgambar" name="image" class="custom-file-input" accept="image/*"/ >
                         <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                       </div>
                   </div>
                </div> 
                 <div class="form-group">
                <label>Award Thumbnail Alt.</label> <br>
                <input type="text" name="alt_text" value="{{ $award['alt_text'] }}" class="form-control" placeholder="fill this Thumbnail Alt...">
                @if($errors->has('alt_text'))
                    <div class="text-danger">
                    {{ $errors->first('alt_text')}}
                  </div>
                @endif
              </div> 
              <div class="form-group">
                  <button type="submit" class="btn btn-primary">Save</button>
                  <a href="/{{ $PREFIX }}/award" class="btn btn-secondary">Back</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
<script src="/assets/plugins/icheck-bootstrap/icheck.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#content').summernote({
      height: "300px",
      styleWithSpan: false
    });
  }); 
</script>
<script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

// $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
//   checkboxClass: 'icheckbox_flat-green',
//   radioClass   : 'iradio_flat-green'
// })

</script>
@endsection
