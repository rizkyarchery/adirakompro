<?php

namespace App\Modules\Award\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Award extends Model
{
    protected $table = 'award';

    use SoftDeletes;
    protected $dates =['deleted_at'];
}
