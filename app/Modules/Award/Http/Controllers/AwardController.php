<?php

namespace App\Modules\Award\Http\Controllers;

use File;
use Session;
use DataTables;
use Validator;
use Auth;
use Illuminate\Http\Request;
Use App\Modules\Award\Models\Award;
use App\Http\Controllers\Controller;

class AwardController extends Controller
{
    public function listAll(request $request) {

      if($request->columns[2]['search']['value'] != null){
          switch ($variable) {
              case 'value':
                    $award = Award::where('lang',$request->columns[2]['search']['value'])->orderBy('id','DESC')->get();
                  break;
              
              default:
                  # code...
                  break;
          }
      } else {
          $award = Award::orderBy('id','DESC')->get();
      }

        return Datatables::of($award)
            ->addColumn('action', function ($award) {
                $html='';
                if(auth()->user()->can('Edit Award')){
                $html = '<a href="/'.config('app.app_prefix').'/award/'.$award->id.'/edit" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Update Award" ><i class="fa fa-edit">&nbspEdit</i></a>';
                }
                if(auth()->user()->can('Delete Award')){
                $html= $html.' <button id="deleteAward" data-id="'. $award->id .'" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete Award"><i class="fa fa-trash">&nbspDelete</i></button>';
                }
                return $html;
           })
        ->addIndexColumn()
        ->make(true);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("award::list");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("award::create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $validator = Validator::make($request->all(), [
            'name'          => 'required|min:4',
            'year'          => 'required',
            'image' =>'required|file|image|mimes:jpeg,png,jpg|max:2048',
            'lang' => 'required'
        ]);

        if(!$validator->fails()) {
            $file = $request->file('image');
            $path       = 'Upload/Award/';
            $nama_file = $path.$file->getClientOriginalName();
            $tujuan_upload =  $path;
            $request->file('image')->move($tujuan_upload,$nama_file);

            $award              = new Award();
            $award->award       = $request->name;
            $award->year        = $request->year;
            $award->brand       = $request->brand;
            $award->from        = $request->from;
            $award->alt_text    = $request->alt_text;
            $award->image       = $nama_file;
            $award->lang        = $request->lang;
            $award->created_by = Auth::user()->name;
            $award->updated_by = Auth::user()->name;
            $award->save();
        
            Session::flash('sukses','Award Has Been Created');
            return redirect(config('app.app_prefix') . '/award');
        }
        
        return redirect(config('app.app_prefix') .'/award/create')
                        ->withErrors($validator)
                        ->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $award = Award::find($id);

        $data['award'] = $award;


        return view('award::edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'          => 'required|min:4',
            'year'          => 'required',
            'image' =>'file|image|mimes:jpeg,png,jpg|max:2048',
            'lang' => 'required'
        ]);

        $award = Award::find($id);

        if(!$validator->fails()) {
            if($request->file('image') == ""){
                $award->image = $award->image;
            }else{
                File::delete($award->image);
                $file       = $request->file('image');
                $path       = 'Upload/Award/';
                $fileName   =  $path.$file->getClientOriginalName();
                $request->file('image')->move($path, $fileName);
                $award->image = $fileName;
            }
            
            $award->award       = $request->name;
            $award->year        = $request->year;
            $award->brand       = $request->brand;
            $award->from        = $request->from;
            $award->alt_text    = $request->alt_text;
            $award->lang        = $request->lang;
            $award->updated_by = Auth::user()->name;
            $award->save();
        
            Session::flash('sukses','Award Has Been Updated');
            return redirect(config('app.app_prefix') . '/award');
        }

        return redirect(config('app.app_prefix') .'/award/'.$id.'/edit')
                        ->withErrors($validator)
                        ->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $award = Award::find($id);
        File::delete($award->image);
        $award->delete();
        
        $data = [
            "status" => 200,
            "message" => "Successfully Delete Award"
        ];

        return json_encode($data);
    }
}
