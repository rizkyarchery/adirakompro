<?php

namespace App\Modules\Career\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('career', 'Resources/Lang', 'app'), 'career');
        $this->loadViewsFrom(module_path('career', 'Resources/Views', 'app'), 'career');
        $this->loadMigrationsFrom(module_path('career', 'Database/Migrations', 'app'), 'career');
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('career', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('career', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
