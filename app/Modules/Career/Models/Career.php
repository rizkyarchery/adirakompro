<?php

namespace App\Modules\Career\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
    //
    protected $table = 'career';

    public function provinces(){
        return $this->belongsTo('App\Modules\Province\Models\Province', 'province_id');
    }

    use SoftDeletes;
    protected $dates =['deleted_at'];
}
