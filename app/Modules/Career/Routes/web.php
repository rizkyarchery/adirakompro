<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::group(['middleware'=> 'auth','prefix' => config('app.app_prefix')], function () {
    Route::get('/career/list', 'CareerController@listAll');
    Route::post('/career/delete/{id}', 'CareerController@delete');
    Route::resource('/career', 'CareerController');
});
