@extends('layouts.index')
@section('content')
<?php 

$PREFIX = config('app.app_prefix');

?>
{{-- <link rel="stylesheet" href="/assets/plugins/icheck-bootstrap/icheck-bootstrap.css"> --}}
<br><br>
<div class="container">
  <div class="card-header bg-info">Create Career</div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <form action="{{route('career.store')}}" method="post">
		          {{ csrf_field() }}
              <div class="form-group">
                <label>Province <sup class="text-danger">*</sup></label><br>
                <select class="form-control" name="province_id" id="province">
                    <option></option>
                    @foreach ($provinces as $province)
                    <option value="{{ $province->id }}">{{ $province->province_name }}</option>
                    @endforeach
                </select>
                @if($errors->has('province_id'))
                    <div class="text-danger">
                    {{ $errors->first('province_id')}}
                    </div>
                @endif
              <div class="form-group">
                <label>Minimal Experience <sup class="text-danger">*</sup></label><br>
                <select name="min_experience" class="form-control">
                    <option></option>
                    <option>Fresh Graduate</option>
                    <option>Min 1 year experience</option>
                    <option>Min 2 year experience</option>
                    <option>Min 3 year experience</option>
                    <option>Min 4 year experience</option>
                </select>
                @if($errors->has('min_experience'))
                    <div class="text-danger">
                    {{ $errors->first('min_experience')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>Position <sup class="text-danger">*</sup></label><br>
                <input type="text" name="position" class="form-control" placeholder="fill this position..." value="{{ old('position') }}">
                @if($errors->has('position'))
                    <div class="text-danger">
                    {{ $errors->first('position')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>Link To Jobstreet <sup class="text-danger">*</sup></label><br>
                <input type="text" name="link" class="form-control" placeholder="fill this link..." value="{{ old('link') }}">
                @if($errors->has('link'))
                    <div class="text-danger">
                    {{ $errors->first('link')}}
                    </div>
                @endif
              </div>
          </div>
          <div class="card-footer">
          <center>
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="/{{ $PREFIX }}/career" class="btn btn-secondary">Back</a>
          </center>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>
<script src="/assets/plugins/icheck-bootstrap/icheck.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#content').summernote({
      height: "300px",
      styleWithSpan: false
    });
  }); 
</script>
<script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

// $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
//   checkboxClass: 'icheckbox_flat-green',
//   radioClass   : 'iradio_flat-green'
// })

</script>
@endsection