<?php

namespace App\Modules\Career\Http\Controllers;

use Illuminate\Http\Request;

use DataTables;
use DB;
use Session;
use Validator;
use Auth;
use App\Modules\Province\Models\Province;
use App\Modules\Career\Models\Career;
use App\Http\Controllers\Controller;

class CareerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listAll(){
        $career = Career::with('provinces')->get();
        //dd($career);
        return DataTables::of($career)
                ->addColumn('action', function($career){
                    $html='';
                if(auth()->user()->can('Edit Career')){
                    $html = '<a href="/'.config('app.app_prefix').'/career/'.$career->id.'/edit" class="btn btn-primary" >Edit</a>';
                }
                 if(auth()->user()->can('Delete Career')){
                    $html=$html.' <button id="btn-career" data-id="'. $career->id .'" class="btn btn-danger">Delete</button>';
                 }
                    return $html;
                })
                ->addIndexColumn()
                ->make(true);
        
    }

    public function index()
    {
        //
        return view('career::list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $provinces = Province::all();

        $data['provinces'] = $provinces;
        return view('career::create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedData = Validator::make($request->all(), [
            'province_id'          => 'required',
            'min_experience'   => 'required',
            'position' => 'required',
            'link' => 'required'
        ]);

        if(!$validatedData->fails()) {
            $career = new Career();
            $career->province_id = $request->province_id;
            $career->min_experience = $request->min_experience;
            $career->position = $request->position;
            $career->link = $request->link;
            $career->created_by = Auth::user()->name;
            $career->updated_by = Auth::user()->name;
            $career->save();
        
            Session::flash('sukses','Career has been inserted!');
            return redirect(config('app.app_prefix') . '/career');
        }

        return redirect(config('app.app_prefix') .'/career/create/')
                        ->withErrors($validatedData)
                        ->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $career = Career::with('provinces')->where('id',$id)->first();
        $data['career'] = $career;
        $provinces = Province::all();

        $data['provinces'] = $provinces;
        return view('career::edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validatedData = Validator::make($request->all(), [
            'province_id'          => 'required',
            'min_experience'   => 'required',
            'position' => 'required',
            'link' => 'required'
        ]);

        if(!$validatedData->fails()) {
            $career = Career::find($id);
            $career->province_id = $request->province_id;
            $career->min_experience = $request->min_experience;
            $career->position = $request->position;
            $career->link = $request->link;
            $career->updated_by = Auth::user()->name;
            $career->save();
        
            Session::flash('sukses','Career has been updated!');
            return redirect(config('app.app_prefix') . '/career');
        }

        return redirect(config('app.app_prefix') .'/career/'.$id.'/edit')
                        ->withErrors($validatedData)
                        ->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
        $career = Career::find($id);
        $career->delete();

        $data = [
            "status" => 200,
            "message" => "Successfully Delete career"
        ];

        return json_encode($data);
    }
}
