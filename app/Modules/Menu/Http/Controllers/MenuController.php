<?php

namespace App\Modules\Menu\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB; 
use App\Modules\Setting\Models\Setting;
use App\Modules\Menu\Models\Menu;
use App\Modules\Menu\Models\Judul;
use DataTables;

use App\Http\Controllers\Controller;

class MenuController extends Controller
{
    /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function saveMenu(Request $request){

    // Retrieve in variable first
    $menudata    = $request->json()->all();

    switch ($menudata['language']) {
      case 'EN':
        $code = '002';
        break;

      case 'ID':
        $code = '001';
        break;
      
      default:
        break;
    }

    // convert array to json
    $content = json_encode($menudata['data']);

    // save
    $menu = Menu::where('code',$code)->first();
    $menu->data = $content;
    $menu->save();

    // Send Callback
    $callback = [
      "message" => "Data has been Save",
      "code"   => 200
    ];

    return json_encode($callback, TRUE);
  }

  public function index(Request $request){

    if($request->lang == null || $request->lang == 'ID') {
      $menu = Menu::where('code','001')->first();
    } else {
      $menu = Menu::where('code','002')->first();
    }

    $data['menu'] = $menu;
    
    return view('menu::menu',$data);
  }

  //   public function insert(Request $request) {
  //     Menu::updateOrCreate(['id' => $request->id],

  //             [
  //                 'link' => $request->link,
  //                 'icon' => $request->icon,
  //                 'childjudul' => $request->childjudul,
  //                 'id_jdl' => $request->id_jdl
  //             ]
  //             );        
      
  //     return response()->json(['success'=>'saved successfully.']);
  // }
  // public function edit($id)
  // {
  // //    $menu = DB::table('tabel_menu')
  // //        ->leftJoin('judul_menu', 'judul_menu.id', '=', 'tabel_menu.id_jdl')
  // //         ->where('tabel_menu.id','=',$id)
  // //        ->get();
  //   $menu = Menu::find($id);
  //     return response()->json($menu);
  // }
  //   public function destroy($id)
  // {
  //     Menu::find($id)->delete();
  //     $callback = [
  //         "message" => "Data has been Deleted",
  //         "code"   => 200
  //     ];

  //     return json_encode($callback, TRUE);
  // }
}
