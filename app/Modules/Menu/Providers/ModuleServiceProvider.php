<?php

namespace App\Modules\Menu\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('menu', 'Resources/Lang', 'app'), 'menu');
        $this->loadViewsFrom(module_path('menu', 'Resources/Views', 'app'), 'menu');
        $this->loadMigrationsFrom(module_path('menu', 'Database/Migrations', 'app'), 'menu');
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('menu', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('menu', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
