@extends('layouts.index')

@section('content')
<?php

$prefix = config('app.app_prefix');

?>

<style type="text/css">
  .cf:after { visibility: hidden; display: block; font-size: 0; content: " "; clear: both; height: 0; }
  * html .cf { zoom: 1; }
  *:first-child+html .cf { zoom: 1; }
  /* html { margin: 0; padding: 0; } */
  /* body { font-size: 100%; margin: 0; padding: 1.75em; font-family: 'Helvetica Neue', Arial, sans-serif; } */
  h1 { font-size: 1.75em; margin: 0 0 0.6em 0; }
  a { color: #2996cc; }
  a:hover { text-decoration: none; }
  p { line-height: 1.5em; }
  .small { color: #666; font-size: 0.875em; }
  .large { font-size: 1.25em; }
  /**
   * Nestable
   */
  .dd { position: relative; display: block; margin: 0; padding: 0; max-width: 600px; list-style: none; font-size: 13px; line-height: 20px; }
  .dd-list { display: block; position: relative; margin: 0; padding: 0; list-style: none; }
  .dd-list .dd-list { padding-left: 30px; }
  .dd-collapsed .dd-list { display: none; }
  .dd-item,
  .dd-empty,
  .dd-placeholder { display: block; align-items: center; position: relative; margin: 0; padding: 0; min-height: 20px; font-size: 13px; line-height: 20px; }
  .dd-handle { 
      margin-right: 10px;
      display: block; height: 30px; width: 100%; margin-right: 3px;padding: 5px 10px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
      background: #fafafa;
      background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
      background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
      background:         linear-gradient(top, #fafafa 0%, #eee 100%);
      -webkit-border-radius: 3px;
              border-radius: 3px;
      box-sizing: border-box; -moz-box-sizing: border-box;
  }
  .dd-handle:hover { color: #2ea8e5; background: #fff; }
  .dd-item > button  { 
    display: block; 
    position: relative; 
    cursor: pointer; 
    float: left; 
    width: 25px; 
    height: 20px; 
    margin: 5px 0; 
    padding: 0; 
    text-indent: 100%; 
    white-space: nowrap; 
    overflow: hidden; 
    border: 0; 
    background: transparent; 
    font-size: 12px; 
    line-height: 1; 
    text-align: center; 
    font-weight: bold; 
  }
  .dd-item .edit-menu {
    display: block;
    cursor: pointer;
    position: absolute;
    right: 32px;
    z-index: 99;
    top: 10px;
  }

  .dd-item .delete-menu {
    display: block;
    cursor: pointer;
    position: absolute;
    right: 10px;
    z-index: 99;
    top: 10px;
  }
  .dd-item > button:before { content: '+'; display: block; position: absolute; width: 100%; text-align: center; text-indent: 0; }
  .dd-item > button[data-action="collapse"]:before { content: '-'; }
  .dd-placeholder,
  .dd-empty { margin: 5px 0; padding: 0; min-height: 30px; background: #f2fbff; border: 1px dashed #b6bcbf; box-sizing: border-box; -moz-box-sizing: border-box; }
  .dd-empty { border: 1px dashed #bbb; min-height: 100px; background-color: #e5e5e5;
      background-image: -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                        -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
      background-image:    -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                           -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
      background-image:         linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                                linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
      background-size: 60px 60px;
      background-position: 0 0, 30px 30px;
  }
  .dd-dragel { position: absolute; pointer-events: none; z-index: 9999; }
  .dd-dragel > .dd-item .dd-handle { margin-top: 0; }
  .dd-dragel .dd-handle {
      -webkit-box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
              box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
  }
  /**
   * Nestable Extras
   */
  .nestable-lists { display: block; clear: both; padding: 30px 0; width: 100%; border: 0; border-top: 2px solid #ddd; border-bottom: 2px solid #ddd; }
  #nestable-menu { padding: 0; margin: 20px 0; }
  #nestable-output,
  #nestable2-output { width: 100%; height: 7em; font-size: 0.75em; line-height: 1.333333em; font-family: Consolas, monospace; padding: 5px; box-sizing: border-box; -moz-box-sizing: border-box; }
  #nestable2 .dd-handle {
      color: #fff;
      border: 1px solid #999;
      background: #bbb;
      background: -webkit-linear-gradient(top, #bbb 0%, #999 100%);
      background:    -moz-linear-gradient(top, #bbb 0%, #999 100%);
      background:         linear-gradient(top, #bbb 0%, #999 100%);
  }
  #nestable2 .dd-handle:hover { background: #bbb; }
  #nestable2 .dd-item > button:before { color: #fff; }
  @media only screen and (min-width: 700px) {
      .dd { float: left; width: 48%; }
      .dd + .dd { margin-left: 2%; }
  }
  .dd-hover > .dd-handle { background: #2ea8e5 !important; }
  /**
   * Nestable Draggable Handles
   */
  .dd3-content { display: block; height: 30px; margin: 5px 0; padding: 5px 10px 5px 40px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
      background: #fafafa;
      background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
      background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
      background:         linear-gradient(top, #fafafa 0%, #eee 100%);
      -webkit-border-radius: 3px;
              border-radius: 3px;
      box-sizing: border-box; -moz-box-sizing: border-box;
  }
  .dd3-content:hover { color: #2ea8e5; background: #fff; }
  .dd-dragel > .dd3-item > .dd3-content { margin: 0; }
  .dd3-item > button { margin-left: 30px; }
  .dd3-handle { position: absolute; margin: 0; left: 0; top: 0; cursor: pointer; width: 30px; text-indent: 100%; white-space: nowrap; overflow: hidden;
      border: 1px solid #aaa;
      background: #ddd;
      background: -webkit-linear-gradient(top, #ddd 0%, #bbb 100%);
      background:    -moz-linear-gradient(top, #ddd 0%, #bbb 100%);
      background:         linear-gradient(top, #ddd 0%, #bbb 100%);
      border-top-right-radius: 0;
      border-bottom-right-radius: 0;
  }
  .dd3-handle:before { content: '≡'; display: block; position: absolute; left: 0; top: 3px; width: 100%; text-align: center; text-indent: 0; color: #fff; font-size: 20px; font-weight: normal; }
  .dd3-handle:hover { background: #ddd; }
  /**
   * Socialite
   */
  .socialite { display: block; float: left; height: 35px; }
</style>

<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Menu</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
          <li class="breadcrumb-item active">Menu</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>

<section class="content">
  <div class="language d-none" data-language="{{ $menu->language }}"></div>
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header bg-info">
            Menu
          </div>
          <div class="card-body">
            <div class="row mb-4">
              <div class="col-md-2">
                 @if(auth()->user()->can('Create Menu'))
                <button type="button" class="btn btn-primary addMenu">Add Menu</button>
                @endif
              </div>
              <div class="col-md-2 offset-md-8">
                <select name="lang" class="form-control" id="change-lang">
                  <option value="" selected disabled>Choose Language</option>
                  <option value="ID">ID</option>
                  <option value="EN">EN</option>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="dd mb-4" id="nestable">
                  <ol class="dd-list first-list"></ol>
                </div>
                
                {{-- <textarea id="nestable-output">{{ $menu->content }}</textarea> --}}
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <button type="button" id="saveMenuDB" class="btn btn-primary">Save</button>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

{{-- Modal Add Menu --}}
<div class="modal fade" id="addMenu" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-primary text-white">
        <h6 class="modal-title" id="modelHeading2">Add Menu</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="menuForm" class="form-horizontal">
          <input type="hidden" name="id" id="id">
            <div class="form-group row">
              <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

              <div class="col-md-6">
                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
              </div>

            </div>
            <div class="form-group row">
              <label for="link" class="col-md-4 col-form-label text-md-right">Link</label>
              
              <div class="col-md-6">
                <input id="link" type="link" class="form-control" name="link" value="{{ old('link') }}" required autocomplete="link">
              </div>

            </div>
          <br>
          <div class="col-sm-offset-2 col-sm-10" style="margin-left:65%">
            <button type="submit" class="btn btn-primary" id="saveMenu" value="create">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

{{-- Modal Edit Menu --}}
<div class="modal fade" id="editMenu" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-primary text-white">
        <h6 class="modal-title" id="modelHeading2">Edit Menu</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="menuForm" class="form-horizontal">
          <input type="hidden" name="id" id="editId">
            <div class="form-group row">
              <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>
              <div class="col-md-6">
                <input id="editName" type="text" class="form-control" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
              </div>
            </div>
            <div class="form-group row">
              <label for="link" class="col-md-4 col-form-label text-md-right">Link</label>
              <div class="col-md-6">
                <input id="editLink" type="link" class="form-control" name="link" value="{{ old('link') }}" required autocomplete="link">
              </div>
            </div>
          <br>
          <div class="col-sm-offset-2 col-sm-10" style="margin-left:65%">
            <button type="submit" class="btn btn-primary" id="updateMenu" value="create">Update</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>


<script src="/assets/plugins/sortable/sortable.min.js"></script>
<script>
  var list_data = {!! $menu->data !!}
  // console.log(data)
  $(document).ready(function() {
    // init data if exist
    initData();

    /**
     * Start Sortable
     */
    var updateOutput = function(e) {
        var list   = e.length ? e : $(e.target),
            output = list.data('output');
            
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };

    // activate Nestable for list 1
    $('#nestable').nestable({
        group: 1
    })
    .on('change', updateOutput);

    // output initial serialised data
    updateOutput($('#nestable').data('output', $('#nestable-output')));
    
    /**
     * End Sortable
     */

    // Click button add menu
    $('.addMenu').click(function(e){
      $('#addMenu').modal('show');
    })

    // click menu item
    $(document).on('click','.edit-menu',function(e){
      
      var name  = $(this).parent().data('name')
      var link  = $(this).parent().data('link')
      var id    = $(this).parent().data('id')

      $('#editId').val(id);
      $('#editName').val(name);
      $('#editLink').val(link);

      $('#editMenu').modal('show')
    })

    // Save Menu
    $('#saveMenu').click(function(e){
      e.preventDefault();

      var random_number = Math.floor(Math.random() * (99999999 - 0 + 1));
      
      var id = $(this).data('id')

      var name = $('#name').val();
      var link = $('#link').val();
      
      let html = '';
          html += '<li class="dd-item menu-item '+ random_number +'-item" data-id="'+ random_number +'" data-name="'+ name +'" data-link="'+ link +'">';
          html += '<div class="dd-handle '+ random_number +'-label">' + name + '</div>';
          html += '<i class="fa fa-trash-alt delete-menu" data-id="' + random_number + '" style="display: block;cursor: pointer;"></i>';
          html += '<i class="fa fa-edit edit-menu" style="display: block;cursor: pointer;"></i>';
          html += '</li>';

      // append 
      $('.first-list').append(html);

      // refresh 
      updateOutput($('#nestable').data('output', $('#nestable-output')));

      // hide modal 
      $('#addMenu').modal('hide');

      // remove again value
      $('#name').val("");
      $('#link').val("");
    });

    // Update Menu
    $('#updateMenu').click(function(e){
      e.preventDefault();

      var selector  = $("#editId").val();
      var name      = $("#editName").val();
      var link      = $("#editLink").val();
      $("." + selector).attr("data-name","wanjay")
      
      // set data
      $("." + selector + "-item").data('id',selector)
      $("." + selector + "-item").data('name',name)
      $("." + selector + "-item").data('link',link)
      $("." + selector + "-label").text(name)
      
      // reset value
      $('#editId').val("");
      $('#editName').val("");
      $('#editLink').val("");

      // Hide modal edit menu
      $('#editMenu').modal('hide')

      // refresh 
      updateOutput($('#nestable').data('output', $('#nestable-output')));
    });

    //save to database
    $('#saveMenuDB').click(function(){

      var code = null;

      switch ($('.language').data('language')) {
        case "EN":
          code      = "002"
          break;
        case "ID":
          code      = "001"
          break;
        default:
          break;
      }
      
      var language  = $('.language').data('language');
      var data      = $('#nestable').nestable('serialize');
      
      $.ajax({
        url: "/{{ $prefix }}/menu/insert",
        type: "POST",
        data: JSON.stringify({ "language": language, "data": data, "code": code }),
        dataType: 'json',
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
          switch (data.code) {
            case 200:
              Swal.fire({
                position: 'top-end',
                type: 'success',
                title: 'Your menu has been saved',
                showConfirmButton: false,
                timer: 1500
              })
              break;
          
            default:
              break;
          }
        }, error: function (data) {
            $('#saveBtn2').html('Save Changes');
        }
      });

    })

    // event when change language
    $('#change-lang').on('change',function(){
      var url = window.location.href;
      
      if (url.indexOf('?') > -1) {
        url = url.substr(0,url.indexOf("?"));

        var searchParams = new URLSearchParams(window.location.search);
        searchParams.set('lang',this.value)
        var newParams = searchParams.toString()
        
        url += '?' + newParams
      } else {
        url += '?lang='  + this.value
      }
      
      window.location.href = url
    });

    // Delete menu
    $('.delete-menu').on('click',function(){
      let id = $(this).data('id')
      
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {

          $('.' + id + '-item').remove();

          Swal.fire(
            'Deleted!',
            'Your menu has been deleted.',
            'success'
          )
        }
      })

      
    })

  });

  function initData() {
    list_data.forEach(function(e) {
      
      html = ''
      html += '<li class="dd-item menu-item '+ e.id +'-item" data-id="'+ e.id +'" data-name="'+ e.name +'" data-link="'+ e.link +'">';

      html += '<div class="dd-handle '+ e.id +'-label">' + e.name + '</div>';
      html += ' @if(auth()->user()->can("Delete Menu"))<i class="fa fa-trash-alt delete-menu" data-id="' + e.id + '" style="display: block;cursor: pointer;"></i>@endif';
      html += ' @if(auth()->user()->can("Edit Menu"))<i class="fa fa-edit edit-menu" style="display: block;cursor: pointer;"></i>@endif';
    
      if(e.children) {
        html += '<ol class="dd-list">'

        e.children.forEach(function(n){
          html += ' <li class="dd-item menu-item '+ n.id +'-item" data-id="'+ n.id +'" data-name="'+ n.name +'" data-link="'+ n.link +'">'
          html += '   <div class="dd-handle '+ n.id +'-label">'+ n.name +'</div>'
          html += '  @if(auth()->user()->can("Delete Menu")) <i class="fa fa-trash-alt delete-menu" data-id="' + n.id + '" style="display: block;cursor: pointer;"></i>@endif';
          html += ' @if(auth()->user()->can("Edit Menu"))<i class="fa fa-edit edit-menu" style="display: block;cursor: pointer;"></i>@endif'

          if(n.children) {
            html += '<ol class="dd-list">'
              n.children.forEach(function(d){
                html += ' <li class="dd-item menu-item '+ d.id +'-item" data-id="'+ d.id +'" data-name="'+ d.name +'" data-link="'+ d.link +'">'
                html += '   <div class="dd-handle '+ d.id +'-label">'+ d.name +'</div>'
                html += ' @if(auth()->user()->can("Delete Menu"))  <i class="fa fa-trash-alt delete-menu" data-id="' + d.id + '" style="display: block;cursor: pointer;"></i>@endif';
                html += '  @if(auth()->user()->can("Edit Menu")) <i class="fa fa-edit edit-menu" style="display: block;cursor: pointer;"></i>@endif'

                if(d.children) {
                  html += '<ol class="dd-list">'
                    d.children.forEach(function(s){
                      html += ' <li class="dd-item menu-item '+ s.id +'-item" data-id="'+ s.id +'" data-name="'+ s.name +'" data-link="'+ s.link +'">'
                      html += '   <div class="dd-handle '+ s.id +'-label">'+ s.name +'</div>'
                      html += ' @if(auth()->user()->can("Delete Menu"))  <i class="fa fa-trash-alt delete-menu" data-id="' + s.id + '" style="display: block;cursor: pointer;"></i>@endif';
                      html += '   @if(auth()->user()->can("Edit Menu"))<i class="fa fa-edit edit-menu" style="display: block;cursor: pointer;"></i>@endif'
                      
                      html += '</li>'
                    })
                  html += '</ol>'
                }

                html += '</li>'
              })
            html += '</ol>'
          }

          html += '</li>'
        })

        html += '</ol>'
      }

      html += '</li>';

      // append 
      $('.first-list').append(html);
    })

    if(getUrlParameter('lang')) {
      $('#change-lang').val(getUrlParameter('lang'));
    } else {
      $('#change-lang').val('ID');
    }

    
  }

  function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
  };
</script>
@endsection

