@extends('layouts.index')
@section('content')
 <link rel="stylesheet" href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css')}}">
    <!-- Content Header (Page header) -->
    <ul class="nav nav-tabs">
   <li class="nav-item d-none d-sm-inline-block">
        <a href="{{ url('/management/menu') }}" class="nav-link">Menu</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{ url('/management/title') }}" class="nav-link active">Title</a>
      </li>
</ul>
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Management Title</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <section class="content">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-info"><i class="fa fa-plus">&nbspAdd Title</i></div>
                 <div class="card-body">
	<form enctype="multipart/form-data" id="productForm" name="productForm">
        {{ csrf_field() }}
        <label>Title</label><br>
		 <input type="text" name="judul" value="{{ old('judul') }}"  class="form-control" placeholder="isi judul..." required>
		 @if($errors->has('judul'))
            <div class="text-danger">
             {{ $errors->first('judul')}}
           </div>
        @endif<br>
        <label>Description</label><br>
		 <textarea name="keterangan"  class="form-control" placeholder="isi keterangan..." required>{{ old('keterangan') }}</textarea>
		<br><br>
		<input type="submit" value="Simpan Data" id="saveBtn" class="btn btn-info btn-sm">
    </form>
    </div>
    </div>
</div></div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-info">Data Title</div>

                <div class="card-body">
    <div class="table-responsive">
	<table  class="table table-striped table-hover table-list tabeljudul">
                  <thead>
                    <tr style="vertical-align:middle;text-align:center;font-weigth:bold">
                        <th>No</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr> 
                  </thead>
                  
                </table>
   </div></div>
        </div>
        </div>
    </div>
 <div class="modal fade" id="ajaxModel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary text-white">
                <h4 class="modal-title" id="modelHeading"></h4>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
        </button>
            </div>
            <div class="modal-body">
                <form id="productForm2" name="productForm2" class="form-horizontal">
                     {{ csrf_field() }}
         <input type="hidden" name="id" id="id">
        <div class="form-group">
       <label class="col-md-4 col-form-label">Title</label><br>
        <div class="col-sm-12">
		<input type="text" name="judul" id="judul" class="form-control" placeholder="isi judul...">
		 @if($errors->has('judul'))
            <div class="text-danger">
             {{ $errors->first('judul')}}
           </div>
        @endif
    </div></div> 
     <div class="form-group">
        <label class="col-md-4 col-form-label">Decription</label><br>
        <div class="col-sm-12">
     <textarea name="keterangan" id="keterangan"  class="form-control" placeholder="isi keterangan..." required></textarea>
         </div></div>
                    <div class="col-sm-offset-2 col-sm-10">
                     <button type="submit" class="btn btn-primary" id="saveBtn2" value="create">Save changes
                     </button>
                    </div>
                </form>
            </div><div class="modal-footer"></div>
        </div>
    </div>
</div>
    </section>
     
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script>
      $(function() {
           $.ajaxSetup({
          headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
          var table_user = $('.tabeljudul').DataTable({
              processing: true,
              serverSide: true,
              ajax: '/management/title/json',
              columns: [
                  { data: 'DT_RowIndex', name:'DT_RowIndex'},
                  { data: 'judul', name: 'judul' },
                  { data: 'keterangan', name: 'keterangan' },
                  {data: 'action', name: 'action', orderable: false, searchable: false}
                  ]
          });
        
          $('body').on('click', '.editProduct', function () {
      var id = $(this).data('id');
          $.get("{{ url('/management/title') }}" +'/' + id +'/edit', function (data) {
          $('#modelHeading').html("Edit Data");
          $('#saveBtn').val("simpan");
          $('#ajaxModel').modal('show');
          $('#id').val(data.id);
          $('#judul').val(data.judul); 
          $('#keterangan').val(data.keterangan);       
       })
   });

          $('#saveBtn').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');
        $.ajax({
          data: $('#productForm').serialize(),
          url: "/management/title/insert",
          type: "POST",
          dataType: 'json',
          success: function (data) {
              $('#productForm').trigger("reset");
                  Swal.fire({
                                type: 'success',
                                title: 'success...',
                                text: 'Data Telah Tersimpan',
                            })
               table_user.ajax.reload();
            },
          error: function (data) {
              console.log('Error:', data);
              $('#saveBtn').html('Save Changes');
          }
          
      });
      
    });
      $('#saveBtn2').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');
        $.ajax({
          data: $('#productForm2').serialize(),
          url: "/management/title/insert",
          type: "POST",
          dataType: 'json',
          success: function (data) {
              $('#productForm2').trigger("reset");
               $('#ajaxModel').modal('hide');
                  Swal.fire({
                                type: 'success',
                                title: 'success...',
                                text: 'Data Telah Tersimpan',
                            })
             table_user.ajax.reload();
            },
          error: function (data) {
              console.log('Error:', data);
              $('#saveBtn2').html('Save Changes');
          }
          
      });
       
    });

     $('body').on('click', '.deleteProduct', function () {
        var id = $(this).data("id");
        $.ajax({
            type: "DELETE",
            url: "/management/title/destroy"+'/'+id,
             success: function(data){
                var json  = JSON.parse(data)

                switch (json.code) {
                  case 200:
                     Swal.fire({
                                type: 'success',
                                title: 'Oops...',
                                text: 'Data has been Deleted!',
                            })
                    break;
                  default:
                    break;
                }

                // refresh datatablesnya kalau sudah menghapus usernya
                table_user.ajax.reload()
              }
           
        });
    });
           });
    </script>
@endsection
