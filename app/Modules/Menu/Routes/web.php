<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::group(['middleware'=> 'auth','prefix' => config('app.app_prefix')], function () {
  Route::get('/title/json','JudulController@json');
  Route::resource('/title','JudulController'); 
  Route::post('/title/insert','JudulController@insert');
  Route::put('/title/edit/{id}', 'JudulController@edit');
  ROUTE::DELETE('/title/destroy/{id}', 'JudulController@destroy');
});

Route::group(['middleware'=> 'auth','prefix' => config('app.app_prefix')], function () {
  Route::group(['prefix' => 'menu'], function () {
    Route::get('/',       'MenuController@index');
    Route::post('/insert', 'MenuController@saveMenu');
  });
});