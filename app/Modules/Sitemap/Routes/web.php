<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/



Route::group(['middleware'=> 'auth','prefix' => config('app.app_prefix')], function () {
      Route::get('/sitemap/json','SiteheaderController@json');
      Route::resource('/sitemap','SiteheaderController'); 
      Route::post('/sitemap/insert','SiteheaderController@insert');
      Route::put('/sitemap/edit/{id}', 'SiteheaderController@edit');
      ROUTE::DELETE('/sitemap/destroy/{id}', 'SiteheaderController@destroy');
      Route::get('/sitemap/show/{id}','SiteheaderController@show'); 
      Route::post('/sitemap/insert_detail','SiteheaderController@insert_detail');
      ROUTE::DELETE('/sitemap/delete/{id}', 'SiteheaderController@delete');
      Route::post('/sitemap/update_detail','SiteheaderController@update_detail');
});