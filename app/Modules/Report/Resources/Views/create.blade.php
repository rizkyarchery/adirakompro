@extends('layouts.index')
@section('content')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" />
<?php 

$PREFIX = config('app.app_prefix');

?>
{{-- <link rel="stylesheet" href="/assets/plugins/icheck-bootstrap/icheck-bootstrap.css"> --}}
<br>
<div class="container">
  <div class="card-header bg-info">Create Finance Report</div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <form action="{{route('report.store')}}" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
               <div class="form-group">
                <label>Language <sup class="text-danger">*</sup></label><br>
                <select name="lang" class="form-control">
                  <option>Choose Language</option>
                  <option value="ID">ID</option>
                  <option value="EN">EN</option>
                </select>
                @if($errors->has('lang'))
                    <div class="text-danger">
                    {{ $errors->first('lang')}}
                  </div>
                @endif
              </div>
              <div class="form-group">
                <label>Name <sup class="text-danger">*</sup></label>
                <input type="text" name="name" class="form-control">
                @if($errors->has('name'))
                    <div class="text-danger">
                    {{ $errors->first('name')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>Finance Report Type <sup class="text-danger">*</sup></label>
                <select name="report_type" class="form-control">
                    <option>Select Finance Report</option>
                    <option>Yearly</option>
                    <option>Quarterly</option>
                </select>
                @if($errors->has('report_type'))
                    <div class="text-danger">
                    {{ $errors->first('report_type')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>Year <sup class="text-danger">*</sup></label>
                <select name="year" class="form-control">
                    <option>Select Year</option>
                    @for($i = 2000;$i <= 2050;$i++)
                    <option value="{{ $i }}">{{ $i }}</option>
                    @endfor
                </select>
                @if($errors->has('year'))
                    <div class="text-danger">
                    {{ $errors->first('year')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>Release Date <sup class="text-danger">*</sup></label>
                <input type="text" name="release_date" id="realase-date" class="form-control" autocomplete="off">
                @if($errors->has('release_date'))
                    <div class="text-danger">
                    {{ $errors->first('release_date')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>PDF File <sup class="text-danger">*</sup></label>&nbsp; <small>Limit Size Upload <span class="text-danger"> 15 Mega Byte</span></small>
                <br />
                <input type="file" name="files" required>
                @if($errors->has('files'))
                    <div class="text-danger">
                    {{ $errors->first('files')}}
                    </div>
                @endif
              </div>
               <div class="form-group">
                <label>Status <sup class="text-danger">*</sup></label>
                <select name="status" id="status" class="form-control @error('status') is-invalid @enderror" >
                    <option value="0" selected disabled>- Choose Status -</option>
                    <option value="1">Active</option>
                    <option value="0">Not Active</option>
                </select>
                @if($errors->has('status'))
                    <div class="text-danger">
                    {{ $errors->first('status')}}
                    </div>
                @endif
              </div>
          </div>
          <div class="card-footer">
          <center>
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="/{{ $PREFIX }}/report" class="btn btn-secondary">Back</a>
          </center>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>
<script src="/assets/plugins/icheck-bootstrap/icheck.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script>
$('#realase-date').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
})
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>
@endsection