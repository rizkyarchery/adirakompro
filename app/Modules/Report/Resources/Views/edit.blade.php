@extends('layouts.index')
@section('content')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" />
<?php 

$PREFIX = config('app.app_prefix');

?>
{{-- <link rel="stylesheet" href="/assets/plugins/icheck-bootstrap/icheck-bootstrap.css"> --}}
<br>
<div class="container">
  <div class="card-header bg-info">Create Finance Report</div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <form action="{{route('report.update', $report->id)}}" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              {{ method_field('PUT') }}
              <div class="form-group">
                <label>Language <sup class="text-danger">*</sup></label><br>
                <select name="lang" class="form-control">
                  @if($report->lang == "ID")
                  <option selected>ID</option>
                  <option>EN</option>
                  @else
                  <option>ID</option>
                  <option selected>EN</option>
                  @endif
                </select>
                @if($errors->has('lang'))
                    <div class="text-danger">
                    {{ $errors->first('lang')}}
                  </div>
                @endif
              </div>
              <div class="form-group">
                <label>Name <sup class="text-danger">*</sup></label>
                <input type="text" name="name" value="{{ $report->name }}" class="form-control">
                @if($errors->has('name'))
                    <div class="text-danger">
                    {{ $errors->first('name')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>Finance Report Type <sup class="text-danger">*</sup></label>
                <select name="report_type" class="form-control">
                    <option>Select Finance Report</option>
                    @if($report->report_type == "Yearly")
                    <option selected>Yearly</option>
                    <option>Quarterly</option>
                    @else
                    <option>Yearly</option>
                    <option selected>Quarterly</option>
                    @endif
                </select>
                @if($errors->has('report_type'))
                    <div class="text-danger">
                    {{ $errors->first('report_type')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>Year <sup class="text-danger">*</sup></label>
                <select name="year" class="form-control">
                    <option></option>
                    @for($i = 2000;$i <= 2050;$i++)
                    <option value="{{ $i }}" @if($i == $report->year) selected @endif>{{ $i }}</option>
                    @endfor
                </select>
                @if($errors->has('year'))
                    <div class="text-danger">
                    {{ $errors->first('year')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>Release Date <sup class="text-danger">*</sup></label>
                <input type="text" name="release_date" value="{{ $report->release_date }}" id="realase-date" class="form-control">
                @if($errors->has('release_date'))
                    <div class="text-danger">
                    {{ $errors->first('release_date')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>PDF File <sup class="text-danger">*</sup></label>&nbsp; <small>Limit Size Upload <span class="text-danger"> 15 Mega Byte</span></small>
                <br />
                <ul class="mailbox-attachments clearfix">
                    <a target="_blank" href="/{{ $report->file }}">
                        <li>
                            <span class="mailbox-attachment-icon"><i class="fa fa-file-pdf"></i></span>
                        </li>
                    </a>
                 </ul>
                <input type="file" name="files">
                @if($errors->has('files'))
                    <div class="text-danger">
                    {{ $errors->first('files')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>Status <sup class="text-danger">*</sup></label>
               <select class="form-control" name="status">
                <option value="1" {{ $report->status == 1 ? 'selected="selected"' : '' }}>Active</option>
                <option value="0" {{ $report->status == 0 ? 'selected="selected"' : '' }}>Not Active</option>
                </select>
                @if($errors->has('status'))
                    <div class="text-danger">
                    {{ $errors->first('status')}}
                    </div>
                @endif
              </div>
              @if($errors->any())
                @foreach ($errors->all() as $error)
                  <div>{{ $error }}</div>
                @endforeach
              @endif
          </div>
          <div class="card-footer">
          <center>
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="/{{ $PREFIX }}/report" class="btn btn-secondary">Back</a>
          </center>
          </div>
        </form>
        </div>
      </div>
      </div>
    </div>
  </div>

<script src="/assets/plugins/icheck-bootstrap/icheck.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script>
$('#realase-date').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
})
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>
@endsection