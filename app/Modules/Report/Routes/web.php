<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Route::group(['prefix' => 'report'], function () {
//     Route::get('/', function () {
//         dd('This is the Report module index page. Build something great!');
//     });
// });

Route::group(['middleware'=> 'auth','prefix' => config('app.app_prefix')], function () {
    Route::get('/report/list', 'ReportController@listAll');
    Route::post('/report/delete/{id}', 'ReportController@delete');
    Route::resource('/report', 'ReportController');
});
