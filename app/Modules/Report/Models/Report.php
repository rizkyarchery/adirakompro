<?php

namespace App\Modules\Report\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    //
    protected $table = 'table_money_report';

    use SoftDeletes;
    protected $dates =['deleted_at'];
}
