<?php

namespace App\Modules\Report\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use DB;
use Validator;
use App\Modules\Report\Models\Report;
use Session;
use Auth;

use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listAll(Request $request){
        if($request->columns[4]['search']['value'] != null) {
             $report = Report::where("lang",$request->columns[4]['search']['value'])->orderBy('id', 'DESC')->get();
        }else{
            $report = Report::orderBy('id', 'DESC')->get(); 
        }

        return DataTables::of($report)
                ->addColumn('action', function($report){
                    $html='';
                 if(auth()->user()->can('Edit Finance Report')){
                    $html = '<a href="/'.config('app.app_prefix').'/report/'.$report->id.'/edit" class="btn btn-primary" >Edit</a>';
                 }
                  if(auth()->user()->can('Delete Finance Report')){
                    $html=$html.' <button id="btn-report" data-id="'. $report->id .'" class="btn btn-danger">Delete</button>';
                  }
                    return $html;
                })
                ->addIndexColumn()
                ->make(true);
    }

    public function index()
    {
        //
        return view('report::list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('report::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'name'          => 'required|min:4',
            'year'          => 'required',
            'release_date'  => 'required',
            'status'        => 'required',
            'lang'          => 'required'
        ]);

        // dd($validator->fails());

        if(!$validator->fails()) {
             //FILE PDF UPLOAD
            $file_pdf = $request->file('files');
            $path_pdf = 'Upload/Report/';
            $file_pdf_name = $path_pdf.$file_pdf->getClientOriginalName();

            $direction_pdf = 'Upload/Report/';
            $request->file('files')->move($direction_pdf, $file_pdf_name);
            //END PDF UPLOAD

            $report = new Report();
            $report->name = $request->name;
            $report->year = $request->year;
            $report->release_date = $request->release_date;
            $report->file = $file_pdf_name;
            $report->updated_by      = Auth::user()->name;
            $report->created_by      = Auth::user()->name;
            $report->report_type     = $request->report_type;
            $report->status          = $request->status;
            $report->lang            = $request->lang;
            $report->save();

            Session::flash('sukses','Finance Report has been inserted!');
            return redirect(config('app.app_prefix') . '/report');
        }

        return redirect(config('app.app_prefix') .'/report/create')
                        ->withErrors($validator)
                        ->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data['report'] = Report::where('id', $id)->first();
        return view('report::edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'name'          => 'required|min:4',
            'year'          => 'required',
            'release_date' => 'required',
            'status'        => 'required',
            'lang'          => 'required'
        ]);

        if(!$validator->fails()){
            $report = Report::find($id);
            if($request->file('files') != null){
                $file_pdf = $request->file('files');
                $path_pdf = 'Upload/Report/';
                $file_pdf_name = $path_pdf.$file_pdf->getClientOriginalName();

                $direction_pdf = 'Upload/Report/';
                $request->file('files')->move($direction_pdf, $file_pdf_name);
                $report->file = $file_pdf_name;
            }
            //END PDF UPLOAD

            $report->name = $request->name;
            $report->year = $request->year;
            $report->release_date = $request->release_date;
            $report->updated_by      = Auth::user()->name;
            $report->report_type     = $request->report_type;
            $report->status          = $request->status;
            $report->lang            = $request->lang;
            $report->save();

            Session::flash('sukses','Finance Report has been edited!');
            return redirect(config('app.app_prefix') . '/report');
        }

        return redirect(config('app.app_prefix') .'/report/'.$id.'/edit')
                        ->withErrors($validator)
                        ->withInput();
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
        $annual = Report::find($id);
        $annual->delete();

        $data = [
            "status" => 200,
            "message" => "Successfully Delete Finance Report"
        ];

        return json_encode($data);
    }
}
