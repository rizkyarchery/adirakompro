<?php

namespace App\Modules\Service\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('service', 'Resources/Lang', 'app'), 'service');
        $this->loadViewsFrom(module_path('service', 'Resources/Views', 'app'), 'service');
        $this->loadMigrationsFrom(module_path('service', 'Database/Migrations', 'app'), 'service');
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('service', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('service', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
