<?php

namespace App\Modules\Service\Models;

use Illuminate\Database\Eloquent\Model;

class Servis extends Model
{
    protected $table = 'service';
    protected $fillable = ['nama','foto','ket','status','link','alt_teks','lang'];
}
