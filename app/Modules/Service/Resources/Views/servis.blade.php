@extends('layouts.index')
@section('content')
<?php

$prefix = config('app.app_prefix');

?>
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css')}}">
 <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Service Management</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
              <li class="breadcrumb-item active">Service</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
     <section class="content">
      <div class="container">
         @if ($message = Session::get('sukses'))
				<div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
					<strong>{{ $message }}</strong>
				</div>
                @endif
                  @if ($message = Session::get('sukses21'))
				<div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
					<strong>{{ $message }}</strong>
				</div>
                @endif
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-info">Data Service</div>
                <div class="card-body">
                    @if(auth()->user()->can('Create Service'))
                  <a class="btn btn-success btn-sm" href="/{{ $prefix }}/sv/tambah" data-toggle="tooltip" title="Add Service"><i class="fa fa-plus">&nbsp Add Service</i></a> @endif <br><hr>
                  <div class="col-md-2">
                       <label>Language</label>
                          <div class="input-group mb-3">
                              <select name="language" id="lang" class="custom-select">
                                <option value="" selected>Select all</option>
                                <option value="ID">ID</option>
                                <option value="EN">EN</option>
                              </select> 
                              <div class="input-group-append">
                              </div>
                            </div>
                          </div>
                      <hr>
  <div class="table-responsive">
	<table  class="table-hover table-striped table-bordered table-list tabelservis">
                  <thead>
                    <tr style="vertical-align:middle;text-align:center;font-weigth:bold">
                        <th>No</th>
                        <th>Name</th>
                        <th>Language</th>
                        <th>Action</th>
                      </tr> 
                  </thead>
                  <tbody><tbody>
                </table>
   </div></div>
             </div>
        </div>
    </div>
</div>
   </section>
   <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
   <script>
        $(function() {
         
          $.ajaxSetup({
          headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
          var table_kat = $('.tabelservis').DataTable({
              processing: true,
              serverSide: true,
              ajax: '/{{ $prefix }}/servis/json',
              columns: [
                  { data: 'DT_RowIndex', name:'DT_RowIndex'},
                  { data: 'nama', name: 'nama' },
                  { data: 'lang', name: 'lang' },
                  {data: 'action', name: 'status', orderable: false, searchable: false}
                   ]
          });
    $('#lang').on('change',function(){
      table_kat.search(this.value).draw();  
     })

           $(document).on('click','#hapusservis',function(){
            // dapetin dulu ID si usernya dari tag element button data-id
            var id = $(this).data('id')

            // kirim data ke server untuk hapus user dengan id 
            $.ajax({
              type: "DELETE",
              url: "/{{ $prefix }}/servis/hapus/" + id,
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              success: function(data){
                var json  = JSON.parse(data)

                switch (json.code) {
                  case 200:
                       Swal.fire({
                                type: 'success',
                                title: 'Success',
                                text: 'Data has been Deleted!',
                            })
                    break;
                  default:
                    break;
                }

                // refresh datatablesnya kalau sudah menghapus usernya
                table_kat.ajax.reload()
              }
            });
          })

      });
   </script>
@endsection
