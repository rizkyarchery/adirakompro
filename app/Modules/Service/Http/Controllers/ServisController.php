<?php

namespace App\Modules\Service\Http\Controllers;

use File;
use Session;
use DataTables;
use App\Modules\Service\Models\Servis;
use App\Modules\Setting\Models\Setting;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class ServisController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
      public function json(request $request){
        if($request->search['value'] != null){
        $servis = Servis::where('lang',$request->search['value'])->orderBy('id','DESC')->get();
        }else{
        $servis = Servis::orderBy('id','DESC')->get();
        }
        return Datatables::of($servis)
         ->addColumn('action', function ($servis) {
              $html='';
                if(auth()->user()->can('Edit Service')){
                    $html = '<center><a href="/'.config('app.app_prefix').'/servis/edit/'.$servis->id.'" data-toggle="tooltip" title="Update" class="btn btn-xs btn-warning" style="margin-bottom:4px;margin-right:2px;"><i class="fa fa-edit"></i></a>';
                }
                 if(auth()->user()->can('Delete Service')){
                    $html=$html.' <a data-id="'.$servis->id.'" class="btn btn-xs btn-danger" style="margin-bottom:4px;" data-toggle="tooltip" title="Delete" id="hapusservis"><i class="fa fa-trash"></i></a></center>';
                 }
                    return $html;
            }
        )
        ->addIndexColumn()
        ->make(true);
    }
     public function index(){
        $servis = Servis::all();
         $settingweb = Setting::find('001');
    	return view('service::servis',['servis' => $servis,'settingweb' => $settingweb]);
    }

     public function tambah(){
        $servis = Servis::all();
         $settingweb = Setting::find('001');
    	return view('service::tambah_servis',['servis' => $servis,'settingweb' => $settingweb]);
  }
  public function insert(Request $request) {
               
    	$this->validate($request,[
            'nama' =>'required',
            'ket' =>'required',
            'foto' =>'required|file|image|mimes:jpeg,png,jpg|max:2048',
            'status' => 'required',
            'lang' => 'required',
            'alt_teks' => 'required',
         ]);

         // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('foto');
        $path       = 'Upload/Service/';
		$nama_file = $path.$file->getClientOriginalName();
        // isi dengan nama folder tempat kemana file diupload
		$tujuan_upload =  $path;
        $request->file('foto')->move($tujuan_upload,$nama_file);
                
        Servis::create([
            'nama' => $request->nama,
            'ket' => $request->ket,
            'foto' => $nama_file,
            'status' => $request->status,
            'link' => '-',
            'alt_teks' => $request->alt_teks,
            'lang' => $request->lang
        ]);
        
         Session::flash('sukses','Service has been Created');
        return redirect(config('app.app_prefix').'/servis');
    }

     public function edit($id){
      $servis = Servis::find($id);
      $settingweb = Setting::find('001');
       return view('service::edit_servis', ['servis' => $servis,'settingweb' => $settingweb]);
    }

     public function update($id, Request $request){
    $this->validate($request,[
	    'nama' =>'required',
        'ket' =>'required',
        'foto' =>'file|image|mimes:jpeg,png,jpg|max:2048',
        'status' => 'required',
        'alt_teks' => 'required',
        'lang' => 'required'
    ]);

    $servis = Servis::find($id);

    if($request->file('foto') == "")
        {
            $servis->foto = $servis->foto;
        } 
        else
        {
            File::delete($servis->foto);
            $file       = $request->file('foto');
            $path       = 'Upload/Service/';
            $fileName   =  $path.$file->getClientOriginalName();
            $request->file('foto')->move($path, $fileName);
            $servis->foto = $fileName;
        }
   
    
	$servis->nama = $request->nama;
	$servis->ket = $request->ket;
    $servis->link = '-';
    $servis->status = $request->status;
    $servis->alt_teks = $request->alt_teks;
    $servis->lang = $request->lang;
    $servis->save();
    Session::flash('sukses21','Service has been Updated');
    return redirect(config('app.app_prefix').'/servis');
}

    public function delete($id) {
        $servis = Servis::find($id);

        File::delete($servis->foto);
         $servis->delete();

        $callback = [
            "message" => "Data has been Deleted",
            "code"   => 200
        ];

        return json_encode($callback, TRUE);
    }
}
