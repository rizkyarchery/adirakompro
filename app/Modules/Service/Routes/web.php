<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/



Route::group(['middleware'=> 'auth','prefix' => config('app.app_prefix')], function () {
    Route::get('/servis/json', 'ServisController@json');
    Route::resource('/servis', 'ServisController');
    Route::get('/sv/tambah','ServisController@tambah');
    Route::post('/servis/insert','ServisController@insert');
    Route::get('/servis/edit/{id}','ServisController@edit');
    Route::put('/servis/update/{id}', 'ServisController@update');
    ROUTE::DELETE('/servis/hapus/{id}', 'ServisController@delete');
});