<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableWorkshop extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('workshop', function (Blueprint $table) {
            //
            $table->string('island');
            $table->integer('province');
            $table->integer('city');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('workshop', function (Blueprint $table) {
            //
            $table->dropColumn('island');
            $table->dropColumn('province');
            $table->dropColumn('city');
        });
    }
}
