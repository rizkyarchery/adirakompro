<?php

namespace App\Modules\Workshop\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Workshop extends Model
{
    protected $table = 'workshop';

    public function provinces(){
        return $this->belongsTo('App\Modules\Province\Models\Province', 'province');
    }

    public function cities(){
        return $this->belongsTo('App\Modules\City\Models\City', 'city');
    }

    use SoftDeletes;
    protected $dates =['deleted_at'];
    
}
