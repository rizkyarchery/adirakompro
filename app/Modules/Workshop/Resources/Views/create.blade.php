
@extends('layouts.index')
@section('content')
<?php 

$PREFIX = config('app.app_prefix');

?>

<br><br>
<div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="card">
          <div class="card-header bg-info">Create Workshop</div>
          <div class="card-body">
            <form action="/{{ $PREFIX }}/workshop" method="post">
		          {{ csrf_field() }}
              <div class="form-group">
                <label>Name <sup class="text-danger">*</sup></label><br>
                <input type="text" name="name" class="form-control" placeholder="fill this name..." value="{{ old('name') }}">
                @if($errors->has('name'))
                    <div class="text-danger">
                    {{ $errors->first('name')}}
                  </div>
                @endif
              </div>
              <div class="form-group">
                <label>Province</label><br>
                <select class="form-control" name="province" id="province">
                    <option>Choose Province</option>
                    @foreach ($provinces as $province)
                    <option value="{{ $province->id }}">{{ $province->province_name }}</option>
                    @endforeach
                </select>
                @if($errors->has('province'))
                    <div class="text-danger">
                    {{ $errors->first('province')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>City</label><br>
                <select class="form-control" name="city" id="city">
                    <option>Choose City</option>
                </select>
                @if($errors->has('city'))
                    <div class="text-danger">
                    {{ $errors->first('city')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>Island</label><br>
                <select class="form-control" name="island">
                    <option>Choose Island</option>
                    <option>Jawa</option>
                    <option>Sumatra</option>
                    <option>Sulawesi</option>
                    <option>Kalimantan</option>
                    <option>Bali</option>
                    <option>Kepulauan Riau</option>
                    <option>Papua</option>
                </select>
                @if($errors->has('island'))
                    <div class="text-danger">
                    {{ $errors->first('island')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>Address</label> <br>
                <textarea name="address" class="form-control" placeholder="fill this address...">{{ old('address') }}</textarea>
                @if($errors->has('address'))
                    <div class="text-danger">
                    {{ $errors->first('address')}}
                  </div>
                @endif
              </div>
              <div class="form-group">
                <label>Phone <sup class="text-danger">*</sup></label> <br>
                <input type="text" name="phone" class="form-control" placeholder="fill this phone..." value="{{ old('phone') }}">
                @if($errors->has('phone'))
                    <div class="text-danger">
                    {{ $errors->first('phone')}}
                  </div>
                @endif
              </div>
              <div class="form-group">
                <label>Authorize</label> <br>
                <input type="text" name="authorize" class="form-control" placeholder="fill this authorize..." value="{{ old('authorize') }}">
                @if($errors->has('authorize'))
                    <div class="text-danger">
                    {{ $errors->first('authorize')}}
                  </div>
                @endif
              </div>
              <div class="form-group">
                  <label>Fax</label> <br>
                  <input type="text" name="fax" class="form-control" placeholder="fill this fax..." value="{{ old('fax') }}">
                  @if($errors->has('fax'))
                      <div class="text-danger">
                      {{ $errors->first('fax')}}
                    </div>
                  @endif
                </div>
              <div class="form-group">
                  <button type="submit" class="btn btn-primary">Save</button>
                  <a href="/{{ $PREFIX }}/workshop" class="btn btn-secondary">Back</a>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<script src="/assets/plugins/icheck-bootstrap/icheck.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#content').summernote({
      height: "300px",
      styleWithSpan: false
    });
  }); 
</script>
<script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

$('#province').change(function(){
      var province_id = $(this).val();

      $.ajax({
        url: '/{{ $PREFIX }}/city/province/'+province_id,
        type: 'GET',
        dataType: 'JSON',
        success: function(response){
          var html = "";
          response.forEach(function(data){
            html += '<option value="'+data.id+'">';
            html += data.city_name;
            html += '</option>';
          })
          $('#city').html(html);
          html = "";
        }
      })
    });

// $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
//   checkboxClass: 'icheckbox_flat-green',
//   radioClass   : 'iradio_flat-green'
// })

</script>
@endsection