<?php

namespace App\Modules\Workshop\Http\Controllers;

use DataTables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Workshop\Models\Workshop;
use App\Modules\Province\Models\Province;
use App\Modules\City\Models\City;
use Validator;
use Auth;

class WorkshopController extends Controller
{

    public function listAll() {
        $workshop = Workshop::with('provinces', 'cities')->get();
        
        return Datatables::of($workshop)
            ->addColumn('action', function ($workshop) {
                $html='';
                  if(auth()->user()->can('Edit Workshop')){
                $html = '<a href="/'.config('app.app_prefix').'/workshop/'.$workshop->id.'/edit" class="btn btn-primary" >Edit</a>';
                  }
                    if(auth()->user()->can('Delete Workshop')){
                $html=$html.' <button id="deleteWorkshop" data-id="'. $workshop->id .'" class="btn btn-danger">Delete</button>';
                    }
                return $html;
           })
        ->addIndexColumn()
        ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("workshop::list");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $provinces = Province::all();

        $data['provinces'] = $provinces;
        return view("workshop::create", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = Validator::make($request->all(), [
            'name'          => 'required|min:6',
            'phone'         => 'required|min:6',
            'address'       => 'required|min:6',
            'province'      => 'required',
            'city'          => 'required',
            'island'        => 'required'
        ]);

        if(!$validatedData->fails()) {
            $workshop                   = new Workshop();
            $workshop->name             = $request->name;
            $workshop->phone            = $request->phone;
            $workshop->address          = $request->address;
            $workshop->fax              = $request->fax;
            $workshop->email            = $request->email;
            $workshop->city             = $request->city;
            $workshop->province         = $request->province;
            $workshop->island           = $request->island;
            $workshop->authorize        = $request->authorize;
            $workshop->updated_by       = Auth::user()->name;
            $workshop->created_by       = Auth::user()->name;
            $workshop->save();
        
            return redirect(config('app.app_prefix') . '/workshop');
        }

        return redirect(config('app.app_prefix') .'/workshop/create')
                        ->withErrors($validatedData)
                        ->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $workshop           = Workshop::where('id', $id)->with(['provinces.cities', 'cities'])->first();
        $data['workshop']   = $workshop;

        $provinces = Province::all();

        $data['provinces'] = $provinces;
        
        return view("workshop::edit",$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = Validator::make($request->all(), [
            'name'          => 'required|min:6',
            'phone'         => 'required|min:6',
            'address'       => 'required|min:6',
            'province'       => 'required',
            'city'       => 'required',
            'island'       => 'required'
        ]);

        if(!$validatedData->fails()) {
            $workshop                   = Workshop::find($id);
            $workshop->name             = $request->name;
            $workshop->phone            = $request->phone;
            $workshop->address          = $request->address;
            $workshop->fax              = $request->fax;
            $workshop->email            = $request->email;
            $workshop->city             = $request->city;
            $workshop->province         = $request->province;
            $workshop->island           = $request->island;
            $workshop->authorize        = $request->authorize;
            $workshop->updated_by       = Auth::user()->name;
            $workshop->save();
        
            return redirect(config('app.app_prefix') . '/workshop');
        }

        return redirect(config('app.app_prefix') .'/workshop/'.$id.'/edit')
                        ->withErrors($validatedData)
                        ->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $workshop = Workshop::find($id);
        $workshop->delete();

        $data = [
            "status" => 200,
            "message" => "Successfully Delete Workshop"
        ];

        return json_encode($data);
    }
}
