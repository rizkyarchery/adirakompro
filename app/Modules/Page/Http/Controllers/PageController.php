<?php

namespace App\Modules\Page\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use DB;
use Str;
use Session;
use Validator;
use Auth;
use App\Modules\Page\Models\Page;
use App\Modules\Fileimagemaster\Models\FileImageMaster;
use App\Modules\Files\Models\Files;
use App\Modules\Award\Models\Award;
use App\Modules\Workshop\Models\Workshop;
use App\Modules\BranchOffice\Models\BranchOffice;
use App\Modules\Structureorganisation\Models\StructureOrganisation;

use App\Http\Controllers\Controller;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listAll(request $request){
        
        if($request['columns'][2]['search']['value'] != null){
            $page = DB::table('page')->where('lang',$request['columns'][2]['search']['value'])->where('deleted_at', '=', null)->orderBy('id','desc')->get();
        }else{
            $page = DB::table('page')->where('deleted_at', '=', null)->orderBy('id','desc')->get();
        }
        return DataTables::of($page)
                ->addColumn('action', function ($page) {
                    
                    if($page->lang == "ID") {

                        $slug = "";
                        if($page->slug != "home-page") {
                            $slug = $page->slug;
                        }

                        $html = '<a href="/'. $slug .'" class="btn btn-secondary" target="_BLANK"><i class="fa fa-eye"></i></a> &nbsp;';

                        $html .= '&nbsp;<a href="/'.config('app.app_prefix').'/page/create_en/'.$page->slug.'" data-toggle="tooltip" title="Create Page in english" class="btn btn-success"><i class="fa fa-plus"></i></a>';
                    } else {

                        $slug = "";
                        if($page->slug != "home-page") {
                            $slug = $page->slug;
                        }

                        $html = '<a href="/en/'. $slug .'" class="btn btn-secondary" target="_BLANK"><i class="fa fa-eye"></i></a> &nbsp;';
                    }

                    if(auth()->user()->can('Edit Page')){
                        $html .= '&nbsp; <a href="/'.config('app.app_prefix').'/page/'.$page->id.'/edit" class="btn btn-primary" ><i class="fa fa-edit"></i></a> &nbsp;';
                    }

                    if(auth()->user()->can('Delete Page')){
                        if($page->slug != "home-page") {
                            $html .= '<button id="btn-page" data-id="'. $page->id .'" class="btn btn-danger"><i class="fa fa-trash"></i></button>';
                        }
                    }

                    if($page->status == 0){
                        $html .= '&nbsp; <button id="btn-status-page" data-id="'. $page->id .'" data-status="1" class="btn btn-warning btn-aja">Show</button>';
                    }else{
                        $html .= '&nbsp; <button id="btn-status-page" data-id="'. $page->id .'" data-status="0" class="btn btn-success">Hide</button>';
                    }
                    
                    return $html;
            })
            ->addIndexColumn()
            ->make(true);
    }

    public function index()
    {
        //
        return view('page::list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['organisations'] = DB::table('table_structure_organisation')->select(DB::raw('position'))->groupBy('position')->get();
        $data['images'] = FileImageMaster::all();
        $data['files'] = Files::all();
        $data['awards'] = DB::table('award')->select(DB::raw('year'))->orderByDesc('year')->get();
        $data['workshops'] = DB::table('workshop')->select(DB::raw('island'))->groupBy('island')->get();
        $data['offices'] = DB::table('branch_office')->select(DB::raw('island'))->groupBy('island')->get();
        return view('page::create', $data);
    }

    public function create_en($id){
         $data['organisations'] = DB::table('table_structure_organisation')->select(DB::raw('position'))->groupBy('position')->get();
        $data['images'] = FileImageMaster::all();
        $data['files'] = Files::all();
        $data['awards'] = DB::table('award')->select(DB::raw('year'))->orderByDesc('year')->get();
        $data['workshops'] = DB::table('workshop')->select(DB::raw('island'))->groupBy('island')->get();
        $data['offices'] = DB::table('branch_office')->select(DB::raw('island'))->groupBy('island')->get();
        $data['page'] = Page::where('slug',$id)->first();     
      return view('page::create_en',$data);
  }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'title'          => 'required',
            'lang'   => 'required',
            // 'link' => 'required',
            'content' => 'required',
            'meta_title' => 'required',
            'meta_description' => 'required',
        ]);

        
        if(!$validator->fails()) {
            if($request->page_parent != null) {
                $slug_page_parent = checkSlug('page',Str::slug($request->title,'-'));
                $pageID = Page::where('slug', $request->page_parent)->first();
                $pageID->page_parent = $slug_page = checkSlug('page',Str::slug($request->title,'-'));
                $pageID->save();
            }

            $page = new Page();
            if($request->file('image') != null){
                $file = $request->file('image');
                $path = 'Upload/Master-image/';
                $file_name = $path.$file->getClientOriginalName();
                
                $direction = 'Upload/Master-image/';
                $request->file('image')->move($direction, $file_name);
                $page->image = $file_name;
            }    
            $page->title = $request->title;
            $page->lang = $request->lang;
            $slug_page = checkSlug('page',Str::slug($request->title,'-'));
            $page->slug = $slug_page;
            // $page->link = "kuda";
            $page->content = $request->content;
            $page->page_parent = $request->page_parent;
            $page->meta_title = $request->meta_title;
            $page->meta_description = $request->meta_description;
            $page->status = 0;
            $page->updated_by      = Auth::user()->name;
            $page->created_by      = Auth::user()->name;
            $page->save();
            
            Session::flash('sukses','Page has been inserted!');
            return redirect(config('app.app_prefix') . '/page');
        }

        return redirect(config('app.app_prefix') .'/page/create')
                        ->withErrors($validator)
                        ->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $page = Page::where('id',$id)->first();
        $data['page'] = $page;
        return view('page::look', $data)->withShortcodes();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data['organisations'] = DB::table('table_structure_organisation')->select(DB::raw('position'))->groupBy('position')->get();
        $data['images'] = FileImageMaster::all();
        $data['files'] = Files::all();
        $page = Page::where('id',$id)->first();
        $data['page'] = $page;
        $data['awards'] = DB::table('award')->select(DB::raw('year'))->orderByDesc('year')->get();
        $data['workshops'] = DB::table('workshop')->select(DB::raw('island'))->groupBy('island')->get();
        $data['offices'] = DB::table('branch_office')->select(DB::raw('island'))->groupBy('island')->get();
        return view('page::edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'title'          => 'required',
            'lang'   => 'required',
            // 'link' => 'required',
            'content' => 'required',
            'meta_title' => 'required',
            'meta_description' => 'required',
        ]);

        $page = Page::find($id);

        if(!$validator->fails()) {
            $file = $request->file('image');
            $page = Page::find($id);
            if($file != null){
                
                $path = 'Upload/Master-image/';
                $file_name = $path.$file->getClientOriginalName();
                
                $direction = 'Upload/Master-image/';
                $request->file('image')->move($direction, $file_name);
                $page->image = $file_name;
            }
            $page->title = $request->title;
            $page->lang = $request->lang;
            // $page->slug = Str::slug($request->title, '-');
            // $page->link = $request->link;
            $page->content = $request->content;
            $page->meta_title = $request->meta_title;
            $page->meta_description = $request->meta_description;
            $page->updated_by      = Auth::user()->name;
            $page->save();
        
            Session::flash('sukses','Page has been updated!');
            return redirect(config('app.app_prefix') . '/page');
        }

        return redirect(config('app.app_prefix') .'/page/'.$id.'/edit')
                        ->withErrors($validator)
                        ->withInput();        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
        $page = Page::find($id);
        $page->delete();

        $data = [
            "status" => 200,
            "message" => "Successfully delete page!"
        ];

        return json_encode($data);
    }

    public function updateStatus($id, $status){
        //dd($id);
        $page = Page::find($id);
        $page->status = $status;
        $page->save();

        if($status == 0){
            $data = [
                "status" => 200,
                "message" => "Successfully hiding the page"
            ];
    
            return json_encode($data);
        }else{
            $data = [
                "status" => 200,
                "message" => "Successfully show the page"
            ];
    
            return json_encode($data);
        }
    }
}
