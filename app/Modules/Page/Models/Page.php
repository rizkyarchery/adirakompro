<?php

namespace App\Modules\Page\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model
{
    //
    protected $table = 'page';
    use SoftDeletes;
    protected $dates =['deleted_at'];
}
