@extends('layouts.index')
@section('content')

<style>

.shortcode-scrollable {
  max-height: 250px;
  height: auto;
  overflow:overlay;
  width:100%;
}

@-moz-document url-prefix() {
  .shortcode-scrollable {
    overflow:auto;
  }
}

</style>

<br />
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-info">Add New Page</div>
                <form action="{{route('page.store')}}" method="post" enctype="multipart/form-data">
                 <div class="card-body">
                    {{ csrf_field() }}
                    {{-- <div class="form-group">
                        <label>Language <sup class="text-danger">*</sup></label><br>
                        <select name="lang" class="form-control" required id="lang">
                            <option>Select Language</option>
                            <option>ID</option>
                            <option>EN</option>
                        </select>
                        @if($errors->has('lang'))
                        <div class="text-danger">
                        {{ $errors->first('lang')}}
                        </div>
                        @endif
                    </div> --}}
                    <input type="hidden" value="ID" name="lang" id="lang">
                    <div class="form-group">
                        <label>Title <sup class="text-danger">*</sup></label><br>
                        <input type="text" name="title" class="form-control" required>
                        @if($errors->has('title'))
                        <div class="text-danger">
                        {{ $errors->first('title')}}
                        </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Image Banner</label>&nbsp;<small>Recommended Size<span class="text-danger"> 1600 x 600</span></small></small><br>
                        <div class="custom-file">
                        <input type="file" name="image" class="custom-file-input" id="customFile" value="{{ old('image') }}">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                        @if($errors->has('image'))
                        <div class="text-danger">
                        {{ $errors->first('image')}}
                        </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Meta Title <sup class="text-danger">*</sup></label><br>
                        <input  type="text" 
                                name="meta_title" 
                                class="form-control" 
                                value="{{ old('meta_title') }}"
                                required>
                        @if($errors->has('meta_title'))
                          <div class="text-danger">
                            {{ $errors->first('meta_title')}}
                          </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Meta Description <sup class="text-danger">*</sup></label><br>
                        <textarea   name="meta_description" 
                                    class="form-control" 
                                    id="meta-title"
                                    required>{{ old('meta_description') }}</textarea>
                        <div id="the-count">
                            <span id="current">0</span>
                            <span id="maximum">/ 100</span>
                        </div>
                        @if($errors->has('meta_description'))
                        <div class="text-danger">
                        {{ $errors->first('meta_description')}}
                        </div>
                        @endif
                    </div>
                    {{-- <div class="form-group">
                        <label>Link</label><br>
                        <input type="text" name="link" class="form-control" value="{{ old('link') }}">
                        @if($errors->has('link'))
                        <div class="text-danger">
                        {{ $errors->first('link')}}
                        </div>
                        @endif
                    </div> --}}
                    <div class="form-group">
                        <label>Content <sup class="text-danger">*</sup></label>
                        <textarea name="content" class="form-control" id="content">{{ old('content') }}</textarea>
                        @if($errors->has('content'))
                        <div class="text-danger">
                            {{ $errors->first('content')}}
                        </div>
                        @endif
                    </div>
                </div>
                <div class="card-footer">
                    <center>
                    <button type="submit" class="btn btn-primary">Save</button>
                    </center>
                </div>
                </form>
            </div>
        </div>
        <div class="col-md-3">
          <div class="card" style="position:fixed;width:300px;">
            <div class="card-header bg-info"></div>
            <div class="card-body">
              <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                  <li class="nav-item has-treeview">
                      <a href="#" class="nav-link">
                        <p>
                          Accordion
                          <i class="right fa fa-angle-left"></i>
                        </p>
                      </a>
                      <ul class="nav nav-treeview shortcode-scrollable">
                        <li class="nav-item">
                          <p>
                            <a id="accordion" class="btn btn-default btn-sm">Accordion</a>
                            <a id="accordion-item" class="btn btn-default btn-sm">Accordion Item</a>
                          </p>
                        </li>
                        <li class="nav-item">
                          <p>
                            <a id="accordion-image" data-id="accordion-image" class="btn btn-default btn-sm">Accordion Image</a>
                            <a id="accordion-text" class="btn btn-default btn-sm">Accordion Text</a>
                          </p>
                        </li>
                        <li class="nav-item">
                          <p>
                            <a id="accordion-button" class="btn btn-default btn-sm">Accordion Button</a>
                          </p>
                        </li>
                        <li class="nav-item">
                          <p>
                            <a id="award" class="btn btn-default btn-sm">Accordion Award</a>
                            <a id="accordion-annual-report" class="btn btn-default btn-sm">Accordion Annual Report</a>
                          </p>
                        </li>
                        <li class="nav-item">
                          <p>
                            <a id="accordion-management" data-id="accordion-management" class="btn btn-default btn-sm">Accordion Management Item</a>
                          </p>
                        </li>
                        <li class="nav-item">
                          <p>
                            <a id="accordion-finance-report" class="btn btn-default btn-sm">Accordion Finance Report</a>
                          </p>
                        </li>
                        <li class="nav-item">
                          <p>
                            <a id="accordion-workshop" class="btn btn-default btn-sm">Accordion Workshop</a>
                          </p>
                        </li>
                        <li class="nav-item">
                          <p>
                            <a id="accordion-branch" class="btn btn-default btn-sm">Accordion Office</a>
                          </p>
                        </li>
                      </ul>
                  </li>
                  <li class="nav-item has-treeview">
                      <a href="#" class="nav-link">
                        <p>
                          Section
                          <i class="right fa fa-angle-left"></i>
                        </p>
                      </a>
                      <ul class="nav nav-treeview shortcode-scrollable">
                          <li class="nav-item">
                              <p>
                                  <a id="section" class="btn btn-default btn-sm">Section</a>
                                  <a id="section-half" class="btn btn-default btn-sm">Section Item Half</a>
                              </p>
                          </li>
                          <li class="nav-item">
                              <p>
                                  <a id="section-text" class="btn btn-default btn-sm">Section Text</a>
                                  <a id="section-image" data-id="section-image" class="btn btn-default btn-sm">Section Image</a>
                              </p>
                          </li>
                      </ul>
                  </li>
                  <li class="nav-item has-treeview">
                      <a href="#" class="nav-link">
                          <p>
                              Gallery
                              <i class="right fa fa-angle-left"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview shortcode-scrollable">
                          <li class="nav-item">
                              <p>
                                  <a id="gallery" class="btn btn-default btn-sm">Gallery</a>
                                  <a id="gallery-item" data-id="gallery-item" class="btn btn-default btn-sm">Gallery Item</a>
                                  <a id="gallery-item-service" data-id="gallery-item-service" class="btn btn-default btn-sm">Gallery Item Service</a>
                              </p>
                          </li>
                      </ul>
                  </li>
                  <li class="nav-item has-treeview">
                      <a href="#" class="nav-link">
                          <p>
                              Sitemap
                              <i class="right fa fa-angle-left"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview shortcode-scrollable">
                          <li class="nav-item">
                              <p>
                                  <a id="sitemap-container" class="btn btn-default btn-sm">Sitemap Container</a>
                                  <a id="sitemap" class="btn btn-default btn-sm">Sitemap</a>
                                  
                              </p>
                          </li>
                          <li class="nav-item">
                              <p>
                                  <a id="sitemap-item" class="btn btn-default btn-sm">Sitemap Item</a>
                                  <a id="sitemap-item-bold" class="btn btn-default btn-sm">Sitemap Item Bold</a>
                              </p>
                          </li>
                      </ul>
                  </li>
                  <li class="nav-item has-treeview">
                      <a href="#" class="nav-link">
                          <p>
                              Other
                              <i class="right fa fa-angle-left"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview shortcode-scrollable">
                          <li class="nav-item">
                              <p>
                                  <a id="tabs" class="btn btn-default btn-sm">Tab</a>
                                  <a id="tab-item" class="btn btn-default btn-sm">Tab Item</a>
                              </p>
                          </li>
                          <li class="nav-item">
                              <p>
                                  <a id="testimony" class="btn btn-default btn-sm">Testimony</a>
                                  <a id="files" class="btn btn-default btn-sm">Files</a>
                              </p>
                          </li>
                          <li class="nav-item">
                              <p>
                                  <a id="find-office" class="btn btn-default btn-sm">Find Office Function</a>
                                </p>
                          </li>
                          <li class="nav-item">
                              <p>
                                  <a id="find-bengkel" class="btn btn-default btn-sm">Find Workshop Function</a>
                              </p>
                          </li>
                          <li class="nav-item">
                              <p>
                                  <a id="career" class="btn btn-default btn-sm">Career</a>
                                  <a id="contact-form" class="btn btn-default btn-sm">Contact Form</a>
                              </p>
                          </li>
                          <li class="nav-item">
                              <p>
                                  <a id="yellow-button" class="btn btn-default btn-sm">Yellow Download Button</a>
                              </p>
                          </li>
                          <li class="nav-item">
                              <p>
                                  <a id="product" class="btn btn-default btn-sm">Product Carousel</a>
                              </p>
                          </li>
                          <li class="nav-item">
                              <p>
                                  <a id="list-parent" class="btn btn-default btn-sm">List Parent</a>
                                  <a id="list-child" class="btn btn-default btn-sm">List Child</a>
                              </p>
                          </li>
                      </ul>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
    </div>
</div>
<!-- image modal -->
<div class="modal fade" id="modal-default">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
              </div>
              <div class="modal-body">
                <input type="hidden" id="val-id">
                <div class="row">
                @foreach($images as $image)
                <div class="col-md-3 col-sm-4 col-xs-6" style="overflow: overlay;">
                    <a class="image-src-modal" data-src="/{{ $image->image }}">
                        <img src="/{{ $image->image }}" style="width:100%;margin-bottom: 5px;" />
                    </a>
                </div>
                @endforeach
                </div>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

<!-- image modal -->
<div class="modal fade" id="modal-default-service">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <input type="hidden" id="val-id">
        <div class="row">
        @foreach($images as $image)
        <div class="col-md-3 col-sm-4 col-xs-6" style="overflow: overlay;">
            <a class="image-src-modal-service" data-src="/{{ $image->image }}">
                <img src="/{{ $image->image }}" style="width:100%;margin-bottom: 5px;" />
            </a>
        </div>
        @endforeach
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<!-- image modal -->
<div class="modal fade" id="modal-files">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
              </div>
              <div class="modal-body">
                <div class="row">
                @foreach($files as $file)
                <div class="col-md-1 col-sm-4 col-xs-6" style="overflow: overlay;">
                    <a class="file-src-modal" data-src="/{{ $file->foto }}">
                        <img src="/assets/files.png" style="height:50px;width:50px;"/>
                        <p>{{ $file->alt_teks }}</p>
                    </a>
                </div>
                @endforeach
                </div>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<!-- /.modal -->

<!-- image modal -->
<div class="modal fade" id="modal-files-product">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
              </div>
              <div class="modal-body">
                <div class="row">
                @foreach($files as $file)
                <div class="col-md-1 col-sm-4 col-xs-6" style="overflow: overlay;">
                    <a class="file-download-yellow" data-src="/{{ $file->foto }}">
                        <img src="/assets/files.png" style="height:50px;width:50px;"/>
                        <p>{{ $file->alt_teks }}</p>
                    </a>
                </div>
                @endforeach
                </div>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<!-- /.modal -->

<!-- structure modal -->
<div class="modal fade" id="modal-structure">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
              </div>
              <div class="modal-body">
                <div class="row">
                @foreach($organisations as $org)
                    <div class="col-md-3 col-sm-4 col-xs-6" style="overflow: overlay;">
                        <a class="btn btn-default btn-md structure-modal" data-category="{{ $org->position }}">
                        {{ $org->position }}
                        </a>
                    </div>
                @endforeach
                </div>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<!-- /.modal -->
<!-- Award modal -->
<div class="modal fade" id="modal-award">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
              </div>
              <div class="modal-body">
                <div class="row">
                    @foreach($awards as $award)
                    <div class="col-md-1 col-sm-4 col-xs-6" style="overflow: overlay;">
                        <a class="btn btn-default btn-md award-year" data-year="{{ $award->year }}">
                        {{ $award->year }}
                        </a>
                    </div>
                    @endforeach
                </div>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<!-- /.modal -->
<!-- Workshop modal -->
<div class="modal fade" id="modal-workshop">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
              </div>
              <div class="modal-body">
                <div class="row">
                    @foreach($workshops as $workshop)
                    <div class="col-md-3 col-sm-4 col-xs-6" style="overflow: overlay;">
                        <a class="btn btn-default btn-md workshop-island" data-workshop="{{ $workshop->island }}">
                        {{ $workshop->island }}
                        </a>
                    </div>
                    @endforeach
                </div>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<!-- /.modal -->
<!-- Offices modal -->
<div class="modal fade" id="modal-office">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
              </div>
              <div class="modal-body">
                <div class="row">
                    @foreach($offices as $office)
                    <div class="col-md-2" style="overflow: overlay;">
                        <a class="btn btn-default btn-md office-island" data-office="{{ $office->island }}">
                        {{ $office->island }}
                        </a>
                    </div>
                    @endforeach
                </div>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<!-- /.modal -->
<!-- Offices modal -->
<div class="modal fade" id="modal-report">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
              </div>
              <div class="modal-body">
                 <div class="row">
                    <div class="col-md-2" style="overflow: overlay;">
                        <a class="btn btn-default btn-md btn-report" data-report="Yearly">
                        Yearly
                        </a>
                    </div>
                    <div class="col-md-2" style="overflow: overlay;">
                        <a class="btn btn-default btn-md btn-report" data-report="Quarterly">
                        Quarterly
                        </a>
                    </div>
                </div>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
<!-- /.modal -->
<script type="text/javascript">
  $(document).ready(function() {
    $('#content').summernote({
      height: "500px",
      styleWithSpan: false
    });

    $(document).on("click", ".image-src-modal", function(event){
        var src = $(this).data('src');
        var idsec = $('#val-id').val();
        $("#content").summernote('pasteHTML', '['+idsec+' link="'+src+'"] [/'+idsec+']');
        $('#modal-default').modal('hide');
    });
    $(document).on("click", ".image-src-modal-service", function(event){
        var src = $(this).data('src');
        var idsec = $('#val-id').val();
        $("#content").summernote('pasteHTML', '['+idsec+' img="'+src+'" title="" desc="" link="" ] [/'+idsec+']');
        $('#modal-default-service').modal('hide');
    });

    var fin_num = 1;
    $(document).on("click", ".btn-report", function(event){
        var src = $(this).data('report');
        var lang = $('#lang').val()
        var idsec = $('#val-id').val();
        $("#content").summernote('pasteHTML', '[accordion-finance-report type="'+src+'" lang="'+lang+'" from="" to="" num="'+fin_num+'"] [/accordion-finance-report]');
        $('#modal-report').modal('hide');
        fin_num++
    });

    $(document).on("click", ".file-src-modal", function(event){
        var src = $(this).data('src');
        $("#content").summernote('pasteHTML', '[download link="'+src+'" title=""] [/download]');
        $('#modal-files').modal('hide');
    });

    $('#accordion').click(function(){
        $("#content").summernote('pasteHTML', "[accordion] [/accordion]");
    })

    $('#accordion-image').click(function(){
        $('#val-id').val($(this).data('id'));
        $('#modal-default').modal('show');
        //$("#content").summernote('pasteHTML', '[accordion-image link=""] [/accordion-image]');
    })

    $('#accordion-text').click(function(){
        $("#content").summernote('pasteHTML', "[accordion-text] [/accordion-text]");
    })

    $('#accordion-button').click(function(){
        $("#content").summernote('pasteHTML', '[accordion-button link=""] [/accordion-button]');
    })

    $('#txt').click(function(){
        $("#content").summernote('pasteHTML', "[text] [/text]");
    })

    $('#banner').click(function(){
        $("#content").summernote('pasteHTML', "[banner] [/banner]");
    })

    var num = 1;
    $('#accordion-item').click(function(){
        $("#content").summernote('pasteHTML', '[accordion-item title="" id="heading_'+ num +'" item="collapse_'+ num +'"] [/accordion-item]');
        num++
    })

    $('#title').click(function(){
        $("#content").summernote('pasteHTML', '[title] [/title]');
    })

    $('#bold').click(function(){
        $("#content").summernote('pasteHTML', '[b] [/b]');
    })

    var num_section = 1;
    $('#section').click(function(){
        $('#content').summernote('pasteHTML', '[section col="'+num_section+'" title="" description=""] [/section]');
        num_section++;
    })

    $('#section-half').click(function(){
        $('#content').summernote('pasteHTML', '[section-half] [/section-half]');
    })

    $('#section-image').click(function(){
        $('#val-id').val($(this).data('id'));
        $('#modal-default').modal('show');
        //$('#content').summernote('pasteHTML', '[section-image link=""] [/section-image]');
    })

    $('#section-text').click(function(){
        $('#content').summernote('pasteHTML', '[section-text button="" link=""] [/section-text]');
    })

    $('#gallery').click(function(){
        $('#content').summernote('pasteHTML', '[gallery] [/gallery]');
    })

    $('#gallery-item').click(function(){
        $('#val-id').val($(this).data('id'));
        $('#modal-default').modal('show');
    })

    $('#gallery-item-service').click(function(){
      $('#val-id').val($(this).data('id'));
      $('#modal-default-service').modal('show');
    })

    $('#testimony').click(function(){
        $('#content').summernote('pasteHTML', '[testimony] [/testimony]');
    })

    $('#award').click(function(){
        //$('#content').summernote('pasteHTML', '[award] [/award]');
        $('#modal-award').modal('show')
    })

    $('.award-year').click(function(){
        var year = $(this).data('year');
        var lang = $('#lang').val()
        $('#content').summernote('pasteHTML', '[award year="'+year+'" lang="'+lang+'"] [/award]');
        $('#modal-award').modal('hide')
    })

    var ann_num = 1;
    $('#accordion-annual-report').click(function(){
         var lang = $('#lang').val()
        $('#content').summernote('pasteHTML', '[accordion-annual-report from="" to="" lang="'+lang+'" num="'+ann_num+'"] [/accordion-annual-report]');
        ann_num++;
    })

    $('#accordion-management').click(function(){
        $('#modal-structure').modal('show');
    })

    $('.structure-modal').click(function(){
        var category_position = $(this).data('category');
        var lang = $('#lang').val()
        $('#content').summernote('pasteHTML', '[accordion-management lang="'+lang+'" position="'+category_position+'"] [/accordion-management]');
        $('#modal-structure').modal('hide');
    })

    $('#accordion-finance-report').click(function(){
        // $('#content').summernote('pasteHTML', '[accordion-finance-report from="" to="" num="'+fin_num+'"] [/accordion-finance-report]');
        // fin_num++;
       $('#modal-report').modal('show');
    })

    $('#tabs').click(function(){
        $('#content').summernote('pasteHTML', '[tab] [/tab]');
    });

    var tab_item_num = 1;
    $('#tab-item').click(function(){
        $('#content').summernote('pasteHTML', '[tab-item num="'+tab_item_num+'" title=""] [/tab-item]');
        tab_item_num++;
    })

    $('#files').click(function(){
        $('#modal-files').modal('show');
    })

    $('#career').click(function(){
        $('#content').summernote('pasteHTML', '[career] [/career]');
    })

    $('#accordion-workshop').click(function(){
        $('#modal-workshop').modal('show');
    })

    $('.workshop-island').click(function(){
        var workshop = $(this).data('workshop');
        var lang = $('#lang').val()
        $('#content').summernote('pasteHTML', '[accordion-workshop lang="'+lang+'" island="'+workshop+'"] [/accordion-workshop]');
        $('#modal-workshop').modal('hide');
    })

    $('#find-bengkel').click(function(){
        var lang = $('#lang').val();
        $('#content').summernote('pasteHTML', '[find-workshop lang="'+lang+'"] [/find-workshop]');
    })

    $('#accordion-branch').click(function(){
        $('#modal-office').modal('show');
    })

    $('.office-island').click(function(){
        var office = $(this).data('office');
        var lang = $('#lang').val()
        $('#content').summernote('pasteHTML', '[accordion-office lang="'+lang+'" island="'+office+'"] [/accordion-office]');
        $('#modal-office').modal('hide');
    })

    $('#sitemap-container').click(function(){
        $('#content').summernote('pasteHTML', '[sitemap-container] [/sitemap-container]');
    })

    $('#sitemap').click(function(){
        $('#content').summernote('pasteHTML', '[sitemap] [/sitemap]');
    })

    $('#sitemap-item').click(function(){
        $('#content').summernote('pasteHTML', '[sitemap-item link=""] [/sitemap-item]');
    })

    $('#sitemap-item-bold').click(function(){
        $('#content').summernote('pasteHTML', '[sitemap-item-bold] [/sitemap-item-bold]');
    })

    $('#contact-form').click(function(){
        $('#content').summernote('pasteHTML', '[contact-form] [/contact-form]');
    })

    $('#yellow-button').click(function(){
        //$('#content').summernote('pasteHTML', '[yellow-button] [/yellow-button]');
        $('#modal-files-product').modal('show');
    })

    $('.file-download-yellow').click(function(){
        var file = $(this).data('src');
        $('#content').summernote('pasteHTML', '[yellow-button link="'+file+'" title=""] [/yellow-button]');
        $('#modal-files-product').modal('hide');
    })

    $('#product').click(function(){
        $('#content').summernote('pasteHTML', '[product] [/product]');
    })

    $('#find-office').click(function(){
        var lang = $('#lang').val();
        $('#content').summernote('pasteHTML', '[find-office lang="'+lang+'"] [/find-office]');
    })

    $('#list-parent').click(function(){
      $('#content').summernote('pasteHTML', '[list-parent] [/list-parent]');
    })

    $('#list-child').click(function(){
      $('#content').summernote('pasteHTML', '[list-child] [/list-child]');
    })
    
  }); 
</script>
<script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>
<script>
// Add the following code if you want the name of the file appear on select
$(".upload").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".lblupload").addClass("selected").html(fileName);
});

$('#meta-title').keyup(function() {
    //alert($(this).val());
    var characterCount = $(this).val().length,
    current = $('#current'),
    maximum = $('#maximum'),
    theCount = $('#the-count');
  
    current.text(characterCount);
   
    
    /*This isn't entirely necessary, just playin around*/
    if (characterCount < 70) {
      current.css('color', '#666');
    }
    if (characterCount > 70 && characterCount < 90) {
      current.css('color', 'blue');
    }
    if (characterCount > 90 && characterCount <= 100) {
      current.css('color', 'red');
    }
    if (characterCount > 100) {
      current.css('color', 'red');
      var tval = $(this).val();
      $('#meta-title').val((tval).substring(0, characterCount - 1))
    }
        
  });
  
</script>
@endsection