<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Route::group(['prefix' => 'page'], function () {
//     Route::get('/', function () {
//         dd('This is the Page module index page. Build something great!');
//     });
// });
Route::group(['middleware'=> 'auth','prefix' => config('app.app_prefix')], function () {
    Route::get('/page/list', 'PageController@listAll');
    Route::get('/page/status/{id}/{status}', 'PageController@updateStatus');
    Route::post('/page/delete/{id}', 'PageController@delete');
    Route::get('/page/create_en/{id}','PageController@create_en');
    Route::resource('/page', 'PageController');
   });
