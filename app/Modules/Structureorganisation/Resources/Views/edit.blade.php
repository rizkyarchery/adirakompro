@extends('layouts.index')
@section('content')
<?php 

$PREFIX = config('app.app_prefix');

?>
{{-- <link rel="stylesheet" href="/assets/plugins/icheck-bootstrap/icheck-bootstrap.css"> --}}
<br>
<div class="container">
  <div class="card-header bg-info">Edit Direction Structure</div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <form action="{{route('structure-organisation.update', $structure->id)}}" method="post" enctype="multipart/form-data">
		          {{ csrf_field() }}
                  {{ method_field('PUT') }}
              <div class="form-group">
                <label>Language <sup class="text-danger">*</sup></label><br>
                <select name="lang" class="form-control">
                  @if($structure->lang == "ID")
                  <option selected>ID</option>
                  <option>EN</option>
                  @else
                  <option>ID</option>
                  <option selected>EN</option>
                  @endif
                </select>
                @if($errors->has('lang'))
                    <div class="text-danger">
                    {{ $errors->first('lang')}}
                  </div>
                @endif
              </div>
              <div class="form-group">
                <label>Position category <sup class="text-danger">*</sup></label>
                <input type="text" name="position" value="{{ $structure->position }}" class="form-control">
                @if($errors->has('position_name'))
                    <div class="text-danger">
                    {{ $errors->first('position_name')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>Position Name <sup class="text-danger">*</sup></label>
                <input type="text" name="position_name" value="{{ $structure->position_name }}" class="form-control">
                @if($errors->has('position_name'))
                    <div class="text-danger">
                    {{ $errors->first('position_name')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>Name <sup class="text-danger">*</sup></label>
                <input type="text" name="name" class="form-control" value="{{ $structure->name }}">
                @if($errors->has('name'))
                    <div class="text-danger">
                    {{ $errors->first('name')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>Description <sup class="text-danger">*</sup></label>
                <textarea name="description" class="form-control" rows="3">{{ $structure->description }}</textarea>
                @if($errors->has('name'))
                    <div class="text-danger">
                    {{ $errors->first('name')}}
                    </div>
                @endif
              </div>
              <div class="form-group">
                <label>Image <sup class="text-danger">*</sup></label>&nbsp;<small>Recommended Size<span class="text-danger"> 300 x 300</span></small></small>
                <br />
                <img src="/{{ $structure->image }}" style="width:300px;height:300px;">
                <br />
                <br />
                <div class="custom-file">
                <input type="file" name="image" class="custom-file-input">
                <label class="custom-file-label" for="customFile">Choose file</label>
                </div>
                @if($errors->has('image'))
                    <div class="text-danger">
                    {{ $errors->first('image')}}
                    </div>
                @endif
              </div>
          </div>
          <div class="card-footer">
          <center>
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="/{{ $PREFIX }}/structure-organisation" class="btn btn-secondary">Back</a>
          </center>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>
<script src="/assets/plugins/icheck-bootstrap/icheck.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#content').summernote({
      height: "300px",
      styleWithSpan: false
    });
  }); 
</script>
<script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

// $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
//   checkboxClass: 'icheckbox_flat-green',
//   radioClass   : 'iradio_flat-green'
// })

</script>
@endsection