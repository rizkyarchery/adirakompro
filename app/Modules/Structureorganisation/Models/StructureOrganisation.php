<?php

namespace App\Modules\Structureorganisation\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StructureOrganisation extends Model
{
    //
    protected $table = 'table_structure_organisation';

    use SoftDeletes;
    protected $dates =['deleted_at'];
}
