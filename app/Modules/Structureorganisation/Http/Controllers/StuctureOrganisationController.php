<?php

namespace App\Modules\Structureorganisation\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use DB;
use Str;
use Auth;
use Session;
use App\Modules\Structureorganisation\Models\StructureOrganisation;
use App\Http\Controllers\Controller;
use Validator;

class StuctureOrganisationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listAll(){
        $stuctures =  StructureOrganisation::orderBy('id', 'DESC')->get();
        
        return DataTables::of($stuctures)
                ->addColumn('action', function($stuctures){
                    $html='';
                     if(auth()->user()->can('Edit Organizational Structure')){
                    $html = '<a href="/'.config('app.app_prefix').'/structure-organisation/'.$stuctures->id.'/edit" class="btn btn-primary" >Edit</a>';
                }
                 if(auth()->user()->can('Delete Organizational Structure')){
                    $html=$html.' <button id="btn-structure" data-id="'. $stuctures->id .'" class="btn btn-danger">Delete</button>';
                 }
                    return $html;           
                })
                ->addIndexColumn()
                ->make(true);
    }

    public function index() {
        //
        return view('structureorganisation::list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
        return view('structureorganisation::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
        $validator = Validator::make($request->all(),[
            'position' => 'required',
            'position_name' => 'required',
            'name' => 'required',
            'description' => 'required',
            'lang'      =>'required',
            'image' =>'required|file|image|mimes:jpeg,png,jpg|max:2048'
        ]);

        if(!$validator->fails()){
            $file = $request->file('image');
            $path = 'Upload/Structure/';
            $file_name = $path.$file->getClientOriginalName();
            
            $direction = 'Upload/Structure/';
            $request->file('image')->move($direction, $file_name);

            $structure = new StructureOrganisation();
            $structure->position = $request->position;
            $structure->position_name = $request->position_name;
            $structure->name = $request->name;
            $structure->lang = $request->lang;
            $structure->description = $request->description;
            $structure->image = $file_name;
            $structure->updated_by      = Auth::user()->name;
            $structure->created_by      = Auth::user()->name;
            $structure->save();

            Session::flash('sukses','Structure organisation has been inserted!');
            return redirect(config('app.app_prefix') . '/structure-organisation');
        }

        return redirect(config('app.app_prefix') .'/structure-organisation/create')
                        ->withErrors($validator)
                        ->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $structure = StructureOrganisation::where('id',$id)->first();
        $data['structure'] = $structure;
        return view('structureorganisation::edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(),[
            'position' => 'required',
            'position_name' => 'required',
            'name' => 'required',
            'description' => 'required',
             'lang' => 'required'
         ]);

        if(!$validator->fails()){
            $structure = StructureOrganisation::find($id);
            if($request->file('image') != null){
                $file = $request->file('image');
                $path = 'Upload/Structure/';
                $file_name = $path.$file->getClientOriginalName();
                
                $direction = 'Upload/Structure/';
                $request->file('image')->move($direction, $file_name);
                $structure->image = $file_name;
            }

            $structure->lang = $request->lang;
            $structure->position = $request->position;
            $structure->position_name = $request->position_name;
            $structure->name = $request->name;
            $structure->description = $request->description;
            $structure->updated_by      = Auth::user()->name;
            $structure->save();

            Session::flash('sukses','Structure organisation has been update!');
            return redirect(config('app.app_prefix') . '/structure-organisation');
        }

        return redirect(config('app.app_prefix') .'/structure-organisation/'.$id.'/edit')
                        ->withErrors($validator)
                        ->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
        $file = StructureOrganisation::find($id);
        $file->delete();

        $data = [
            "status" => 200,
            "message" => "Successfully Delete Structure"
        ];

        return json_encode($data);
    }
}
