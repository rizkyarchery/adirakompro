<?php

namespace App\Modules\Structureorganisation\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('structureorganisation', 'Resources/Lang', 'app'), 'structureorganisation');
        $this->loadViewsFrom(module_path('structureorganisation', 'Resources/Views', 'app'), 'structureorganisation');
        $this->loadMigrationsFrom(module_path('structureorganisation', 'Database/Migrations', 'app'), 'structureorganisation');
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('structureorganisation', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('structureorganisation', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
