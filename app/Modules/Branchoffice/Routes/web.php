<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Route::group(['prefix' => 'branchoffice'], function () {
//     Route::get('/', function () {
//         dd('This is the BranchOffice module index page. Build something great!');
//     });
// });

Route::group(['middleware'=> 'auth','prefix' => config('app.app_prefix')], function () {
    Route::get('/branchoffice/list', 'BranchOfficeController@listAll');
    Route::post('/branchoffice/delete/{id}', 'BranchOfficeController@delete');
    Route::resource('/branchoffice', 'BranchOfficeController');
});