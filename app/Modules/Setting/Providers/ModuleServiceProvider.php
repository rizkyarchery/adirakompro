<?php

namespace App\Modules\Setting\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('setting', 'Resources/Lang', 'app'), 'setting');
        $this->loadViewsFrom(module_path('setting', 'Resources/Views', 'app'), 'setting');
        $this->loadMigrationsFrom(module_path('setting', 'Database/Migrations', 'app'), 'setting');
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('setting', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('setting', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
