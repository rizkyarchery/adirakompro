<?php

namespace App\Modules\Setting\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'settingweb';
    protected $fillable = ['title','nm_web','link_web','logo_web','nm_perusahaan','alamat','fax','logo_sosmed1','link_sosmed1','logo_sosmed2','link_sosmed2','logo_sosmed3','link_sosmed3','copyright','no_telp','alt_teks','alt_teks_fb','alt_teks_ig','alt_teks_twit','email','title_en','nm_web_en','link_web_en','logo_web_en','nm_perusahaan_en','alamat_en','fax_en','logo_sosmed1_en','link_sosmed1_en','logo_sosmed2_en','link_sosmed2_en','logo_sosmed3_en','link_sosmed3_en','copyright_en','no_telp_en','alt_teks_en','alt_teks_fb_en','alt_teks_ig_en','alt_teks_twit_en','email_en','city','city_en','disclaimer','disclaimer_en','sitemap','sitemap_en','google_analytic'];
}
