<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDiscSiteSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('settingweb', function($table) {
           $table->string('disclaimer')->nullable();
        //    $table->string('disclaimer_en')->nullable();
           $table->string('sitemap')->nullable();
        //    $table->string('sitemap_en')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('settingweb', function($table) {
             $table->dropColumn('disclaimer');
             $table->dropColumn('sitemap');
            //  $table->dropColumn('disclaimer_en');
            //  $table->dropColumn('sitemap_en');
             });
    }
}
