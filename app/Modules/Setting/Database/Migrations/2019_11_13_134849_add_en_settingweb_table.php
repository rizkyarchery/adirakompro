<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEnSettingwebTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('settingweb', function($table) {
            // $table->string('title_en')->nullable();
            // $table->string('nm_web_en')->nullable();
            // $table->string('link_web_en')->nullable();
            // $table->string('logo_web_en')->nullable();
            // $table->string('nm_perusahaan_en')->nullable();
            // $table->text('alamat_en')->nullable();
            // $table->string('no_telp_en')->nullable();
            // $table->string('fax_en')->nullable();
            // $table->string('logo_sosmed1_en')->nullable();
            // $table->string('link_sosmed1_en')->nullable();
            // $table->string('logo_sosmed2_en')->nullable();
            // $table->string('link_sosmed2_en')->nullable();
            // $table->string('logo_sosmed3_en')->nullable();
            // $table->string('link_sosmed3_en')->nullable();
            // $table->string('copyright_en')->nullable(); 
            // $table->string('kode_en')->nullable();
            // $table->string('alt_teks_en')->nullable();
            // $table->string('alt_teks_fb_en')->nullable();
            // $table->string('alt_teks_ig_en')->nullable();
            // $table->string('alt_teks_twit_en')->nullable();
            // $table->string('email_en_en')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //  Schema::table('settingweb', function($table) {
        //     $table->dropColumn('title_en');
        //     $table->dropColumn('nm_web_en');
        //     $table->dropColumn('link_web_en');
        //     $table->dropColumn('logo_web_en');
        //     $table->dropColumn('nm_perusahaan_en');
        //     $table->dropColumn('alamat_en');
        //     $table->dropColumn('no_telp_en');
        //     $table->dropColumn('fax_en');
        //     $table->dropColumn('logo_sosmed1_en');
        //     $table->dropColumn('link_sosmed1_en');
        //     $table->dropColumn('logo_sosmed2_en');
        //     $table->dropColumn('link_sosmed2_en');
        //     $table->dropColumn('logo_sosmed3_en');
        //     $table->dropColumn('link_sosmed3_en');
        //     $table->dropColumn('copyright_en');
        //     $table->dropColumn('kode_en');
        //     $table->dropColumn('alt_teks_en');
        //     $table->dropColumn('alt_teks_fb_en');
        //     $table->dropColumn('alt_teks_ig_en');
        //     $table->dropColumn('alt_teks_twit_en');
        //     $table->dropColumn('email_en_en');
        //     });
    }
}
