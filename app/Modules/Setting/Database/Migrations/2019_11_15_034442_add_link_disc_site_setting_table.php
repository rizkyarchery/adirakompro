<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLinkDiscSiteSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('settingweb', function($table) {
           $table->string('link_sitemap')->nullable();
        //    $table->string('link_sitemap_en')->nullable();
           $table->string('link_disclaimer')->nullable();
        //    $table->string('link_disclaimer_en')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('settingweb', function($table) {
            $table->dropColumn('link_sitemap');
            // $table->dropColumn('link_sitemap_en');
            $table->dropColumn('link_disclaimer');
            // $table->dropColumn('link_disclaimer_en');
             });
    }
}
