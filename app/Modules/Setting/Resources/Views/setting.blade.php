@extends('layouts.index')
@section('content')


@php
    
$PREFIX = config('app.app_prefix');

@endphp

<link rel="stylesheet" href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css')}}">
   <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1 class="m-0 text-dark"></h1>
             </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
              <li class="breadcrumb-item active">Global Setting</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
 <section class="content">
  <div class="container">
      @if ($message = Session::get('sukses'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button> 
      <strong>{{ $message }}</strong>
    </div>
    @endif
    <div class="row">

      <!------Setting web ID------>

    <div class="col-md-12">
      <div class="card">
        <div class="card-header d-flex p-0">
          <h1 class="card-title p-3">Global Setting</h1>
          <ul class="nav nav-pills ml-auto p-2">
            <li class="nav-item"><a class="nav-link active" href="#tab_1" data-toggle="tab">Title</a></li>
            <li class="nav-item"><a class="nav-link" href="#tab_2" data-toggle="tab">Company</a></li>
            <li class="nav-item"><a class="nav-link" href="#tab_3" data-toggle="tab">Social media</a></li>
          </ul>
        </div><!-- /.card-header -->
      </div>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_1"> 
        <div class="row">
          <div class="col-md-6">
           <div class="card">
            <form action="/{{ $PREFIX }}/setting/update" method="post" class="form-horizontal" enctype="multipart/form-data">
              {{ csrf_field() }}
              {{ method_field('PUT') }}
            <br>
            <input type="hidden" name="id" id="id" value="{{ isset($settingweb->id) ? $settingweb->id : null }}">
                <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">Title Web</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title" value="{{ isset($settingweb->title) ? $settingweb->title : null}}" maxlength="50">
               </div>
                    </div>
                    <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">Name Web</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="nm_web" name="nm_web" placeholder="Enter nama web" value="{{ isset($settingweb->nm_web) ? $settingweb->nm_web : null }}" maxlength="50">
                     </div></div>
                     <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-6 control-label">Logo Web <small>Recommended Size<span class="text-danger"> 255 x 50</span></small></label>
                        <div class="col-sm-12">
                          <div class="row">
                             <div class="col s6">
                            <img src="{{ url(''.isset($settingweb->logo_web) ? $settingweb->logo_web : null.'') }}" id="showgambar" style="max-width:200px;max-height:200px;float:left;" />    
                          </div>
                        </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customFile" name="logo_web" accept="image/*">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                              </div>
                          </div>
                          </div>
                      <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-6 control-label">Logo Web Alternative Teks</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="alt_teks" name="alt_teks" placeholder="Enter alt teks" value="{{ isset($settingweb->alt_teks) ? $settingweb->alt_teks : null }}" maxlength="50">
                     </div></div>
                     <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">Link Web</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="link_web" name="link_web" placeholder="Enter link web" value="{{ isset($settingweb->link_web) ? $settingweb->link_web : null }}" maxlength="50">
                     </div>
                    </div>
                      <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">Disclaimer</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="disclaimer" name="disclaimer" placeholder="Enter Disclaimer" value="{{ isset($settingweb->disclaimer) ? $settingweb->disclaimer : null }}">
                     </div>
                    </div>
                    <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">Link Disclaimer</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="link_disclaimer" name="link_disclaimer" placeholder="Enter Link" value="{{ isset($settingweb->link_disclaimer) ? $settingweb->link_disclaimer : null }}">
                     </div>
                    </div>
                     <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">Sitemap</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="sitemap" name="sitemap" placeholder="Enter Sitemap" value="{{ isset($settingweb->sitemap) ? $settingweb->sitemap : null }}">
                     </div>
                    </div>
                    <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">Link Sitemap</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="link_sitemap" name="link_sitemap" placeholder="Enter Link" value="{{ isset($settingweb->link_sitemap) ? $settingweb->link_sitemap : null }}">
                     </div>
                    </div>
                    <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">Google Analytic</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="google_analytic" name="google_analytic" placeholder="Enter Google Analytic" value="{{ isset($settingweb->google_analytic) ? $settingweb->google_analytic : null }}">
                     </div>
                    </div>
          </div>
       </div>
        <div class="col-md-6">
           <div class="card">
            <br>
            <input type="hidden" name="id" id="id" value="{{ isset($settingwebEn->id) ? $settingwebEn->id :null }}">
                <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">Title Web</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="title_en" name="title_en" placeholder="Enter Title" value="{{ isset($settingwebEn->title) ? $settingwebEn->title : null }}" maxlength="50">
               </div>
                    </div>
                    <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">Name Web</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="nm_web_en" name="nm_web_en" placeholder="Enter nama web" value="{{ isset($settingwebEn->nm_web) ? $settingwebEn->nm_web : null }}" maxlength="50">
                     </div></div>
                     <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-6 control-label">Logo Web <small>Recommended Size<span class="text-danger"> 255 x 50</span></small></label>
                        <div class="col-sm-12">
                          <div class="row">
                             <div class="col s6">
                            <img src="{{ url(''.isset($settingwebEn->logo_web) ?$settingwebEn->logo_web : null.'')}}" id="showgambar" style="max-width:200px;max-height:200px;float:left;" />    
                          </div>
                        </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customFile" name="logo_web_en" accept="image/*">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                              </div>
                          </div>
                          </div>
                      <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-6 control-label">Logo Web Alternative Teks</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="alt_teks_en" name="alt_teks_en" placeholder="Enter alt teks" value="{{ isset($settingwebEn->alt_teks) ? $settingwebEn->alt_teks : null }}" maxlength="50">
                     </div></div>
                     <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">Link Web</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="link_web_en" name="link_web_en" placeholder="Enter link web" value="{{ isset($settingwebEn->link_web) ? $settingwebEn->link_web : null }}" maxlength="50">
                     </div>
                    </div>
                    <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">Disclaimer</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="disclaimer_en" name="disclaimer_en" placeholder="Enter Disclaimer" value="{{ isset($settingwebEn->disclaimer) ? $settingwebEn->disclaimer : null }}">
                     </div>
                    </div>
                     <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">Link Disclaimer</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="link_disclaimer_en" name="link_disclaimer_en" placeholder="Enter Link" value="{{ isset($settingwebEn->link_disclaimer) ? $settingwebEn->link_disclaimer : null }}">
                     </div>
                    </div>
                     <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">Sitemap</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="sitemap_en" name="sitemap_en" placeholder="Enter Sitemap" value="{{ isset($settingwebEn->sitemap) ? $settingwebEn->sitemap : null }}">
                     </div>
                    </div>
                     <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">Link Sitemap</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="link_sitemap_en" name="link_sitemap_en" placeholder="Enter Sitemap" value="{{ isset($settingwebEn->link_sitemap) ? $settingwebEn->link_sitemap : null }}">
                     </div>
                    </div>
                    <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">Google Analytic</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="google_analytic_en" name="google_analytic_en" placeholder="Enter Google Analytic" value="{{ isset($settingwebEn->google_analytic) ? $settingwebEn->google_analytic : null }}">
                     </div>
                    </div>
          </div>
       </div>
     </div> <!--row-->
       </div><!--tab-pane 1-->
         <div class="tab-pane" id="tab_2"> 
    <div class="row">
       <div class="col-md-6">
           <div class="card">
             <br>        
            <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">Company</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="nm_perusahaan" name="nm_perusahaan" placeholder="Enter perusahaan" value="{{ isset($settingweb->nm_perusahaan) ? $settingweb->nm_perusahaan : null }}" maxlength="50">
               </div>
                    </div>
                    <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">Address</label>
                        <div class="col-sm-12">
                      <textarea class="form-control" name="alamat" id="alamat">{{ isset($settingweb->alamat) ? $settingweb->alamat : null  }}</textarea>
                     </div></div>
                     <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">City</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="city" name="city" placeholder="Enter city" value="{{ isset($settingweb->city) ? $settingweb->city : null }}" maxlength="50">
                     </div>
                    </div>
                     <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">Postal Code</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="postal_code" name="postal_code" placeholder="Enter Postal code" value="{{ isset($settingweb->postal_code) ? $settingweb->postal_code : null }}" maxlength="50">
                     </div>
                    </div>
                    <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">No Telp</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="no_telp" name="no_telp" placeholder="Enter NO Telpon" value="{{ isset($settingweb->no_telp) ? $settingweb->no_telp : null }}" maxlength="50">
                     </div>
                    </div>
                     <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">Fax</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="fax" name="fax" placeholder="Enter NO Fax" value="{{ isset($settingweb->fax) ? $settingweb->fax : null }}" maxlength="50">
                     </div>
                    </div>
                     <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">Email</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="email" name="email" placeholder="Enter Email" value="{{ isset($settingweb->email) ? $settingweb->email : null }}" maxlength="50">
                     </div>
                    </div>
                      <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">Copyright</label>
                        <div class="col-sm-12">
                           <textarea class="form-control" name="copyright" id="copyright" rows="4" cols="50">{{ isset($settingweb->copyright) ? $settingweb->copyright : null }}</textarea>
                     </div>
                    </div>
                    </div>
          </div>
         <div class="col-md-6">
           <div class="card">
             <br>        
            <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">Company</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="nm_perusahaan_en" name="nm_perusahaan_en" placeholder="Enter Company" value="{{ isset($settingwebEn->nm_perusahaan) ?  $settingwebEn->nm_perusahaan : null }}" maxlength="50">
               </div>
                    </div>
                    <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">Address</label>
                        <div class="col-sm-12">
                      <textarea class="form-control" name="alamat_en" id="alamat_en">{{ isset($settingwebEn->alamat) ? $settingwebEn->alamat : null }}</textarea>
                     </div></div>
                     <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">City</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="city_en" name="city_en" placeholder="Enter City" value="{{ isset($settingwebEn->city) ? $settingwebEn->city : null }}" maxlength="50">
                     </div>
                    </div>
                      <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">Postal Code</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="postal_code_en" name="postal_code_en" placeholder="Enter Postal code" value="{{ isset($settingwebEn->postal_code) ? $settingwebEn->postal_code : null }}" maxlength="50">
                     </div>
                    </div>
                    <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">No Telp</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="no_telp_en" name="no_telp_en" placeholder="Enter NO Telpon" value="{{ isset($settingwebEn->no_telp) ? $settingwebEn->no_telp : null }}" maxlength="50">
                     </div>
                    </div>
                     <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">Fax</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="fax_en" name="fax_en" placeholder="Enter NO Fax" value="{{ isset($settingwebEn->fax) ? $settingwebEn->fax : null }}" maxlength="50">
                     </div>
                    </div>
                     <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">Email</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="email_en" name="email_en" placeholder="Enter Email" value="{{ isset($settingwebEn->email) ? $settingwebEn->email : null }}" maxlength="50">
                     </div>
                    </div>
                      <div class="form-group" style="margin-left:12px;margin-right:12px;">
                        <label for="name" class="col-sm-4 control-label">Copyright</label>
                        <div class="col-sm-12">
                          <textarea class="form-control" name="copyright_en" id="copyright_en" rows="4" cols="50">{{ isset($settingwebEn->copyright) ? $settingwebEn->copyright : null }}</textarea>
                        </div>
                    </div>
          </div>
       </div>
     </div> <!--row-->
       </div><!--tab-pane 2-->
 <div class="tab-pane" id="tab_3"> 
    <div class="row">
       <div class="col-md-6">
           <div class="card"><br>
               <div class="form-group" style="margin-left:30px;margin-right:12px;">
                        <label for="name" class="col-sm-12 control-label">Logo Facebook</label>
                        <div class="col-md-12">
                          <div class="row">
                             <div class="col s6">
                             <img src="{{ url(''.isset($settingweb->logo_sosmed1) ?$settingweb->logo_sosmed1 : null.'')}}" id="showgambar" style="max-width:100px;max-height:100px;float:left;" />
                          </div>
                        </div><br>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customFile" name="logo_sosmed1" accept="image/*">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                              </div><br><br>
                               <div class="form-group">
                        <label for="name" class="col-sm-12 control-label">Link Facebook</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="link_web" name="link_sosmed1" placeholder="Enter link" value="{{ isset($settingweb->link_sosmed1) ? $settingweb->link_sosmed1 : null }}" maxlength="50">
                     </div>
                    </div>
                     <div class="form-group">
                        <label for="name" class="col-sm-12 control-label">Alt text Facebook</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="alt_teks_fb" name="alt_teks_fb" placeholder="Enter alt teks fb" value="{{ isset($settingweb->alt_teks_fb) ? $settingweb->alt_teks_fb : null }}" maxlength="50">
                     </div>
                    </div>
                          </div>
                          </div>
                     <div class="form-group" style="margin-left:30px;margin-right:12px;">
                        <label for="name" class="col-sm-12 control-label">Logo Instagram</label>
                        <div class="col-md-12">
                          <div class="row">
                             <div class="col s6">
                              <img src="{{ url(''.isset($settingweb->logo_sosmed2) ?$settingweb->logo_sosmed2 : null.'')}}" id="showgambar" style="max-width:100px;max-height:100px;float:left;" />
                               </div>
                        </div><br>
                          <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customFile" name="logo_sosmed2" accept="image/*">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                              </div>
                              <br><br>
                               <div class="form-group">
                        <label for="name" class="col-sm-6 control-label">Link Instagram</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="link_web" name="link_sosmed2" placeholder="Enter link" value="{{ isset($settingweb->link_sosmed2) ?  $settingweb->link_sosmed2 : null }}" maxlength="50">
                     </div>
                    </div>
                     <div class="form-group">
                        <label for="name" class="col-sm-12 control-label">Alt text Instagram</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="alt_teks_ig" name="alt_teks_ig" placeholder="Enter alt teks ig" value="{{ isset($settingweb->alt_teks_ig) ? $settingweb->alt_teks_ig : null }}" maxlength="50">
                     </div>
                          </div>
                          </div></div>
                   <div class="form-group" style="margin-left:30px;margin-right:12px;">
                        <label for="name" class="col-sm-12 control-label">Logo Twitter</label>
                        <div class="col-md-12">
                          <div class="row">
                             <div class="col s6">
                               <img src="{{ url(''.isset($settingweb->logo_sosmed3) ?$settingweb->logo_sosmed3 : null.'')}}" id="showgambar" style="max-width:100px;max-height:100px;float:left;" />
                            </div>
                        </div><br>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customFile" name="logo_sosmed3" accept="image/*">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                              </div>
                              <br><br>
                      <div class="form-group">
                        <label for="name" class="col-sm-12 control-label">Link Twitter</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="link_web" name="link_sosmed3" placeholder="Enter link web" value="{{ isset($settingweb->link_sosmed3) ? $settingweb->link_sosmed3 : null }}" maxlength="50">
                     </div>
                    </div>
                        <div class="form-group">
                        <label for="name" class="col-sm-12 control-label">Alt text Twitter</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="alt_teks_twit" name="alt_teks_twit" placeholder="Enter alt teks twitter" value="{{ isset($settingweb->alt_teks_twit) ? $settingweb->alt_teks_twit : null }}" maxlength="50">
                     </div>
                          </div>
                          </div>
                          </div>
          
          </div>
          </div>
         <div class="col-md-6">
           <div class="card"><br>
               <div class="form-group" style="margin-left:30px;margin-right:12px;">
                        <label for="name" class="col-sm-12 control-label">Logo Facebook</label>
                        <div class="col-md-12">
                          <div class="row">
                             <div class="col s6">
                             <img src="{{ url(''.isset($settingwebEn->logo_sosmed1) ?$settingwebEn->logo_sosmed1 : null.'')}}" id="showgambar" style="max-width:100px;max-height:100px;float:left;" />
                          </div>
                        </div><br>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customFile" name="logo_sosmed1_en" accept="image/*">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                              </div><br><br>
                               <div class="form-group">
                        <label for="name" class="col-sm-12 control-label">Link Facebook</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="link_sosmed1_en" name="link_sosmed1_en" placeholder="Enter link web" value="{{ isset($settingwebEn->link_sosmed1) ? $settingwebEn->link_sosmed1 : null }}" maxlength="50">
                     </div>
                    </div>
                     <div class="form-group">
                        <label for="name" class="col-sm-12 control-label">Alt text Facebook</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="alt_teks_fb_en" name="alt_teks_fb_en" placeholder="Enter alt teks fb" value="{{ isset($settingwebEn->alt_teks_fb) ? $settingwebEn->alt_teks_fb : null }}" maxlength="50">
                     </div>
                    </div>
                          </div>
                          </div>
                      <div class="form-group" style="margin-left:30px;margin-right:12px;">
                        <label for="name" class="col-sm-12 control-label">Logo Instagram</label>
                        <div class="col-md-12">
                          <div class="row">
                             <div class="col s6">
                              <img src="{{ url(''.isset($settingwebEn->logo_sosmed2) ?$settingwebEn->logo_sosmed2 : null.'')}}" id="showgambar" style="max-width:100px;max-height:100px;float:left;" />
                               </div>
                        </div><br>
                          <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customFile" name="logo_sosmed2_en" accept="image/*">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                              </div>
                              <br><br>
                               <div class="form-group">
                        <label for="name" class="col-sm-6 control-label">Link Instagram</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="link_sosmed2_en" name="link_sosmed2_en" placeholder="Enter link web" value="{{ isset($settingwebEn->link_sosmed2) ? $settingwebEn->link_sosmed2 : null }}" maxlength="50">
                     </div>
                    </div>
                     <div class="form-group">
                        <label for="name" class="col-sm-12 control-label">Alt text Instagram</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="alt_teks_ig_en" name="alt_teks_ig_en" placeholder="Enter alt teks ig" value="{{ isset($settingwebEn->alt_teks_ig) ? $settingwebEn->alt_teks_ig : null }}" maxlength="50">
                     </div>
                          </div>
                          </div></div>
                     <div class="form-group" style="margin-left:30px;margin-right:12px;">
                        <label for="name" class="col-sm-12 control-label">Logo Twitter</label>
                        <div class="col-md-12">
                          <div class="row">
                             <div class="col s6">
                               <img src="{{ url(''.isset($settingwebEn->logo_sosmed3) ?$settingwebEn->logo_sosmed3 : null.'')}}" id="showgambar" style="max-width:100px;max-height:100px;float:left;" />
                            </div>
                        </div><br>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customFile" name="logo_sosmed3_en" accept="image/*">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                              </div>
                              <br><br>
                      <div class="form-group">
                        <label for="name" class="col-sm-12 control-label">Link Twitter</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="link_sosmed3_en" name="link_sosmed3_en" placeholder="Enter link web" value="{{ isset($settingwebEn->link_sosmed3) ? $settingwebEn->link_sosmed3 : null }}" maxlength="50">
                     </div>
                    </div>
                        <div class="form-group">
                        <label for="name" class="col-sm-12 control-label">Alt text Twitter</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="alt_teks_twit_en" name="alt_teks_twit_en" placeholder="Enter alt teks twitter" value="{{ isset($settingwebEn->alt_teks_twit) ? $settingwebEn->alt_teks_twit : null }}" maxlength="50">
                     </div>
                          </div>
                          </div>
                          </div>
          </div>
       </div>
     </div> <!--row-->
       </div><!--tab-pane 3--> 
      </div> <!--tab content-->    
   <!------END Col 12------> 
      {{-- </div> --}}
    </div>
         <div class="col-md-6">
       <div class="card">
      </div>
    </div>
</div>
<div class="card">
      <div class="card-body">
   <div class="col-sm-offset-2 col-sm-4" style="margin-left:65%;">
                     <button type="submit" class="btn btn-primary  btn-block" id="saveBtn" value="create" data-toggle="tooltip" title="Update Setting"><span class="fa fa-save">&nbspSave changes</span>
                     </button>
                    </div>
                </form>
      </div>
</div>

 </section>
   <!-- /.content -->
   <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>

<script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>
@endsection