<?php

namespace App\Modules\Setting\Http\Controllers;

use File;
use Session;
use Illuminate\Http\Request;
use App\Modules\Setting\Models\Setting;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function json(){
        $settingweb = Setting::all();
        return Datatables::of($settingweb)
         ->addColumn('action', function ($settingweb) {
                $btn = '<center><a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$settingweb->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editProduct"><i class="fa fa-edit"></i>&nbspEdit</a>';
                           $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$settingweb->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteProduct"><i class="fa fa-trash"></i>&nbspDelete</a></center>';
                      return $btn;
            }
        )
        ->addIndexColumn()
        ->make(true);
    }

    public function index(){
        $settingweb   = Setting::where('kode','001')->first();
        $settingwebEn = Setting::where('kode','002')->first();
        return view('setting::setting',['settingweb' => $settingweb,'settingwebEn' => $settingwebEn]);
    }

     //  public function show(){
	// 	$settingweb = Settingweb::find('001');
    //     return view('layouts.index',['settingweb' => $settingweb]);
    // }

     public function update(Request $request)
    {
          $this->validate($request,[
                'logo_web' =>'file|image|mimes:jpeg,png,jpg|max:2048',
                'logo_sosmed1' =>'file|image|mimes:jpeg,png,jpg|max:2048',
                'logo_sosmed2' =>'file|image|mimes:jpeg,png,jpg|max:2048',
                'logo_sosmed3' =>'file|image|mimes:jpeg,png,jpg|max:2048',
                'logo_web_en' =>'file|image|mimes:jpeg,png,jpg|max:2048',
                'logo_sosmed1_en' =>'file|image|mimes:jpeg,png,jpg|max:2048',
                'logo_sosmed2_en' =>'file|image|mimes:jpeg,png,jpg|max:2048',
                'logo_sosmed3_en' =>'file|image|mimes:jpeg,png,jpg|max:2048',
               ]);
     //save ID


    /**
     * Upload dulu filenya sebelum disimpan ke DB
     */

    // cari filenya di DB
    $settingweb             = Setting::where('kode','001')->first();

    // reinstance kalau datanya dari database kosong
    if(!$settingweb) {
        $settingweb = new Setting();
        $settingweb->kode                 = '001';
        $settingweb->save();
    }
    
    if($request->file('logo_web')) {
        if(isset($settingweb->logo_web)) {
            File::delete($settingweb->logo_web);
        }

        $file       = $request->file('logo_web');
        $path       = 'assets/logo_web/';
        $fileName   =  $path.$file->getClientOriginalName();
        $request->file('logo_web')->move($path, $fileName);
        $settingweb->logo_web = $fileName;
    }

        //logo facebook
     if($request->file('logo_sosmed1') == ""){
            $settingweb->logo_sosmed1 = $settingweb->logo_sosmed1;
        }else{
            File::delete($settingweb->logo_sosmed1);
            $file1       = $request->file('logo_sosmed1');
            $path1       = 'data_file/logo_sosmed/';
            $fileName1   =  $path1.$file1->getClientOriginalName();
            $request->file('logo_sosmed1')->move($path1, $fileName1);
            $settingweb->logo_sosmed1 = $fileName1;
        }

//logo instagram
         if($request->file('logo_sosmed2') == ""){
            $settingweb->logo_sosmed2 = $settingweb->logo_sosmed2;
        }else{
            File::delete($settingweb->logo_sosmed2);
            $file2       = $request->file('logo_sosmed2');
            $path2       = 'data_file/logo_sosmed/';
            $fileName2   =  $path2.$file2->getClientOriginalName();
            $request->file('logo_sosmed2')->move($path2, $fileName2);
            $settingweb->logo_sosmed2 = $fileName2;
        }

//logo twitter
         if($request->file('logo_sosmed3') == ""){
            $settingweb->logo_sosmed3 = $settingweb->logo_sosmed3;
        }else{
            File::delete($settingweb->logo_sosmed3);
            $file3       = $request->file('logo_sosmed3');
            $path3       = 'data_file/logo_sosmed/';
            $fileName3   =  $path3.$file3->getClientOriginalName();
            $request->file('logo_sosmed3')->move($path3, $fileName3);
            $settingweb->logo_sosmed3 = $fileName3;
        }

          $settingweb->title                = $request->title;
          $settingweb->kode                 = '001';
          $settingweb->alt_teks             = $request->alt_teks;
          $settingweb->nm_web               = $request->nm_web;
          $settingweb->link_web             = $request->link_web; 
          $settingweb->nm_perusahaan        = $request->nm_perusahaan;
          $settingweb->alamat               = $request->alamat;
          $settingweb->no_telp              = $request->no_telp; 
          $settingweb->fax                  = $request->fax;
          $settingweb->email                = $request->email;
          $settingweb->city                 = $request->city;
          $settingweb->postal_code          = $request->postal_code;
          $settingweb->copyright            = $request->copyright; 
          $settingweb->link_sosmed1         = $request->link_sosmed1;
          $settingweb->link_sosmed2         = $request->link_sosmed2;
          $settingweb->link_sosmed3         = $request->link_sosmed3; 
          $settingweb->alt_teks_fb          = $request->alt_teks_fb;
          $settingweb->alt_teks_ig          = $request->alt_teks_ig;
          $settingweb->alt_teks_twit        = $request->alt_teks_twit;
          $settingweb->city                 = $request->city;
          $settingweb->disclaimer           = $request->disclaimer;
          $settingweb->sitemap              = $request->sitemap;
          $settingweb->link_disclaimer      = $request->link_disclaimer;
          $settingweb->link_sitemap         = $request->link_sitemap;
          $settingweb->google_analytic      = $request->google_analytic;
          $settingweb->save();
         //save ID END
         
        //save EN
        $settingwebEn             = Setting::where('kode','002')->first();

     if(!$settingwebEn) {
        $settingwebEn = new Setting();
        $settingwebEn->kode                 = '002';
        $settingwebEn->save();
    }

          if($request->file('logo_web_en') == ""){
            $settingwebEn->logo_web = $settingwebEn->logo_web;
        }else{
            File::delete($settingwebEn->logo_web);
            $file       = $request->file('logo_web_en');
            $path       = 'assets/logo_web/';
            $fileName   =  $path.$file->getClientOriginalName();
            $request->file('logo_web_en')->move($path, $fileName);
            $settingwebEn->logo_web = $fileName;
        }

        //logo facebook
     if($request->file('logo_sosmed1_en') == ""){
            $settingwebEn->logo_sosmed1 = $settingwebEn->logo_sosmed1;
        }else{
            File::delete($settingwebEn->logo_sosmed1);
            $file1       = $request->file('logo_sosmed1_en');
            $path1       = 'data_file/logo_sosmed/';
            $fileName1   =  $path1.$file1->getClientOriginalName();
            $request->file('logo_sosmed1_en')->move($path1, $fileName1);
            $settingwebEn->logo_sosmed1 = $fileName1;
        }

//logo instagram
         if($request->file('logo_sosmed2_en') == ""){
            $settingwebEn->logo_sosmed2 = $settingwebEn->logo_sosmed2;
        }else{
            File::delete($settingwebEn->logo_sosmed2);
            $file2       = $request->file('logo_sosmed2_en');
            $path2       = 'data_file/logo_sosmed/';
            $fileName2   =  $path2.$file2->getClientOriginalName();
            $request->file('logo_sosmed2_en')->move($path2, $fileName2);
            $settingwebEn->logo_sosmed2 = $fileName2;
        }

//logo twitter
         if($request->file('logo_sosmed3_en') == ""){
            $settingwebEn->logo_sosmed3 = $settingwebEn->logo_sosmed3;
        }else{
            File::delete($settingwebEn->logo_sosmed3);
            $file3       = $request->file('logo_sosmed3_en');
            $path3       = 'data_file/logo_sosmed/';
            $fileName3   =  $path3.$file3->getClientOriginalName();
            $request->file('logo_sosmed3_en')->move($path3, $fileName3);
            $settingwebEn->logo_sosmed3 = $fileName3;
        }

          $settingwebEn->title           = $request->title_en;
          $settingwebEn->alt_teks        = $request->alt_teks_en;
          $settingwebEn->nm_web          = $request->nm_web_en;
          $settingwebEn->link_web        = $request->link_web_en; 
          $settingwebEn->nm_perusahaan   = $request->nm_perusahaan_en;
          $settingwebEn->alamat          = $request->alamat_en;
          $settingwebEn->no_telp         = $request->no_telp_en; 
          $settingwebEn->fax             = $request->fax_en;
          $settingwebEn->city            = $request->city_en;
          $settingwebEn->email           = $request->email_en;
          $settingwebEn->postal_code     = $request->postal_code_en;
          $settingwebEn->copyright       = $request->copyright_en; 
          $settingwebEn->link_sosmed1    = $request->link_sosmed1_en;
          $settingwebEn->link_sosmed2    = $request->link_sosmed2_en;
          $settingwebEn->link_sosmed3    = $request->link_sosmed3_en; 
          $settingwebEn->alt_teks_fb     = $request->alt_teks_fb_en;
          $settingwebEn->alt_teks_ig     = $request->alt_teks_ig_en;
          $settingwebEn->alt_teks_twit   = $request->alt_teks_twit_en;
          $settingwebEn->city            = $request->city_en;
          $settingwebEn->disclaimer      = $request->disclaimer_en;
          $settingwebEn->sitemap         = $request->sitemap_en;
          $settingwebEn->link_disclaimer = $request->link_disclaimer_en;
          $settingwebEn->link_sitemap    = $request->link_sitemap_en;
          $settingwebEn->google_analytic = $request->google_analytic_en;
          $settingwebEn->save();
         //save EN END
         Session::flash('sukses','Setup Has Been Updated');
        return redirect('/'.config('app.app_prefix').'/setting');

    }
}
