<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::group(['middleware'=> 'auth','prefix' => config('app.app_prefix')], function () {
    Route::get('/setting/json', 'SettingController@json');
    Route::resource('/setting','SettingController');
    Route::put('/setting/update','SettingController@update');
    Route::put('/setting/update_perusahaan','SettingController@update_perusahaan');
    Route::put('/setting/update_sosmed','SettingController@update_sosmed');
});