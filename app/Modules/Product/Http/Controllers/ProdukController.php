<?php

namespace App\Modules\Product\Http\Controllers;

use Str;
use File;
use Session;
use Validator;
use DataTables;
use App\Modules\Product\Models\Produk;
use App\Modules\Setting\Models\Setting;
use Illuminate\Http\Request;
use Auth;

use App\Http\Controllers\Controller;

class ProdukController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function json(Request $request){

        if($request->columns[3]['search']['value'] != null){
          $produk = Produk::where('lang',$request->columns[3]['search']['value'])->orderBy('id','DESC')->get();
        }else{
          $produk = Produk::orderBy('id','DESC')->get();
        }
      return Datatables::of($produk)
        ->addColumn('action', function ($produk) {
               $html='';
                if(auth()->user()->can('Edit Product')){
                    $html = '<center><a href="/'.config('app.app_prefix').'/produk/edit/'.$produk->id.'" data-toggle="tooltip" title="Update" class="btn btn-xs btn-warning" style="margin-bottom:4px;margin-right:2px;"><i class="fa fa-edit"></i></a>';
                }
                 if(auth()->user()->can('Delete Product')){
                    $html=$html.' <a data-id="'.$produk->id.'" class="btn btn-xs btn-danger" style="margin-bottom:4px;" data-toggle="tooltip" title="Delete" id="hapusproduk"><i class="fa fa-trash"></i></a></center>';
                 }
                    return $html;
          }
      )
      ->addIndexColumn()
      ->make(true);
  }

  public function index(){
      $produk = Produk::all();
      $settingweb = Setting::find('001');
    return view('product::produk',['produk' => $produk,'settingweb' => $settingweb]);
  }

  public function tambah(){
    $produk       = Produk::all();
    $settingweb   = Setting::where('kode','001')->first();

    return view('product::tambah_produk',['produk' => $produk,'settingweb' => $settingweb]);
  }
  public function insert(Request $request) {
               
        $validator = Validator::make($request->all(),[
            'nama' =>'required',
            // 'ket' =>'required',
            'foto' =>'required|file|image|mimes:jpeg,png,jpg|max:2048',
            'status' => 'required',
            'link' => 'required',
            'alt_teks' => 'required',
            'lang' => 'required'
         ]);

        if(!$validator->fails()){
            // menyimpan data file yang diupload ke variabel $file
            $file = $request->file('foto');
            $path       = 'Upload/product/';
            $nama_file = Str::random(40) .'.'.$file->getClientOriginalExtension();
            // isi dengan nama folder tempat kemana file diupload
            $tujuan_upload =  $path;
            $request->file('foto')->move($tujuan_upload,$nama_file);
                    
            Produk::create([
                'nama' => $request->nama,
                'ket' => '-',
                'foto' => $nama_file,
                'status' => $request->status,
                'link' => $request->link,
                'alt_teks' => $request->alt_teks,
                'lang' => $request->lang,
                'created_by' => Auth::user()->name,
                'updated_by' => Auth::user()->name
            ]);
            
            Session::flash('sukses','Product has been Created');
            return redirect(config('app.app_prefix').'/produk');
        }

        return redirect(config('app.app_prefix') .'/pr/tambah')
        ->withErrors($validator)
        ->withInput();
    }

     public function edit($id){
      $produk = Produk::find($id);
      $settingweb = Setting::find('001');
       return view('product::edit_produk', ['produk' => $produk,'settingweb' => $settingweb]);
    }

     public function update($id, Request $request){
        $validator = Validator::make($request->all(),[
            'nama' =>'required',
            // 'ket' =>'required',
            'status' => 'required',
            'link' => 'required',
            'alt_teks' => 'required',
            'lang' => 'required'
         ]);

        if(!$validator->fails()){
            $produk = Produk::find($id);

            if($request->file('foto') == "")
            {
                $produk->foto = $produk->foto;
            } 
            else
            {
                $file       = $request->file('foto');
                $path       = 'Upload/product/';
                $fileName   =  Str::random(40) .'.'.$file->getClientOriginalExtension();
                $request->file('foto')->move($path, $fileName);
                $produk->foto = $fileName;
            }
    
        
            $produk->nama = $request->nama;
            $produk->ket = '-';
            $produk->link = $request->link;
            $produk->status = $request->status;
            $produk->alt_teks = $request->alt_teks;
            $produk->lang = $request->lang;
            $produk->updated_by      = Auth::user()->name;
            $produk->created_by      = Auth::user()->name;
            $produk->save();
            Session::flash('sukses21','Product has been updated');
            return redirect(config('app.app_prefix').'/produk');
        }
        return redirect(config('app.app_prefix') .'/produk/edit/'.$id)
        ->withErrors($validator)
        ->withInput();
    }

    public function delete($id) {
        $produk = Produk::find($id);

        $produk->delete();

        $callback = [
            "message" => "Data has been Deleted",
            "code"   => 200
        ];

        return json_encode($callback, TRUE);
    }

}
