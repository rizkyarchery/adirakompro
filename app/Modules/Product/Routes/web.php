<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/



Route::group(['middleware'=> 'auth','prefix' => config('app.app_prefix')], function () {
    Route::get('/produk/json', 'ProdukController@json');
    Route::resource('/produk', 'ProdukController');
    Route::get('/pr/tambah','ProdukController@tambah');
    Route::post('/produk/insert','ProdukController@insert');
    Route::get('/produk/edit/{id}','ProdukController@edit');
    Route::put('/produk/update/{id}', 'ProdukController@update');
    ROUTE::post('/produk/hapus/{id}', 'ProdukController@delete');
});