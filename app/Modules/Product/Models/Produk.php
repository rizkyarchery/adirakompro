<?php

namespace App\Modules\Product\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Produk extends Model
{
    protected $table = 'produk';
    protected $fillable = ['nama','foto','ket','status','link','alt_teks','lang'];

    use SoftDeletes;
    protected $dates =['deleted_at'];
}
