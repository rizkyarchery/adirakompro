<?php

namespace App\Modules\Testimony\Http\Controllers;

use File;
use Session;
use DataTables;
use Illuminate\Http\Request;
use App\Modules\Testimony\Models\Testimoni;
use App\Modules\Setting\Models\Setting;
use App\Http\Controllers\Controller;
use Validator;
use Auth;

class TestimonyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
      public function json(request $request){

        if($request->columns[2]['search']['value'] != null){
           $testimoni = Testimoni::where('lang',$request->columns[2]['search']['value'])->orderBy('id','DESC')->get();
        }else{
             $testimoni = Testimoni::orderBy('id','DESC')->get();
        }

        return Datatables::of($testimoni)
         ->addColumn('action', function ($testimoni) {
            $html='';
                if(auth()->user()->can('Edit Testimony')){
                    $html = '<center><a href="/'.config('app.app_prefix').'/testimoni/edit/'.$testimoni->id.'" data-toggle="tooltip" title="Update" class="btn btn-xs btn-warning" style="margin-bottom:4px;margin-right:2px;"><i class="fa fa-edit"></i></a>';
                }
                 if(auth()->user()->can('Delete Testimony')){
                    $html=$html.' <a data-id="'.$testimoni->id.'" class="btn btn-xs btn-danger" style="margin-bottom:4px;" data-toggle="tooltip" title="Delete" id="hapustestimoni"><i class="fa fa-trash"></i></a></center>';
                 }
                    return $html;
            }
        )
        ->addIndexColumn()
        ->make(true);
    }
     public function index(){
        $testimoni = Testimoni::all();
         $settingweb = Setting::find('001');
    	return view('testimony::testimoni',['testimoni' => $testimoni,'settingweb' => $settingweb]);
    }

     public function tambah(){
        $testimoni = Testimoni::all();
        $settingweb = Setting::find('001');
    	return view('testimony::tambah_testimoni',['testimoni' => $testimoni,'settingweb' => $settingweb]);
  }
  public function insert(Request $request) {
               
        $validator = Validator::make($request->all(),[
            'nama' =>'required',
            'ket' =>'required',
            'foto' =>'required|file|image|mimes:jpeg,png,jpg|max:2048',
            'status' => 'required',
            'alt_teks' => 'required',
            'lang' => 'required'
        ]);

        if(!$validator->fails()){
            // menyimpan data file yang diupload ke variabel $file
            $file = $request->file('foto');
            $path       = 'Upload/Testimony/';
            $nama_file = $path.$file->getClientOriginalName();
            // isi dengan nama folder tempat kemana file diupload
            $tujuan_upload =  $path;
            $request->file('foto')->move($tujuan_upload,$nama_file);
                    
            Testimoni::create([
                'nama' => $request->nama,
                'ket' => $request->ket,
                'foto' => $nama_file,
                'status' => $request->status,
                'alt_teks' => $request->alt_teks,
                'lang' => $request->lang,
                'created_by' => Auth::user()->name,
                'updated_by' => Auth::user()->name
                ]);
            
            Session::flash('sukses','Testimony Has Been Created');
            return redirect(config('app.app_prefix').'/testimoni');
        }

        return redirect(config('app.app_prefix') .'/ts/tambah')
                        ->withErrors($validator)
                        ->withInput();
        
    }
     public function edit($id){
      $testimoni = Testimoni::find($id);
      $settingweb = Setting::find('001');
       return view('testimony::edit_testimoni', ['testimoni' => $testimoni,'settingweb' => $settingweb]);
    }

     public function update($id, Request $request){
    
        $validator = Validator::make($request->all(),[
            'nama' =>'required',
            'ket' =>'required',
            'foto' =>'file|image|mimes:jpeg,png,jpg|max:2048',
            'status' => 'required',
            'alt_teks' => 'required',
            'lang' => 'required'
        ]);
        
        if(!$validator->fails()){
            $testimoni = Testimoni::find($id);

            if($request->file('foto') == "")
            {
                $testimoni->foto = $testimoni->foto;
            } 
            else
            {
                File::delete($testimoni->foto);
                $file       = $request->file('foto');
                $path       = 'Upload/Testimony/';
                $fileName   =  $path.$file->getClientOriginalName();
                $request->file('foto')->move($path, $fileName);
                $testimoni->foto = $fileName;
            }
        
            
            $testimoni->nama = $request->nama;
            $testimoni->ket = $request->ket;
            $testimoni->status = $request->status;
            $testimoni->alt_teks = $request->alt_teks;
            $testimoni->lang = $request->lang;
            $testimoni->updated_by      = Auth::user()->name;
            $testimoni->created_by      = Auth::user()->name;
            $testimoni->save();
            Session::flash('sukses21','Testimony Has Been Updated');
            return redirect(config('app.app_prefix').'/testimoni');
        }

    return redirect(config('app.app_prefix') .'/testimoni/edit/'.$id)
                        ->withErrors($validator)
                        ->withInput();
}

    public function delete($id) {
        $testimoni = Testimoni::find($id);

        File::delete($testimoni->foto);
         $testimoni->delete();

        $callback = [
            "message" => "Data has been Deleted",
            "code"   => 200
        ];

        return json_encode($callback, TRUE);
    }
}
