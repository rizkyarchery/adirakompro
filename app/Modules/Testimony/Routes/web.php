<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::group(['middleware'=> 'auth','prefix' => config('app.app_prefix')], function () {
    Route::get('/testimoni/json', 'TestimonyController@json');
    Route::resource('/testimoni', 'TestimonyController');
    Route::get('/ts/tambah','TestimonyController@tambah');
    Route::post('/testimoni/insert','TestimonyController@insert');
    Route::get('/testimoni/edit/{id}','TestimonyController@edit');
    Route::put('/testimoni/update/{id}', 'TestimonyController@update');
    ROUTE::post('/testimoni/hapus/{id}', 'TestimonyController@delete');
});