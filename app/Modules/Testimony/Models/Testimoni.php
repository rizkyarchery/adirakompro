<?php

namespace App\Modules\Testimony\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Testimoni extends Model
{
     protected $table = 'testimoni';
     protected $fillable = ['nama','foto','ket','status','alt_teks','lang'];

     use SoftDeletes;
    protected $dates =['deleted_at'];
}
