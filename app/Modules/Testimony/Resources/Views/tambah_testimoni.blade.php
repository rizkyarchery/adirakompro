@extends('layouts.index')
@section('content')
<?php
$prefix = config('app.app_prefix');
?>
<br><br>
<div class="container">
  <div class="card-header bg-info">Add Testimony</div>
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-body">
            <form action="/{{ $prefix }}/testimoni/insert" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="form-group">
                <label>Language <sup class="text-danger">*</sup></label><br>
                <select name="lang" class="form-control" required>
                  <option>Select Language</option>
                  <option>ID</option>
                  <option>EN</option>
                </select>
                @if($errors->has('lang'))
                    <div class="text-danger">
                    {{ $errors->first('lang')}}
                  </div>
                @endif
              </div>
              <div class="form-group">
                <label>Name <sup class="text-danger">*</sup></label><br>
                <input  type="text" 
                        name="nama" 
                        class="form-control" 
                        placeholder="fill Name..." 
                        value="{{ old('nama') }}"
                        required>
                @if($errors->has('nama'))
                  <div class="text-danger">
                    {{ $errors->first('nama')}}
                  </div>
                @endif
              </div>
              <div class="form-group">
                <label>Descrption <sup class="text-danger">*</sup></label> <br>
                <textarea name="ket" class="form-control" id="content">{{ old('ket') }}</textarea>
                @if($errors->has('ket'))
                  <div class="text-danger">
                    {{ $errors->first('ket')}}
                  </div>
                @endif
              </div>
              
          </div>
        </div>
      </div>
      <div class="col-md-4">
          <div class="card">
                <div class="card-body">
                  <br/>
                  <label>Upload Image <sup class="text-danger">*</sup></label>&nbsp;<small>Recommended Size<span class="text-danger"> 300 x 300</span></small></small><br>
            <div class="custom-file">
              <input type="file" class="custom-file-input" id="customFile" name="foto" accept="image/*">
              <label class="custom-file-label" for="customFile">Choose file</label>
            </div>
                <br/><br/>
                <label>Testimony Thumbnail Alt. <sup class="text-danger">*</sup></label><br>
              <input type="text" name="alt_teks" class="form-control" value="{{ old('alt_teks') }}">
              @if($errors->has('alt_teks'))
                      <div class="text-danger">
                      {{ $errors->first('alt_teks')}}
                    </div>
                  @endif
              <br/>
                  <label>Status <sup class="text-danger">*</sup></label><br>
                  <select name="status" id="status" class="form-control @error('status') is-invalid @enderror" >
                              <option value="0" selected disabled>- Choose Status -</option>
                              <option value="1">Active</option>
                              <option value="0">Not Active</option>
                          </select>
                                      @error('status')
                                              <span class="invalid-feedback" role="alert">
                                                  <strong>{{ $message }}</strong>
                                              </span>
                                          @enderror
                                          <br>
              <input type="submit" value="Simpan Data" class="btn btn-info btn-sm">
            </form>

                </div>
    </div>
</div></div></div>

<script type="text/javascript">
  $(document).ready(function() {
    $('#content').summernote({
      height: "300px",
      styleWithSpan: false
    });
  }); 
</script>
<script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>
@endsection