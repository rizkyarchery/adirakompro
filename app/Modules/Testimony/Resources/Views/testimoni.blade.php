@extends('layouts.index')
@section('content')

<?php

$prefix = config('app.app_prefix');

?>

<link rel="stylesheet" href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css')}}">
 <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Testimonies</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
              <li class="breadcrumb-item active">Testimonies</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
     <section class="content">
      <div class="container">
         @if ($message = Session::get('sukses'))
				<div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
					<strong>{{ $message }}</strong>
				</div>
                @endif
                  @if ($message = Session::get('sukses21'))
				<div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
					<strong>{{ $message }}</strong>
				</div>
                @endif
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-info">Testimonies</div>

                <div class="card-body">
                    @if(auth()->user()->can('Create Testimony'))
                   <a class="btn btn-success btn-sm" href="/{{ $prefix }}/ts/tambah" data-toggle="tooltip" title="Add Testimony"><i class="fa fa-plus">&nbsp Add Testimony</i></a> @endif <br> 
                  <div class="col-md-2"><br>
               <label>Language</label>
              <div class="input-group mb-3">
                  <select name="language" id="lang" class="custom-select">
                    <option value="" selected>Select all</option>
                    <option value="ID">ID</option>
                    <option value="EN">EN</option>
                  </select> 
                  <div class="input-group-append">
                  </div>
                </div>
              </div>
            <hr>
  <div class="table-responsive">
	<table  class="table-hover table-striped table-bordered table-list tabeltestimoni">
                  <thead>
                    <tr style="vertical-align:middle;text-align:center;font-weigth:bold">
                        <th>No</th>
                        <th>Name</th>
                        <th>Language</th>
                        <th>Action</th>
                      </tr> 
                  </thead>
                  <tbody><tbody>
                </table>
   </div></div>
             </div>
        </div>
    </div>
</div>
   </section>
   <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
   <script>
        $(function() {
         
              $.ajaxSetup({
                headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
              });

              var table_kat = $('.tabeltestimoni').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                  url: '/{{ $prefix }}/testimoni/json',
                  // order: [[ 0, "desc" ]],
                  },
                columns: [
                    { data: 'DT_RowIndex', name:'DT_RowIndex'},
                    { data: 'nama', name: 'nama' },
                    { data: 'lang', name: 'lang' },
                    {data: 'action', name: 'status', orderable: false, searchable: false} 
                  ]
            });
                 
            $('#lang').on('change',function(){
                table_kat.columns(2).search( this.value ).draw();
              })
     

           $(document).on('click','#hapustestimoni',function(){
            // dapetin dulu ID si usernya dari tag element button data-id
            var id = $(this).data('id')

            Swal.fire({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
              if (result.value) {
                  $.ajax({
                  type: "POST",
                  url: "/{{ $prefix }}/testimoni/hapus/" + id,
                  success: function(data){
                    var json  = JSON.parse(data)
                    Swal.fire({
                                type: 'success',
                                title: 'Success',
                                text: 'Data has been Deleted!',
                            })

                    // refresh datatablesnya kalau sudah menghapus rolenya
                    table_kat.ajax.reload()
                    }
                  });
              }
            })
          })
      });
   </script>
@endsection
