<?php

namespace App\Modules\Testimony\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('testimony', 'Resources/Lang', 'app'), 'testimony');
        $this->loadViewsFrom(module_path('testimony', 'Resources/Views', 'app'), 'testimony');
        $this->loadMigrationsFrom(module_path('testimony', 'Database/Migrations', 'app'), 'testimony');
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('testimony', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('testimony', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
