<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::group(['middleware'=> 'auth','prefix' => config('app.app_prefix')], function () {
    Route::get('/banner/json', 'BannerController@json');
    Route::resource('/banner', 'BannerController');
    Route::get('/br/tambah','BannerController@tambah');
    Route::post('/banner/insert','BannerController@insert');
    Route::get('/banner/edit/{id}','BannerController@edit');
    Route::put('/banner/update/{id}', 'BannerController@update');
    ROUTE::post('/banner/hapus/{id}', 'BannerController@delete');
});
