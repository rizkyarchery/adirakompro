<?php

namespace App\Modules\Banner\Http\Controllers;

use File;
use Session;
use DataTables;
use Validator;
use Auth;
use App\Modules\Banner\Models\Banner;
use App\Modules\Setting\Models\Setting;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class BannerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
     public function json(Request $request){

      if($request->columns[2]['search']['value'] != null) {
          $banner = Banner::where('lang',$request->columns[2]['search']['value'])->orderBy('id', 'DESC')->get();
      }else{
          $banner = Banner::orderBy('id', 'DESC')->get();
      }
        return Datatables::of($banner)
               ->addColumn('action', function ($banner) {
                   $html='';
                    if(auth()->user()->can('Edit Banner')){
                        $html='<center><a href="/'.config('app.app_prefix').'/banner/edit/'.$banner->id.'" data-toggle="tooltip" title="update" class="btn btn-xs btn-warning" style="margin-bottom:4px;margin-right:2px;"><i class="fa fa-edit"></i></a>';
                       }  
                     if(auth()->user()->can('Delete Banner')){
                        $html=$html.' <a data-id="'.$banner->id.'" class="btn btn-xs btn-danger" style="margin-bottom:4px;" data-toggle="tooltip" title="delete" id="hapusbanner"><i class="fa fa-trash"></i></a></center>';
                    }
                        return $html;
            //      $status=$banner->status;
            //      if($status==1){
            //         $status='<center><b style="color:blue;">Active</b></center>';
            //    }else{
            //     $status='<center><b style="color:red;">Not Active</b></center>';
            //  }
            //     return 
            //     '<span>'.$status.'</span>';
            }
        )
        ->addIndexColumn()
        ->make(true);
    }

     public function index(){
        $banner = Banner::all();
        $settingweb = Setting::find('001');
    	return view('banner::banner',['banner' => $banner,'settingweb' => $settingweb]);
    }

     public function tambah(){
        $banner = Banner::all();
        $settingweb = Setting::find('001');
    	return view('banner::tambah_banner',['banner' => $banner,'settingweb' => $settingweb]);
  }

  public function insert(Request $request) {
               
        $validatedData = Validator::make($request->all(),[
            'foto' =>'required|file|image|mimes:jpeg,png,jpg|max:2048',
            'status' => 'required',
            'link' => 'required',
            'alt_teks' => 'required',
            'lang' => 'required'
         ]);

         if(!$validatedData->fails()){
            // menyimpan data file yang diupload ke variabel $file
            $file = $request->file('foto');
            $path       = 'Upload/Banner/';
            $nama_file = $path.$file->getClientOriginalName();
            // isi dengan nama folder tempat kemana file diupload
            $tujuan_upload =  $path;
            $request->file('foto')->move($tujuan_upload,$nama_file);
                    
            Banner::create([
                'foto' => $nama_file,
                'status' => $request->status,
                'link' => $request->link,
                'alt_teks' => $request->alt_teks,
                'lang' => $request->lang,
                'created_by' => Auth::user()->name,
                'update_by' => Auth::user()->name
            ]);
            
            Session::flash('sukses','Banner Telah Ditambahkan');
            return redirect(config('app.app_prefix').'/banner');
        }

        return redirect(config('app.app_prefix') .'/br/tambah')
                        ->withErrors($validatedData)
                        ->withInput();
    }


  public function edit($id){
      $banner = Banner::find($id);
      $settingweb = Setting::find('001');
       return view('banner::edit_banner', ['banner' => $banner,'settingweb' => $settingweb]);
    }

    public function update($id, Request $request){
        $validatedData = Validator::make($request->all(), [
            'foto' =>'file|image|mimes:jpeg,png,jpg|max:2048',
            'link' => 'required',
            'status' => 'required',
            'alt_teks' => 'required',
            'lang' => 'required'
        ]);

        if(!$validatedData->fails()){
            $banner = Banner::find($id);

            if($request->file('foto') == "")
            {
                $banner->foto = $banner->foto;
            } 
            else
            {
                File::delete($banner->foto);
                $file       = $request->file('foto');
                $path       = 'Upload/Banner/';
                $fileName   =  $path.$file->getClientOriginalName();
                $request->file('foto')->move($path, $fileName);
                $banner->foto = $fileName;
            }
    
        
            $banner->link = $request->link;
            $banner->alt_teks = $request->alt_teks;
            $banner->status = $request->status;
            $banner->lang = $request->lang;
            $banner->created_by = Auth::user()->name;
            $banner->updated_by = Auth::user()->name;
            $banner->save();
            Session::flash('sukses21','Banner Telah Diupdate');
            return redirect('/'.config('app.app_prefix').'/banner');
        }

        return redirect(config('app.app_prefix') .'/banner/'.$id.'/edit')
                            ->withErrors($validatedData)
                            ->withInput();
    }


      public function delete($id) {
        $banner = Banner::find($id);

        File::delete($banner->foto);
         $banner->delete();

        $callback = [
            "message" => "Data has been Deleted",
            "code"   => 200
        ];

        return json_encode($callback, TRUE);
    }

}
