@extends('layouts.index')
@section('content')
<?php

$PREFIX = config('app.app_prefix');
?>
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css')}}">
 <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Banner</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
              <li class="breadcrumb-item active">Banner</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
     <section class="content">
      <div class="container">
         @if ($message = Session::get('sukses'))
				<div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
					<strong>{{ $message }}</strong>
				</div>
                @endif
                  @if ($message = Session::get('sukses21'))
				<div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button> 
					<strong>{{ $message }}</strong>
				</div>
                @endif
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-info">Banners</div>
                 <div class="card-body">
                   @if(auth()->user()->can('Create Banner'))
                   <a class="btn btn-success btn-sm" href="br/tambah" data-toggle="tooltip" title="Create Banner"><i class="fa fa-plus">&nbsp Create Banner</i></a>
                   @endif
                   <br><hr>
                <div class="col-md-2"><br>
               <label>Language</label>
              <div class="input-group mb-3">
                  <select name="language" id="lang" class="custom-select">
                    <option value="" selected>Select all</option>
                    <option value="ID">ID</option>
                    <option value="EN">EN</option>
                  </select> 
                  <div class="input-group-append">
                  </div>
                </div>
              </div>
            <hr>
  <div class="table-responsive">
	<table  class="table-hover table-striped table-bordered table-list tabelbaner">
                  <thead>
                    <tr style="vertical-align:middle;text-align:center;font-weigth:bold">
                        <th>No</th>
                        <th>Link</th>
                        <th>Language</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr> 
                  </thead>
                  <tbody><tbody>
                </table>
   </div></div>
             </div>
        </div>
    </div>
</div>
   </section>
   <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
   <script>
    $(function() {
         
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
      var table_kat = $('.tabelbaner').DataTable({
          processing: true,
          serverSide: true,
          ajax: '/{{ $PREFIX }}/banner/json',
          columns: [
              { data: 'DT_RowIndex', 
                name:'DT_RowIndex',
                   render:function(data){
                   return'<center>'+data+'</center>'
                 }, 
                }, 
              { data: 'link', 
                name: 'link',
                  render:function(data){
                   return'<center>'+data+'</center>'
                 }, 
               },
              { data: 'lang',
                name: 'lang',
                 render:function(data){
                   return'<center>'+data+'</center>'
                 }, 
              },
              { data: 'status',
                name: 'status',
                render:function(data){ 
                if(data==1){
                  return'<center><b style="color:blue;">Active</b></center>'
                }else{
                  return '<center><b style="color:red;">Not Active</b></center>'
                }
               }, 
               defaultContent:'<center><b style="color:red;">Not Active</b></center>'
               },
              {data: 'action', name: 'action', orderable: false, searchable: false}
               ]
          });

      $('#lang').on('change',function(){
         table_kat.columns(2).search( this.value ).draw();
     })

           $(document).on('click','#hapusbanner',function(){
            // dapetin dulu ID si usernya dari tag element button data-id
            var id = $(this).data('id')

            Swal.fire({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
              if (result.value) {
                  $.ajax({
                  type: "POST",
                  url: "/{{ $PREFIX }}/banner/hapus/" + id,
                  success: function(data){
                    var json  = JSON.parse(data)
                    Swal.fire({
                                type: 'success',
                                title: 'Success',
                                text: 'Data has been Deleted!',
                            })

                    // refresh datatablesnya kalau sudah menghapus rolenya
                    table_kat.ajax.reload()
                    }
                  });
              }
            })
          })

      });
   </script>
@endsection
