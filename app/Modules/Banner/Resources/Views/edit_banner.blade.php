@extends('layouts.index')
@section('content')
<?php
$prefix = config('app.app_prefix');
?>
<br><br>
<div class="container">
       <div class="row">
        <div class="col-md-12">
             <div class="card-header bg-info">Update Banner</div>
            <div class="card">
                <div class="card-body">
                       <form method="post" action="/{{ $prefix }}/banner/update/{{ $banner->id }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group">
                <label>Language <sup class="text-danger">*</sup></label><br>
                <select name="lang" class="form-control">
                  @if($banner->lang == "ID")
                  <option selected>ID</option>
                  <option>EN</option>
                  @else
                  <option>ID</option>
                  <option selected>EN</option>
                  @endif
                </select>
                @if($errors->has('lang'))
                    <div class="text-danger">
                    {{ $errors->first('lang')}}
                  </div>
                @endif
              </div>
                      <div class="form-group">
                            <label>Link <sup class="text-danger">*</sup></label>
                            <input type="text" name="link" class="form-control" value=" {{ $banner->link }}">
                            @if($errors->has('link'))
                                <div class="text-danger">
                                    {{ $errors->first('link')}}
                                </div>
                            @endif
                        </div>
                      <div class="form-group">
                        <label>Upload Image <sup class="text-danger">*</sup><small>Recommended Size<span class="text-danger"> 1600x600</span></small></label> 
                          <div class="row">
                             <div class="col s6">
                               <img src="{{ URL::to("$banner->foto")}}" id="showgambar" style="max-width:200px;max-height:200px;float:left;" />
                             </div>
                        </div>
                        <br>
                    <div class="row">
                       <div class="input-field col s6">
                           <div class="custom-file">
                           <input type="file" id="inputgambar" name="foto" class="custom-file-input" accept="image/*"/ >
                         <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                       </div>
                   </div>
                </div>  
                <div class="form-group">
                <label>Banner Thumbnail Alt. <sup class="text-danger">*</sup></label>
                <input type="text" name="alt_teks" class="form-control" value="{{ $banner->alt_teks }}">
                </div>
                 <div class="form-group">
                <label>Status <sup class="text-danger">*</sup></label>
                <select class="form-control" name="status">
                <option value="1" {{ $banner->status == 1 ? 'selected="selected"' : '' }}>Active</option>
                <option value="0" {{ $banner->status == 0 ? 'selected="selected"' : '' }}>Not Active</option>
                </select>
            </div>
             <div class="form-group">
                    <input type="submit"  value="Update" class="btn btn-success btn-sm">
                </div>
            </form>
                </div>
            </div>
        </div>
    </div>
</div>

    <script type="text/javascript">
  $(document).ready(function() {
    $('#content').summernote({
      height: "300px",
      styleWithSpan: false
    });
  }); 
</script>
<script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>
@endsection