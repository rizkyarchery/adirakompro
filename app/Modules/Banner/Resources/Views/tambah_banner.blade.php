@extends('layouts.index')
@section('content')
<?php
$prefix = config('app.app_prefix');
?>
<br><br>
<div class="container">
   <div class="row">
        <div class="col-md-12">
            <div class="card-header bg-info">Create Banner</div>
            <div class="card">
                 <div class="card-body">
	<form action="/{{ $prefix }}/banner/insert" method="post" enctype="multipart/form-data">
		{{ csrf_field() }}
    <div class="form-group">
                <label>Language <sup class="text-danger">*</sup></label><br>
                <select name="lang" class="form-control">
                  <option>Choose Language</option>
                  <option value="ID">ID</option>
                  <option value="EN">EN</option>
                </select>
                @if($errors->has('lang'))
                    <div class="text-danger">
                    {{ $errors->first('lang')}}
                  </div>
                @endif
              </div>
		  <br/>
        <label>Upload Image <sup class="text-danger">*</sup> <small>Recommended Size<span class="text-danger"> 1600x600</span></small></label><br>
   <div class="custom-file">
    <input type="file" class="custom-file-input" id="customFile" name="foto" accept="image/*">
    <label class="custom-file-label" for="customFile">Choose file</label>
  </div>
    <br/><br/>
     <label>Banner Thumbnail Alt. <sup class="text-danger">*</sup> </label><br>
		<input type="text" name="alt_teks" class="form-control" value="{{ old('alt_teks') }}" placeholder="fill Thumbnail Alt...">
		 @if($errors->has('alt_teks'))
            <div class="text-danger">
             {{ $errors->first('alt_teks')}}
           </div>
        @endif
		 <br/>
    <label>Link <sup class="text-danger">*</sup></label><br>
		<input type="text" name="link" class="form-control" placeholder="fill Link..." value="{{ old('link') }}">
		 @if($errors->has('link'))
            <div class="text-danger">
             {{ $errors->first('link')}}
           </div>
        @endif
		 <br/>
        <label>Status <sup class="text-danger">*</sup></label><br>
        <select name="status" id="status" class="form-control @error('status') is-invalid @enderror" >
                    <option value="0" selected disabled>- Choose Status -</option>
                    <option value="1">Active</option>
                    <option value="0">Not Active</option>
                </select>
                            @error('status')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <br>
		<input type="submit" value="Save" class="btn btn-info btn-sm">
	</form>

                </div>
    </div>
</div></div></div>

<script type="text/javascript">
  $(document).ready(function() {
    $('#content').summernote({
      height: "300px",
      styleWithSpan: false
    });
  }); 
</script>
<script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>
@endsection