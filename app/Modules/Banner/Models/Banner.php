<?php

namespace App\Modules\Banner\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banner extends Model
{
    protected $table = 'banner';
    protected $fillable = ['foto','status','link','alt_teks','lang'];

    use SoftDeletes;
    protected $dates =['deleted_at'];
}
