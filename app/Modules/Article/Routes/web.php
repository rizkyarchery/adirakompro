<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::group(['middleware'=> 'auth','prefix' => config('app.app_prefix')], function () {
    Route::get('/article/json', 'ArtikelController@json');
    Route::resource('/article', 'ArtikelController');
    Route::get('/art/tambah','ArtikelController@tambah');
    Route::post('/article/insert','ArtikelController@insert');
    Route::get('/article/edit/{id}','ArtikelController@edit');
    Route::put('/article/update/{id}', 'ArtikelController@update');
    ROUTE::post('/article/hapus/{id}', 'ArtikelController@delete');
    Route::get('/article/create_article/en/{id}','ArtikelController@create_article');
});

Route::group(['middleware'=> 'auth','prefix' => config('app.app_prefix')], function () {
    Route::get('/category/json', 'KategoriController@json');
    Route::resource('/category', 'KategoriController');
    Route::post('/category/store','KategoriController@store');
    ROUTE::DELETE('/category/destroy/{id}', 'KategoriController@destroy');
    Route::put('/category/edit/{id}', 'KategoriController@edit');
});

// Route::post('/article/{slug}','ArtikelController@viewArticle');