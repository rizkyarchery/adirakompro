<?php

namespace App\Modules\Article\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Artikel extends Model
{
    protected $table = 'artikel';
    protected $fillable = ['judul','slug','isi_artikel','foto','id_kategori','keyword','file_artikel','artikel_parent','language','alt_teks','meta_title','meta_description','status','excerpt','publisher','thumbnail','updated_by','created_by'];

    public function tags() {
       return $this->belongsToMany('App\Modules\Article\Models\Tag','App\Modules\Article\Models\Artikel_tag','id_artikel','id_tag');
    }

    use SoftDeletes;
    protected $dates =['deleted_at'];

}
