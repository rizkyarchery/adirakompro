<?php

namespace App\Modules\Article\Http\Controllers;

use DB;
use Str;
use Auth;
use File;
use Session;
use Validator;
use DataTables;
use Illuminate\Http\Request;
use App\Modules\Article\Models\Tag;
use App\Modules\Setting\Models\Setting;
use App\Modules\Article\Models\Artikel;
use App\Modules\Article\Models\Kategori;
use App\Modules\Article\Models\Artikel_tag;

use App\Http\Controllers\Controller;

class ArtikelController extends Controller
{
    /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */

    public function json(Request $request){
      
      if($request->columns[2]['search']['value'] != null) {
        $artikel = Artikel::where("language",$request->columns[2]['search']['value'])->orderBy('id', 'DESC')->get();
      } else {
        $artikel = Artikel::orderBy('id', 'DESC')->get();
      }

      return Datatables::of($artikel)
          ->addColumn('action', function($artikel){
            
              // filter article language
              switch ($artikel->language) {
                case 'ID':
                    $html   = '<a href="/'.$artikel->slug.'" target="_BLANK" class="btn btn-xs btn-info" style="margin-bottom:4px;margin-right:2px;"><i class="fa fa-eye"></i></a>';
                  break;
                case 'EN':
                    $html   = '<a href="/en/'.$artikel->slug.'" target="_BLANK" class="btn btn-xs btn-info" style="margin-bottom:4px;margin-right:2px;"><i class="fa fa-eye"></i></a>';
                  break;
                
                default:
                    $html   = '<a href="/'.$artikel->slug.'" target="_BLANK" class="btn btn-xs btn-info" style="margin-bottom:4px;margin-right:2px;"><i class="fa fa-eye"></i></a>';
                  break;
              }

              $html .= '<a href="/'.config('app.app_prefix').'/article/create_article/en/'.$artikel->slug.'" data-toggle="tooltip" title="Create Article in english" class="btn btn-xs btn-success" style="margin-bottom:4px;margin-right:2px;"><i class="fa fa-plus"></i></a>';
               if(auth()->user()->can('Delete Article')){
              $html .= '<a href="/'.config('app.app_prefix').'/article/edit/'.$artikel->id.'" data-toggle="tooltip" title="update" class="btn btn-xs btn-warning" style="margin-bottom:4px;margin-right:2px;"><i class="fa fa-edit"></i></a>';
               } 
              if(auth()->user()->can('Edit Article')){
              $html .= '<a data-id="'.$artikel->id.'" class="btn btn-xs btn-danger" style="margin-bottom:4px;" data-toggle="tooltip" title="delete" id="hapusartikel"><i class="fa fa-trash"></i></a>';
              }
              return $html;
          })
          ->addIndexColumn()
          ->make(true);
  }

    public function index(){
      $artikel = Artikel::all();
      $settingweb = Setting::find('001');
      return view('article::artikel',['artikel' => $artikel,'settingweb' => $settingweb]);
  }

   
  public function tambah(){
      $settingweb = Setting::find('001');
    return view('article::tambah_artikel',['settingweb' => $settingweb]);
  }

  public function insert(Request $request) {
      
    $validator = Validator::make($request->all(),[
          'judul' =>'required',
          'excerpt' =>'required',
          'isi_artikel' =>'required',
          'foto' =>'file|image|mimes:jpeg,png,jpg|max:2048',
          'thumbnail' =>'file|image|mimes:jpeg,png,jpg|max:2048',
          'file_artikel'=>'file|mimes:pdf|max:2048',
          // 'id_kategori' => 'required',
          'keyword' => 'required',
          'alt_teks' => 'required',
          'meta_title' => 'required',
          'status' => 'required',
          'meta_description' => 'required',
    ]);

    if(!$validator->fails()){
       //thumbnail
      if($request->file('thumbnail') != null){
        $file1                = $request->file('thumbnail');
        $path1                = 'Upload/Article/thumbnail/';
        $nama_file1           = Str::random(40) .'.'.$file1->getClientOriginalExtension();
        $request->thumbnail   = $nama_file1;
        $request->file('thumbnail')->move($path1,$nama_file1);
      }

      if($request->file('foto') != null){
        // menyimpan data file yang diupload ke variabel $file
        $file                   = $request->file('foto');
        $path                   = 'Upload/Article/';
        $nama_file              = Str::random(40) .'.'.$file->getClientOriginalExtension();
        $request->foto          = $nama_file;
        $request->file('foto')->move($path,$nama_file);
      }
        $slug_artikel = checkSlug('artikel',Str::slug($request->judul,'-'));
        Artikel::create([
          'judul'                 => $request->judul,
          'slug'                  => $slug_artikel,
          'excerpt'               => $request->excerpt,
          'isi_artikel'           => $request->isi_artikel,
          'foto'                  => isset($nama_file) ? $nama_file : null,
          'thumbnail'             => isset($nama_file1) ? $nama_file1 : null,
          // 'file_artikel' => $nama_file2,
          // 'id_kategori' => $request->id_kategori,
          'keyword'               =>  $request->keyword,
          'language'              =>  $request->language,
          'artikel_parent'        =>  $request->artikel_parent,
          'meta_title'            =>  $request->meta_title,
          'meta_description'      =>  $request->meta_description,
          'alt_teks'              =>  $request->alt_teks,
          'status'                =>  $request->status,
          'publisher'             =>  Auth::user()->name,
          'created_by'            =>  Auth::user()->name,
          'updated_by'            =>  Auth::user()->name
        ]);
        if($request->language == 'EN'){
          $artikel_parent=$request->artikel_parent;
        $update_artikel_slug    = Artikel::where('slug',$artikel_parent)->first();
        $update_artikel_slug->artikel_parent = $slug_artikel;
        $update_artikel_slug->save();
        }
      
        $tabel_artikel = Artikel::orderBy('id', 'Asc')->get(); 
        foreach ($tabel_artikel as $tb_artikel) {
          $id_artikel = $tb_artikel->id;
        }
            
        $tag = strtolower($request->tag);  
        $tags = explode(',', $tag);
                  
        if($request->tag != null || $request->tag != "") {
          foreach ($tags as $tag1) {

            $slug = Str::slug($tag1, '-');
            $tagss = Tag::where('slug',$slug)->exists(); 
    
            if($tagss <> $tag1) {
              $tagsave = new Tag();
              $tagsave->name =  $tag1;
              $tagsave->slug = $slug;
              $tagsave->save();
            }         

            $tabel_tag = Tag::where('slug',$slug)->get();
            
            foreach ($tabel_tag as $tb_tag) {
              $id_tag = $tb_tag->id;
            }
            
            $artikel_tag = new Artikel_tag();
            $artikel_tag->id_artikel = $id_artikel;
            $artikel_tag->id_tag = $id_tag;
            $artikel_tag->save();
          }
        }
        
            
      Session::flash('sukses','Article has been Created');
      return redirect(config('app.app_prefix'). '/article');
    }

    return redirect(config('app.app_prefix') .'/art/tambah')
                        ->withErrors($validator)
                        ->withInput();
  }

  public function create_article($slug){
      $settingweb = Setting::find('001');
      $artikel    = Artikel::where('slug',$slug)->first();
      return view('article::create_en_article',['artikel' =>$artikel,'settingweb'=>$settingweb]);
  }

   
  public function edit($id){
      $artikel = Artikel::find($id);
      $settingweb = Setting::find('001');
      //   $kategori = Kategori::all();
      $artikel_tag= DB::table('artikel_tag') 
                      ->select(DB::raw('artikel_tag.id_artikel,artikel_tag.id_tag,tag.name,tag.slug'))
                      ->Join('tag','artikel_tag.id_tag', '=', 'tag.id')
                      ->where('artikel_tag.id_artikel','=',$id)
                      ->get();
      
      $name=array();
      foreach($artikel_tag as $a_tag){
          $name[]=$a_tag->name;
      }
      $tag= implode(', ', $name);   
                              
      return view('article::edit_artikel', ['artikel' => $artikel,'settingweb' => $settingweb,'tag'=>$tag]);
  }
    
  public function update($id, Request $request){
          
    $tag = strtolower($request->tag);  
    //  $tag= str_replace(' ', ',', $tag1);
    $tags = explode(',', $tag);
   
    if($request->tag == "") {
        $artikel = Artikel_tag::where('id_artikel', $id);
        $artikel->delete();
    }    
  
     if($request->tag != null || $request->tag != "") {
      foreach ($tags as $tag1) {
        $slug = Str::slug($tag1, '-');
        
        $tagss = Tag::where('slug',$slug)->exists(); 
        if($tagss <> $tag1) {
          $tagsave = new Tag();
          $tagsave->name =  $tag1;
          $tagsave->slug = $slug;
          $tagsave->save();

        // Artikel_tag::updateOrCreate(['id_artikel' => $id], ['id_tag' => $id_tag]);
         }
        $tabel_tag = Tag::where('slug',$slug)->get();
        foreach ($tabel_tag as $tb_tag) {
            $id_tag = $tb_tag->id;
             }
         $arc = DB::table('artikel_tag') 
                      ->select(DB::raw('artikel_tag.id_artikel,artikel_tag.id_tag,tag.name,tag.slug'))
                      ->Join('tag','artikel_tag.id_tag', '=', 'tag.id')
                      ->where('artikel_tag.id_artikel','=',$id)
                      ->where('tag.slug','=',$slug)
                      ->exists(); 
        if(!$arc){
             $artikel_tag = new Artikel_tag();
             $artikel_tag->id_artikel = $id;
             $artikel_tag->id_tag = $id_tag;
             $artikel_tag->save();
        }
         if($arc){
            DB::table('artikel_tag')
            ->where('id_artikel', $id)
            ->where('id_tag',$id_tag)
            ->Update(['id_artikel' => $id, 'id_tag' => $id_tag]);
        }
    }
    }
                    
    $validator = Validator::make($request->all(),[
        'judul' =>'required',
        'excerpt' =>'required',
        'isi_artikel' =>'required',
        'foto' =>'file|image|mimes:jpeg,png,jpg|max:2048',
        'thumbnail' =>'file|image|mimes:jpeg,png,jpg|max:2048',
        'keyword' => 'required',
        'alt_teks' => 'required',
        'meta_title' => 'required',
        'meta_description' => 'required',
    ]);

    if(!$validator->fails()){
      $artikel = Artikel::find($id);
    
      if($request->file('foto') == "") {
        $artikel->foto = $artikel->foto;
      } else {
        $file       = $request->file('foto');
        $path       = 'Upload/Article/';
        $fileName   = Str::random(40) .'.'.$file->getClientOriginalExtension();
        $request->file('foto')->move($path, $fileName);
        $artikel->foto = $fileName;
      }

      if($request->file('thumbnail') == "") {
        $artikel->thumbnail = $artikel->thumbnail;
      } else {
        $file1       = $request->file('thumbnail');
        $path1       = 'Upload/Article/thumbnail/';
        $fileName1   = Str::random(40) .'.'.$file1->getClientOriginalExtension();
        $request->file('thumbnail')->move($path1, $fileName1);
        $artikel->thumbnail = $fileName1;
      }
      
      $artikel->alt_teks              = $request->alt_teks;
      $artikel->meta_title            = $request->meta_title;
      $artikel->meta_description      = $request->meta_description;
      $artikel->judul                 = $request->judul;
      $artikel->excerpt               = $request->excerpt;
      $artikel->isi_artikel           = $request->isi_artikel;
      $artikel->keyword               = $request->keyword;
      $artikel->status                = $request->status;
      $artikel->publisher             = Auth::user()->name;
      $artikel->updated_by            = Auth::user()->name;
      
      $artikel->save();

      Session::flash('sukses21','Article has been Updated');

      return redirect(config('app.app_prefix').'/article');
    }

    return redirect(config('app.app_prefix') .'/article/'.$id.'/edit')
                        ->withErrors($validator)
                        ->withInput();    
  }

  public function delete($id) {
      $artikel = Artikel::find($id);

      File::delete($artikel->foto);
      File::delete($artikel->thumbnail);
      File::delete($artikel->file_artikel);
      $artikel->delete();

      $callback = [
          "message" => "Data has been Deleted",
          "code"   => 200
      ];

      return json_encode($callback, TRUE);
  }
}
