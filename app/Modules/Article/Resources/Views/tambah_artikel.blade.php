@extends('layouts.index')
@section('content')
<?php
$prefix = config('app.app_prefix');
?>
<br><br>
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/css/bootstrap-tagsinput.css')}}">
<div class="container">
  <div class="card-header bg-info">Add Article</div>
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                 <div class="card-body">
	<form action="/{{ $prefix }}/article/insert" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    	<input type="hidden" name="language" value="ID" class="form-control" placeholder="isi Judul...">
		<label>Title <sup class="text-danger">*</sup></label> 
		<input type="text" name="judul" class="form-control" placeholder=" fill Title..." value="{{ old('judul') }}" maxlength='190'>
		 @if($errors->has('judul'))
            <div class="text-danger">
             {{ $errors->first('judul')}}
           </div>
        @endif
         <label>Excerpt <sup class="text-danger">*</sup></label>
		<textarea name="excerpt" class="form-control" maxlength='190'>{{ old('excerpt') }}</textarea>
		 @if($errors->has('excerpt'))
            <div class="text-danger">
             {{ $errors->first('excerpt')}}
           </div>
        @endif
        <label>Article <sup class="text-danger">*</sup></label>
		<textarea name="isi_artikel" class="form-control" id="content">{{ old('isi_artikel') }}</textarea>
		 @if($errors->has('isi_artikel'))
            <div class="text-danger">
             {{ $errors->first('isi_artikel')}}
           </div>
        @endif
        <label>Keyword <sup class="text-danger">*</sup></label>
		<input type="text" name="keyword" class="form-control" placeholder="fill Keyword..." value="{{ old('keyword') }}">
		 @if($errors->has('keyword'))
            <div class="text-danger">
             {{ $errors->first('keyword')}}
           </div>
        @endif
      </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                  <div class="card-body">
          <label>Add a Tag</label>
		<input type="text" name="tag" placeholder="add a tag..." data-role="tagsinput" id="form-tags-1" value="{{ old('tag') }}">
		 @if($errors->has('tag'))
            <div class="text-danger">
             {{ $errors->first('tag')}}
           </div>
        @endif
            <label>Meta Title <sup class="text-danger">*</sup></label>
		<input type="text" name="meta_title" class="form-control" placeholder="fill meta title..." value="{{ old('meta_title') }}">
		 @if($errors->has('meta_title'))
            <div class="text-danger">
             {{ $errors->first('meta_title')}}
           </div>
        @endif
      <label>Meta Description <sup class="text-danger">*</sup></label>
		<input type="text" name="meta_description" class="form-control" placeholder="fill meta description..." value="{{ old('meta_description') }}"><br>
		 @if($errors->has('meta_description'))
            <div class="text-danger">
             {{ $errors->first('meta_description')}}
           </div>
        @endif
   <label>Upload Image <small>Recommended Size<span class="text-danger"> 1600 x 600</span></small></label>
   <div class="custom-file">
    <input type="file" class="custom-file-input" id="customFile" name="foto" accept="image/*">
    <label class="custom-file-label" for="customFile">Choose file</label>
    @if($errors->has('foto'))
            <div class="text-danger">
             {{ "Image Required" }}
           </div>
      @endif
  </div>
  <label>Thumbnail  <small>Recommended Size<span class="text-danger"> 320 x 302</span></small></label>
   <div class="custom-file">
    <input type="file" class="custom-file-input" id="customFile" name="thumbnail" accept="image/*">
    <label class="custom-file-label" for="customFile">Choose file</label>
    @if($errors->has('thumbnail'))
            <div class="text-danger">
             {{ "Thumbnail Required" }}
           </div>
      @endif
  </div>
    <label>Article Thumbnail Alt. <sup class="text-danger">*</sup></label>
		<input type="text" name="alt_teks" class="form-control" value="{{ old('alt_teks') }}" placeholder="fill Thumbnail Alt...">
		 @if($errors->has('alt_teks'))
            <div class="text-danger">
             {{ $errors->first('alt_teks')}}
           </div>
        @endif
    {{-- <label>Upload File</label><b> (*pdf)</b><br>
    <div class="custom-file">
    <input type="file" class="custom-file-input upload" id="" name="file_artikel" accept="application/pdf">
    <label class="custom-file-label lblupload" for="customFile">Choose file</label>
  </div>
        <br/><br> --}}
        {{-- <label>Kategori</label><br>
                     <select name="id_kategori" class="form-control @error('id_kategori') is-invalid @enderror" >
                    <option value="0" selected disabled>- Pilih Kategori Barang -</option>
                    @foreach ($kategori as $pemilik)
                        <option value="{{ $pemilik->id }}">{{ ($pemilik->kategori) }}</option>
                    @endforeach
                     </select>
                            @error('id_kategori')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <br> --}}
         <label>Status <sup class="text-danger">*</sup></label>
        <select name="status" id="status" class="form-control @error('status') is-invalid @enderror">
                    <option value="0" selected disabled>- Choose Status -</option>
                    <option value="1">Active</option>
                    <option value="0">Not Active</option>
                </select>
                            @error('status')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                <br>                 
		<input type="submit" value="Save" class="btn btn-info btn-sm">
	</form>

                </div>
    </div>
</div></div></div>
<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap-tagsinput.js')}}"></script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#content').summernote({
      height: "300px",
      styleWithSpan: false
    });
  }); 
</script>
<script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});
</script>
<script>
// Add the following code if you want the name of the file appear on select
$(".upload").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".lblupload").addClass("selected").html(fileName);
});
</script>
@endsection