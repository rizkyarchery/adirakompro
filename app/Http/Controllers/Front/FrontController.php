<?php

namespace App\Http\Controllers\Front;

use DB;
use App;
use Mail;
use Redirect;
use Validator;
use App\Mail\ContactEmail;
use App\Modules\News\Models\News;
use App\Modules\Page\Models\Page;
use App\Modules\Banner\Models\Banner;
use App\Modules\Product\Models\Produk as Product;
use App\Modules\Setting\Models\Setting;
use App\Modules\Article\Models\Artikel as Article;
use App\Modules\Workshop\Models\Workshop;
use App\Modules\Branchoffice\Models\BranchOffice;
use App\Modules\City\Models\City;
use App\Modules\Contactinbox\Models\ContactInbox;
use Session;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FrontController extends Controller {

  private $lang;
  private $code;
  private $lang_link;

  public function __construct() {
    // get the language first
    $lang = App::getLocale();

    // Set Language
    $this->lang = $lang;
    
    // Set code lang
    switch ($this->lang) {
      case 'en':
      case 'EN':
          $this->code       = "002";
          $this->lang_link  = "en/";
        break;

      case 'id':
      case 'ID':
          $this->code       = "001";
          $this->lang_link  = null;
        break;
      
      default:
        # code...
        break;
    }
  }

  public function index() {

    $page               = Page::where('slug', 'home-page')->where("lang",strtoupper($this->lang))->first();
    
    $contentReplace = str_replace('<p>','',$page->content);
    $content = str_replace('</p>', '', $contentReplace);
    
    $data['content']    = $content;
    $data['page']       = $page;
    
    $data['news']       = News::where('status', '1')->where("lang",$this->lang)->orderBy('id', 'DESC')->limit(5)->get();

    $data['banner']     = Banner::where('status', '1')->where('lang', $this->lang)->orderBy('id', 'DESC')->get(); 
    $data['setting']    = Setting::where('kode',$this->code)->first();
    $data['products']   = Product::where('lang',$this->lang)->where('status','1')->get();
    $data['language']   = $this->lang_link;

    return view('frontend.content.home', $data)->withShortcodes();
  }

  public function content($slug) {
    
    // Check in page first
    $page = Page::where('slug', $slug)->where('status',1)->first();
    
    // Check in page if not exist find into article
    // find article
    if($page == null) {
      $article = Article::with(['tags'])->where('slug',$slug)->where('language', $this->lang)->first();

      // check if have parent artikel
      if(isset($article->artikel_parent) != null) {
        $child_article = Article::where('slug', $article->artikel_parent)->first();
        $data['article_child']  = $child_article;
      }

      $news = News::where('slug', $slug)->where('lang', $this->lang)->first();
      
      //CHECK IF HAVE PARENT NEWS
      if(isset($news->news_parent) != null){
        $child_news = News::where('slug', $news->news_parent)->first();
        $data['news_child']   = $child_news;
      }
      if($article != null){
        // buat whats on
        $articles = Article::where('language',strtoupper($this->lang))->orderBy('id','DESC')->take(5)->get();

        $data['article']  = $article;
        $data['articles'] = $articles;
        return view('frontend.content.article_single', $data);
      }

      if($news != null){
        // buat whats on
        $berita = News::where('lang',strtoupper($this->lang))->orderBy('id','DESC')->take(5)->get();
       
        $data['news']  = $news;
        $data['berita'] = $berita;
        return view('frontend.content.news_single', $data)->withShortcodes();
      }
    }

    // if page & article null return 404
    if($page == null) {
      return \Response::view('errors.404', [], 404);
    }

    $contentReplace = str_replace('<p>','',$page->content);
    $content = str_replace('</p>', '', $contentReplace);
  
    // if($this->lang == 'id'){
    //   $data['page_parent'] = Page::where('page_parent', $page->slug)->where('status',0)->first(); 
    //   $data['page'] = $page;
    // }else{
    //   $data['page_parent'] = $page; 
    //   $data['page'] = Page::where('slug', $page->page_parent)->where('status',0)->first();
    //   // $data['page_parent'] = Page::where('page_parent', $page->slug)->where('status',0)->first(); 
    //   // $data['page'] = $page;
    // }

    $data['page_parent'] = Page::where('page_parent', $page->slug)->where('status',1)->first(); 
    $data['page'] = $page;
    
    $data['content'] = $content;
    return view('frontend.content.content', $data)->withShortcodes();
  }

  public function listArticle() {

    // Get list articles by current language
    $articles = Article::where('language',strtoupper($this->lang))->orderBy('id', 'DESC')->paginate(6);

    $data['articles']   = $articles;
    
    return view('frontend.content.article',$data);
  }

  public function listNews(){
    //dd('news');
    $news = News::where('lang', strtoupper($this->lang))->orderBy('id', 'DESC')->paginate(6);
    $data['news'] = $news;
    return view('frontend.content.news', $data);
  }

  public function search(Request $request){
    $q        = $request->get('q');
    $articles = Article::where('language', strtoupper($this->lang))->where('judul', 'like', '%'.$q.'%')->limit(6)->get();
    $news     = News::where('lang', strtoupper($this->lang))->Where('title', 'like', '%'. $q .'%')->limit(6)->get();
    $pages    = Page::where('lang', strtoupper($this->lang))->where('title', 'like', '%'. $q .'%')->limit(6)->get();
    $data['articles'] = $articles;
    $data['news']     = $news;
  
    return view('frontend.content.search', $data);
  }

  public function findBengkel(Request $request){
    $find = urldecode($request->get('q'));
    $workshops = Workshop::where('name', 'like', '%'.$find.'%')->get();
    $cities = City::with('workshop')->where('city_name', '=', $find)->first();
    //dd($cities);
    $html = '<div class="row workshop-delete">';
    if($workshops != null){
      foreach($workshops as $workshop){
        $html .= '<div class="col-md-6">
          <div class="adira-garage-wrap">
            <div class="adira-garage-location-label p-2">
              <span>'.$workshop->name.'</span>
            </div>
            <div class="p-2">
              <table>
                <tr>
                  <td><i class="fas fa-map-marker-alt adira-garage-icon"></i> &nbsp;</td><td> '.$workshop->address.'</td>
                </tr>
                <tr>
                  <td><i class="fas fa-phone-alt adira-garage-icon"></i></i> &nbsp;</td><td> '.$workshop->phone.'</td>
                </tr>
                <tr>
                  <td><img src="/frontend/assets/images/common/office.png" alt=""> &nbsp;</td><td>'.$workshop->authorize.'</td>
                </tr>
                <tr>
                  <td><i class="fas fa-fax adira-garage-icon"></i> &nbsp;</td><td> '.$workshop->fax.'</td>
                </tr>
              </table>
            </div>
          </div>
          </div>';
      }
    }

    if($cities != null){
      foreach($cities['workshop'] as $city){
        $html .= '<div class="col-md-6">
          <div class="adira-garage-wrap">
            <div class="adira-garage-location-label p-2">
              <span>'.$city->name.'</span>
            </div>
            <div class="p-2">
              <table>
                <tr>
                  <td><i class="fas fa-map-marker-alt adira-garage-icon"></i> &nbsp;</td><td> '.$city->address.'</td>
                </tr>
                <tr>
                  <td><i class="fas fa-phone-alt adira-garage-icon"></i></i> &nbsp;</td><td> '.$city->phone.'</td>
                </tr>
                <tr>
                  <td><img src="/frontend/assets/images/common/office.png" alt=""> &nbsp;</td><td> Non Authorize</td>
                </tr>
                <tr>
                  <td><i class="fas fa-fax adira-garage-icon"></i> &nbsp;</td><td> '.$city->fax.'</td>
                </tr>
              </table>
            </div>
          </div>
          </div>';
      }
    }
    $html .= '</div>';
    
    return response()->json($html);
  }

  public function findOffice(Request $request){
    $find = urldecode($request->get('q'));
    $offices = City::with(['office', 'provinces'])->where('city_name', '=', $find)->first();
    //dd($offices['office']);
    $html = '<div class="row remove-office">';
    if($offices != null){
      $office_type = "";
      $office_type_down = "";
      $authorize = "";
      foreach($offices['office'] as $office){
        if($office->office_type == "head-office"){
          if($this->lang == "id"){
            $office_type = "KANTOR PUSAT";
            $office_type_down = "Kantor Pusat";
            $authorize = "Kantor Pusat";
          }else{
            $office_type = "HEAD OFFICE";
            $office_type_down = "Head Office";
            $authorize = "Head Office";
          }
        }else{
          $office_type = strtoupper($office->cities->city_name);
          if($this->lang = "id"){
            $office_type_down = "Kepala Kantor : ".$office->head_office;
            $authorize = "Kantor Cabang";
          }else{
            $office_type_down = "Branch Manager : ".$office->head_office;
            $authorize = "Branch Office";
          }
        }
        $html .= '<div class="col-md-6">
          <div class="adira-garage-wrap">
            <div class="adira-garage-location-label p-2">
              <span>'.$office->provinces->province_name.' - '.$office_type.'</span>
            </div>
            <div class="adira-garage-location-label p-2">
              <span>'.$office_type_down.'</span>
            </div>
            <div class="p-2">
              <table>
                <tr>
                  <td><i class="fas fa-map-marker-alt adira-garage-icon"></i> &nbsp;</td><td> '.$office->address.'</td>
                </tr>
                <tr>
                  <td><i class="fas fa-phone-alt adira-garage-icon"></i></i> &nbsp;</td><td> '.$office->phone_number.'</td>
                </tr>
                <tr>
                  <td><img src="/frontend/assets/images/common/office.png" alt=""> &nbsp;</td><td> '.$authorize.'</td>
                </tr>
                <tr>
                  <td><i class="fas fa-fax adira-garage-icon"></i> &nbsp;</td><td> '.$office->fax.'</td>
                </tr>
              </table>
            </div>
          </div>
          </div>';
      }
    }
    $html .= '</div>';
    
    return response()->json($html);
  }

  public function contact(Request $request){
    $validatedData = Validator::make($request->all(), [
      'name'          => 'required',
      'email'   => 'required',
      'phone_number' => 'required',
      'message' => 'required'
    ]);

    if(!$validatedData->fails()) {
      // Mail::to($request->email)->send(new ContactEmail($request));
      $contact = new ContactInbox();
      $contact->name = $request->name;
      $contact->email = $request->email;
      $contact->phone_number = $request->phone_number;
      $contact->message = $request->message;
      $contact->save();

      Session::flash('success', trans('global.success'));
      return Redirect::back();
    }

    return Redirect::back()->withErrors($validatedData)->withInput();
  }

  public function articles_slug($slug) {
    $articles = Article::where('judul', 'like', '%' . $slug . '%')->where('language',strtoupper($this->lang))->paginate(6);
    
    $data['articles']   = $articles;
    
    return view('frontend.content.article_tag',$data);
  }
}
