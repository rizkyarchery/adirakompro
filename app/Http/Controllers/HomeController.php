<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Setting\Models\Setting;
use Illuminate\Encryption\Encrypter;

class HomeController extends Controller
{
  private const OPENSSL_CIPHER_NAME = "aes-128-cbc";
  private const CIPHER_KEY_LEN = 16; //128 bits

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    { 
      $settingweb = Setting::find('001');
      return view('admin.adminlte',['settingweb' => $settingweb]);
    }
}
