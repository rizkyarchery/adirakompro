<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Validator;
use DB;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Auth\LoginController as Auth;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/webadmin';

    public function reset(Request $request){
        //some validation
        $message = [
            "password.min" => "Password Minimal 10 Character",
            "password.regex" => "Password Must Have 1 Number, 1 Lowercase, 1 Uppercase and 1 Symbol"
        ];

        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:users,email',
            'password' => ['required', 'min:10', 'confirmed', 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$/']
        ], $message);

        if(!$validator->fails()){
            $password = $request->password;
            $tokenData = DB::table('password_resets')
            ->where('email', $request->email)->first();

            $user = User::where('email', $tokenData->email)->first();
            if ( !$user ) return redirect()->to('home'); //or wherever you want

            $user->password = Hash::make($password);
            $user->update(); //or $user->save();

            //do we log the user directly or let them login and try their password for the first time ? if yes 
            // Auth::login($user);

            // If the user shouldn't reuse the token later, delete the token 
            DB::table('password_resets')->where('email', $user->email)->delete();
            return redirect(config('app.app_prefix').'/login');
        }

        return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
    }
}
