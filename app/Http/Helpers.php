<?php

function secEnv($enc){
  $options = 0;
  
  // Store the cipher method 
  $ciphering = "AES-128-CTR"; 
  // Non-NULL Initialization Vector for decryption 
  $decryption_iv = '1234567891011121'; 
    
  // Store the decryption key 
  $decryption_key = "bandingincom"; 
    
  // Use openssl_decrypt() function to decrypt the data 
  $decryption=openssl_decrypt ($enc, $ciphering,  
          $decryption_key, $options, $decryption_iv); 
    
  // Display the decrypted string 
  $dec = str_replace('bandingincom', '', $decryption);
  return $dec; 
}

function checkSlug($table,$slug) {
  $new_slug = null;
  $object   =      DB::table($table)->where('slug', 'like', $slug . '%')->count();

  if($object > 0) {
    $new_slug = $slug . "-" . ($object + 1);
  } else {
    $new_slug = $slug;
  }

  return $new_slug;
}
