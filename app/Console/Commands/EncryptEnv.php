<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Encryption\Encrypter;

class EncryptEnv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'env-encrypt';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Encrypt environment variable value so it can be safely stored';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $crypt = new \Illuminate\Encryption\Encrypter(config('env.key'));
        
        // Read .env-file
        $env = file(base_path() . '/.env');

        foreach($env as $env_key => $env_value) {
            $env[$env_key] = $env_value; //Default env value
            $entry = explode("=", $env_value, 2);
            if (sizeof($entry) > 1) {
                if (substr($entry[1], 0, 3) == 'IN:') {
                    //Overwrite env value
                    $env[$env_key] = $entry[0].'=OUT:'.$crypt->encrypt(substr($entry[1], 3)).PHP_EOL;
                }
            }
        }

        $env = implode("", $env);
        file_put_contents(base_path() . '/.env', $env);
    }
}
