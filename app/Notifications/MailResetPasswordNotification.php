<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class MailResetPasswordNotification extends Notification
{
    use Queueable;

    public $token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
      $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $link = url(config('app.app_prefix') . "/password/reset/" . $this->token );
        $name = null;
        
        try {
            $user = User::where('email',$email)->first();

            $name = $user->name;
        } catch (\Throwable $th) {
            //throw $th;
        }

      return ( new MailMessage )
         ->markdown('vendor.notifications.email')
         ->from(config('mail.sender'))
         ->greeting('Dear ' . $notifiable->name . ',') // Applicant name 
         ->subject( 'Reset your password' )
         ->line( "We received a request to reset your password. " )
         ->line( "Here is the link to reset your password: " )
         ->action( 'Reset Password', $link )
         ->line( 'If you do not request to change your password please ignore this email.' );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
