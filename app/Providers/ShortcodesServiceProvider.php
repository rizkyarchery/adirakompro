<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Shortcode;

class ShortcodesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        Shortcode::register('accordion', 'App\Shortcode\PageShortCode@accordion');
        Shortcode::register('accordion-text', 'App\Shortcode\PageShortCode@accordionText');
        Shortcode::register('accordion-image', 'App\Shortcode\PageShortCode@accordionImage');
        Shortcode::register('accordion-button', 'App\Shortcode\PageShortCode@accordionButton');
        Shortcode::register('accordion-item', 'App\Shortcode\PageShortCode@accordionItem');
        Shortcode::register('accordion-text', 'App\Shortcode\PageShortCode@accordionText');
        Shortcode::register('accordion-half', 'App\Shortcode\PageShortCode@accordionHalf');
        Shortcode::register('accordion-annual-report', 'App\Shortcode\PageShortCode@annualReportAccordion');
        Shortcode::register('accordion-management', 'App\Shortcode\PageShortCode@managementAccordion');
        Shortcode::register('accordion-finance-report', 'App\Shortcode\PageShortCode@accordionFinanceReport');
        Shortcode::register('accordion-workshop', 'App\Shortcode\PageShortCode@workshopPage');
        Shortcode::register('accordion-office',         'App\Shortcode\PageShortCode@branchPage');
        Shortcode::register('sitemap',                  'App\Shortcode\PageShortCode@sitemapSection');
        Shortcode::register('sitemap-item',             'App\Shortcode\PageShortCode@sitemapItem');
        Shortcode::register('sitemap-item-bold',        'App\Shortcode\PageShortCode@sitemapLabelBold');
        Shortcode::register('sitemap-container',        'App\Shortcode\PageShortCode@sitemapContainer');
        Shortcode::register('contact-form',             'App\Shortcode\PageShortCode@contactForm');
        Shortcode::register('find-workshop',            'App\Shortcode\PageShortCode@findBengkelTextbox');
        Shortcode::register('find-office',              'App\Shortcode\PageShortCode@findOfficeTextbox');
        Shortcode::register('career',                   'App\Shortcode\PageShortCode@careerPage');
        Shortcode::register('download',                 'App\Shortcode\PageShortCode@buttonDownload');
        Shortcode::register('tab',                      'App\Shortcode\PageShortCode@tabs');
        Shortcode::register('tab-item',                 'App\Shortcode\PageShortCode@tabItem');
        Shortcode::register('text',                     'App\Shortcode\PageShortCode@textPage');
        Shortcode::register('title',                    'App\Shortcode\PageShortCode@titlePage');
        Shortcode::register('section',                  'App\Shortcode\PageShortCode@section');
        Shortcode::register('section-half',             'App\Shortcode\PageShortCode@halfSection');
        Shortcode::register('section-image',            'App\Shortcode\PageShortCode@imageSection');
        Shortcode::register('section-text',             'App\Shortcode\PageShortCode@textSection');
        Shortcode::register('gallery',                  'App\Shortcode\PageShortCode@gallery');
        Shortcode::register('gallery-item',             'App\Shortcode\PageShortCode@galleryItem');
        Shortcode::register('gallery-item-service',     'App\Shortcode\PageShortCode@galleryItemService');
        Shortcode::register('testimony',                'App\Shortcode\PageShortCode@testimony');
        Shortcode::register('award',                    'App\Shortcode\PageShortCode@awardAccordion');
        Shortcode::register('yellow-button',            'App\Shortcode\PageShortCode@downloadService');
        Shortcode::register('product',                  'App\Shortcode\PageShortCode@productSection');
        Shortcode::register('list-parent',                  'App\Shortcode\PageShortCode@listParent');
        Shortcode::register('list-child',                  'App\Shortcode\PageShortCode@listChild');
    }
}
