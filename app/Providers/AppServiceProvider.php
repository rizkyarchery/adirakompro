<?php

namespace App\Providers;

use DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
// use Spatie\Permission\Models\Permission;
use App\Modules\Permission\Http\Controllers\PermissionController as Permission;

use Illuminate\Support\Facades\Crypt;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $exist = Schema::hasTable('permissions');

        if($exist) {
            $permissions = [
                [
                    "name"              => "View Role",
                    "description"       => "to view role"
                ],
                [
                    "name"              => "Create Role",
                    "description"       => "to Create role"
                ],
                [
                    "name"              => "Edit Role",
                    "description"       => "to Edit role"
                ],
                [
                    "name"              => "Delete User",
                    "description"       => "to Delete User"
                ],
                [
                    "name"              => "View User",
                    "description"       => "to view User"
                ],
                [
                    "name"              => "Create User",
                    "description"       => "to Create User"
                ],
                [
                    "name"              => "Edit User",
                    "description"       => "to Edit User"
                ],
                [
                    "name"              => "Delete User",
                    "description"       => "to Delete User"
                ],
                [
                    "name"              => "View Article",
                    "description"       => "to view Article"
                ],
                [
                    "name"              => "Create Article",
                    "description"       => "to Create Article"
                ],
                [
                    "name"              => "Edit Article",
                    "description"       => "to Edit Article"
                ],
                [
                    "name"              => "Delete Article",
                    "description"       => "to Delete Article"
                ],
                [
                    "name"              => "View Award",
                    "description"       => "to view Award"
                ],
                [
                    "name"              => "Create Award",
                    "description"       => "to Create Award"
                ],
                [
                    "name"              => "Edit Award",
                    "description"       => "to Edit Award"
                ],
                [
                    "name"              => "Delete Award",
                    "description"       => "to Delete Award"
                ],
                [
                    "name"              => "View Workshop",
                    "description"       => "to view Workshop"
                ],
                [
                    "name"              => "Create Workshop",
                    "description"       => "to Create Workshop"
                ],
                [
                    "name"              => "Edit Workshop",
                    "description"       => "to Edit Workshop"
                ],
                [
                    "name"              => "Delete Workshop",
                    "description"       => "to Delete Workshop"
                ],
                [
                    "name"              => "View Banner",
                    "description"       => "to view Banner"
                ],
                [
                    "name"              => "Create Banner",
                    "description"       => "to Create Banner"
                ],
                [
                    "name"              => "Edit Banner",
                    "description"       => "to Edit Banner"
                ],
                [
                    "name"              => "Delete Banner",
                    "description"       => "to Delete Banner"
                ],
                [
                    "name"              => "View Testimony",
                    "description"       => "to view Testimony"
                ],
                [
                    "name"              => "Create Testimony",
                    "description"       => "to Create Testimony"
                ],
                [
                    "name"              => "Edit Testimony",
                    "description"       => "to Edit Testimony"
                ],
                [
                    "name"              => "Delete Testimony",
                    "description"       => "to Delete Testimony"
                ],
                [
                    "name"              => "View Product",
                    "description"       => "to view Product"
                ],
                [
                    "name"              => "Create Product",
                    "description"       => "to Create Product"
                ],
                [
                    "name"              => "Edit Product",
                    "description"       => "to Edit Product"
                ],
                [
                    "name"              => "Delete Product",
                    "description"       => "to Delete Product"
                ],
                [
                    "name"              => "View Page",
                    "description"       => "to view Page"
                ],
                [
                    "name"              => "Create Page",
                    "description"       => "to Create Page"
                ],
                [
                    "name"              => "Edit Page",
                    "description"       => "to Edit Page"
                ],
                [
                    "name"              => "Delete Page",
                    "description"       => "to Delete Page"
                ],
                [
                    "name"              => "View Master Image",
                    "description"       => "to view Master Image"
                ],
                [
                    "name"              => "Create Master Image",
                    "description"       => "to Create Master Image"
                ],
                [
                    "name"              => "Edit Master Image",
                    "description"       => "to Edit Master Image"
                ],
                [
                    "name"              => "Delete Master Image",
                    "description"       => "to Delete Master Image"
                ],
                [
                    "name"              => "View Career",
                    "description"       => "to view Career"
                ],
                [
                    "name"              => "Create Career",
                    "description"       => "to Create Career"
                ],
                [
                    "name"              => "Edit Career",
                    "description"       => "to Edit Career"
                ],
                [
                    "name"              => "Delete Career",
                    "description"       => "to Delete Career"
                ],
                [
                    "name"              => "View Footer Brand",
                    "description"       => "to view Footer Brand"
                ],
                [
                    "name"              => "Create Footer Brand",
                    "description"       => "to Create Footer Brand"
                ],
                [
                    "name"              => "Edit Footer Brand",
                    "description"       => "to Edit Footer Brand"
                ],
                [
                    "name"              => "Delete Footer Brand",
                    "description"       => "to Delete Footer Brand"
                ],
                [
                    "name"              => "View Annual Report",
                    "description"       => "to view Annual Report"
                ],
                [
                    "name"              => "Create Annual Report",
                    "description"       => "to Create Annual Report"
                ],
                [
                    "name"              => "Edit Annual Report",
                    "description"       => "to Edit Annual Report"
                ],
                [
                    "name"              => "Delete Annual Report",
                    "description"       => "to Delete Annual Report"
                ],
                [
                    "name"              => "View Menu",
                    "description"       => "to view Menu"
                ],
                [
                    "name"              => "Create Menu",
                    "description"       => "to Create Menu"
                ],
                [
                    "name"              => "Edit Menu",
                    "description"       => "to Edit Menu"
                ],
                [
                    "name"              => "Delete Menu",
                    "description"       => "to Delete Menu"
                ],
                [
                    "name"              => "View Branch Office",
                    "description"       => "to view Branch Office"
                ],
                [
                    "name"              => "Create Branch Office",
                    "description"       => "to Create Branch Office"
                ],
                [
                    "name"              => "Edit Branch Office",
                    "description"       => "to Edit Branch Office"
                ],
                [
                    "name"              => "Delete Branch Office",
                    "description"       => "to Delete Branch Office"
                ],
                [
                    "name"              => "View File",
                    "description"       => "to view File"
                ],
                [
                    "name"              => "Create File",
                    "description"       => "to Create File"
                ],
                [
                    "name"              => "Edit File",
                    "description"       => "to Edit File"
                ],
                [
                    "name"              => "Delete File",
                    "description"       => "to Delete File"
                ],
                [
                    "name"              => "View News",
                    "description"       => "to view News"
                ],
                [
                    "name"              => "Create News",
                    "description"       => "to Create News"
                ],
                [
                    "name"              => "Edit News",
                    "description"       => "to Edit News"
                ],
                [
                    "name"              => "Delete News",
                    "description"       => "to Delete News"
                ],
                [
                    "name"              => "View Finance Report",
                    "description"       => "to view Finance Report"
                ],
                [
                    "name"              => "Create Finance Report",
                    "description"       => "to Create Finance Report"
                ],
                [
                    "name"              => "Edit Finance Report",
                    "description"       => "to Edit Finance Report"
                ],
                [
                    "name"              => "Delete Finance Report",
                    "description"       => "to Delete Finance Report"
                ],
                [
                    "name"              => "View Organizational Structure",
                    "description"       => "to view Organizational Structure"
                ],
                [
                    "name"              => "Create Organizational Structure",
                    "description"       => "to Create Organizational Structure"
                ],
                [
                    "name"              => "Edit Organizational Structure",
                    "description"       => "to Edit Organizational Structure"
                ],
                [
                    "name"              => "Delete Organizational Structure",
                    "description"       => "to Delete Organizational Structure"
                ],
                [
                    "name"              => "Manage Setting",
                    "description"       => "to Manage Setting"
                ],
                [
                    "name"              => "View Contact Inbox",
                    "description"       => "to View Contact Inbox"
                ]
            ];
    
            // Save Permission
            $permission                 = new Permission;
            $permission->savePermission($permissions);
        }
        
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }
}
