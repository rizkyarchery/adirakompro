<?php

namespace App\Shortcode;

use App;
use Illuminate\Support\Str;
use Illuminate\Support\ServiceProvider;
use App\Modules\Testimony\Models\Testimoni;
use App\Modules\Award\Models\Award;
use App\Modules\Anualreport\Models\AnnualReport;
use App\Modules\Report\Models\Report;
use App\Modules\Structureorganisation\Models\StructureOrganisation;
use App\Modules\Career\Models\Career;
use App\Modules\Workshop\Models\Workshop;
use App\Modules\Branchoffice\Models\BranchOffice;
use App\Modules\Product\Models\Produk;

class PageShortCode
{
    public function accordion($shortcode, $content, $compiler, $name){
        $html = '<div class="mb-5 mt-3" id="accordion">';
        $html .= $content;
        $html .= '</div>'; 
        //dd($content);
        return $html;
    }

    public function accordionItem($shortcode, $content, $compiler, $name){
        // $html = '<div class="card">';
        // $html .= '<div class="card-header" id="'.$shortcode->id.'">';
        // $html .= '<h2 class="mb-0">';
        // $html .= '<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#'.$shortcode->item.'" aria-expanded="true" aria-controls="collapse_'.$shortcode->item.'">';
        // $html .= $shortcode->title;
        // $html .= '</button>';
        // $html .= '</h2>';
        // $html .= '</div>';
        // //dd();
        // if($shortcode->item == "collapse_1"){
        //     $html .= '<div id="'.$shortcode->item.'" class="collapse show" aria-labelledby="'.$shortcode->id.'" data-parent="#accordionExample">';
        // }else{
        //     $html .= '<div id="'.$shortcode->item.'" class="collapse" aria-labelledby="'.$shortcode->id.'" data-parent="#accordionExample">';
        // }
        // $html .= '<div class="card-body">';
        // $html .= $content;
        // $html .= '</div>';
        // $html .= '</div>';
        // $html .= '</div>';
        if($shortcode->item == "collapse_1"){
            $html = '<div class="accordion accordion-adira bg-gray accordion-active" style="display:flex;">';
        }else{
            $html = '<div class="accordion accordion-adira bg-white" style="display:flex;">';
        }
        $html .= '<i class="fas fa-chevron-circle-right icon-accordion" style="font-size:27px;"></i> <span style="margin-left: 25px;"> '.$shortcode->title.'</span>';
        $html .= '</div>';
        if($shortcode->item == "collapse_1"){
            $html .= '<div class="panel collapse_1" style="max-height:136px;">';
        }else{
            $html .= '<div class="panel">';
        }
        $html .= '<div class="row p-3">';
        $html .= $content;
        $html .= '</div>';
        $html .= '</div>';
        return $html;
    }

    public function textPage($shortcode, $content, $compiler, $name){
        $html = '<p style="text-align: justify;">';
        $html .= $content;
        $html .= '</p>';
        return $html;
    }

    public function titlePage($shortcode, $content, $compiler, $name){
        $html = '<h1>';
        $html .= $content;
        $html .= '</h1>';
        return $html;
    }

    public function accordionText($shortcode, $content, $compiler, $name){
        $html = '<p style="text-align: justify;">'.$content.'</p>';
        return $html;
    }

    public function accordionImage($shortcode, $content, $compiler, $name){
        $html = '<img src="'.$shortcode->link.'" class="mb-3" style="width:100%"/>';
        return $html;
    }

    public function accordionButton($shortcode, $content, $compiler, $name){
        $html = '<a class="btn btn-secondary mb-2" href="'.$shortcode->link.'">';
        $html .= $content;
        $html .= '</a>';
        return $html;
    }

    public function accordionHalf($shortcode, $content, $compiler, $name){
        $html = '<div class="col-md-6">';
        $html .= $content;
        $html .= '</div>';
        return $html;
    }

    public function section($shortcode, $content, $compiler, $name){
        if($shortcode->col % 2 == 0){
            $html = '<section class="section p-5 adira-bg-white">';
        } else{
            $html = '<section class="section p-5 adira-bg-yellow">';
        }
        $html .= '<div class="container">';
        if($shortcode->title != null){
            $html .= '<div class="separator">'.$shortcode->title.'</div>';
        }
        $html .= '<div class="row mt-4">';
        if($shortcode->description != null){
            $html .= '<div class="p-2">';
            $html .= '<div class="text-center">';
            $html .= '<p>'.$shortcode->description.'</p>';
            $html .= '</div>';
            $html .= '</div>';
        }
        $html .= $content;
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</section>';
        return $html;
    }

    public function halfSection($shortcode, $content, $compiler, $name){
        $html = '<div class="col-md-6">';
        $html .= $content;
        $html .= '</div>';
        return $html;
    }

    public function imageSection($shortcode, $content, $compiler, $name){
        $html = '<img src="'.$shortcode->link.'" class="w-100"/>';
        //dd($shortcode);
        return $html;
    }

    public function textSection($shortcode, $content, $compiler, $name){
        $html = '<div class="content-text">';
        $html .= '<p>'.$content.'</p>';
        if($shortcode->button != null){
            $html .= '<a class="btn btn-dark" href="'.$shortcode->link.'">'.$shortcode->button.'</a>';
        }
        $html .= '</div>';
        return $html;
    }

    public function gallery($shortcode, $content, $compiler, $name){
        $html = '<div class="service-main-gallery">';
        $html .= '<div class="service-inner">';
        $html .= $content;
        $html .= '</div>';
        $html .= '</div>';
        return $html;
    }

    public function galleryItem($shortcode, $content, $compiler, $name){
        $html = '<div class="tile mt-2">';
        $html .= '<a href="'.$shortcode->href.'"><img src="'.$shortcode->link.'" class="w-100 mb-3" alt=""></a>';
        $html .= '</div>';
        return $html;
    }

    public function galleryItemService($shortcode, $content, $compiler, $name){
        $html = '<div class="adira-gallery-item-tile mt-2">';
        $html .= '  <div class="adira-gallery-item-service-parent">';
        $html .= '      <div class="adira-gallery-item-service p-3">';
        $html .= '          <a href="'.$shortcode->link.'" target="_BLANK">';
        $html .= '          <div class="adira-gallery-item-service-title">';
        $html .= '              <h3>'.$shortcode->title.'</h3>';
        $html .= '          </div>';
        $html .= '          </a>';
        $html .= '          <a href="'.$shortcode->link.'" target="_BLANK">';
        $html .= '              <div class="adira-gallery-item-service-description">';
        $html .= '                  <span>'.Str::limit($shortcode->description, 100,'...').'</span>';
        $html .= '              </div>';
        $html .= '          </a>';
        $html .= '          <div class="mt-1" style="display:flex;justify-content:center;">';
        $html .= '              <a target="_BLANK" href="https://www.facebook.com/sharer/sharer.php?u='.$shortcode->link.'">';
        $html .= '                  <img src="/frontend/assets/images/common/facebook.png" />';
        $html .= '              </a>';
        $html .= '              <a target="_BLANK" href="http://www.twitter.com/share?url='.$shortcode->link.'">';
        $html .= '                  <img src="/frontend/assets/images/common/twitter.png" />';
        $html .= '              </a>';
        $html .= '          </div>';
        $html .= '      </div>';
        $html .= '  </div>';
        $html .= '  <img src="'.$shortcode->img.'" class="w-100 mb-3" height="" alt="">';
        $html .= '</div>';
        return $html;
    }

    public function testimony($shortcode, $content, $compiler, $name){
        $html = '<div id="testimony-slider" class="owl-carousel owl-theme">';
        //ITEM
        $testis = Testimoni::where('status', 1)->orderBy('id', 'DESC')->limit(5)->get();
        foreach($testis as $testi){
        $html .= '<div class="testimony-item">';
        $html .= '<div class="row">';
        $html .= '<div class="col-md-3">';
        $html .= '<div style="margin: 30px;">';
        $html .= '<img src="/'.$testi->foto.'" class="w-100 rounded-circle" alt="">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="col-md-8 border-testi">';
        $html .= '<p>'.$testi->ket.'</p>';
        $html .= '<span style="font-weight:bold;">'.$testi->nama.'</span>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        }
        //END ITEM
        $html .= '</div>';
        return $html;
    }

    public function awardAccordion($shortcode, $content, $compiler, $name){
        $html = '';
        $awards = Award::where('year', $shortcode->year)->where('lang',strtoupper(App::getLocale()))->get();
        foreach($awards as $award){
        $html .= '<div class="col-md-6">';
        $html .= '<div class="row my-5">';
        $html .= '<div class="col-md-6">';
        $html .= '<img src="/'.$award->image.'" data-img="/'.$award->image.'" class="w-100 imgmodal"  alt="'.$award->alt.'">';
        $html .= '</div>';
        $html .= '<div class="col-md-6">';
        $html .= '<table>';
        $html .= '<tr>';
        $html .= '<td>';
        //ID
        // if($shortcode->lang == "ID"){
            $html .= '<tr>';
            $html .= '<td class="font-weight-bold">'.trans('global.year').'</td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td>'.$award->year.'</td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td class="font-weight-bold">'.trans('global.award').'</td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td>'.$award->award.'</td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td class="font-weight-bold">Brand</td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td>'.$award->brand.'</td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td class="font-weight-bold">'.trans('global.from').'</td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td>'.$award->from.'</td>';
            $html .= '</tr>';
        // }
        // else{
        //     $html .= '<tr>';
        //     $html .= '<td class="font-weight-bold">Year</td>';
        //     $html .= '</tr>';
        //     $html .= '<tr>';
        //     $html .= '<td>'.$award->year.'</td>';
        //     $html .= '</tr>';
        //     $html .= '<tr>';
        //     $html .= '<td class="font-weight-bold">Award</td>';
        //     $html .= '</tr>';
        //     $html .= '<tr>';
        //     $html .= '<td>'.$award->award.'</td>';
        //     $html .= '</tr>';
        //     $html .= '<tr>';
        //     $html .= '<td class="font-weight-bold">Brand</td>';
        //     $html .= '</tr>';
        //     $html .= '<tr>';
        //     $html .= '<td>'.$award->brand.'</td>';
        //     $html .= '</tr>';
        //     $html .= '<tr>';
        //     $html .= '<td class="font-weight-bold">From</td>';
        //     $html .= '</tr>';
        //     $html .= '<tr>';
        //     $html .= '<td>'.$award->from.'</td>';
        //     $html .= '</tr>';
        // }
        //END ID
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        // END ITEM
        }
        return $html;
    }

    public function annualReportAccordion($shortcode, $content, $compiler, $name){
        $years = AnnualReport::where('year', '>=', $shortcode->from)
                        ->where('year', '<=', $shortcode->to)->where('lang', '=', $shortcode->lang)->get();
        $html = '<div class="accordion accordion-adira accordion-active bg-gray" style="display:flex;">';
        $html .= '<i class="fas fa-chevron-circle-right icon-accordion" style="font-size:27px;"></i> <span style="margin-left: 25px;">'.trans('global.annual_report').' '.$shortcode->from.' - '.$shortcode->to.'</span>';
        $html .= '</div>';
        //OPEN
        if($shortcode->num == 1){
            $html .= '<div class="panel collapse_1">';
        }else{
            $html .= '<div class="panel">';
        }
        //END OPEN
        $html .= '<div class="p-3">';
        $html .= '<div class="row">';
        //ITEM
        foreach($years as $year){
        $html .= '<div class="col-md-6">';
        $html .= '<div class="row my-5">';
        $html .= '<div class="col-md-6">';
        $html .= '<img src="/'.$year->image.'" class="w-100"  alt="'.$year->name.'">';
        $html .= '</div>';
        $html .= '<div class="col-md-6">';
        $html .= '<table>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<tr>';
        $html .= '<td class="font-weight-bold">'.$year->name.'</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>'.trans('global.release_date').' '.date('d/m/Y', strtotime($year->release_date)).'</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>'.$year->description.'</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td><a class="mt-5 btn btn-secondary" target="_blank" href="/'.$year->file.'">'.trans('global.download').'</a></td>';
        $html .= '</tr>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        // END ITEM
        }
        $html .= '</div>';
        //END YEAR
        $html .= '</div>';
        $html .= '</div>';
        return $html;
    }

    public function managementAccordion($shortcode, $content, $compiler, $name){
        $structures = StructureOrganisation::where('position', $shortcode->position)->get();
        $html = '';
        foreach($structures as $structure){
            $html .= '<div class="col-md-4">';
            $html .= '<div class="adira-structure-management-wrapper">';
            $html .= '<div class="adira-structure-management-img">';
            $html .= '<img src="/'.$structure->image.'" alt="">';
            $html .= '</div>';
            $html .= '<div class="p-2">';
            $html .= '<div>';
            $html .= '<span class="bold">'.$structure->name.'</span>';
            $html .= '</div>';
            $html .= '<div class="mb-3">';
            $html .= '<small>'.$structure->position_name.'</small>';
            $html .= '</div>';
            $html .= '<div class="mb-1">';
            $html .= '<span>'.Str::limit($structure->description, 50,'...').'</span>';
            $html .= '</div>';
            $html .= '<div class="mb-1 adira-structure-management-read-more">';
            $html .= '<u>';
            $html .= '<a href="#" class="structure-modal" 
            data-structure-name="'.$structure->name.'"
            data-structure-council="'.$structure->position.'"
            data-structure-description="'.$structure->description.'"
            data-structure-image="/'.$structure->image.'">';
            if($shortcode->lang == 'ID'){
                $html .= 'Profile Selengkapnya</a>';
            }else{
                $html .= 'Full Profile</a>';
            }
            $html .= '</u>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
        }
        return $html;
    }

    public function tabs($shortcode, $content, $compiler, $name){
        $html = '<div class="adira-tabs">';
        $html .= $content;
        $html .= '</div>';
        return $html;
    }

    public function tabItem($shortcode, $content, $compiler, $name){
        if($shortcode->num == 1){
            $html = '<input type="radio" name="tabs" id="tab_'.$shortcode->num.'" checked="checked">';
        }else{
            $html = '<input type="radio" name="tabs" id="tab_'.$shortcode->num.'">';
        }
        $html .= '<label for="tab_'.$shortcode->num.'">'.$shortcode->title.'</label>';
        $html .= '<div class="adira-tab">';
        $html .= $content;
        $html .= '</div>';
        return $html;
    }

    public function accordionFinanceReport($shortcode, $content, $compiler, $name){
        $years = Report::where('year', '>=', $shortcode->from)
                        ->where('year', '<=', $shortcode->to)
                        ->where('report_type', '=', $shortcode->type)
                        ->where('lang', '=', $shortcode->lang)->get();
        $html = '<div class="accordion accordion-adira accordion-active bg-gray" style="display:flex;">';
        $html .= '<i class="fas fa-chevron-circle-right icon-accordion" style="font-size:27px;"></i> <span style="margin-left: 25px;">'.trans('global.finance_report').' '.$shortcode->from.' - '.$shortcode->to.'</span>';
        $html .= '</div>';
        //OPEN
        if($shortcode->num == 1){
            $html .= '<div class="panel collapse_1">';
        }else{
            $html .= '<div class="panel">';
        }
        //END OPEN
        $html .= '<div class="p-3">';
        $html .= '<div class="row">';
        //ITEM
        foreach($years as $year){
        $html .= '<div class="col-md-6">';
        $html .= '<div class="row my-1">';
        $html .= '<div class="col-md-12">';
        $html .= '<table>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<tr>';
        $html .= '<td>'.$year->name.'</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<tr>';
        $html .= '<td style="font-size: 10px;">'.trans('global.release_date').' '.date('d/m/Y', strtotime($year->release_date)).'</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td><a class="mt-2 btn btn-secondary" target="_blank" href="/'.$year->file.'">'.trans('global.download').'</a></td>';
        $html .= '</tr>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        // END ITEM
        }
        $html .= '</div>';
        //END YEAR
        $html .= '</div>';
        $html .= '</div>';
        return $html;
    }

    public function buttonDownload($shortcode, $content, $compiler, $name){
        $html = '<a href="'.$shortcode->link.'" class="btn btn-secondary" target="_blank">'.$shortcode->title.'</a>';
        return $html;
    }

    public function careerPage($shortcode, $content, $compiler, $name){
        $html = '<div class="row">';
        //ITEM
        $careers = Career::orderBy('id', 'DESC')->get();
        foreach($careers as $career){
        $html .= '<div class="col-md-6 mt-3">';
        $html .= '<div class="adira-carrer-wrap">';
        $html .= '<div class="adira-carrer-position">';
        $html .= '<span>'.$career->position.'</span>';
        $html .= '</div>';
        $html .= '<div class="mt-2">';
        $html .= '<i class="far fa-circle pr-2"></i> <span>Min: '.$career->min_experience.'</span>';
        $html .= '</div>';
        $html .= '<div class="mt-2">';
        $html .= '<i class="far fa-circle pr-2"></i> <span>Indonesia - '.$career->provinces->province_name.'</span>';
        $html .= '</div>';
        $html .= '<div class="mt-4">';
        $html .= '<a class="btn btn-secondary" href="'.$career->link.'" target="_blank">'.trans('global.job_aplication').'</a>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        }
        //END ITEM
        $html .= '</div>';
        return $html;
    }

    public function workshopPage($shortcode, $content, $compiler, $name){
        $html = '';
        $workshops = Workshop::where('island', $shortcode->island)->get();
        foreach($workshops as $workshop){
        $html .= '<div class="col-md-6">';
        $html .= '<div class="adira-garage-wrap">';
        $html .= '<div class="adira-garage-location-label p-2">';
        $html .= '<span>'.$workshop->name.'</span>';
        $html .= '</div>';
        $html .= '<div class="p-2">';
        $html .= '<table>';
        $html .= '<tr>';
        $html .= '<td><i class="fas fa-map-marker-alt adira-garage-icon"></i> &nbsp;</td><td data-toggle="tooltip" data-placement="right" title="'.$workshop->address.'"> '.Str::limit($workshop->address, 55,'...').'</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td><i class="fas fa-phone-alt adira-garage-icon"></i></i> &nbsp;</td><td> '.$workshop->phone.'</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td><img src="/frontend/assets/images/common/office.png" alt=""> &nbsp;</td><td> '.$workshop->authorize.'</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td><i class="fas fa-fax adira-garage-icon"></i> &nbsp;</td><td> '.$workshop->fax .'</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        }
        return $html;
    }

    public function findBengkelTextbox($shortcode, $content, $compiler, $name){
        $html = '<div class="row">';
        $html .= '<div class="col-md-8">';
        $html .= '<div>';
        if($shortcode->lang == "ID"){
            $html .= '<input type="text" class="form-control adira-input-garage" placeholder="Cari berdasarkan Nama atau Kota" />';
        }else{
            $html .= '<input type="text" class="form-control adira-input-garage" placeholder="Find by Name or City" />';
        }
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="col-md-4">';
        $html .= '<div>';
        if($shortcode->lang == "ID"){
            $html .= '<button class="btn btn-secondary btn-bengkel">Cari</button>';
        }else{
            $html .= '<button class="btn btn-secondary btn-bengkel">Find</button>';
        }
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        return $html;
    }

    public function branchPage($shortcode, $content, $compiler, $name){
        $html = '';
        $offices = BranchOffice::with(['provinces.cities', 'cities'])->where('island', $shortcode->island)->get();
        foreach($offices as $office){
        $html .= '<div class="col-md-6">';
        $html .= '<div class="adira-garage-wrap">';
        $html .= '<div class="adira-garage-location-label p-2">';
        $html .= '<span>'.$office->provinces->province_name.' - ';
        if($office->office_type == 'head-office'){
            if($shortcode->lang == 'ID'){
                $html .= 'KANTOR PUSAT</span>';
            }else{
                $html .= 'HEAD OFFICE</span>';
            }
        }else{
        $html .= strtoupper($office->cities->city_name).'</span>';
        }
        $html .= '</div>';
        $html .= '<div class="adira-garage-location-label p-2">';
        if($office->office_type == 'head-office'){
            if($shortcode->lang == 'ID'){
                $html .= '<span>Kantor Pusat</span>';
            }else{
                $html .= '<span>Head Office</span>';
            }
        }else{
            if($shortcode->lang == 'ID'){
                $html .= '<span>Kepala Kantor : '.$office->head_office.'</span>';
            }else{
                $html .= '<span>Branch Manager : '.$office->head_office.'</span>';
            }
        }
        $html .= '</div>';
        $html .= '<div class="p-2">';
        $html .= '<table>';
        $html .= '<tr>';
        $html .= '<td><i class="fas fa-map-marker-alt adira-garage-icon"></i> &nbsp;</td><td data-toggle="tooltip" data-placement="right" title="'.$office->address.'"> '.Str::limit($office->address, 55,'...').'</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td><i class="fas fa-phone-alt adira-garage-icon"></i></i> &nbsp;</td><td> '.$office->phone_number.'</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        if($office->office_type == 'head-office'){
            if($shortcode->lang == "ID"){
                $html .= '<td><img src="/frontend/assets/images/common/office.png" alt=""> &nbsp;</td><td> Kantor Pusat</td>';
            }else{
                $html .= '<td><img src="/frontend/assets/images/common/office.png" alt=""> &nbsp;</td><td> Head Office</td>';
            }
        }else{
            if($shortcode->lang == "ID"){
                $html .= '<td><img src="/frontend/assets/images/common/office.png" alt=""> &nbsp;</td><td> Kantor Cabang </td>';;
            }else{
                $html .= '<td><img src="/frontend/assets/images/common/office.png" alt=""> &nbsp;</td><td> Branch Office</td>';
            }
        }
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td><i class="fas fa-fax adira-garage-icon"></i> &nbsp;</td><td> '.$office->fax.'</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        }
        return $html;
    }

    public function downloadService($shortcode, $content, $compiler, $name){
        $html = '<section class="section">';
        $html .= '<div class="container mt-5 mb-5">';
        $html .= '<div class="row">';
        $html .= '<div class="col-md-12">';
        $html .= '<a href="'.$shortcode->link.'" target="_blank"><i class="far fa-arrow-alt-circle-down adira-unduh-yellow"></i> <span><u>'.$shortcode->title.'</u></span></a>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</section>';
        return $html;
    }

    public function sitemapContainer($shortcode, $content, $compiler, $name){
        $html = '<div class="row mt-5">';
        $html .= $content;
        $html .= '</div>';
        return $html;
    }

    public function sitemapSection($shortcode, $content, $compiler, $name){
        $html = '<div class="col-md-2">';
        $html .= '<ul class="adira-sitemap-wrap">';
        $html .= $content;
        $html .= '</ul>';
        $html .= '</div>';
        return $html;
    }

    public function sitemapItem($shortcode, $content, $compiler, $name){
        $html = '<a href="'.$shortcode->link.'"><li>'.$content.'</li></a>';
        return $html;
    }

    public function sitemapLabelBold($shortcode, $content, $compiler, $name){
        $html = '<li class="adira-sitemap-bold">'.$content.'</li>';
        return $html;
    }

    public function contactForm($shortcode, $content, $compiler, $name){
       // $prefix = config('app.app_prefix');
        $html = '<div class="row">';
        $html .= '<div class="col-md-12">';
        $html .= '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.151968681224!2d106.85139891413783!3d-6.243694362874458!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f3bb776ff2a5%3A0x9b1ee3e3dbb2a3a!2sGraha%20Adira!5e0!3m2!1sen!2sid!4v1574182011802!5m2!1sen!2sid" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="row mb-5">';
        $html .= '<div class="col-md-7 mt-5">';
        $html .= '<form action="'.trans('global.contact_link').'" method="POST">';
        $html .= csrf_field();
        $html .= '<div class="form-group">';
        $html .= '<label for="name" class="adira-contact-bold">'.trans("global.full_name").' <sup class="text-danger">*</sup></label>';
        $html .= '<input id="name" type="text" value="'.old("name").'" name="name" class="form-control" placeholder="'.trans("global.full_name").'" required/>';
        $html .= '</div>';
        $html .= '<div class="form-group">';
        $html .= '<label for="email" class="adira-contact-bold">'.trans("global.email").' <sup class="text-danger">*</sup></label>';
        $html .= '<input id="email" type="text" name="email" value="'.old("email").'" class="form-control" placeholder="'.trans("global.email").'" required/>';
        $html .= '</div>';
        $html .= '<div class="form-group">';
        $html .= '<label for="telephone" class="adira-contact-bold">'.trans("global.telephone").' <sup class="text-danger">*</sup></label>';
        $html .= '<input id="telephone" name="phone_number" value="'.old("phone_number").'" type="text" class="form-control" placeholder="'.trans("global.telephone").'" required/>';
        $html .= '</div>';
        $html .= '<div class="form-group">';
        $html .= '<label for="message" class="adira-contact-bold">'.trans("global.message").' <sup class="text-danger">*</sup></label>';
        $html .= '<textarea name="message" id="message" cols="30" rows="10" class="form-control" required>'.old("message").'</textarea>';
        $html .= '</div>';
        $html .= '<div class="form-group">';
        $html .= '<button type="submit" class="btn btn-secondary float-right">SEND</button>';
        $html .= '</div>';
        $html .= '</form>';
        $html .= '</div>';
        $html .= '<div class="col-md-4 mt-5">';
        $html .= '<div>';
        $html .= '<ul style="padding:0px;list-style-type: none;">';
        $html .= '<li style="font-weight:bold;">Graha Adira</li>';
        $html .= '<li>MT. Haryono Kav.42</li>';
        $html .= '<li>Jakarta 12780</li>';
        $html .= '<li>T. +62-21 2966 7373 | F. +62-21 2966 7345</li>';
        $html .= '</ul>';
        $html .= '</div>';
        $html .= '<div>';
        $html .= '<ul style="padding:0px;list-style-type: none;">';
        $html .= '<li style="font-weight:bold;">Adira Care</li>';
        $html .= '<li>Call 1500 456</li>';
        $html .= '<li>SMS +62-812 111 3456</li>';
        $html .= '<li>adiracare@asuransiadira.co.id</li>';
        $html .= '</ul>';
        $html .= '</div>';
        $html .= '</div>';
        return $html;
    }

    public function productSection($shortcode, $content, $compiler, $name){
        $html = '<div id="product-slider" class="owl-carousel owl-theme">';
        //ITEM
        $products = Produk::orderBy('id', 'DESC')->get();
        foreach($products as $product){
        $html .= '<div class="product-slider-item p-2">';
        $html .= '<div class="adira-bg-yellow product-item-wrapper">';
        $html .= '<div class="adira-bg-white" style="border-radius: 50%;">';
        $html .= '<a href="'.isset($product->link) ? $product->link : null.'" target="blank"><img src="/Upload/product/'.isset($product->foto) ? $product->foto : null.'" class="product-icon-img" alt="'.isset($product->alt_teks) ? $product->alt_teks : null.'"/></a>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        }
        //END ITEM
        $html .= '</div>';
        return $html;
    }

    public function findOfficeTextbox($shortcode, $content, $compiler, $name){
        $html = '<div class="row">';
        $html .= '<div class="col-md-8">';
        $html .= '<div>';
        if($shortcode->lang == "ID"){
            $html .= '<input type="text" class="form-control adira-input-office" placeholder="Cari berdasarkan Kota" />';
        }else{
            $html .= '<input type="text" class="form-control adira-input-office" placeholder="Find by City" />';
        }
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="col-md-4">';
        $html .= '<div>';
        if($shortcode->lang == "ID"){
            $html .= '<button class="btn btn-secondary btn-office">Cari</button>';
        }else{
            $html .= '<button class="btn btn-secondary btn-office">Find</button>';
        }
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        return $html;
    }

    public function listParent($shortcode, $content, $compiler, $name){
        $html = '<ul class="ul-remove">';
        $html .= $content;
        $html .= '</ul>';
        return $html;
    }

    public function listChild($shortcode, $content, $compiler, $name){
        $html = '<li>';
        $html .= $content;
        $html .= '</li>';
        return $html;
    }
}
