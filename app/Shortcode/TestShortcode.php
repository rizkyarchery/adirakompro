<?php

namespace App\Shortcode;

use Illuminate\Support\ServiceProvider;

class TestShortcode
{
    public function testAja($shortcode, $content, $compiler, $name){
        $html = "<b>";
        $html .= $content;
        $html .= "</b>";
        return $html;
    }
}
