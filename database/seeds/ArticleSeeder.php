<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use App\Modules\Article\Models\Artikel as Article;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 10; $i++) { 
            $article                    = new Article();
            $article->judul             = "Asuransi Jiwa untuk Perlindungan Setiap Tahap Kehidupan Anda";
            $article->slug              = "asuransi-jiwa-untuk-perlindungan-setiap-tahap-kehidupan-anda" . $i ;
            $article->isi_artikel       = '<p>Fase kehidupan yang dinamis tentu membuat prioritas dalam hidup kita menjadi berbeda-beda. Efeknya adalah tujuan memiliki asuransi jiwa juga harus beradaptasi seiring dengan perubahan fase kehidupan kita tersebut.</p><p><span style="font-size: 1rem;">Kepentingan dan tujuan kita memiliki asuransi jiwa saat berada di usia muda pasti berbeda dengan pada saat sudah berkeluarga atau pada saat kita tua nanti. Namun, satu hal yang pasti adalah asuransi jiwa kita harus mampu memberikan manfaat perlindungannya yang optimal untuk kita dan orang-orang tercinta di sekitar kita.</span></p><h4><b>Fase Jiwa Muda&nbsp;</b></h4><p><span style="font-size: 1rem;">Pada fase kehidupan ini, kebanyakan profil kita adalah pekerja muda dan belum menikah dan asuransi jiwa sangat bermanfaat untuk digunakan sebagai berikut.&nbsp;</span></p><ol><li><span style="font-size: 1rem;">Proteksi jiwa.</span></li><li>Tabungan sebagai simpanan untuk menikah dan berkeluarga.</li><li>Investasi sebagai tambahan penghasilan.</li><li>Penunjang gaya hidup.</li></ol><p>Jadi, memiliki asuransi jiwa berjangka waktu 5-10 tahun sudah cukup memadai bagi kita.</p><h4><b>Fase Berkeluarga dan menjadi Orang Tua</b></h4><p>Bagi yang sudah menikah, berkeluarga, dan menjadi orang tua maka manfaat asuransi jiwanya harus dapat memberikan jaminan yang optimal kepada dia dan keluarga tercintanya. Asuransi jiwa sangat bermanfaat untuk digunakan sebagai berikut.</p><ol><li>Proteksi jiwa kita dan keluarga.</li><li>Tabungan untuk biaya pendidikan anak-anak.</li><li>Tambahan penghasilan.</li><li>Membantu membayar hipotek Anda, seperti rumah, mobil, dan sebagainya.</li><li>Perlindungan finansial keluarga.</li></ol><p>Jadi, jenis asuransi jiwa berjangka dengan unsur tabungan atau investasi akan sangat sesuai dengan profil kebutuhan kita karena manfaat nilai tunai dari tabungan atau investasi tersebut dapat digunakan untuk memenuhi kebutuhan kita dan keluarga tercinta.</p><h4><b>Memasuki Masa Pensiun</b></h4><p>Peranan asuransi jiwa sangat bermanfaat untuk digunakan sebagai persiapan kita memasuki masa pensiun.</p><ol><li>Proteksi jiwa kita dan keluarga.</li><li>Jaminan hari tua.</li><li>Pengganti penghasilan reguler.</li><li>Penggantian biaya pemakaman.</li><li>Perlindungan finansial keluarga jika Anda wafat.</li></ol><p>Oleh karena itu, bagi kita yang sudah memasuki usia paruh baya maka lebih tepat untuk memiliki jenis asuransi jiwa seumur hidup dengan unsur tabungan atau investasi.</p><h4><b>Tidak hanya Faktor Usia</b></h4><p>Saat ini, tingkatan usia sudah tidak lagi menjadi indikator utama penentu kebutuhan asuransi jiwa. Fase kehidupan kitalah yang justru menentukan kebutuhan asuransi jiwa yang sesuai.</p><p>Namun, satu hal yang pasti adalah tidak pernah ada kata terlalu dini atau terlambat untuk memiliki asuransi jiwa. Beli sekarang dan dapatkan ketenangan jiwa dan pikiran yang menyertai dalam kehidupan kita.</p>';
            $article->foto              = "CbxkfbTG7V60EKlQMo8GOBhQPvYI8DPHvnZhWadR.png";
            $article->keyword           = "Asuransi Jiwa untuk Perlindungan Setiap Tahap Kehidupan Anda";
            $article->language          = "ID";
            $article->alt_teks          = "Asuransi Jiwa untuk Perlindungan Setiap Tahap Kehidupan Anda";
            $article->status            = 1;
            $article->meta_title        = "Asuransi Jiwa untuk Perlindungan Setiap Tahap Kehidupan Anda";
            $article->meta_description  = "Asuransi Jiwa untuk Perlindungan Setiap Tahap Kehidupan Anda";
            $article->excerpt           = "Fase kehidupan yang dinamis tentu membuat prioritas dalam hidup kita menjadi berbeda-beda. Efeknya adalah tujuan memiliki asuransi jiwa juga harus beradaptasi seiring dengan perubahan fase k";
            $article->publisher         = "System";
            $article->save();
        }
        

        $article                    = new Article();
        $article->judul             = "Life Insurance for the Protection of Every Stage of Your Life";
        $article->slug              = Str::slug("Life Insurance for the Protection of Every Stage of Your Life","-");
        $article->isi_artikel       = '<p>The dynamic life phase naturally makes our priorities in life different. The effect is the goal of having life insurance must also adapt along with the changing phases of our lives.</p><p>Our interests and goals of having life insurance when you are at a young age are definitely different from when you are married or when you are old. However, one thing that is certain is that our life insurance must be able to provide optimal protection benefits for us and loved ones around us.</p><p><b>The Young Soul Phase</b></p><p>In this phase of life, most of our profiles are young and unmarried workers and life insurance is very useful to use as follows.</p><ol><li>Soul protection.</li><li>Savings as savings for marriage and family.</li><li>Investment as additional income.</li><li>Lifestyle support.</li></ol><p>So, having a 5-10 year life insurance is sufficient for us.</p><p><b>The family and parent phases</b></p><p>For those who are married, married, and become parents, the benefits of life insurance must be able to provide optimal guarantees to him and his beloved family. Life insurance is very useful to be used as follows.</p><ol><li>Protection of our souls and family.</li><li>Savings for children"s education expenses.</li><li>Additional income.</li><li>Help pay for your mortgage, such as a house, car, and so on.</li><li>Family financial protection.</li></ol><p>So, the type of term life insurance with an element of savings or investment will be very suitable with our needs profile because the benefits of the cash value of the savings or investment can be used to meet the needs of us and our beloved families.</p><p><b>Entering retirement</b></p><p>The role of life insurance is very useful to be used as our preparation to retire.</p><ol><li>Protection of our souls and family.</li><li>Pension plan.</li><li>Substitute regular income.</li><li>Funeral reimbursement.</li><li>Family financial protection if you die.</li></ol><p>Therefore, for those of us who have entered middle age it is more appropriate to have a type of life insurance for life with savings or investment elements.</p><p><b>Not only the Age Factor</b></p><p>At present, age is no longer the main indicator of life insurance needs. It is our life phase that precisely determines the need for appropriate life insurance.</p><p>However, one thing that is certain is that it is never too early or too late to have life insurance. Buy now and get the peace of mind and mind that accompanies your life.</p>';
        $article->foto              = "dummy.png";
        $article->keyword           = "Life Insurance for the Protection of Every Stage of Your Life";
        $article->language          = "EN";
        $article->alt_teks          = "Life Insurance for the Protection of Every Stage of Your Life";
        $article->status            = 1;
        $article->meta_title        = "Life Insurance for the Protection of Every Stage of Your Life";
        $article->meta_description  = "Life Insurance for the Protection of Every Stage of Your Life";
        $article->excerpt           = "Life Insurance for the Protection of Every Stage of Your Life";
        $article->publisher         = "System";
        $article->save();
    }
}
