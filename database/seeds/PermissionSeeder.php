<?php

use Illuminate\Database\Seeder;
use \Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $permission               = new Permission();
    $permission->name         = "View Role";
    $permission->guard_name   = "web";
    $permission->slug         = "view-role";
    $permission->save();

    $permission               = new Permission();
    $permission->name         = "Create Role";
    $permission->guard_name   = "web";
    $permission->slug         = "Create-role";
    $permission->save();

    $permission               = new Permission();
    $permission->name         = "Edit Role";
    $permission->guard_name   = "web";
    $permission->slug         = "Edit-role";
    $permission->save();

    $permission               = new Permission();
    $permission->name         = "Delete Role";
    $permission->guard_name   = "web";
    $permission->slug         = "Delete-role";
    $permission->save();
    
  }
}
