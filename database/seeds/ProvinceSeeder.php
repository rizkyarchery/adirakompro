<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProvinceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = DB::raw("INSERT INTO province ([province_name]) VALUES
        ('Bali'),
        ('Bangka Belitung'),
        ('Banten'),
        ('Bengkulu'),
        ('DI Yogyakarta'),
        ('DKI Jakarta'),
        ('Gorontalo'),
        ('Jambi'),
        ('Jawa Barat'),
        ('Jawa Tengah'),
        ('Jawa Timur'),
        ('Kalimantan Barat'),
        ('Kalimantan Selatan'),
        ('Kalimantan Tengah'),
        ('Kalimantan Timur'),
        ('Kalimantan Utara'),
        ('Kepulauan Riau'),
        ('Lampung'),
        ('Maluku'),
        ('Maluku Utara'),
        ('Nanggroe Aceh Darussalam (NAD)'),
        ('Nusa Tenggara Barat (NTB)'),
        ('Nusa Tenggara Timur (NTT)'),
        ('Papua'),
        ('Papua Barat'),
        ('Riau'),
        ('Sulawesi Barat'),
        ('Sulawesi Selatan'),
        ('Sulawesi Tengah'),
        ('Sulawesi Tenggara'),
        ('Sulawesi Utara'),
        ('Sumatera Barat'),
        ('Sumatera Selatan'),
        ('Sumatera Utara')");
        
        DB::statement($sql);
    }
}
