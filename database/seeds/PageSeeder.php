<?php

use Illuminate\Database\Seeder;
use App\Modules\Page\Models\Page;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $page                       = new Page();
        $page->title                = 'Homepage';
        $page->lang                 = "ID";
        $page->content              = "";
        $page->slug                 = "home-page";
        $page->meta_title           = "homapage";
        $page->meta_description     = "homapage";
        $page->status               = "1";
        $page->save();

        $page                       = new Page();
        $page->title                = 'Homepage';
        $page->lang                 = "EN";
        $page->content              = "";
        $page->slug                 = "home-page";
        $page->meta_title           = "homapage";
        $page->meta_description     = "homapage";
        $page->status               = "1";
        $page->save();
    }
}
