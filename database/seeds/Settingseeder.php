<?php

use App\Modules\Setting\Models\Setting;
use Illuminate\Database\Seeder;

class Settingseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $setting                      = new Setting();
        $setting->title               = "Adira Admin";
        $setting->nm_web              = "Adira Company";
        $setting->link_web            = "http://adirakompro.local";
        $setting->logo_web            = "assets/logo_web/adira-logo.png";
        $setting->nm_perusahaan       = "Graha Adira";
        $setting->alamat              = "MT. Haryono Kav.42";
        $setting->fax                 = "+62-21 2966 7345";
        $setting->logo_sosmed1        = "data_file/logo_sosmed/facebook.png";
        $setting->link_sosmed1        = "http://www.facebook.com";
        $setting->logo_sosmed2        = "data_file/logo_sosmed/instagram.png";
        $setting->link_sosmed2        = "http://www.instagram.com";
        $setting->logo_sosmed3        = "data_file/logo_sosmed/twitter.png";
        $setting->link_sosmed3        = "http://www.twitter.com";
        $setting->copyright           = "copyright adira";
        $setting->no_telp             = "+62-21 2966 7373";
        $setting->kode                = "001";
        $setting->alt_teks            = "adira";
        $setting->alt_teks_fb         = "adira";
        $setting->alt_teks_ig         = "adira";
        $setting->alt_teks_twit       = "adira";
        $setting->email               = "adira@mail.com";
        $setting->postal_code         = "12780";
        $setting->city                = "Jakarta";
        $setting->disclaimer          = "Disclaimer";
        $setting->sitemap             = "Sitemap";
        $setting->save();

        $setting                      = new Setting();
        $setting->title               = "Adira Admin";
        $setting->nm_web              = "Adira Company";
        $setting->link_web            = "http://adirakompro.local";
        $setting->logo_web            = "assets/logo_web/adira-logo.png";
        $setting->nm_perusahaan       = "Graha Adira";
        $setting->alamat              = "MT. Haryono Kav.42";
        $setting->fax                 = "+62-21 2966 7345";
        $setting->logo_sosmed1        = "data_file/logo_sosmed/facebook.png";
        $setting->link_sosmed1        = "http://www.facebook.com";
        $setting->logo_sosmed2        = "data_file/logo_sosmed/instagram.png";
        $setting->link_sosmed2        = "http://www.instagram.com";
        $setting->logo_sosmed3        = "data_file/logo_sosmed/twitter.png";
        $setting->link_sosmed3        = "http://www.twitter.com";
        $setting->copyright           = "copyright adira";
        $setting->no_telp             = "+62-21 2966 7373";
        $setting->kode                = "002";
        $setting->alt_teks            = "adira";
        $setting->alt_teks_fb         = "adira";
        $setting->alt_teks_ig         = "adira";
        $setting->alt_teks_twit       = "adira";
        $setting->email               = "adira@mail.com";
        $setting->postal_code         = "12780";
        $setting->city                = "Jakarta";
        $setting->disclaimer          = "Disclaimer";
        $setting->sitemap             = "Sitemap";
        $setting->save();
    }
}
